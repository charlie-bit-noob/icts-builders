<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmItemMasterlist
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.cbNonStock = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnChange = New System.Windows.Forms.Button
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Category = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnApply = New System.Windows.Forms.Button
        Me.cmbGroup = New System.Windows.Forms.ComboBox
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.lblCategory = New System.Windows.Forms.Label
        Me.cmbItemType = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtProduct = New System.Windows.Forms.TextBox
        Me.txtBrand = New System.Windows.Forms.TextBox
        Me.lblBrand = New System.Windows.Forms.Label
        Me.txtModel = New System.Windows.Forms.TextBox
        Me.lblModel = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblItemID = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.pnlInputs = New System.Windows.Forms.Panel
        Me.rbNonTile = New System.Windows.Forms.RadioButton
        Me.rbTile = New System.Windows.Forms.RadioButton
        Me.btnVerify = New System.Windows.Forms.Button
        Me.btn_EDMSG = New System.Windows.Forms.Button
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInputs.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbNonStock
        '
        Me.cbNonStock.AutoSize = True
        Me.cbNonStock.Location = New System.Drawing.Point(765, 97)
        Me.cbNonStock.Name = "cbNonStock"
        Me.cbNonStock.Size = New System.Drawing.Size(75, 17)
        Me.cbNonStock.TabIndex = 14
        Me.cbNonStock.Text = "&Non-Stock"
        Me.cbNonStock.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(-2, 335)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(859, 2)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'btnChange
        '
        Me.btnChange.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChange.Location = New System.Drawing.Point(518, 510)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(75, 23)
        Me.btnChange.TabIndex = 7
        Me.btnChange.Text = "C&hange"
        Me.btnChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnChange.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDescription.Location = New System.Drawing.Point(82, 96)
        Me.txtDescription.MaxLength = 50
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(334, 21)
        Me.txtDescription.TabIndex = 7
        '
        'Description
        '
        Me.Description.DataPropertyName = "Description"
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.ReadOnly = True
        Me.Description.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 99)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Description:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-15, 490)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(871, 2)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'Category
        '
        Me.Category.DataPropertyName = "Category"
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Category.DefaultCellStyle = DataGridViewCellStyle1
        Me.Category.HeaderText = "Item type"
        Me.Category.Name = "Category"
        Me.Category.ReadOnly = True
        Me.Category.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(430, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Group:"
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(763, 510)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 10
        Me.btnClose.Text = "&Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Enabled = False
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(680, 510)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnApply
        '
        Me.btnApply.Enabled = False
        Me.btnApply.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.Location = New System.Drawing.Point(599, 510)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 8
        Me.btnApply.Text = "&Apply"
        Me.btnApply.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'cmbGroup
        '
        Me.cmbGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbGroup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbGroup.FormattingEnabled = True
        Me.cmbGroup.ItemHeight = 13
        Me.cmbGroup.Location = New System.Drawing.Point(476, 69)
        Me.cmbGroup.MaxLength = 50
        Me.cmbGroup.Name = "cmbGroup"
        Me.cmbGroup.Size = New System.Drawing.Size(364, 21)
        Me.cmbGroup.TabIndex = 13
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvItems.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvItems.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Location = New System.Drawing.Point(13, 11)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.ReadOnly = True
        Me.dgvItems.RowHeadersVisible = False
        Me.dgvItems.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvItems.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvItems.Size = New System.Drawing.Size(825, 306)
        Me.dgvItems.TabIndex = 0
        Me.dgvItems.TabStop = False
        '
        'lblCategory
        '
        Me.lblCategory.AutoSize = True
        Me.lblCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategory.Location = New System.Drawing.Point(430, 18)
        Me.lblCategory.Name = "lblCategory"
        Me.lblCategory.Size = New System.Drawing.Size(35, 13)
        Me.lblCategory.TabIndex = 8
        Me.lblCategory.Text = "Type:"
        '
        'cmbItemType
        '
        Me.cmbItemType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cmbItemType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbItemType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbItemType.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbItemType.FormattingEnabled = True
        Me.cmbItemType.IntegralHeight = False
        Me.cmbItemType.Location = New System.Drawing.Point(476, 15)
        Me.cmbItemType.MaxLength = 50
        Me.cmbItemType.Name = "cmbItemType"
        Me.cmbItemType.Size = New System.Drawing.Size(364, 21)
        Me.cmbItemType.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Product:"
        '
        'txtProduct
        '
        Me.txtProduct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProduct.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtProduct.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtProduct.Location = New System.Drawing.Point(82, 42)
        Me.txtProduct.MaxLength = 50
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.Size = New System.Drawing.Size(334, 21)
        Me.txtProduct.TabIndex = 3
        '
        'txtBrand
        '
        Me.txtBrand.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBrand.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBrand.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBrand.Location = New System.Drawing.Point(82, 15)
        Me.txtBrand.MaxLength = 50
        Me.txtBrand.Name = "txtBrand"
        Me.txtBrand.Size = New System.Drawing.Size(334, 21)
        Me.txtBrand.TabIndex = 1
        '
        'lblBrand
        '
        Me.lblBrand.AutoSize = True
        Me.lblBrand.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBrand.Location = New System.Drawing.Point(12, 18)
        Me.lblBrand.Name = "lblBrand"
        Me.lblBrand.Size = New System.Drawing.Size(39, 13)
        Me.lblBrand.TabIndex = 0
        Me.lblBrand.Text = "Brand:"
        '
        'txtModel
        '
        Me.txtModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtModel.Location = New System.Drawing.Point(82, 69)
        Me.txtModel.MaxLength = 50
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(334, 21)
        Me.txtModel.TabIndex = 5
        '
        'lblModel
        '
        Me.lblModel.AutoSize = True
        Me.lblModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModel.Location = New System.Drawing.Point(12, 72)
        Me.lblModel.Name = "lblModel"
        Me.lblModel.Size = New System.Drawing.Size(39, 13)
        Me.lblModel.TabIndex = 4
        Me.lblModel.Text = "Model:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Enabled = False
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label5.Location = New System.Drawing.Point(254, 515)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "New Item ID:"
        Me.Label5.UseCompatibleTextRendering = True
        Me.Label5.Visible = False
        '
        'lblItemID
        '
        Me.lblItemID.AutoSize = True
        Me.lblItemID.Enabled = False
        Me.lblItemID.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblItemID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblItemID.Location = New System.Drawing.Point(331, 515)
        Me.lblItemID.Name = "lblItemID"
        Me.lblItemID.Size = New System.Drawing.Size(77, 16)
        Me.lblItemID.TabIndex = 2
        Me.lblItemID.Text = "CNM00000000"
        Me.lblItemID.UseCompatibleTextRendering = True
        Me.lblItemID.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(430, 45)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Unit:"
        '
        'cmbUnit
        '
        Me.cmbUnit.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.ItemHeight = 13
        Me.cmbUnit.Location = New System.Drawing.Point(476, 42)
        Me.cmbUnit.MaxLength = 50
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(364, 21)
        Me.cmbUnit.TabIndex = 11
        '
        'pnlInputs
        '
        Me.pnlInputs.Controls.Add(Me.rbNonTile)
        Me.pnlInputs.Controls.Add(Me.rbTile)
        Me.pnlInputs.Controls.Add(Me.cmbItemType)
        Me.pnlInputs.Controls.Add(Me.Label4)
        Me.pnlInputs.Controls.Add(Me.lblModel)
        Me.pnlInputs.Controls.Add(Me.cmbUnit)
        Me.pnlInputs.Controls.Add(Me.txtModel)
        Me.pnlInputs.Controls.Add(Me.lblBrand)
        Me.pnlInputs.Controls.Add(Me.txtBrand)
        Me.pnlInputs.Controls.Add(Me.cbNonStock)
        Me.pnlInputs.Controls.Add(Me.txtProduct)
        Me.pnlInputs.Controls.Add(Me.Label1)
        Me.pnlInputs.Controls.Add(Me.lblCategory)
        Me.pnlInputs.Controls.Add(Me.txtDescription)
        Me.pnlInputs.Controls.Add(Me.cmbGroup)
        Me.pnlInputs.Controls.Add(Me.Label3)
        Me.pnlInputs.Controls.Add(Me.Label2)
        Me.pnlInputs.Location = New System.Drawing.Point(-2, 353)
        Me.pnlInputs.Name = "pnlInputs"
        Me.pnlInputs.Size = New System.Drawing.Size(855, 131)
        Me.pnlInputs.TabIndex = 4
        '
        'rbNonTile
        '
        Me.rbNonTile.AutoSize = True
        Me.rbNonTile.Location = New System.Drawing.Point(540, 96)
        Me.rbNonTile.Name = "rbNonTile"
        Me.rbNonTile.Size = New System.Drawing.Size(64, 17)
        Me.rbNonTile.TabIndex = 16
        Me.rbNonTile.TabStop = True
        Me.rbNonTile.Text = "Non-Tile"
        Me.rbNonTile.UseVisualStyleBackColor = True
        '
        'rbTile
        '
        Me.rbTile.AutoSize = True
        Me.rbTile.Location = New System.Drawing.Point(476, 96)
        Me.rbTile.Name = "rbTile"
        Me.rbTile.Size = New System.Drawing.Size(41, 17)
        Me.rbTile.TabIndex = 15
        Me.rbTile.TabStop = True
        Me.rbTile.Text = "Tile"
        Me.rbTile.UseVisualStyleBackColor = True
        '
        'btnVerify
        '
        Me.btnVerify.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVerify.Location = New System.Drawing.Point(414, 510)
        Me.btnVerify.Name = "btnVerify"
        Me.btnVerify.Size = New System.Drawing.Size(98, 23)
        Me.btnVerify.TabIndex = 6
        Me.btnVerify.Text = "&Verify/Confirm"
        Me.btnVerify.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVerify.UseVisualStyleBackColor = True
        '
        'btn_EDMSG
        '
        Me.btn_EDMSG.Location = New System.Drawing.Point(12, 510)
        Me.btn_EDMSG.Name = "btn_EDMSG"
        Me.btn_EDMSG.Size = New System.Drawing.Size(119, 23)
        Me.btn_EDMSG.TabIndex = 110
        Me.btn_EDMSG.Text = "Enable/Disable DMS"
        Me.btn_EDMSG.UseVisualStyleBackColor = True
        '
        'frmItemMasterlist
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(850, 545)
        Me.Controls.Add(Me.btn_EDMSG)
        Me.Controls.Add(Me.btnVerify)
        Me.Controls.Add(Me.pnlInputs)
        Me.Controls.Add(Me.lblItemID)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnChange)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.dgvItems)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmItemMasterlist"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Item Masterlist"
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInputs.ResumeLayout(False)
        Me.pnlInputs.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbNonStock As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnChange As System.Windows.Forms.Button
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents cmbGroup As System.Windows.Forms.ComboBox
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents lblCategory As System.Windows.Forms.Label
    Friend WithEvents cmbItemType As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtProduct As System.Windows.Forms.TextBox
    Friend WithEvents txtBrand As System.Windows.Forms.TextBox
    Friend WithEvents lblBrand As System.Windows.Forms.Label
    Friend WithEvents txtModel As System.Windows.Forms.TextBox
    Friend WithEvents lblModel As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblItemID As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents pnlInputs As System.Windows.Forms.Panel
    Friend WithEvents btnVerify As System.Windows.Forms.Button
    Friend WithEvents rbTile As System.Windows.Forms.RadioButton
    Friend WithEvents rbNonTile As System.Windows.Forms.RadioButton
    Friend WithEvents btn_EDMSG As System.Windows.Forms.Button
End Class

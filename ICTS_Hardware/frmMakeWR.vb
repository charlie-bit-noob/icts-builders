Imports System.Data
Imports System.Data.SqlClient

Public Class frmMakeWR


    Private AM As Decimal
    Private FCM As Decimal
    Private IALD As Decimal

    Private intItemType As Integer
    Private Pct As Decimal
    Private PM As Decimal
    Private Row As DataRow
    Private SerialItemCount As Integer

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String
    Private TAWF As Decimal
    Private TAWFA As Decimal
    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function HasSameItem() As Boolean
        Try
            sqlStr = "SELECT item_id FROM Temp_AFsIn WHERE item_id = '" & Me.txtItemID.Text & "' " & _
            "AND ua_id = " & My.Forms.MDI.stbUserID.Text & ""
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read Then
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Return True
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("desc")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_unit)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        Dim col_discount As DataColumn = New DataColumn("discount")
        col_discount.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_discount)

        Dim col_amtpermodel As DataColumn = New DataColumn("amtpermodel")
        col_amtpermodel.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_amtpermodel)

        TableTemp.DefaultView.Sort = "pop_id ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnCaptureItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCaptureItem.Click
        Try
            Me.txtItemID.Text = SelectedItemID

            Me.sqlStr = "SELECT unit_lib_id, type_lib_id, brand + ' ' + product + ' ' + model + ' ' + description AS concat, " & _
            "(SELECT units FROM Library WHERE lib_id = im.unit_lib_id) AS imunit " & _
            "FROM ItemMasterlist AS im WHERE item_id = '" & Me.txtItemID.Text & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()

            Me.sqlReader.Read()
            Me.txtItemName.Text = sqlReader("concat")
            Me.txtUnit.Text = Me.sqlReader("imunit")
            Me.intItemType = Me.sqlReader("type_lib_id")
            Me.sqlReader.Close()
            sqlDBconn.Close()

            If IsTile(Me.txtItemID.Text) = True Then
                Me.cmbClass.SelectedIndex = 0
                Me.cmbClass.Enabled = True
                Me.cmbClass.Focus()
            Else
                Me.cmbClass.SelectedIndex = 0
                Me.cmbClass.Enabled = False
                Me.txtQuantity.Focus()
            End If

            If Me.intItemType < 3 Then
                Me.txtSerialNumber.Enabled = True
            Else
                Me.txtSerialNumber.Enabled = False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnCompute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompute.Click
        Try
            If Me.intItemType > 2 Then
                If HasSameItem() = True Then
                    If MessageBox.Show("The items must be group according to their models. " & _
                    "Please confirm if it is an exception by clicking Yes in order to continue " & _
                    "and No to cancel.", "Same item Detected!", _
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                        Exit Sub
                    End If
                End If
            End If
            If Me.txtQuantity.Text = "" Then
                MessageBox.Show("Please provide a quantity.", "Tsk! Tsk! Tsk!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If
            ComputeUnitCost()
            If GenPopID = 1 Then Me.pnlNonItem.Enabled = True

            Me.btnItemFinder.Enabled = False
            Me.btnCaptureItem.Enabled = False

            btnPop.Enabled = True
            btnPop.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
        Me.btnPop.Enabled = True
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Try
            If IsEmptyMakeGrid(Me.dgvMake) = True Then Me.btnClose.PerformClick()

            If Val(Me.txtTotalAmount.Text) = 0.0 Then
                If NumOfItems > Me.dgvMake.RowCount Then
                    MessageBox.Show("Incomplete Entry!", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Exit Sub
                End If
            End If

            If Val(Me.txtBalance.Text.Replace(",", "")) < 0.5 Then
                Me.pnlNonItem.Enabled = False

                Me.btnItemFinder.Enabled = False
                Me.btnCaptureItem.Enabled = False
                Me.btnSave.Enabled = True
                Me.btnSave.Focus()
                Me.btnDone.Enabled = False
            End If

            If Val(Me.txtBalance.Text.Replace(",", "")) > 0.5 Then
                If MessageBox.Show("" & Me.Text & " is not yet complete. " & _
                    "To avoid erroneous data, encoded entries will be discarded once you continue. " & _
                    "Click Yes to discard entries and create new, No to cancel and complete the " & Me.Text & "", _
                    Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                    Me.pnlNonItem.Enabled = False
                    Me.btnClose.Enabled = True
                    Me.btnItemFinder.Enabled = False
                    Me.btnCaptureItem.Enabled = False
                    Me.btnDone.Enabled = False
                Else
                    Me.btnClose.Enabled = True
                    Exit Sub
                End If
            End If

            Me.btnClose.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnItemFinder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItemFinder.Click
        If Val(Me.txtTotalAmount.Text) = 0.0 And Me.dgvMake.RowCount > 0 Then
            If NumOfItems < 1 Then
                MessageBox.Show("Invalid number of items! Click Close.", "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            Else
                If NumOfItems = Me.dgvMake.RowCount Then
                    MessageBox.Show("Number of items completed. Click Done!.", "oOops!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
            End If
        End If

        Me.btnCaptureItem.Focus()
        My.Forms.frmFinderItem.ShowDialog()
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Me.pnlAFDetails.Enabled = True
            Me.pnlItemDetails.Enabled = True
            Me.gbGiven.Enabled = True
            Me.pnlNonItem.Enabled = True

            DelPrevTempIn()
            Me.FilldgvMake()
            AutoGenRunID()

            Dim ctrl As Control

            For Each ctrl In Me.gbGiven.Controls
                If TypeOf ctrl Is TextBox Then ctrl.Text = "0.000"
            Next
            GenPopID = 1
            Me.txtCost.Text = "0.000"
            Me.txtBalance.Text = "0.000"

            Me.btnDone.Enabled = True
            Me.btnNew.Enabled = False

            Me.cmbSupplier.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnNewModel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewModel.Click
        Try
            Dim ctrl As Control
            AutoGenRunID()
            For Each ctrl In Me.pnlItemDetails.Controls
                If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
                If TypeOf ctrl Is ComboBox Then ReIndexComboBox(ctrl)
                If TypeOf ctrl Is CheckBox Then UncheckCB(ctrl)
            Next
            Me.btnItemFinder.Enabled = True
            Me.btnCaptureItem.Enabled = True
            Me.btnItemFinder.Focus()
            Me.btnDone.Enabled = True
            Me.btnPop.Enabled = False

            Me.txtAmountPerModel.Text = "0.000"
            IALD = 0.0
            PM = 0.0
            FCM = 0.0
            AM = 0.0
            Me.txtCost.Text = "0.000"
            TAwF = 0.0
            TAwFA = 0.0

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnPop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPop.Click
        Try
            If IsAFNumberAlreadyExists("AFsIn", Me.cmbBranch.SelectedIndex, Me.txtNumber.Text, Me.txtNumber) = True Then Exit Sub
            If InCompleteInAF(Me.pnlAFDetails) = True Or InCompleteInItemDetails(Me.txtItemID, Me.txtQuantity, Me.txtSerialNumber) = True Then Exit Sub
            If TileNoClassDetected(Me.txtItemName.Text, Me.cmbClass) = True Then Exit Sub

            CallTrimmer()
            Me.btnItemFinder.Enabled = False
            Me.btnCaptureItem.Enabled = False

            Me.pnlNonItem.Enabled = False

            If Me.intItemType < 3 Then
                If IsSerialExistsOnTempIn(Me.txtSerialNumber, Val(MDI.stbUserID.Text)) = True Then Exit Sub
                If IsSerialExists(Me.txtSerialNumber, Me.cmbBranch.SelectedIndex) = True Then Exit Sub

                ComputeRemainBal(Me.txtItemID.Text, Val(Me.txtCost.Text.Replace(",", "")), Val(Me.txtArrastre.Text.Replace(",", "")), Val(Me.txtAmountPerModel.Text.Replace(",", "")), Val(Me.txtDiscount.Text.Replace(",", "")), Me.txtBalance, Me.TAWF, Me.TAWFA)
                ExecTempInserter()

                GenPopID += 1
                Me.txtSerialNumber.Clear()
                Me.txtSerialNumber.Focus()

                Me.SerialItemCount -= 1
                If Me.SerialItemCount = 0 Then Me.btnNewModel.PerformClick()
            Else
                ComputeRemainBal(Me.txtItemID.Text, Val(Me.txtCost.Text.Replace(",", "")), Val(Me.txtArrastre.Text.Replace(",", "")), Val(Me.txtAmountPerModel.Text.Replace(",", "")), Val(Me.txtDiscount.Text.Replace(",", "")), Me.txtBalance, Me.TAWF, Me.TAWFA)
                ExecTempInserter()
                GenPopID += 1
                Me.btnNewModel.PerformClick()
            End If

            FilldgvMake()

            FocusThisEntry(Me.dgvMake, GenPopID)

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.intItemType > 3 Then
                If Not Me.SerialItemCount = 0 Then
                    MessageBox.Show("Please provide a serial number to all serial items before saving. " & _
                    "" & Me.SerialItemCount & " item(s) remaining.", "Tsk Tsk Tsk!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.txtSerialNumber.Focus()
                    Exit Sub
                End If
            End If

            InserterIn(Me.cmbBranch.SelectedIndex, Me.dgvMake.RowCount, Me.mtxtDate.Text, Me.txtNumber.Text, _
            Me.txtTotalAmount.Text.Replace(",", ""), Me.txtFreight.Text.Replace(",", ""), Me.txtDiscount.Text, Me.txtArrastre.Text.Replace(",", ""), _
            Me.txtRemarks.Text)

            MessageBox.Show("" & GenShortAF & " " & Me.txtNumber.Text & " saved succesfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Me.RefreshCtrlVars()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.pnlAFDetails.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
        For Each ctrlToTrim In Me.pnlItemDetails.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Sub CleaningUpObjects()
        'Local
        Me.ResetVariables()

        'Global
        GenAFID = Nothing
        GenShortAF = Nothing
        GenTransIDPurchasesOnly = Nothing

        intItemType = Nothing
        SerialItemCount = Nothing

        Row = Nothing
        TempDataView = Nothing

        sqlCmd = Nothing
        sqlReader = Nothing
        sqlStr = Nothing
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtDate.Focus()
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        IsInvalidBranch(Me.cmbBranch.SelectedIndex, Me.cmbBranch)
    End Sub

    Private Sub cmbClass_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbClass.EnabledChanged
        Me.cmbClass.SelectedIndex = 0
    End Sub

    Private Sub cmbClass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbClass.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtQuantity.Focus()
    End Sub

    Private Sub cmbSupplier_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cmbSupplier.KeyDown
        If e.KeyCode = 13 Then Me.cmbBranch.Focus()
    End Sub

    Private Sub ComputeUnitCost()
        Try
            If Val(Me.txtTotalAmount.Text) = 0.0 And Me.dgvMake.RowCount < 1 Then
                NumberOfRowsPrompt()
                Exit Sub
            End If

            If Me.intItemType < 3 Then
                If Me.HasSameItem = False Then
                    Me.SerialItemCount = Val(Me.txtQuantity.Text)
                End If
            End If

            If Val(Me.txtDiscount.Text) >= 1 Then Me.txtDiscount.Text = Val(Me.txtDiscount.Text) / 100
            If Val(Me.txtDiscount.Text.Replace(",", "")) > 0 Then
                Me.Pct = Val(Me.txtAmountPerModel.Text.Replace(",", "")) * Val(Me.txtDiscount.Text) ' Get the percentage
                Me.IALD = Val(Me.txtAmountPerModel.Text.Replace(",", "")) - Me.Pct
            Else
                Me.IALD = Me.txtAmountPerModel.Text.Replace(",", "")
            End If

            Me.PM = Val(Me.txtAmountPerModel.Text.Replace(",", "")) / (Val(Me.txtTotalAmount.Text.Replace(",", "")) - (Val(Me.txtFreight.Text.Replace(",", "")) + Val(Me.txtArrastre.Text.Replace(",", ""))))

            If Convert.ToString(PM) = "NAN" Then Me.PM = 0

            If Val(Me.txtDiscount.Text) > 0 Then
                Me.PM = Me.IALD / (Val(Me.txtTotalAmount.Text.Replace(",", "")) - (Val(Me.txtFreight.Text.Replace(",", "")) + Val(Me.txtArrastre.Text.Replace(",", ""))))
            End If

            Me.FCM = Me.PM * Val(Me.txtFreight.Text.Replace(",", "")) 'Total Freight Charge per Model
            Me.AM = Me.PM * Val(Me.txtArrastre.Text.Replace(",", "")) 'Arrastre per Model

            If Val(Me.txtDiscount.Text) > 0 Then 'Has percent discount
                Me.TAWF = Me.IALD + Me.FCM
            Else
                Me.TAWF = Val(Me.txtAmountPerModel.Text.Replace(",", "")) + Me.FCM
            End If

            Me.TAWFA = Me.TAWF + Me.AM 'Total Amount with Freight and Arrastre

            Me.txtCost.Text = Me.TAWFA / Val(Me.txtQuantity.Text) 'Unit Cost

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvMake_PreviewKeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles dgvMake.PreviewKeyDown
        Try
            If Me.dgvMake.Rows.Count < 1 Then Exit Sub

            If e.KeyCode = Keys.F5 Then
                Me.FilldgvMake()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub ExecTempInserter()
        Try
            Dim classALT As String = Me.cmbClass.Text
            'Dim freeALT As Integer = ConvertState(Me.cbFreeItem.CheckState)

            If cmbClass.SelectedIndex < 1 Then classALT = ""

            TempInserterIn(GenRunID, GenPopID, Me.txtItemID.Text, ConvertState(Me.cbFreeItem.CheckState), classALT, _
            Me.txtCost.Text.Replace(",", ""), Me.txtSerialNumber.Text, Me.txtQuantity.Text, Me.txtUnit.Text, Me.txtAmountPerModel.Text.Replace(",", ""), _
            Me.txtDiscount.Text, Me.mtxtDate.Text, Me.cmbBranch.SelectedIndex, GenAFID, Me.txtNumber.Text, _
            GenTransIDPurchasesOnly, "", Me.cmbSupplier.SelectedIndex, MDI.stbUserID.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillBranch()
        Try
            sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillClass()
        Try
            sqlStr = "SELECT classes FROM Library WHERE classes IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbClass.Items.Clear()
            Me.cmbClass.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbClass.Items.Add(sqlReader("classes"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbClass.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvMake()
        Try
            TempTable = MakeDataTableTemp()

            sqlStr = "SELECT order_id, item_id, (SELECT brand FROM ItemMasterlist AS im " & _
            "WHERE im.item_id = tmp.item_id) AS tmpbrand, (SELECT product FROM ItemMasterlist AS im1 " & _
            "WHERE im1.item_id = tmp.item_id) AS tmpproduct, (SELECT model FROM ItemMasterlist AS im2 " & _
            "WHERE im2.item_id = tmp.item_id) AS tmpmodel, (SELECT description FROM ItemMasterlist AS im3 " & _
            "WHERE im3.item_id = tmp.item_id) AS tmpdesc, class, is_free, serial_num, qty, unit, cost, discount, " & _
            "amtpermodel FROM Temp_AFsIn AS tmp WHERE ua_id = " & MDI.stbUserID.Text & ""
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()
                Row("pop_id") = sqlReader("order_id")
                Row("brand") = sqlReader("tmpbrand")
                Row("product") = sqlReader("tmpproduct")
                Row("model") = sqlReader("tmpmodel")
                Row("desc") = sqlReader("tmpdesc")
                Row("class") = sqlReader("class")
                Row("free") = sqlReader("is_free").ToString.Remove(1)
                Row("serial") = sqlReader("serial_num")
                Row("qty") = sqlReader("qty")
                Row("unit") = sqlReader("unit")
                Row("cost") = sqlReader("cost")
                Row("discount") = sqlReader("discount")
                Row("amtpermodel") = sqlReader("amtpermodel")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            dgvMake.DataSource = TempDataView

            If Me.dgvMake.RowCount < 1 Then dgvMakeConfig(Me.dgvMake)
        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillSupplier()
        Try
            sqlStr = "SELECT name FROM Suppliers ORDER BY supplier_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbSupplier.Items.Clear()
            Me.cmbSupplier.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbSupplier.Items.Add(sqlReader("name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbSupplier.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
    
    Private Sub frmMakeWR_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        GetAFID(Me.Text.ToUpper)
        GetShortAF(Me.Text.ToUpper)
        Me.btnNew.Focus()
    End Sub

    Private Sub frmMakeWR_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        DelPrevTempIn()
        Me.CleaningUpObjects()
        ResetAFsMenusMDI()
    End Sub

    Private Sub frmMakeWR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        GetAFID(Me.Text.ToUpper)
        GetShortAF(Me.Text.ToUpper)
        GetTransIDWarehouseReceiptOnly()
        Me.LoadDataAndRefresh()
        Me.RefreshCtrlVars()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FillSupplier()
        Me.FillBranch()
        Me.FillClass()
    End Sub

    Private Sub mtxtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtNumber.Focus()
    End Sub

    Private Sub rbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCancel.Click
        If InvalidCancelledAF(Me.txtNumber.Text, Me.cmbBranch.SelectedIndex, Me.mtxtDate.Text) = True Then
            MessageBox.Show("Minimum required data not sufficient or invalid.", " Be Cautious!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Me.rbCancel.Checked = False
            Exit Sub
        End If

        If MessageBox.Show("This is only intended for cancelled documents. Are you sure you want to proceed?", "Please confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            CancelledAF(Me.txtNumber.Text, Me.cmbBranch.SelectedIndex, Me.mtxtDate.Text)

            MessageBox.Show("" & GenShortAF & " " & Me.txtNumber.Text & " saved succesfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.RefreshCtrlVars()
        Else
            Me.rbCancel.Checked = False
            Exit Sub
        End If
    End Sub

    Private Sub RefreshCtrlVars()
        Me.ResetVariables()
        ResetAFcontrols(Me)
    End Sub

    Private Sub ResetVariables()
        'Local
        AM = Nothing
        FCM = Nothing
        IALD = Nothing
        TAwF = Nothing
        TAwFA = Nothing
        Pct = Nothing
        PM = Nothing

        'Global
        SelectedItemID = Nothing
        GenRootID = Nothing
        GenRunID = Nothing
        GenPopID = Nothing
    End Sub

    Private Sub txtAmountPerModel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmountPerModel.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.btnCompute.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtAmountPerModel_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmountPerModel.Leave
        OnLeaveFormat(Me.txtAmountPerModel)
    End Sub

    Private Sub txtAmountPerModel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAmountPerModel.TextChanged
        InputFormat(Me.txtAmountPerModel)
    End Sub

    Private Sub txtArrastre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtArrastre.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtAmountPerModel.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtArrastre_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArrastre.Leave
        OnLeaveFormat(Me.txtArrastre)
    End Sub

    Private Sub txtArrastre_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArrastre.TextChanged
        InputFormat(Me.txtArrastre)
    End Sub

    Private Sub txtBalance_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBalance.TextChanged
        InputFormat(Me.txtBalance)
    End Sub

    Private Sub txtCost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCost.TextChanged
        InputFormat(Me.txtCost)
    End Sub

    Private Sub txtDiscount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiscount.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtArrastre.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtDiscount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.Leave
        OnLeaveFormat(Me.txtDiscount)
    End Sub

    Private Sub txtFreight_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFreight.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtDiscount.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtFreight_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFreight.Leave
        OnLeaveFormat(Me.txtFreight)
    End Sub

    Private Sub txtFreight_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFreight.TextChanged
        InputFormat(Me.txtFreight)
    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtRemarks.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.Leave
        If Me.txtNumber.Text.ToString.Trim <> "" Then IsNumberExists("AFsIn", Me.cmbBranch.SelectedIndex, Me.txtNumber.Text, Me.txtNumber)
    End Sub

    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then
            If Me.txtSerialNumber.Enabled = True Then
                Me.txtSerialNumber.Focus()
            Else
                Me.txtTotalAmount.Focus()
            End If
        End If
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnItemFinder.Focus()
    End Sub

    Private Sub txtSerialNumber_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerialNumber.EnabledChanged
        Me.txtSerialNumber.Clear()
    End Sub

    Private Sub txtSerialNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerialNumber.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtTotalAmount.Focus()
    End Sub

    Private Sub txtTotalAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTotalAmount.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtFreight.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtTotalAmount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalAmount.Leave
        OnLeaveFormat(Me.txtTotalAmount)
    End Sub

    Private Sub txtTotalAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalAmount.TextChanged
        Me.txtBalance.Text = Me.txtTotalAmount.Text
        InputFormat(Me.txtTotalAmount)
    End Sub
End Class
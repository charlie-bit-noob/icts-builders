Imports System.Data
Imports System.Data.SqlClient

Public Class frmMakeDR

    Private intItemType As Integer
    Private Row As DataRow
    Private SerialItemCount As Integer = 0

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As New DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_ref As DataColumn = New DataColumn("ref")
        col_ref.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_ref)

        Dim col_ref_num As DataColumn = New DataColumn("number")
        col_ref_num.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_ref_num)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("desc")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_unit)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        Dim col_ofisUse As DataColumn = New DataColumn("OfisUse")
        col_ofisUse.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_ofisUse)

        TableTemp.DefaultView.Sort = "pop_id ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnCaptureItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCaptureItem.Click
        Try
            Me.txtItemID.Text = SelectedItemID

            Me.sqlStr = "SELECT unit_lib_id, type_lib_id, brand + ' ' + product + ' ' + model + ' ' + description AS concat, " & _
            "(SELECT units FROM Library WHERE lib_id = im.unit_lib_id) AS imunit " & _
            "FROM ItemMasterlist AS im WHERE item_id = '" & Me.txtItemID.Text & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()

            Me.sqlReader.Read()
            Me.txtItemName.Text = sqlReader("concat")
            Me.txtUnit.Text = Me.sqlReader("imunit")
            Me.intItemType = Me.sqlReader("type_lib_id")
            Me.sqlReader.Close()
            sqlDBconn.Close()

            If IsTile(Me.txtItemID.Text) = True Then
                Me.cmbClass.SelectedIndex = 0
                Me.cmbClass.Enabled = True
                Me.cmbClass.Focus()
            Else
                Me.cmbClass.SelectedIndex = 0
                Me.cmbClass.Enabled = False
                Me.txtQuantity.Focus()
            End If

            If Me.intItemType < 3 Then
                Me.txtSerialNumber.Enabled = True
            Else
                Me.txtSerialNumber.Enabled = False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCustomer.Click
        Me.txtRemarks.Focus()
        My.Forms.frmFinderCustomer.ShowDialog()
    End Sub

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Try
            If NumOfItems > CountModelsInTempTable("Temp_AFsOut") Then
                MessageBox.Show("Incomplete Entry!", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            Me.pnlNonItem.Enabled = False

            Me.btnItemFinder.Enabled = False
            Me.btnCaptureItem.Enabled = False
            Me.btnPop.Enabled = False
            Me.btnSave.Enabled = True
            Me.btnSave.Focus()
            Me.btnDone.Enabled = False
            Me.btnClose.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnItemFinder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnItemFinder.Click
        If NumOfItems < 1 Then
            MessageBox.Show("Invalid number of items! Click Close.", "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        Else
            If NumOfItems = CountModelsInTempTable("Temp_AFsOut") Then
                MessageBox.Show("Number of items completed. Click Done!.", "oOops!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        End If

        Me.btnCaptureItem.Focus()
        My.Forms.frmFinderItem.ShowDialog()
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            NumberOfRowsPrompt()
            Me.pnlAFDetails.Enabled = True
            Me.pnlItemDetails.Enabled = True
            Me.pnlNonItem.Enabled = True
            Me.pnlAmt.Enabled = True

            DelPrevTempOut()
            FilldgvMake()

            GenPopID = 1
            Me.txtTotal.Text = "0.000"

            Me.btnPop.Enabled = True
            Me.btnDone.Enabled = True
            Me.btnNew.Enabled = False

            Me.cmbBranch.Focus()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnNewModel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewModel.Click
        Try
            Dim ctrl As Control

            For Each ctrl In Me.pnlItemDetails.Controls
                If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
                If TypeOf ctrl Is ComboBox Then ReIndexComboBox(ctrl)
                If TypeOf ctrl Is CheckBox Then UncheckCB(ctrl)
            Next

            Me.btnItemFinder.Enabled = True
            Me.btnCaptureItem.Enabled = True
            Me.btnItemFinder.Focus()
            Me.btnDone.Enabled = True

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnPop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPop.Click
        Try
            If IsAFNumberAlreadyExists("AFsOut", Me.cmbBranch.SelectedIndex, Me.txtNumber.Text, Me.txtNumber) = True Then Exit Sub
            If InCompleteInAF(Me.pnlAFDetails) = True Or InCompleteInItemDetails(Me.txtItemID, Me.txtQuantity, Me.txtSerialNumber) = True Then Exit Sub
            If TileNoClassDetected(Me.txtItemName.Text, Me.cmbClass) = True Then Exit Sub

            CallTrimmer()
            Me.btnItemFinder.Enabled = False
            Me.btnCaptureItem.Enabled = False

            Me.pnlNonItem.Enabled = False

            If Me.intItemType < 3 Then
                If IsSerialExistsOnTempOut(Me.txtSerialNumber, Val(MDI.stbUserID.Text)) = True Then Exit Sub
                If IsSerialExists(Me.txtSerialNumber, Me.cmbBranch.SelectedIndex) = True Then Exit Sub

                ExecTempInserterSI()

                Me.txtSerialNumber.Clear()
                Me.txtSerialNumber.Focus()
            Else
                If HasOpenItem("Temp_AFsOut", Me.txtItemID.Text, Me.btnItemFinder, Me.btnCaptureItem) Then Exit Sub

                ExecTempInserterNSI()
                Me.btnNewModel.PerformClick()
            End If

            FilldgvMake()

            FocusThisEntry(Me.dgvMake, GenPopID)
            ComputeCost(Me.dgvMake, Me.txtTotal)

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.intItemType > 3 Then
                If Not Me.SerialItemCount = 0 Then
                    MessageBox.Show("Please provide a serial number to all serial items before saving. " & _
                    "" & Me.SerialItemCount & " item(s) remaining.", "Tsk Tsk Tsk!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.txtSerialNumber.Focus()
                    Exit Sub
                End If
            End If

            InserterOut(Me.cmbBranch.SelectedIndex, Me.dgvMake.RowCount, Me.txtNumber.Text, Me.txtRemarks.Text)

            MessageBox.Show("" & GenShortAF & " " & Me.txtNumber.Text & " saved succesfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Me.RefreshCtrlVars()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnSourceFinder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSourceFinder.Click
        Try
            If Convert.ToInt32(Val(Me.txtQuantity.Text)) < 1 Then Exit Sub

            If Me.intItemType > 2 Then
                If Not Me.cmbClass.Text.Contains("-") Then
                    My.Forms.frmFinderNonSerial.lblClass.Text = Me.cmbClass.Text
                Else
                    My.Forms.frmFinderNonSerial.lblClass.Text = "N/A"
                End If
                My.Forms.frmFinderNonSerial.ShowDialog()
            Else
                My.Forms.frmFinderSerialNumber.ShowDialog()
                If Me.SerialItemCount = 0 Then Me.SerialItemCount = Val(Me.txtQuantity.Text)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.pnlAFDetails.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
        For Each ctrlToTrim In Me.pnlItemDetails.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Sub cbNonCust_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbNonCust.CheckStateChanged
        If Me.cbNonCust.CheckState = CheckState.Checked Then
            Me.txtCustID.Clear()
            Me.txtCustomer.ReadOnly = False
            Me.txtCustomer.Clear()
            Me.btnCustomer.Enabled = False
            Me.cmbDepartment.Enabled = True
            Me.cmbDepartment.SelectedIndex = 0
            Me.txtCustomer.Focus()
        Else
            Me.txtCustID.Clear()
            Me.txtCustomer.Clear()
            Me.txtCustomer.ReadOnly = True
            Me.btnCustomer.Enabled = True
            Me.cmbDepartment.SelectedIndex = 0
            Me.cmbDepartment.Enabled = False
            Me.btnCustomer.Focus()
        End If
    End Sub

    Private Sub CleaningUpObjects()
        'Local
        Me.ResetVariables()

        'Global
        GenAFID = Nothing
        GenShortAF = Nothing

        intItemType = Nothing
        SerialItemCount = Nothing

        Row = Nothing
        TempDataView = Nothing

        sqlCmd = Nothing
        sqlReader = Nothing
        sqlStr = Nothing
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbTransaction.Focus()
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        SelectedSrcBranchID = Me.cmbBranch.SelectedIndex
    End Sub

    Private Sub cmbClass_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbClass.EnabledChanged
        Me.cmbClass.SelectedIndex = 0
    End Sub

    Private Sub cmbClass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbClass.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtQuantity.Focus()
    End Sub

    Private Sub cmbDepartment_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbDepartment.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnSourceFinder.Focus()
    End Sub

    Private Sub cmbTransaction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTransaction.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtDate.Focus()
    End Sub

    Private Sub ExecTempInserterNSI()
        Try
            Dim i As Integer
            Dim cur_qtybal, tmp_qtybal, cache_qty, miss_qty As Decimal
            Dim classALT As String = Me.cmbClass.Text
            Dim ClassPresume As String = Nothing

            tmp_qtybal = 0

            If Me.cmbClass.SelectedIndex < 1 Then classALT = ""

            If selectedCount > 0 Then
                For i = 0 To selectedCount - 1
                    cur_qtybal = qtyList.Item(i)

                    tmp_qtybal = cur_qtybal + tmp_qtybal
                    cache_qty = cur_qtybal

                    If tmp_qtybal > Val(Me.txtQuantity.Text) Then
                        miss_qty = cur_qtybal - (tmp_qtybal - Val(Me.txtQuantity.Text))
                        cache_qty = miss_qty
                    End If

                    TempInserterOut(rootList.Item(i), runList.Item(i), Me.txtItemID.Text, IsFreeList.Item(i), _
                    classList.Item(i), classALT, Me.txtSerialNumber.Text, cache_qty, Me.txtUnit.Text, costList.Item(i), 0, _
                    Me.mtxtDate.Text, Me.cmbBranch.SelectedIndex, afList.Item(i), afnumList.Item(i), GenAFID, Me.txtNumber.Text, _
                    Me.cmbTransaction.SelectedIndex, Me.txtCustID.Text, Me.txtCustomer.Text, Me.cmbDepartment.SelectedIndex, MDI.stbUserID.Text)

                    GenPopID += 1
                    ClassPresume = classList.Item(i) 'Use for extra that has no source

                Next
                CleanUpListOfString()
            End If

            If tmp_qtybal < Val(Me.txtQuantity.Text) Then
                cache_qty = Val(Me.txtQuantity.Text) - tmp_qtybal
                If IsNothing(ClassPresume) Then ClassPresume = classALT 'Use when no source at all

                TempInserterOut(NullRoot, NullRun, Me.txtItemID.Text, NullFree, _
                ClassPresume, classALT, NullSerial, cache_qty, Me.txtUnit.Text, 0.0, 0, _
                Me.mtxtDate.Text, Me.cmbBranch.SelectedIndex, Nullaf, Nullnum, GenAFID, Me.txtNumber.Text, _
                Me.cmbTransaction.SelectedIndex, Me.txtCustID.Text, Me.txtCustomer.Text, Me.cmbDepartment.SelectedIndex, MDI.stbUserID.Text)

                GenPopID += 1
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub ExecTempInserterSI()
        Try
            Dim classALT As String = Me.cmbClass.Text
            Dim i, cache_qty As Integer

            If Me.cmbClass.SelectedIndex < 1 Then classALT = ""

            If selectedCount > 0 Then
                For i = 0 To selectedCount - 1
                    cache_qty = 1

                    TempInserterOut(rootList.Item(i), runList.Item(i), Me.txtItemID.Text, IsFreeList.Item(i), _
                    NullClass, classALT, Me.txtSerialNumber.Text, cache_qty, Me.txtUnit.Text, costList.Item(i), 0, _
                    Me.mtxtDate.Text, Me.cmbBranch.SelectedIndex, afList.Item(i), afnumList.Item(i), GenAFID, Me.txtNumber.Text, _
                    Me.cmbTransaction.SelectedIndex, Me.txtCustID.Text, Me.txtCustomer.Text, Me.cmbDepartment.SelectedIndex, MDI.stbUserID.Text)

                    GenPopID += 1
                    Me.SerialItemCount -= 1
                    If Me.SerialItemCount = 0 Then Me.btnNewModel.PerformClick()
                Next
                CleanUpListOfString()
            Else
                cache_qty = 1

                TempInserterOut(NullRoot, NullRun, Me.txtItemID.Text, NullFree, _
                NullClass, classALT, Me.txtSerialNumber.Text, cache_qty, Me.txtUnit.Text, 0, 0, _
                Me.mtxtDate.Text, Me.cmbBranch.SelectedIndex, NullAF, NullNum, GenAFID, Me.txtNumber.Text, _
                Me.cmbTransaction.SelectedIndex, Me.txtCustID.Text, Me.txtCustomer.Text, Me.cmbDepartment.SelectedIndex, MDI.stbUserID.Text)

                GenPopID += 1
                Me.SerialItemCount -= 1
                If Me.SerialItemCount = 0 Then Me.btnNewModel.PerformClick()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillBranch()
        Try
            sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillClass()
        Try
            sqlStr = "SELECT classes FROM Library WHERE classes IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbClass.Items.Clear()
            Me.cmbClass.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbClass.Items.Add(sqlReader("classes"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbClass.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillDepartIndiv()
        Try
            sqlStr = "SELECT sections FROM Library WHERE sections IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbDepartment.Items.Clear()
            Me.cmbDepartment.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbDepartment.Items.Add(sqlReader(0))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbDepartment.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvMake()
        Try
            Me.TempTable = MakeDataTableTemp()

            sqlStr = "SELECT order_id, item_id, " & _
            "(SELECT short_name FROM AFs WHERE af_id = tmp.ref_af_id) AS ref, ref_af_num, " & _
            "(SELECT brand FROM ItemMasterlist AS im " & _
            "WHERE im.item_id = tmp.item_id) AS tmpbrand, (SELECT product FROM ItemMasterlist AS im1 " & _
            "WHERE im1.item_id = tmp.item_id) AS tmpproduct, (SELECT model FROM ItemMasterlist AS im2 " & _
            "WHERE im2.item_id = tmp.item_id) AS tmpmodel, (SELECT description FROM ItemMasterlist AS im3 " & _
            "WHERE im3.item_id = tmp.item_id) AS tmpdesc, class, is_free, serial_num, qty, unit, cost, section_lib_id, " & _
            "(SELECT sections FROM Library WHERE lib_id = tmp.section_lib_id) AS tmpsection " & _
            "FROM Temp_AFsOut AS tmp WHERE ua_id = " & MDI.stbUserID.Text & ""
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()
                Row("pop_id") = sqlReader("order_id")
                Row("ref") = Me.sqlReader("ref")
                Row("number") = Me.sqlReader("ref_af_num")
                Row("brand") = sqlReader("tmpbrand")
                Row("product") = sqlReader("tmpproduct")
                Row("model") = sqlReader("tmpmodel")
                Row("desc") = sqlReader("tmpdesc")
                Row("class") = sqlReader("class")
                Row("free") = sqlReader("is_free").ToString.Remove(1)
                Row("serial") = sqlReader("serial_num")
                Row("qty") = sqlReader("qty")
                Row("unit") = sqlReader("unit")
                Row("cost") = sqlReader("cost")
                Row("ofisUse") = sqlReader("tmpsection")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            dgvMake.DataSource = TempDataView

            If Me.dgvMake.RowCount < 1 Then dgvMakeConfig(Me.dgvMake)
        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillTransaction()
        Try
            sqlStr = "SELECT " & GenShortAF & " FROM Transactions WHERE " & GenShortAF & " IS NOT NULL ORDER BY trans_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbTransaction.Items.Clear()
            Me.cmbTransaction.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbTransaction.Items.Add(sqlReader(0))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbTransaction.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmMakeDR_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        GetAFID(Me.Text.ToUpper)
        GetShortAF(Me.Text.ToUpper)
        Me.btnNew.Focus()
    End Sub

    Private Sub frmMakeDR_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        DelPrevTempOut()
        Me.CleaningUpObjects()
        ResetAFsMenusMDI()
    End Sub

    Private Sub frmMakeDR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        GetAFID(Me.Text.ToUpper)
        GetShortAF(Me.Text.ToUpper)
        Me.LoadDataAndRefresh()
        Me.RefreshCtrlVars()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FillBranch()
        Me.FillTransaction()
        Me.FillClass()
        Me.FillDepartIndiv()
    End Sub

    Private Sub mtxtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtNumber.Focus()
    End Sub

    Private Sub rbCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCancel.Click
        If InvalidCancelledAF(Me.txtNumber.Text, Me.cmbBranch.SelectedIndex, Me.mtxtDate.Text) = True Then
            MessageBox.Show("Minimum required data not sufficient or invalid.", " Be Cautious!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Me.rbCancel.Checked = False
            Exit Sub
        End If

        If MessageBox.Show("This is only intended for cancelled documents. Are you sure you want to proceed?", "Please confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
            CancelledAF(Me.txtNumber.Text, Me.cmbBranch.SelectedIndex, Me.mtxtDate.Text)

            MessageBox.Show("" & GenShortAF & " " & Me.txtNumber.Text & " saved succesfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.RefreshCtrlVars()
        Else
            Me.rbCancel.Checked = False
            Exit Sub
        End If
    End Sub

    Private Sub RefreshCtrlVars()
        Me.ResetVariables()
        ResetAFcontrols(Me)
    End Sub

    Private Sub ResetVariables()
        'Global
        SelectedItemID = Nothing
        GenRootID = Nothing
        GenRunID = Nothing
        GenPopID = Nothing
    End Sub

    Private Sub txtCustomer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCustomer.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtRemarks.Focus()
    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.btnCustomer.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.Leave
        If Me.txtNumber.Text.ToString.Trim <> "" Then IsNumberExists("AFsOut", Me.cmbBranch.SelectedIndex, Me.txtNumber.Text, Me.txtNumber)
    End Sub

    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then
            Me.btnSourceFinder.Focus()
            If Me.cmbDepartment.Enabled = True Then Me.cmbDepartment.Focus()
            If Me.txtSerialNumber.Enabled = True Then Me.txtSerialNumber.Focus()
        End If

        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnItemFinder.Focus()
    End Sub

    Private Sub txtSerialNumber_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerialNumber.EnabledChanged
        Me.txtSerialNumber.Clear()
    End Sub

    Private Sub txtSerialNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerialNumber.KeyPress
        If e.KeyChar = Chr(13) Then
            If Me.cmbDepartment.Enabled = True Then
                Me.cmbDepartment.Focus()
            Else
                Me.btnSourceFinder.Focus()
            End If
        End If
    End Sub
End Class
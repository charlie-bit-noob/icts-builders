Imports System.Data
Imports System.Data.SqlClient

Public Class frmNewUserAccount

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmNewUserAccount_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtusername.Focus()
    End Sub

    Private Sub frmNewUserAccount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub txtusername_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtusername.KeyPress
        Dim ValidInputChar = AlphaOnly.Replace(" ", "") + vbBack

        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then Me.txtpassword.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtpassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpassword.KeyPress
        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then Me.txtretypepassword.Focus()
    End Sub

    Private Sub txtretypepassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtretypepassword.KeyPress
        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then Me.cmbAccessLevel.Focus()
    End Sub

    Private Sub cmbAccessLevel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbAccessLevel.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnCreate.PerformClick()
    End Sub

    Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            If HasInvalidInputs() = True Then Exit Sub
            CallTrimmer()
            If HasAlreadyExists(Me.txtusername.Text) = True Then Exit Sub

            sqlStr = "INSERT INTO UserAccounts(username, password, access_level_lib_id)VALUES(@username, @password, @access_level_lib_id)"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr
            With sqlCmd.Parameters
                .Add(New SqlParameter("@username", SqlDbType.NVarChar)).Value = Me.txtusername.Text
                .Add(New SqlParameter("@password", SqlDbType.NVarChar)).Value = Me.txtpassword.Text
                .Add(New SqlParameter("@access_level_lib_id", SqlDbType.Int)).Value = Me.cmbAccessLevel.SelectedIndex
            End With
            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            ResetControls()
            MessageBox.Show("New account created!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        CleaningUpObjects()
        Me.Close()
    End Sub

    Private Sub LoadDataAndRefresh()
        FillAccessLevel()
        Me.ResetControls()
    End Sub

    Private Sub FillAccessLevel()
        Try
            sqlStr = "SELECT access_levels FROM Library WHERE access_levels IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbAccessLevel.Items.Clear()
            Me.cmbAccessLevel.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbAccessLevel.Items.Add(sqlReader("access_levels"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbAccessLevel.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Function HasInvalidInputs() As Boolean
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is TextBox Then
                If HasNullValue(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
            If TypeOf ctrl Is ComboBox Then
                If HasInvalidIndex(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Function HasAlreadyExists(ByVal strUserName As String) As Boolean
        Try
            sqlStr = "SELECT * FROM User_Accounts WHERE username = '" & strUserName & "'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Username already exists!", "Duplicate Found!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Sub ResetControls()
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
            If TypeOf ctrl Is ComboBox Then ReIndexComboBox(ctrl)
        Next
        Me.btnclose.Focus()
    End Sub

    Private Sub CleaningUpObjects()
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmStockPosition

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub btnCaptureItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCaptureItem.Click
        Try
            Me.txtItemID.Text = SelectedItemID

            If Me.txtItemID.Text = "" Then Exit Sub

            Me.dsTransaction.Tables("tblStockPos").Clear()

            Dim retMaxID As Integer

            Me.sqlStr = "SELECT MAX(branch_id) AS MaxID FROM Branches"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()
            retMaxID = Me.sqlReader(0)
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.sqlStr = "SELECT im.brand + ' ' + im.product + ' ' + im.model + ' ' + im.description AS Particular, " & _
            "l.Units FROM ItemMasterlist as im, Library l " & _
            "WHERE im.item_id = '" & Me.txtItemID.Text & "' AND l.lib_id = im.unit_lib_id"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()
            Dim strParticular As String = Me.sqlReader(0)
            Dim strUnit As String = Me.sqlReader(1)
            Me.sqlReader.Close()
            sqlDBconn.Close()

            For i As Integer = 1 To retMaxID
                If strParticular.Contains(" TILES ") Then
                    Me.sqlStr = "SELECT b.long_name, bal = 0, A = (SELECT [" & i & "] FROM StockFragile WHERE class = 'A' AND item_id = '" & Me.txtItemID.Text & "'), " & _
                    "B = (SELECT [" & i & "] FROM StockFragile WHERE class = 'B' AND item_id = '" & Me.txtItemID.Text & "'), " & _
                    "C = (SELECT [" & i & "] FROM StockFragile WHERE class = 'C' AND item_id = '" & Me.txtItemID.Text & "') " & _
                    "FROM Branches b WHERE b.branch_id = " & i & ""

                Else
                    Me.sqlStr = "SELECT b.long_name, bal = (SELECT [" & i & "] FROM StockDurable WHERE item_id = '" & Me.txtItemID.Text & "'), " & _
                    "A = 0, B = 0, C = 0 FROM Branches b WHERE b.branch_id = " & i & ""
                End If

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()

                If Me.sqlReader.Read() Then
                    Dim tblRow As DataRow
                    tblRow = Me.dsTransaction.Tables("tblStockPos").NewRow

                    tblRow("Branch") = Me.sqlReader("long_name")
                    tblRow("Date") = Date.Now
                    tblRow("Particular") = strParticular
                    tblRow("Balance") = Me.sqlReader("bal")
                    tblRow("A") = Me.sqlReader("A")
                    tblRow("B") = Me.sqlReader("B")
                    tblRow("C") = Me.sqlReader("C")
                    tblRow("Unit") = strUnit
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsTransaction.Tables("tblStockPos").Rows.Add(tblRow)

                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                    Exit For
                End If
            Next

            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            Me.RptViewer.ZoomMode = ZoomMode.Percent
            Me.RptViewer.ZoomPercent = 100

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnItemFinder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItemFinder.Click
        Me.btnCaptureItem.Focus()
        My.Forms.frmFinderItem.ShowDialog()
    End Sub

    Private Sub frmStockPosition_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        MDI.mnuStockPosition.Enabled = True
    End Sub

    Private Sub frmStockPosition_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.btnItemFinder.Focus()
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmAdjustmentSerial

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function HasErrorInputs() As Boolean
        If Me.txtSerialCurrent.Text = "" Or Me.txtSerialReplacement.Text = "" Then
            MessageBox.Show("Please provide all the data in order to proceed.", "Pag sure ra gud diha!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return True
            Exit Function
        End If

        If Me.txtSerialCurrent.Text = Me.txtSerialReplacement.Text Then
            MessageBox.Show("serial numbers cannot be equal", "Pag sure ra gud diha!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Return True
            Exit Function
        End If
    End Function

    Private Function IsCurrentExists() As Boolean
        Try
            '--Check if Current exists
            Me.sqlStr = "SELECT * FROM TypeSerial WHERE serial_num = '" & Me.txtSerialCurrent.Text.Trim & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            If Me.sqlReader.Read() Then
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Return False
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Cannot find current serial number!", "Pag sure ra gud diha!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.txtSerialCurrent.Focus()
                Return True
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function IsReplacementExists() As Boolean
        Try
            '--Check if Replacement is already exists
            Me.sqlStr = "SELECT * FROM TypeSerial WHERE serial_num = '" & Me.txtSerialReplacement.Text.Trim & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            If Me.sqlReader.Read() Then
                Me.sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Replacement serial number already exists!", "Pag sure ra gud diha!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.txtSerialCurrent.Focus()
                Return True
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Sub ApplySerialNumberReplacement()
        Try
            '--change serial
            Me.sqlStr = "UPDATE TypeSerial SET serial_num = '" & Me.txtSerialReplacement.Text.Trim & "' " & _
            "WHERE serial_num = '" & Me.txtSerialCurrent.Text.Trim & "'"
            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr

            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            '--change username
            'Dim ArrayAFs(0 To 2) As String
            'Dim objAFs As Object

            'ArrayAFs(0) = "AFsIn"
            'ArrayAFs(1) = "AFsTransfer"
            'ArrayAFs(2) = "AFsOut"

            'For Each objAFs In ArrayAFs
            'Me.sqlStr = "UPDATE  " & objAFs.ToString & " " & _
            '"SET ua_id = " & MDI.stbUserID.Text & ", date_updated = '" & DateTime.Now & "' " & _
            '"WHERE serial_num = '" & Me.txtSerialReplacement.Text.Trim & "'"
            'Me.sqlCmd = New SqlCommand
            'Me.sqlCmd.Connection = sqlDBconn
            'Me.sqlCmd.CommandText = Me.sqlStr

            'sqlDBconn.Open()
            'Me.sqlCmd.ExecuteNonQuery()
            'sqlDBconn.Close()
            'Next

            MessageBox.Show("serial number has been successfully applied!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.ClearTextBox()
            Me.btnClose.Focus()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If Me.HasErrorInputs = True Then Exit Sub
            If Me.IsCurrentExists = True Then Exit Sub
            If Me.IsReplacementExists = True Then Exit Sub

            If MessageBox.Show("Are you sure you want to replace the current serial number " & Me.txtSerialCurrent.Text & " with " & Me.txtSerialReplacement.Text & "?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Me.ApplySerialNumberReplacement()
            'My.Forms.MDI.mnuAdjustments.Enabled = False

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub ClearTextBox()
        Me.txtSerialCurrent.Clear()
        Me.txtSerialReplacement.Clear()
    End Sub
    Private Sub frmAdjustmentSerial_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ClearTextBox()
        Me.txtSerialCurrent.Focus()
    End Sub

    Private Sub txtSerialCurrent_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerialCurrent.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtSerialReplacement.Focus()
    End Sub

    Private Sub txtSerialReplacement_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerialReplacement.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmlistDR

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr, strFilter, strPara As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_date As DataColumn = New DataColumn("date")
        col_date.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_date)

        Dim col_number As DataColumn = New DataColumn("number")
        col_number.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_number)

        Dim col_trans As DataColumn = New DataColumn("trans")
        col_trans.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_trans)

        Dim col_branch As DataColumn = New DataColumn("branch")
        col_branch.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_branch)

        Dim col_custid As DataColumn = New DataColumn("custid")
        col_custid.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_custid)

        Dim col_deliveredto As DataColumn = New DataColumn("deliveredto")
        col_deliveredto.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_deliveredto)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("desc")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_unit)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        Dim col_remarks As DataColumn = New DataColumn("remarks")
        col_remarks.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_remarks)

        Dim col_user As DataColumn = New DataColumn("user")
        col_user.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_user)

        Dim col_datepost As DataColumn = New DataColumn("datepost")
        col_datepost.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_datepost)

        TableTemp.DefaultView.Sort = "datepost DESC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Function TrappingExtract() As Boolean
        If Me.mtxtFrom.MaskFull = True Then
            If Not IsDate(Me.mtxtFrom.Text) Then
                MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            End If
        End If
        If Me.mtxtTo.MaskFull = True Then
            If Not IsDate(Me.mtxtTo.Text) Then
                MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            End If
        End If
        If Me.txtSpecific.Text <> "" Then
            If Me.mtxtFrom.MaskFull Or Me.mtxtTo.MaskFull Then
                MessageBox.Show("Clear all unnecessary data when the numbers are specified.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return True
            End If
        End If
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Try
            If Me.TrappingExtract() = True Then Exit Sub

            Dim dvDate, dvBranch, dvTransaction, dvDeliveredTo, dvCustID As String
            Dim dvSpecificNum As String = ""

            If Me.mtxtFrom.MaskFull = False Then
                dvDate = "date <= '" & Me.mtxtTo.Text & "' AND "
            Else
                dvDate = "date >= '" & Me.mtxtFrom.Text & "' AND date <= '" & Me.mtxtTo.Text & "' AND "
            End If

            If Me.mtxtTo.MaskFull = False Then
                dvDate = ""
            End If

            If Me.cmbBranch.SelectedIndex > 0 Then
                dvBranch = "branch LIKE '%" & Me.cmbBranch.Text & "%' AND "
            Else
                dvBranch = ""
            End If

            If Me.cmbTransaction.SelectedIndex > 0 Then
                dvTransaction = "trans LIKE '%" & Me.cmbTransaction.Text & "%' AND "
            Else
                dvTransaction = ""
            End If

            If Me.txtNonCust.Text <> "" Then
                dvDeliveredTo = "deliveredto LIKE '%" & Me.txtNonCust.Text & "%' AND "
            Else
                dvDeliveredTo = ""
            End If

            If Me.txtCustID.Text <> "" Then
                dvDeliveredTo = ""
                dvCustID = "custid LIKE '%" & Me.txtCustID.Text & "%' AND "
            Else
                dvCustID = ""
            End If

            If Me.txtSpecific.Text <> "" Then
                dvDate = ""
                dvTransaction = ""
                dvDeliveredTo = ""
                dvCustID = ""
                dvSpecificNum = "number IN (" & Me.txtSpecific.Text & ")"
            End If

            Me.strFilter = dvDate + dvBranch + dvTransaction + dvDeliveredTo + dvCustID + dvSpecificNum

            If Me.strFilter.StartsWith("AND ") Then Me.strFilter = Me.strFilter.Remove(0, 4)
            If Me.strFilter.EndsWith("AND ") Then Me.strFilter = Me.strFilter.Remove(Me.strFilter.Length - 4)

            Me.TempDataView.RowFilter = Me.strFilter
            If Me.mtxtTo.MaskFull = False Then Me.GetMinMaxDate()
            Me.TempDataView.Sort = "number DESC"

            If Me.dgvList.RowCount < 1 Then Exit Sub

            Me.txtHeader.Text = (Me.cmbBranch.Text.Insert(Me.cmbBranch.Text.Length, " - ") + Me.Text + " Summary").ToUpper
            Me.txtFooter.Text = "< DR > " + "" & Me.cmbTransaction.Text.Insert(Me.cmbTransaction.Text.Length, " > ") & "" + "" & Me.cmbBranch.Text & "" + _
            " " + "" & Me.mtxtFrom.Text.Insert(0, "(") & "" + " - " + "" & Me.mtxtTo.Text.Insert(Me.mtxtTo.Text.Length, ")")

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If Me.dgvList.RowCount < 1 Then Exit Sub
            If My.Forms.frmRptAFDR.Visible = True Then My.Forms.frmRptAFDR.Close()
            If Me.TrappingExtract() = True Then Exit Sub

            Dim sqlAFID, sqlDate, sqlBranch, sqlTransaction, sqlDeliveredTo, sqlCustID As String
            Dim sqlSpecificNum As String = ""

            sqlAFID = "af_id = " & GenAFID & " AND "

            If Me.mtxtFrom.MaskFull = False Then
                sqlDate = "date_issued <= '" & Me.mtxtTo.Text & "' AND "
            Else
                sqlDate = "date_issued BETWEEN '" & Me.mtxtFrom.Text & "' AND '" & Me.mtxtTo.Text & "' AND "
            End If

            If Me.mtxtTo.MaskFull = False Then
                sqlDate = ""
            End If

            If Me.cmbBranch.SelectedIndex > 0 Then
                sqlBranch = "branch_id = " & Me.cmbBranch.SelectedIndex & " AND "
            Else
                sqlBranch = ""
            End If

            If Me.cmbTransaction.SelectedIndex > 0 Then
                sqlTransaction = "trans_id = " & Me.cmbTransaction.SelectedIndex & " AND "
            Else
                sqlTransaction = ""
            End If

            'The last problem occurs here. Choose either cust_id or non_cust to be queried
            If Me.txtNonCust.Text <> "" Then 'Then
                sqlDeliveredTo = "non_cust LIKE '%" & Me.txtNonCust.Text & "%' AND "
            Else
                sqlDeliveredTo = ""
            End If

            If Me.txtCustID.Text <> "" Then
                sqlDeliveredTo = ""
                sqlCustID = "cust_id LIKE '%" & Me.txtCustID.Text & "%' AND "
            Else
                sqlCustID = ""
            End If

            If Me.txtSpecific.Text <> "" Then
                sqlDate = ""
                sqlTransaction = ""
                sqlDeliveredTo = ""
                sqlCustID = ""
                sqlSpecificNum = "af_num IN (" & Me.txtSpecific.Text & ")"
            End If

            Me.strPara = sqlAFID + sqlDate + sqlBranch + sqlTransaction + sqlDeliveredTo + sqlCustID + sqlSpecificNum

            If Me.strPara.StartsWith("AND ") Then Me.strPara = Me.strPara.Remove(0, 4)
            If Me.strPara.EndsWith("AND ") Then Me.strPara = Me.strPara.Remove(Me.strPara.Length - 4)

            Me.txtSQLpara.Text = Me.strPara

            'My.Forms.frmRptAFDR.Show()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub cbExtract_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbExtract.CheckStateChanged
        If Me.cbExtract.CheckState = CheckState.Checked Then
            Me.panelExtract.Enabled = True
            Me.FillTransaction()
            Me.txtNonCust.Clear()
            Me.mtxtFrom.ResetText()
            Me.mtxtTo.ResetText()
            Me.txtHeader.Clear()
            Me.txtFooter.Clear()
            Me.txtSQLpara.Clear()
            Me.mtxtFrom.Focus()
        Else
            Me.cmbTransaction.Items.Clear()
            Me.cmbTransaction.ResetText()
            Me.txtNonCust.Clear()
            Me.mtxtFrom.ResetText()
            Me.mtxtTo.ResetText()
            Me.txtHeader.Clear()
            Me.txtFooter.Clear()
            Me.txtSpecific.Clear()
            Me.txtSQLpara.Clear()
            Me.panelExtract.Enabled = False
        End If
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtNumber.Focus()
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        Try
            If Me.txtNumber.Text = "" And Me.cmbBranch.SelectedIndex = 0 Then
                Me.TempDataView.RowFilter = ""
                Me.TempDataView.Sort = "number DESC, branch DESC"
                Exit Sub
            End If
            Me.TempDataView.RowFilter = "branch LIKE '%" & Me.cmbBranch.Text & "%' AND number LIKE '%" & Me.txtNumber.Text & "%'"
            Me.TempDataView.Sort = "number DESC, branch DESC"
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub cmbTransaction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTransaction.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtCustID.Focus()
    End Sub

    Private Sub dgvList_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellDoubleClick
        DisplayAF(Me.Text, Me.dgvList)
    End Sub

    Private Sub FillBranch()
        Try
            sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvList()
        Try
            TempTable = MakeDataTableTemp()

            sqlStr = "SELECT root_id, run_id, order_id, date_issued, af_num, trans_id, " & _
            "(SELECT " & GenShortAF & " FROM Transactions WHERE trans_id = af.trans_id) AS af_trans, " & _
            "branch_id, (SELECT long_name FROM Branches WHERE branch_id = af.branch_id) AS af_branch, " & _
            "cust_id, af_cust = CASE " & _
            "WHEN cust_id <> '' THEN (SELECT first_name + ' ' + last_name + company_name AS flc FROM Customers WHERE cust_id = af.cust_id) " & _
            "ELSE non_cust " & _
            "End, " & _
            "item_id, (SELECT brand FROM ItemMasterlist AS im WHERE im.item_id = af.item_id) AS af_brand, " & _
            "(SELECT product FROM ItemMasterlist AS im1 WHERE im1.item_id = af.item_id) AS af_product, " & _
            "(SELECT model FROM ItemMasterlist AS im2 WHERE im2.item_id = af.item_id) AS af_model, " & _
            "(SELECT description FROM ItemMasterlist AS im3 WHERE im3.item_id = af.item_id) AS af_desc, class, " & _
            "is_free, (SELECT serial_num FROM TypeSerial WHERE item_id = af.item_id AND af_id = " & GenAFID & " AND " & _
            "date_issued = af.date_issued AND run_id = af.run_id AND root_id = af.root_id) AS af_serial, qty, " & _
            "(SELECT units FROM Library WHERE lib_id = (SELECT unit_lib_id FROM ItemMasterlist WHERE item_id = af.item_id)) AS af_unit, " & _
            "cost, (SELECT remarks FROM AFsRemarks WHERE branch_id = af.branch_id AND af_id = " & GenAFID & " AND af_num = af.af_num AND run_id = af.run_id) AS af_remarks, " & _
            "ua_id, (SELECT username FROM UserAccounts WHERE ua_id = af.ua_id) AS af_user, date_updated FROM AFsOut AS af " & _
            "WHERE af_id = " & GenAFID & " AND order_id = (SELECT MIN(order_id) AS min_order FROM AFsOut AS af1 WHERE af1.af_num = af.af_num)"

            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("pop_id") = sqlReader("order_id")
                Row("date") = sqlReader("date_issued")
                Row("number") = sqlReader("af_num")
                Row("trans") = sqlReader("af_trans")
                Row("custid") = sqlReader("cust_id")
                Row("deliveredto") = sqlReader("af_cust")
                Row("branch") = sqlReader("af_branch")
                Row("brand") = sqlReader("af_brand")
                Row("product") = sqlReader("af_product")
                Row("model") = sqlReader("af_model")
                Row("desc") = sqlReader("af_desc")
                Row("class") = sqlReader("class")
                Row("free") = sqlReader("is_free")
                Row("serial") = sqlReader("af_serial")
                Row("qty") = sqlReader("qty")
                Row("unit") = sqlReader("af_unit")
                Row("cost") = sqlReader("cost")
                Row("remarks") = sqlReader("af_remarks")
                Row("user") = sqlReader("af_user")
                Row("datepost") = sqlReader("date_updated")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "number DESC, branch DESC"
            Me.dgvList.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillTransaction()
        Try
            Me.sqlStr = "SELECT " & GenShortAF & " FROM Transactions WHERE " & GenShortAF & " IS NOT NULL ORDER BY trans_id"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.cmbTransaction.Items.Clear()
            Me.cmbTransaction.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbTransaction.Items.Add(sqlReader(0))
            End While

            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbTransaction.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmlistDR_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.cmbBranch.Focus()
    End Sub

    Private Sub frmlistDR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI

            GetAFID(Me.Text.ToUpper)
            GetShortAF(Me.Text.ToUpper)

            Me.LoadDataAndRefresh()
            dgvListConfig(Me.dgvList)

            If Me.dgvList.RowCount < 1 Then Me.PanelSearch.Enabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetMinMaxDate()
        Dim dMin, dMax As String
        Dim RowCount As Integer = Me.dgvList.RowCount

        If RowCount < 1 Or Me.txtSpecific.Text <> "" Then Exit Sub

        Me.TempDataView.Sort = "date DESC"
        dMin = Format(Me.dgvList.Item("date", RowCount - 1).Value, "MM/dd/yyyy")
        dMax = Format(Me.dgvList.Item("date", 0).Value, "MM/dd/yyyy")
        Me.mtxtFrom.Text = dMin
        Me.mtxtTo.Text = dMax
    End Sub

    Private Sub LinkCreate_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkCreate.LinkClicked
        CreateNewLink(Me)
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FilldgvList()
        Me.FillBranch()
        Me.txtNumber.Clear()
        Me.cmbBranch.Focus()
    End Sub

    Private Sub mtxtFrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtFrom.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtTo.Focus()
    End Sub

    Private Sub mtxtTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtTo.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtHeader.Focus()
    End Sub

    Private Sub panelExtract_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles panelExtract.EnabledChanged
        If Me.PanelSearch.Enabled = True Then
            Me.cbExtract.Enabled = True
        Else
            Me.cbExtract.Enabled = False
        End If
    End Sub

    Private Sub txtCustID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCustID.KeyPress
        Dim ValidInputChar = AlphaNumeric + "'" + vbBack
        If e.KeyChar = Chr(13) Then Me.txtNonCust.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtDeliveredTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNonCust.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtSpecific.Focus()
    End Sub

    Private Sub txtFooter_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFooter.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnFilter.Focus()
    End Sub

    Private Sub txtHeader_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHeader.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtFooter.Focus()
    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.TextChanged
        If Me.txtNumber.Text = "" And Me.cmbBranch.SelectedIndex = 0 Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "number DESC, branch DESC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "branch = '" & Me.cmbBranch.Text & "' AND number = '" & Me.txtNumber.Text & "'"
        Me.TempDataView.Sort = "number DESC, branch DESC"
    End Sub

    Private Sub txtSpecific_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSpecific.KeyPress
        Dim ValidInputChar = NumericOnly + ",'" + vbBack
        If e.KeyChar = Chr(13) Then Me.btnFilter.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub
End Class
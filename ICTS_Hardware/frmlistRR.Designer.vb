<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmlistRR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.PanelSearch = New System.Windows.Forms.Panel
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.LinkCreate = New System.Windows.Forms.LinkLabel
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.btnClose = New System.Windows.Forms.Button
        Me.panelExtract = New System.Windows.Forms.Panel
        Me.txtFooter = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmbTransaction = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmbSupplier = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtSpecific = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnFilter = New System.Windows.Forms.Button
        Me.txtSQLpara = New System.Windows.Forms.TextBox
        Me.txtHeader = New System.Windows.Forms.TextBox
        Me.cbVATX = New System.Windows.Forms.CheckBox
        Me.btnOK = New System.Windows.Forms.Button
        Me.gbDate = New System.Windows.Forms.GroupBox
        Me.mtxtTo = New System.Windows.Forms.MaskedTextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.mtxtFrom = New System.Windows.Forms.MaskedTextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cbExtract = New System.Windows.Forms.CheckBox
        Me.PanelSearch.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelExtract.SuspendLayout()
        Me.gbDate.SuspendLayout()
        Me.SuspendLayout()
        '
        'PanelSearch
        '
        Me.PanelSearch.Controls.Add(Me.txtNumber)
        Me.PanelSearch.Controls.Add(Me.Label1)
        Me.PanelSearch.Controls.Add(Me.Label5)
        Me.PanelSearch.Controls.Add(Me.cmbBranch)
        Me.PanelSearch.Location = New System.Drawing.Point(12, 10)
        Me.PanelSearch.Name = "PanelSearch"
        Me.PanelSearch.Size = New System.Drawing.Size(458, 31)
        Me.PanelSearch.TabIndex = 36
        '
        'txtNumber
        '
        Me.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumber.Location = New System.Drawing.Point(349, 5)
        Me.txtNumber.MaxLength = 10
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(100, 21)
        Me.txtNumber.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Branch:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(295, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "&Number:"
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(59, 5)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(231, 21)
        Me.cmbBranch.TabIndex = 1
        '
        'LinkCreate
        '
        Me.LinkCreate.AutoSize = True
        Me.LinkCreate.Location = New System.Drawing.Point(765, 19)
        Me.LinkCreate.Name = "LinkCreate"
        Me.LinkCreate.Size = New System.Drawing.Size(113, 13)
        Me.LinkCreate.TabIndex = 31
        Me.LinkCreate.TabStop = True
        Me.LinkCreate.Text = "Create new document"
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvList.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvList.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvList.Location = New System.Drawing.Point(12, 47)
        Me.dgvList.MultiSelect = False
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(862, 377)
        Me.dgvList.TabIndex = 30
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(687, 47)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 32
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'panelExtract
        '
        Me.panelExtract.Controls.Add(Me.txtFooter)
        Me.panelExtract.Controls.Add(Me.Label9)
        Me.panelExtract.Controls.Add(Me.Label10)
        Me.panelExtract.Controls.Add(Me.cmbTransaction)
        Me.panelExtract.Controls.Add(Me.Label8)
        Me.panelExtract.Controls.Add(Me.cmbSupplier)
        Me.panelExtract.Controls.Add(Me.Label3)
        Me.panelExtract.Controls.Add(Me.txtSpecific)
        Me.panelExtract.Controls.Add(Me.Label4)
        Me.panelExtract.Controls.Add(Me.btnFilter)
        Me.panelExtract.Controls.Add(Me.txtSQLpara)
        Me.panelExtract.Controls.Add(Me.txtHeader)
        Me.panelExtract.Controls.Add(Me.cbVATX)
        Me.panelExtract.Controls.Add(Me.btnOK)
        Me.panelExtract.Controls.Add(Me.gbDate)
        Me.panelExtract.Enabled = False
        Me.panelExtract.Location = New System.Drawing.Point(12, 446)
        Me.panelExtract.Name = "panelExtract"
        Me.panelExtract.Size = New System.Drawing.Size(871, 121)
        Me.panelExtract.TabIndex = 39
        '
        'txtFooter
        '
        Me.txtFooter.Location = New System.Drawing.Point(83, 92)
        Me.txtFooter.Name = "txtFooter"
        Me.txtFooter.Size = New System.Drawing.Size(469, 21)
        Me.txtFooter.TabIndex = 227
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(31, 95)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 226
        Me.Label9.Text = "Footer:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(31, 68)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 13)
        Me.Label10.TabIndex = 225
        Me.Label10.Text = "Header:"
        '
        'cmbTransaction
        '
        Me.cmbTransaction.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbTransaction.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbTransaction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTransaction.FormattingEnabled = True
        Me.cmbTransaction.Location = New System.Drawing.Point(332, 26)
        Me.cmbTransaction.Name = "cmbTransaction"
        Me.cmbTransaction.Size = New System.Drawing.Size(220, 21)
        Me.cmbTransaction.TabIndex = 224
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(329, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(67, 13)
        Me.Label8.TabIndex = 223
        Me.Label8.Text = "Transaction:"
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.FormattingEnabled = True
        Me.cmbSupplier.Location = New System.Drawing.Point(558, 26)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(304, 21)
        Me.cmbSupplier.TabIndex = 222
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(574, 68)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 220
        Me.Label3.Text = "RR(s):"
        '
        'txtSpecific
        '
        Me.txtSpecific.Location = New System.Drawing.Point(615, 65)
        Me.txtSpecific.Name = "txtSpecific"
        Me.txtSpecific.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSpecific.Size = New System.Drawing.Size(247, 21)
        Me.txtSpecific.TabIndex = 219
        Me.txtSpecific.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(555, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 218
        Me.Label4.Text = "Supplier:"
        '
        'btnFilter
        '
        Me.btnFilter.Location = New System.Drawing.Point(706, 92)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(75, 23)
        Me.btnFilter.TabIndex = 217
        Me.btnFilter.Text = "&Filter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'txtSQLpara
        '
        Me.txtSQLpara.Location = New System.Drawing.Point(558, 92)
        Me.txtSQLpara.Name = "txtSQLpara"
        Me.txtSQLpara.ReadOnly = True
        Me.txtSQLpara.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSQLpara.Size = New System.Drawing.Size(139, 21)
        Me.txtSQLpara.TabIndex = 216
        Me.txtSQLpara.TabStop = False
        Me.txtSQLpara.Visible = False
        '
        'txtHeader
        '
        Me.txtHeader.Location = New System.Drawing.Point(83, 65)
        Me.txtHeader.Name = "txtHeader"
        Me.txtHeader.Size = New System.Drawing.Size(469, 21)
        Me.txtHeader.TabIndex = 18
        '
        'cbVATX
        '
        Me.cbVATX.AutoSize = True
        Me.cbVATX.Location = New System.Drawing.Point(808, 3)
        Me.cbVATX.Name = "cbVATX"
        Me.cbVATX.Size = New System.Drawing.Size(60, 17)
        Me.cbVATX.TabIndex = 17
        Me.cbVATX.Text = "&VAT Ex"
        Me.cbVATX.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(787, 92)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 9
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'gbDate
        '
        Me.gbDate.Controls.Add(Me.mtxtTo)
        Me.gbDate.Controls.Add(Me.Label2)
        Me.gbDate.Controls.Add(Me.mtxtFrom)
        Me.gbDate.Controls.Add(Me.Label6)
        Me.gbDate.Location = New System.Drawing.Point(21, 7)
        Me.gbDate.Name = "gbDate"
        Me.gbDate.Size = New System.Drawing.Size(302, 49)
        Me.gbDate.TabIndex = 8
        Me.gbDate.TabStop = False
        Me.gbDate.Text = "Date Range"
        '
        'mtxtTo
        '
        Me.mtxtTo.BeepOnError = True
        Me.mtxtTo.Location = New System.Drawing.Point(186, 20)
        Me.mtxtTo.Mask = "00/00/0000"
        Me.mtxtTo.Name = "mtxtTo"
        Me.mtxtTo.Size = New System.Drawing.Size(102, 21)
        Me.mtxtTo.TabIndex = 219
        Me.mtxtTo.ValidatingType = GetType(Date)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(157, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(23, 13)
        Me.Label2.TabIndex = 218
        Me.Label2.Text = "To:"
        '
        'mtxtFrom
        '
        Me.mtxtFrom.BeepOnError = True
        Me.mtxtFrom.Location = New System.Drawing.Point(51, 20)
        Me.mtxtFrom.Mask = "00/00/0000"
        Me.mtxtFrom.Name = "mtxtFrom"
        Me.mtxtFrom.Size = New System.Drawing.Size(102, 21)
        Me.mtxtFrom.TabIndex = 217
        Me.mtxtFrom.ValidatingType = GetType(Date)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(10, 23)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "From:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(77, 438)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(813, 2)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        '
        'cbExtract
        '
        Me.cbExtract.AutoSize = True
        Me.cbExtract.Location = New System.Drawing.Point(12, 430)
        Me.cbExtract.Name = "cbExtract"
        Me.cbExtract.Size = New System.Drawing.Size(61, 17)
        Me.cbExtract.TabIndex = 37
        Me.cbExtract.Text = "E&xtract"
        Me.cbExtract.UseVisualStyleBackColor = True
        '
        'frmlistRR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(886, 572)
        Me.Controls.Add(Me.panelExtract)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cbExtract)
        Me.Controls.Add(Me.PanelSearch)
        Me.Controls.Add(Me.LinkCreate)
        Me.Controls.Add(Me.dgvList)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmlistRR"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receiving Report"
        Me.PanelSearch.ResumeLayout(False)
        Me.PanelSearch.PerformLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelExtract.ResumeLayout(False)
        Me.panelExtract.PerformLayout()
        Me.gbDate.ResumeLayout(False)
        Me.gbDate.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelSearch As System.Windows.Forms.Panel
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents LinkCreate As System.Windows.Forms.LinkLabel
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents panelExtract As System.Windows.Forms.Panel
    Friend WithEvents txtFooter As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbTransaction As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSpecific As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents txtSQLpara As System.Windows.Forms.TextBox
    Friend WithEvents txtHeader As System.Windows.Forms.TextBox
    Friend WithEvents cbVATX As System.Windows.Forms.CheckBox
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents gbDate As System.Windows.Forms.GroupBox
    Friend WithEvents mtxtTo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents mtxtFrom As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbExtract As System.Windows.Forms.CheckBox
End Class

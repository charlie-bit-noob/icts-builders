Imports System.Data
Imports System.Data.SqlClient

Public Class frmStockCard

    Private DataVWin, DataVWout As DataView
    Private InTable, OutTable As DataTable
    Private InTableRow, OutTableRow As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function MakeINtable() As DataTable
        Dim tbl_StockCard As DataTable
        tbl_StockCard = New DataTable("Stock_Card_IN")

        Dim BranchCol As DataColumn = New DataColumn("Branch")
        BranchCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(BranchCol)

        Dim DateCol As DataColumn = New DataColumn("Date")
        DateCol.DataType = System.Type.GetType("System.DateTime")
        tbl_StockCard.Columns.Add(DateCol)

        Dim CostCol As DataColumn = New DataColumn("Cost")
        CostCol.DataType = System.Type.GetType("System.Decimal")
        tbl_StockCard.Columns.Add(CostCol)

        Dim RefCol As DataColumn = New DataColumn("Reference")
        RefCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(RefCol)

        Dim NumberCol As DataColumn = New DataColumn("Number")
        NumberCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(NumberCol)

        Dim TransCol As DataColumn = New DataColumn("Transaction")
        TransCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(TransCol)

        Dim SourceCol As DataColumn = New DataColumn("Source")
        SourceCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(SourceCol)

        Dim SerialCol As DataColumn = New DataColumn("Serial_Number")
        SerialCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(SerialCol)

        Dim QtyCol As DataColumn = New DataColumn("Quantity")
        QtyCol.DataType = System.Type.GetType("System.Double")
        tbl_StockCard.Columns.Add(QtyCol)

        Dim ClassCol As DataColumn = New DataColumn("Class")
        ClassCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(ClassCol)

        Dim RootCol As DataColumn = New DataColumn("RootID")
        RootCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(RootCol)

        Dim RunCol As DataColumn = New DataColumn("RunID")
        RunCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(RunCol)

        tbl_StockCard.DefaultView.Sort = "Date ASC"

        MakeINtable = tbl_StockCard
    End Function

    Private Function MakeOUTtable() As DataTable
        Dim tbl_StockCard As DataTable
        tbl_StockCard = New DataTable("Stock_Card_OUT")

        Dim BranchCol As DataColumn = New DataColumn("Branch")
        BranchCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(BranchCol)

        Dim DateCol As DataColumn = New DataColumn("Date")
        DateCol.DataType = System.Type.GetType("System.DateTime")
        tbl_StockCard.Columns.Add(DateCol)

        Dim CostCol As DataColumn = New DataColumn("Cost")
        CostCol.DataType = System.Type.GetType("System.Decimal")
        tbl_StockCard.Columns.Add(CostCol)

        Dim PriceCol As DataColumn = New DataColumn("Price")
        PriceCol.DataType = System.Type.GetType("System.Decimal")
        tbl_StockCard.Columns.Add(PriceCol)

        Dim RefCol As DataColumn = New DataColumn("Reference")
        RefCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(RefCol)

        Dim NumberCol As DataColumn = New DataColumn("Number")
        NumberCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(NumberCol)

        Dim TransCol As DataColumn = New DataColumn("Transaction")
        TransCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(TransCol)

        Dim DeliverCol As DataColumn = New DataColumn("Delivered_To")
        DeliverCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(DeliverCol)

        Dim SerialCol As DataColumn = New DataColumn("Serial_Number")
        SerialCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(SerialCol)

        Dim QtyCol As DataColumn = New DataColumn("Quantity")
        QtyCol.DataType = System.Type.GetType("System.Decimal")
        tbl_StockCard.Columns.Add(QtyCol)

        Dim ClassCol As DataColumn = New DataColumn("Class")
        ClassCol.DataType = System.Type.GetType("System.String")
        tbl_StockCard.Columns.Add(ClassCol)

        tbl_StockCard.DefaultView.Sort = "Date ASC"

        MakeOUTtable = tbl_StockCard
    End Function

    Private Sub AFsIncomings()
        Me.InTableRow = Me.InTable.NewRow()

        Me.InTableRow("Branch") = Me.sqlReader("long_name")
        Me.InTableRow("Date") = Me.sqlReader("date_issued")
        Me.InTableRow("Cost") = Me.sqlReader("cost")
        Me.InTableRow("Reference") = Me.sqlReader("short_name")
        Me.InTableRow("Number") = Me.sqlReader("af_num")
        Me.InTableRow("Transaction") = Me.sqlReader("trans")
        Me.InTableRow("Source") = Me.sqlReader("source")
        Me.InTableRow("Serial_Number") = Me.sqlReader("serial").ToString
        Me.InTableRow("Quantity") = Me.sqlReader("qty")
        Me.InTableRow("Class") = Me.sqlReader("class")
        Me.InTableRow("RootID") = Me.sqlReader("root_id")
        Me.InTableRow("RunID") = Me.sqlReader("run_id")
        Me.InTable.Rows.Add(Me.InTableRow)
    End Sub

    Private Sub AFsOutgoings()
        Me.OutTableRow = Me.OutTable.NewRow()

        Me.OutTableRow("Branch") = Me.sqlReader("long_name")
        Me.OutTableRow("Date") = Me.sqlReader("date_issued")
        Me.OutTableRow("Cost") = Me.sqlReader("cost")
        Me.OutTableRow("Price") = Me.sqlReader("price")
        Me.OutTableRow("Reference") = Me.sqlReader("short_name")
        Me.OutTableRow("Number") = Me.sqlReader("af_num")
        Me.OutTableRow("Transaction") = Me.sqlReader("trans")
        Me.OutTableRow("Delivered_To") = Me.sqlReader("deliveredto")
        Me.OutTableRow("Serial_Number") = Me.sqlReader("serial").ToString
        Me.OutTableRow("Quantity") = Me.sqlReader("qty")
        Me.OutTableRow("Class") = Me.sqlReader("class")
        Me.OutTable.Rows.Add(Me.OutTableRow)
    End Sub

    Private Sub btnCaptureItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCaptureItem.Click
        Try
            Me.txtItemID.Text = SelectedItemID

            Me.sqlStr = "SELECT im.brand + ' ' + im.product + ' ' + im.model + ' ' + im.description AS particular, l.units " & _
            "FROM ItemMasterlist im, Library l WHERE im.item_id = '" & Me.txtItemID.Text & "' AND l.lib_id = im.unit_lib_id"

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.sqlReader.Read()
            Me.txtParticular.Text = Me.sqlReader("particular")
            Me.txtUnit.Text = Me.sqlReader("units")

            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnExecute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExecute.Click
        Try
            If Me.txtItemID.Text = "" Then
                MessageBox.Show("Please select an item.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            Dim intLoc As Integer

            InTable = MakeINtable()
            For i As Integer = 1 To Me.cmbBranch.Items.Count - 1
                intLoc = i

                Me.sqlStr = "SELECT b.long_name, af.date_issued, af.cost, a.short_name, af.af_num, " & _
                "trans = CASE " & _
                "WHEN a.short_name = 'WR' THEN (SELECT WR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'RR' THEN (SELECT RR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'PUR' THEN (SELECT PUR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "END, " & _
                "source = CASE " & _
                "WHEN af.supplier_id > 0 THEN (SELECT name FROM Suppliers WHERE supplier_id = af.supplier_id) " & _
                "WHEN COALESCE(af.non_cust, '') <> '' THEN af.non_cust " & _
                "WHEN af.cust_id <> '' THEN (SELECT first_name + ' ' + last_name + ' ' + company_name FROM Customers WHERE cust_id = af.cust_id) " & _
                "END, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id " & _
                "AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS serial, af.qty, " & _
                "class, af.root_id, af.run_id " & _
                "FROM AFsIn af, Branches b, AFs a " & _
                "WHERE af.item_id = '" & Me.txtItemID.Text & "' AND af.branch_id =  " & intLoc & " " & _
                "AND b.branch_id = " & intLoc & " AND a.af_id = af.af_id " & _
                "ORDER BY af.date_issued"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()
                While Me.sqlReader.Read
                    Me.AFsIncomings()
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Me.sqlStr = "SELECT b.long_name, af.date_issued, af.cost, a.short_name, af.af_num, " & _
                "trans = CASE " & _
                "WHEN a.short_name = 'SRS' THEN (SELECT SRS FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'STR' THEN (SELECT STR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "END, " & _
                "(SELECT long_name FROM Branches as b1 WHERE branch_id = af.src_branch_id) AS source, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id " & _
                "AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS serial, af.qty, " & _
                "class, af.root_id, af.run_id " & _
                "FROM AFsTransfer af, Branches b, AFs a " & _
                "WHERE af.item_id = '" & Me.txtItemID.Text & "' AND af.branch_id = " & intLoc & " " & _
                "AND b.branch_id = " & intLoc & " AND a.af_id = af.af_id " & _
                "ORDER BY af.date_issued"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()
                While Me.sqlReader.Read
                    Me.AFsIncomings()
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Next

            DataVWin = New DataView
            DataVWin.Table = InTable
            DataVWin.Sort = "Date ASC"
            dgvIn.DataSource = DataVWin

            If dgvIn.RowCount < 1 Then
                panel_ParaIN.Enabled = False
            Else
                panel_ParaIN.Enabled = True
            End If

            OutTable = MakeOUTtable()
            For i As Integer = 0 To Me.cmbBranch.Items.Count - 1
                intLoc = i

                sqlStr = "SELECT b.long_name, af.date_issued, af.cost, 0.000 AS price, a.short_name, af.af_num, " & _
                "trans = CASE " & _
                "WHEN a.short_name = 'SRS' THEN (SELECT SRS FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'STR' THEN (SELECT STR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "END, " & _
                "(SELECT long_name FROM Branches as b1 WHERE branch_id = af.branch_id) AS deliveredto, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id " & _
                "AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS serial, af.qty, class " & _
                "FROM AFsTransfer af, Branches b, AFs a " & _
                "WHERE af.item_id = '" & Me.txtItemID.Text & "' AND af.src_branch_id = " & intLoc & " AND b.branch_id = " & intLoc & " " & _
                "AND a.af_id = af.af_id ORDER BY af.date_issued"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()
                While Me.sqlReader.Read
                    Me.AFsOutgoings()
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()

                sqlStr = "SELECT b.long_name, af.date_issued, af.cost, af.price, a.short_name, af.af_num, " & _
                "trans=CASE " & _
                "WHEN a.short_name = 'CI' THEN (SELECT CI FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'CA' THEN (SELECT CA FROM Transactions WHERE trans_id = af.trans_id) " & _
                "WHEN a.short_name = 'DR' THEN (SELECT DR FROM Transactions WHERE trans_id = af.trans_id) " & _
                "END, " & _
                "deliveredto = CASE " & _
                "WHEN COALESCE(af.non_cust, '') <> '' THEN af.non_cust " & _
                "WHEN af.cust_id <> '' THEN (SELECT first_name + ' ' + last_name + ' ' + company_name FROM Customers WHERE cust_id = af.cust_id) " & _
                "END, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id " & _
                "AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id ) AS serial, af.qty, class " & _
                "FROM AFsOut af, Branches b, AFs a " & _
                "WHERE af.item_id = '" & Me.txtItemID.Text & "' AND af.branch_id =  " & intLoc & " AND b.branch_id = " & intLoc & " AND af.istransposed = 'False' " & _
                "AND a.af_id = af.af_id ORDER BY af.date_issued"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()
                While Me.sqlReader.Read
                    Me.AFsOutgoings()
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Next

            DataVWout = New DataView
            DataVWout.Table = OutTable
            DataVWout.Sort = "Date ASC"
            dgvOut.DataSource = DataVWout

            If dgvOut.RowCount < 1 Then
                panel_ParaOUT.Enabled = False
            Else
                panel_ParaOUT.Enabled = True
            End If

            Me.ResizeColumns()
            Me.HideThisColumn()
            Me.cmbBranch.SelectedIndex = 0
            Me.cbHasBalance.CheckState = CheckState.Unchecked
            Me.btnExecute.Enabled = False

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnExecute_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExecute.EnabledChanged
        If btnExecute.Enabled = True Then
            Me.cmbBranch.Enabled = False
        Else
            Me.cmbBranch.Enabled = True
        End If
        Me.txtTotalIn.ResetText()
        Me.txtTotalOut.ResetText()
    End Sub

    Private Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        Try
            Me.cbHasBalance.CheckState = CheckState.Unchecked

            If Me.cmbBranch.SelectedIndex < 1 Then
                Me.DataVWin = New DataView
                Me.DataVWin.Table = Me.InTable
                Me.DataVWin.Sort = "Date ASC"
                Me.dgvIn.DataSource = Me.DataVWin

                Me.DataVWin.RowFilter = String.Empty
                Me.DataVWin.Sort = "Date ASC"

                Me.DataVWout = New DataView
                Me.DataVWout.Table = Me.OutTable
                Me.DataVWout.Sort = "Date ASC"
                Me.dgvOut.DataSource = Me.DataVWout

                Me.DataVWout.RowFilter = String.Empty
                Me.DataVWout.Sort = "Date ASC"

                If Me.dgvIn.RowCount < 1 Then
                    Me.panel_ParaIN.Enabled = False
                Else
                    Me.panel_ParaIN.Enabled = True
                End If
                If Me.dgvOut.RowCount < 1 Then
                    Me.panel_ParaOUT.Enabled = False
                Else
                    Me.panel_ParaOUT.Enabled = True
                End If

                Me.SumQty()

                Me.btnFilter.Enabled = False
                Exit Sub
            End If

            Me.DataVWin = New DataView
            Me.DataVWin.Table = Me.InTable
            Me.DataVWin.Sort = "Date ASC"
            Me.dgvIn.DataSource = Me.DataVWin

            Me.DataVWin.RowFilter = "Branch = '" & Me.cmbBranch.Text & "'"
            Me.DataVWin.Sort = "Date ASC"

            Me.DataVWout = New DataView
            Me.DataVWout.Table = Me.OutTable
            Me.DataVWout.Sort = "Date ASC"
            Me.dgvOut.DataSource = Me.DataVWout

            Me.DataVWout.RowFilter = "Branch = '" & Me.cmbBranch.Text & "'"
            Me.DataVWout.Sort = "Date ASC"

            Me.SumQty()

            If Me.dgvIn.RowCount < 1 Then
                Me.panel_ParaIN.Enabled = False
            Else
                Me.panel_ParaIN.Enabled = True
            End If
            If dgvOut.RowCount < 1 Then
                Me.panel_ParaOUT.Enabled = False
            Else
                Me.panel_ParaOUT.Enabled = True
            End If

            Me.btnFilter.Enabled = False

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnItemFinder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnItemFinder.Click
        Me.btnCaptureItem.Focus()
        My.Forms.frmFinderItem.ShowDialog()
    End Sub

    Private Sub cbHasBalance_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbHasBalance.CheckStateChanged
        Try
            If Not Me.dgvIn.RowCount > 0 Then Exit Sub

            Dim iType As String = Nothing
            Dim tarCol As String = Nothing
            Dim gridRow As DataGridViewRow
            Dim cm As CurrencyManager = CType(dgvIn.BindingContext(dgvIn.DataSource), CurrencyManager)

            If Me.cbHasBalance.CheckState = CheckState.Unchecked Then
                For Each gridRow In Me.dgvIn.Rows
                    If gridRow.Visible = False Then gridRow.Visible = True
                Next
                Exit Sub
            End If

            Me.UnHideThisColumn()

            If Not Me.dgvIn.Item("Serial_Number", 0).Value = "" Then
                iType = "TypeSerial"
                tarCol = "has_bal"
            Else
                iType = "TypeNonSerial"
                tarCol = "qty_bal"
            End If

            For Each gridRow In dgvIn.Rows
                Dim celClass As String = gridRow.Cells("Class").Value.ToString
                Dim celSerial As String = gridRow.Cells("Serial_Number").Value.ToString
                Dim celRoot As String = gridRow.Cells("RootID").Value.ToString
                Dim celRun As String = gridRow.Cells("RunID").Value.ToString
                Dim celRef As String = gridRow.Cells("Reference").Value.ToString
                Dim celNum As String = gridRow.Cells("Number").Value.ToString
                Dim celBranch As String = gridRow.Cells("Branch").Value.ToString

                Me.sqlStr = "SELECT CAST(" & tarCol & " as float) AS status " & _
                "FROM " & iType & " " & _
                "WHERE af_id = (SELECT af_id FROM AFs WHERE short_name = '" & celRef & "') AND af_num = '" & celNum & "' " & _
                "AND branch_id = (SELECT branch_id FROM Branches WHERE long_name = '" & celBranch & "') " & _
                "AND run_id = '" & celRun & "' AND root_id = '" & celRoot & "' AND item_id = '" & Me.txtItemID.Text & "'"

                If iType.Contains("Non") Then
                    Me.sqlStr = Me.sqlStr + " AND class = '" & celClass & "'"
                Else
                    Me.sqlStr = Me.sqlStr + " AND serial_num = '" & celSerial & "'"
                End If

                Me.sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = sqlCmd.ExecuteReader()

                Me.sqlReader.Read()
                If Not Me.sqlReader("status") > 0 Then
                    cm.SuspendBinding()
                    gridRow.Visible = False
                    cm.ResumeBinding()
                End If
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Next gridRow

            HideThisColumn()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CloseActiveDocViewer()
        For Each childfrm As Form In MDI.MdiChildren
            If childfrm.Name.Contains("frmView") Then
                childfrm.Close()
            End If
        Next
    End Sub

    Private Sub cmbBranch_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.EnabledChanged
        If Me.cmbBranch.Enabled = False Then
            Me.btnFilter.Enabled = False
            Me.cbHasBalance.Enabled = False
        Else
            Me.btnFilter.Enabled = True
            Me.cbHasBalance.Enabled = True
        End If
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        Dim ctlonPanel As Control

        For Each ctlonPanel In Me.panel_ParaIN.Controls
            If TypeOf ctlonPanel Is TextBox Then ctlonPanel.Text = ""
            Me.txtDateIn.ResetText()
        Next
        For Each ctlonPanel In Me.panel_ParaOUT.Controls
            If TypeOf ctlonPanel Is TextBox Then ctlonPanel.Text = ""
            Me.txtDateOut.ResetText()
        Next
        Me.btnFilter.Enabled = True
    End Sub

    Private Sub dgvIn_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvIn.CellDoubleClick
        If Me.dgvIn.RowCount < 1 Then Exit Sub
        Me.dgvIn_JumpToAF()
    End Sub

    Private Sub dgvIn_JumpToAF()
        Try
            Dim selRow As Integer = Me.dgvIn.CurrentCell.RowIndex
            Dim JumpTo As String = Me.dgvIn.Item("Reference", selRow).Value.ToString

            If JumpTo = "WR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewWR
                    .txtNumber.Text = Me.dgvIn.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvIn.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "SRS" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewSRS
                    .txtNumber.Text = Me.dgvIn.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvIn.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "PUR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewPUR
                    .txtNumber.Text = Me.dgvIn.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvIn.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "RR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewRR
                    .txtNumber.Text = Me.dgvIn.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvIn.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "STR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewSTR
                    .txtNumber.Text = Me.dgvIn.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvIn.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvOut_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOut.CellDoubleClick
        If Me.dgvOut.RowCount < 1 Then Exit Sub
        Me.dgvOut_JumpToAF()
    End Sub

    Private Sub dgvOut_JumpToAF()
        Try
            Dim selRow As Integer = Me.dgvOut.CurrentCell.RowIndex
            Dim JumpTo As String = Me.dgvOut.Item("Reference", selRow).Value.ToString

            If JumpTo = "CI" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewCI
                    .txtNumber.Text = Me.dgvOut.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvOut.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "CA" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewCA
                    .txtNumber.Text = Me.dgvOut.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvOut.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "DR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewDR
                    .txtNumber.Text = Me.dgvOut.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvOut.Item("Branch", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "SRS" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewSRS
                    .txtNumber.Text = Me.dgvOut.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvOut.Item("Delivered_To", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
            If JumpTo = "STR" Then
                Me.CloseActiveDocViewer()
                With My.Forms.frmViewSTR
                    .txtNumber.Text = Me.dgvOut.Item("Number", selRow).Value.ToString
                    .txtBranch.Text = Me.dgvOut.Item("Delivered_To", selRow).Value.ToString
                    .Show()
                    JumpTo = Nothing
                End With
                Exit Sub
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillBranch()
        Try
            Me.sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            Me.sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While Me.sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmStockCard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.CloseActiveDocViewer()
        MDI.mnuStockCard.Enabled = True
    End Sub

    Private Sub frmStockCard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.FillBranch()
            Me.txtItemID.Clear()
            Me.panel_ParaIN.Enabled = False
            Me.panel_ParaOUT.Enabled = False
            Me.btnItemFinder.Focus()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub HideThisColumn()
        dgvIn.Columns("RootID").Visible = False
        dgvIn.Columns("RunID").Visible = False
    End Sub

    Private Sub panel_ParaIN_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles panel_ParaIN.EnabledChanged
        txtDateIn.ResetText()
    End Sub

    Private Sub panel_ParaOUT_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles panel_ParaOUT.EnabledChanged
        txtDateOut.ResetText()
    End Sub

    Private Sub ResizeColumns()
        Dim INCol As DataGridViewColumn
        For Each INCol In dgvIn.Columns
            If INCol.Name = "Cost" Then INCol.DefaultCellStyle.Format = "#,###.###"
            INCol.Width = 150
        Next
        Dim OUTCol As DataGridViewColumn
        For Each OUTCol In dgvOut.Columns
            If OUTCol.Name = "Price" Or OUTCol.Name = "Cost" Then OUTCol.DefaultCellStyle.Format = "#,###.###"
            OUTCol.Width = 150
        Next
    End Sub

    Private Sub SumQty()
        If Me.dgvIn.RowCount < 1 Then
            Me.txtTotalIn.Text = "0"
        Else
            Dim Row As DataGridViewRow
            Dim DumTotal As Double

            For Each Row In Me.dgvIn.Rows
                Dim strQty As String = Row.Cells("Quantity").Value.ToString

                If Not strQty = "" Then
                    Dim QtyCell As Double = Row.Cells("Quantity").Value

                    DumTotal = QtyCell + DumTotal
                End If
            Next Row
            Me.txtTotalIn.Text = DumTotal.ToString
        End If

        If Me.dgvOut.RowCount < 1 Then
            Me.txtTotalOut.Text = "0"
        Else
            Dim Row As DataGridViewRow
            Dim DumTotal As Double

            For Each Row In dgvOut.Rows
                Dim strQty As String = Row.Cells("Quantity").Value.ToString

                If Not strQty = "" Then
                    Dim QtyCell As Double = Row.Cells("Quantity").Value

                    DumTotal = QtyCell + DumTotal
                End If
            Next Row
            Me.txtTotalOut.Text = DumTotal.ToString
        End If
    End Sub

    Private Sub txtDateIn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDateIn.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtQtyIn.Focus()
    End Sub

    Private Sub txtDateIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDateIn.TextChanged
        If Me.txtDateIn.MaskFull = False Then
            If Me.btnExecute.Enabled = False Then
                Me.DataVWin.RowFilter = String.Empty
                Me.DataVWin.Sort = "Date ASC"

                If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + "Branch LIKE '%" & Me.cmbBranch.Text & "%'"
                Exit Sub
            End If
        Else
            If IsDate(Me.txtDateIn.Text) Then
                Me.DataVWin.RowFilter = "Date = '" & Me.txtDateIn.Text & "'"

                If Me.cmbBranch.SelectedIndex > 0 And btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            End If
            Exit Sub
        End If
    End Sub

    Private Sub txtDateOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDateOut.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtQtyOut.Focus()
    End Sub

    Private Sub txtDateOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDateOut.TextChanged
        If Me.txtDateOut.MaskFull = False Then
            If Me.btnExecute.Enabled = False Then
                Me.DataVWout.RowFilter = String.Empty
                Me.DataVWout.Sort = "Date ASC"

                If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + "Branch LIKE '%" & Me.cmbBranch.Text & "%'"
                Exit Sub
            End If
        Else
            If IsDate(Me.txtDateOut.Text) Then
                Me.DataVWout.RowFilter = "Date = '" & Me.txtDateOut.Text & "'"

                If Me.cmbBranch.SelectedIndex > 0 And btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            End If
            Exit Sub
        End If
    End Sub

    Private Sub txtDeliveredTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDeliveredTo.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtRefOut.Focus()
    End Sub

    Private Sub txtDeliveredTo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDeliveredTo.TextChanged
        If Me.txtDeliveredTo.Text = "" And Me.txtRefOut.Text = "" And Me.txtNumberOut.Text = "" Then
            Me.DataVWout.RowFilter = String.Empty
            Me.DataVWout.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWout.RowFilter = "Delivered_To LIKE '%" & Me.txtDeliveredTo.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefOut.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberOut.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtItemID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtItemID.TextChanged
        If Me.txtItemID.Text = "" Then Exit Sub
        btnExecute.Enabled = True
    End Sub

    Private Sub txtNumberIn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumberIn.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtDateIn.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumberIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumberIn.TextChanged
        If Me.txtSource.Text = "" And Me.txtRefIn.Text = "" And Me.txtNumberIn.Text = "" Then
            Me.DataVWin.RowFilter = String.Empty
            Me.DataVWin.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWin.RowFilter = "Source LIKE '%" & Me.txtSource.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefIn.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberIn.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtNumberOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumberOut.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtDateOut.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumberOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumberOut.TextChanged
        If Me.txtDeliveredTo.Text = "" And Me.txtRefOut.Text = "" And Me.txtNumberOut.Text = "" Then
            Me.DataVWout.RowFilter = String.Empty
            Me.DataVWout.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWout.RowFilter = "Delivered_To LIKE '%" & Me.txtDeliveredTo.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefOut.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberOut.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtQtyIn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQtyIn.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtSerialIn.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtQtyIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyIn.TextChanged
        If Me.txtQtyIn.Text = "" Then
            Me.DataVWin.RowFilter = String.Empty
            Me.DataVWin.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWin.RowFilter = "Quantity = '" & Val(Me.txtQtyIn.Text) & "'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtQtyOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQtyOut.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtSerialOut.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtQtyOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQtyOut.TextChanged
        If Me.txtQtyOut.Text = "" Then
            Me.DataVWout.RowFilter = String.Empty
            Me.DataVWout.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWout.RowFilter = "Quantity = '" & Val(Me.txtQtyOut.Text) & "'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtRefIn_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRefIn.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtNumberIn.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtRefIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefIn.TextChanged
        If Me.txtSource.Text = "" And Me.txtRefIn.Text = "" And Me.txtNumberIn.Text = "" Then
            Me.DataVWin.RowFilter = String.Empty
            Me.DataVWin.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWin.RowFilter = "Source LIKE '%" & Me.txtSource.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefIn.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberIn.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtRefOut_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRefOut.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtNumberOut.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtRefOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefOut.TextChanged
        If Me.txtDeliveredTo.Text = "" And Me.txtRefOut.Text = "" And Me.txtNumberOut.Text = "" Then
            Me.DataVWout.RowFilter = String.Empty
            Me.DataVWout.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWout.RowFilter = "Delivered_To LIKE '%" & Me.txtDeliveredTo.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefOut.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberOut.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtSerialIn_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerialIn.TextChanged
        If Me.txtSerialIn.Text = "" Then
            Me.DataVWin.RowFilter = String.Empty
            Me.DataVWin.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWin.RowFilter = "Serial_Number LIKE '%" & Me.txtSerialIn.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtSerialOut_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerialOut.TextChanged
        If Me.txtSerialOut.Text = "" Then
            Me.DataVWout.RowFilter = String.Empty
            Me.DataVWout.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWout.RowFilter = "Serial_Number LIKE '%" & Me.txtSerialOut.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWout.RowFilter = Me.DataVWout.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub txtSource_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSource.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtRefIn.Focus()
    End Sub

    Private Sub txtSource_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSource.TextChanged
        If Me.txtSource.Text = "" And Me.txtRefIn.Text = "" And Me.txtNumberIn.Text = "" Then
            Me.DataVWin.RowFilter = String.Empty
            Me.DataVWin.Sort = "Date ASC"

            If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " Branch LIKE '%" & Me.cmbBranch.Text & "%'"
            Exit Sub
        End If

        Me.DataVWin.RowFilter = "Source LIKE '%" & Me.txtSource.Text & "%' " & _
        "AND Reference LIKE '%" & Me.txtRefIn.Text & "%' " & _
        "AND Number LIKE '%" & Me.txtNumberIn.Text & "%'"

        If Me.cmbBranch.SelectedIndex > 0 And Me.btnFilter.Enabled = False Then Me.DataVWin.RowFilter = Me.DataVWin.RowFilter + " AND Branch LIKE '%" & Me.cmbBranch.Text & "%'"
    End Sub

    Private Sub UnHideThisColumn()
        Me.dgvIn.Columns("RootID").Visible = True
        Me.dgvIn.Columns("RunID").Visible = True
    End Sub
End Class
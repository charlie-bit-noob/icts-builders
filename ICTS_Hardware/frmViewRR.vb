Imports System.Data
Imports System.Data.SqlClient

Public Class frmViewRR

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_run_id As DataColumn = New DataColumn("run_id")
        col_run_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_run_id)

        Dim col_root_id As DataColumn = New DataColumn("root_id")
        col_root_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_root_id)

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_item_id As DataColumn = New DataColumn("item_id")
        col_item_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_item_id)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("desc")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_unit)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        Dim col_user As DataColumn = New DataColumn("user")
        col_user.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_user)

        Dim col_datepost As DataColumn = New DataColumn("datepost")
        col_datepost.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_datepost)

        TableTemp.DefaultView.Sort = "pop_id ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnMakeChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeChanges.Click
        My.Forms.frmAFChanges.strNumber = Me.txtNumber.Text

        My.Forms.frmAFChanges.frmList = My.Forms.frmlistRR
        My.Forms.frmAFChanges.frmView = Me
        My.Forms.frmAFChanges.ShowDialog()
    End Sub

    Private Sub cbVATXview_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbVATXview.CheckStateChanged
        Try
            If Me.dgvView.RowCount < 1 Then Exit Sub
            If Me.cbVATXview.CheckState = CheckState.Checked Then
                VATXview(Me.dgvView, Me.txtTotal)
            Else
                Me.VATview()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvHistory_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellDoubleClick
        ViewThis(Me, Me.dgvHistory)
    End Sub

    Private Sub dgvView_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvView.CellMouseClick
        If Me.dgvView.RowCount < 1 Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Right Then Me.dgvView.CurrentCell = Me.dgvView.Rows(e.RowIndex).Cells(e.ColumnIndex)
        GenHistory(Me.txtNumber.Text, Me.dgvView, Me.dgvHistory, Me.lblstockBal)

        If MDI.stbAccessLevel.Text <> "Guest" Then
            If Me.dgvHistory.RowCount > 1 Then
                Me.mnuChangeItem.Enabled = False
            Else
                Me.mnuChangeItem.Enabled = True
            End If
        Else
            Me.mnuChangeItem.Enabled = False
            Me.mnuUpdateCost.Enabled = False
        End If
    End Sub

    Private Sub dgvView_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvView.MouseUp
        Dim indexPos As Integer = Me.dgvView.CurrentCell.RowIndex
        Dim itemID As String = Me.dgvView.Item("item_id", indexPos).Value.ToString

        If e.Button = Windows.Forms.MouseButtons.Right And Not itemID = "" Then Me.cmRR.Show(Me.dgvView, e.Location, LeftRightAlignment.Right)
    End Sub

    Private Sub FillAFdata()
        Try
            TempTable = MakeDataTableTemp()

            Me.sqlStr = "SELECT run_id, branch_id, af_id, date_issued, (SELECT name FROM Suppliers WHERE supplier_id = af.supplier_id) AS af_supplier, " & _
            "(SELECT " & GenShortAF & " FROM Transactions WHERE trans_id = af.trans_id) AS af_trans, " & _
            "(SELECT remarks FROM AFsRemarks WHERE run_id = af.run_id AND branch_id = af.branch_id AND af_id = af.af_id AND af_num = '" & Me.txtNumber.Text & "') AS af_remarks, " & _
            "(SELECT total FROM PurchasesAmounts WHERE run_id = af.run_id AND date_issued = af.date_issued) AS af_total, " & _
            "(SELECT freight FROM PurchasesAmounts WHERE run_id = af.run_id AND date_issued = af.date_issued) AS af_freight, " & _
            "(SELECT arrastre FROM PurchasesAmounts WHERE run_id = af.run_id AND date_issued = af.date_issued) AS af_arrastre " & _
            "FROM AFsIn AS af WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' AND branch_id = " & GenBranchID & ""

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()

            Me.txtDate.Text = Format(Me.sqlReader("date_issued"), "MMMM dd, yyyy")
            Me.txtSource.Text = Me.sqlReader("af_supplier").ToString
            Me.txtTransaction.Text = Me.sqlReader("af_trans")
            Me.txtRemarks.Text = Me.sqlReader("af_remarks")

            Me.txtTotal.Text = CDec(IIf(Decimal.TryParse(Me.sqlReader("af_total").ToString, Nothing), Me.sqlReader("af_total"), "0.000")).ToString("N")
            Me.txtFreight.Text = CDec(IIf(Decimal.TryParse(Me.sqlReader("af_freight").ToString, Nothing), Me.sqlReader("af_freight"), "0.000")).ToString("N")
            Me.txtArrastre.Text = CDec(IIf(Decimal.TryParse(Me.sqlReader("af_arrastre").ToString, Nothing), Me.sqlReader("af_arrastre"), "0.000")).ToString("N")

            Me.sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            Me.dgvView.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvView()
        Try
            TempTable = MakeDataTableTemp()

            sqlStr = "SELECT run_id, root_id, order_id, date_issued, af_id, af_num, item_id, " & _
            "(SELECT brand FROM ItemMasterlist AS im WHERE im.item_id = af.item_id) AS af_brand, " & _
            "(SELECT product FROM ItemMasterlist AS im1 WHERE im1.item_id = af.item_id) AS af_product, " & _
            "(SELECT model FROM ItemMasterlist AS im2 WHERE im2.item_id = af.item_id) AS af_model, " & _
            "(SELECT description FROM ItemMasterlist AS im3 WHERE im3.item_id = af.item_id) AS af_desc, class, " & _
            "is_free, (SELECT serial_num FROM TypeSerial WHERE item_id = af.item_id AND af_id = af.af_id " & _
            "AND af_num = af.af_num AND date_issued = af.date_issued AND order_id = af.order_id) AS af_serial, qty, " & _
            "(SELECT units FROM Library WHERE lib_id = (SELECT unit_lib_id FROM ItemMasterlist WHERE item_id = af.item_id)) AS af_unit, " & _
            "cost, ua_id, (SELECT username FROM UserAccounts WHERE ua_id = af.ua_id) AS af_user, date_updated FROM AFsIn AS af " & _
            "WHERE branch_id = " & GenBranchID & " AND af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "'"

            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("run_id") = sqlReader("run_id")
                Row("root_id") = sqlReader("root_id")
                Row("pop_id") = sqlReader("order_id")
                Row("item_id") = sqlReader("item_id")
                Row("brand") = sqlReader("af_brand")
                Row("product") = sqlReader("af_product")
                Row("model") = sqlReader("af_model")
                Row("desc") = sqlReader("af_desc")
                Row("class") = sqlReader("class")
                Row("free") = sqlReader("is_free")
                Row("serial") = sqlReader("af_serial")
                Row("qty") = sqlReader("qty")
                Row("unit") = sqlReader("af_unit")
                Row("cost") = sqlReader("cost")
                Row("user") = sqlReader("af_user")
                Row("datepost") = sqlReader("date_updated")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            dgvView.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmViewRR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.LoadDataAndRefresh()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub LoadDataAndRefresh()
        GetAFID(Me.Text.ToUpper)
        GetBranchID(Me.txtBranch.Text)
        GetShortAF(Me.Text.ToUpper)

        FillAFdata()
        FilldgvView()
        dgvViewConfig(Me.dgvView)
    End Sub

    Private Sub mnuChangeItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuChangeItem.Click
        With My.Forms.frmAdjustmentChangeItem
            .MainSourceData(Me.txtNumber.Text, Me.txtDate.Text, Me.dgvView)
            .ShowDialog()
        End With
    End Sub

    Private Sub mnuUpdateCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUpdateCost.Click
        With My.Forms.frmAdjustmentCost
            .MainSourceData(Me.txtNumber.Text, Me.txtDate.Text, Me.dgvView)
            .ShowDialog()
        End With
    End Sub

    Private Sub VATview()
        Try
            Me.FilldgvView()
            ComputeCost(Me.dgvView, Me.txtTotal)
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmNewCustomer

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmNewCustomer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.cmbBranch.Focus()
    End Sub

    Private Sub frmNewCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbCustType.Focus()
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        If Me.cmbBranch.SelectedIndex = 0 Then
            Me.cmbCustType.SelectedIndex = 0
            Me.cmbCustType.Enabled = False
        Else
            Me.cmbCustType.Enabled = True
        End If
    End Sub

    Private Sub cmbCustType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustType.SelectedValueChanged
        Dim SelIndex As Integer = Me.cmbCustType.SelectedIndex

        Select Case SelIndex
            Case 0
                Me.gbIndiv.Enabled = False
                Me.gbNonIndiv.Enabled = False
                Me.pnlAddress.Enabled = False
            Case 1
                Me.gbIndiv.Enabled = True
                Me.gbNonIndiv.Enabled = False
                Me.pnlAddress.Enabled = True
                Me.txtFirstName.Focus()
            Case Is > 1
                Me.gbNonIndiv.Enabled = True
                Me.gbIndiv.Enabled = False
                Me.pnlAddress.Enabled = True
                Me.txtComName.Focus()
        End Select
    End Sub

    Private Sub gbIndiv_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbIndiv.EnabledChanged
        Dim ctrl As Control

        For Each ctrl In Me.gbIndiv.Controls
            If TypeOf ctrl Is TextBox Then ctrl.Text = ""
        Next
    End Sub

    Private Sub gbNonIndiv_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbNonIndiv.EnabledChanged
        Dim ctrl As Control

        For Each ctrl In Me.gbNonIndiv.Controls
            If TypeOf ctrl Is TextBox Then ctrl.Text = ""
        Next
    End Sub

    Private Sub pnlAddress_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pnlAddress.EnabledChanged
        If Me.pnlAddress.Enabled = True Then
            Me.gbContacts.Enabled = True
        Else
            Me.txtAddress.Clear()
            Me.gbContacts.Enabled = False
        End If
    End Sub

    Private Sub gbContacts_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles gbContacts.EnabledChanged
        Dim ctrl As Control

        For Each ctrl In Me.gbContacts.Controls
            If TypeOf ctrl Is TextBox Then ctrl.Text = ""
        Next

        Me.mtxtTelephone.Text = "000-0000"
        Me.mtxtMobile.Text = "00000000000"
    End Sub

    Private Sub txtFirstName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstName.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtLastName.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtLastName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLastName.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtAddress.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtComName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtComName.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtAddress.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtAddress_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAddress.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtTelephone.Focus()
    End Sub

    Private Sub mtxtTelephone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtTelephone.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtMobile.Focus()
    End Sub

    Private Sub mtxtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtMobile.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtEmail.Focus()
    End Sub

    Private Sub txtEmail_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmail.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnSave.PerformClick()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If HasInvalidInputs() = True Then Exit Sub
            CallTrimmer()

            Dim TypeIndex As Integer = Me.cmbCustType.SelectedIndex

            Select Case TypeIndex
                Case Is = 1
                    If HasAlreadyExistsIndiv(Me.txtFirstName.Text, Me.txtLastName.Text) = True Then Exit Sub
                Case Is > 1
                    If HasAlreadyExistsNonIndiv(Me.txtComName.Text) = True Then Exit Sub
            End Select

            Me.AutoGenCustID()

            sqlStr = "INSERT INTO Customers(cust_id, branch_id, cust_type_lib_id, first_name, last_name, " & _
            "company_name, address, telephone_num, mobile_num, email_add, ua_id, date_added)" & _
            "VALUES(@cust_id, @branch_id, @cust_type_lib_id, @first_name, @last_name, " & _
            "@company_name, @address, @telephone_num, @mobile_num, @email_add, @ua_id, getdate())"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr
            With sqlCmd.Parameters
                .Add(New SqlParameter("@cust_id", SqlDbType.NVarChar)).Value = Me.lblCustID.Text
                .Add(New SqlParameter("@branch_id", SqlDbType.Int)).Value = Me.cmbBranch.SelectedIndex
                .Add(New SqlParameter("@cust_type_lib_id", SqlDbType.Int)).Value = Me.cmbCustType.SelectedIndex
                .Add(New SqlParameter("@first_name", SqlDbType.NVarChar)).Value = Me.txtFirstName.Text
                .Add(New SqlParameter("@last_name", SqlDbType.NVarChar)).Value = Me.txtLastName.Text
                .Add(New SqlParameter("@company_name", SqlDbType.NVarChar)).Value = Me.txtComName.Text
                .Add(New SqlParameter("@address", SqlDbType.NVarChar)).Value = Me.txtAddress.Text
                .Add(New SqlParameter("@telephone_num", SqlDbType.NVarChar)).Value = Me.mtxtTelephone.Text
                .Add(New SqlParameter("@mobile_num", SqlDbType.NVarChar)).Value = Me.mtxtMobile.Text
                .Add(New SqlParameter("@email_add", SqlDbType.NVarChar)).Value = Me.txtEmail.Text
                .Add(New SqlParameter("@ua_id", SqlDbType.Int)).Value = Val(MDI.stbUserID.Text)
            End With
            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            Me.cmbCustType.SelectedIndex = 0
            Me.cmbBranch.SelectedIndex = 0

            MessageBox.Show("New customer saved!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            AutoGenCustID()
            Me.btnClose.Focus()

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CleaningUpObjects()
        Me.Close()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FillBranch()
        Me.FillCustType()
        Me.cmbCustType.SelectedIndex = 0
        Me.cmbBranch.SelectedIndex = 0
    End Sub

    Private Sub FillBranch()
        Try
            sqlStr = "SELECT long_name FROM Branches ORDER BY branch_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillCustType()
        Try
            sqlStr = "SELECT cust_types FROM Library WHERE cust_types IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbCustType.Items.Clear()
            Me.cmbCustType.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbCustType.Items.Add(sqlReader("cust_types"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbCustType.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub AutoGenCustID()
        Try
            Dim prefix As String = "HW-"
            Dim LastID, NewID As String


            sqlStr = "SELECT TOP 1 cust_id FROM Customers ORDER BY cust_id DESC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader
            If sqlReader.HasRows Then
                sqlReader.Close()

                LastID = sqlCmd.ExecuteScalar
                LastID = LastID.Replace(prefix, "")

                NewID = prefix + Format(Val(LastID) + 1, "0000000")
                Me.lblCustID.Text = NewID

                sqlDBconn.Close()
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Me.lblCustID.Text = prefix + "0000001"
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Function HasInvalidInputs() As Boolean
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is ComboBox Then
                If HasInvalidIndex(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
        If Me.gbIndiv.Enabled = True Then
            For Each ctrl In Me.gbIndiv.Controls
                If TypeOf ctrl Is TextBox Then
                    If HasNullValue(ctrl) = True Then
                        Return True
                        Exit Function
                    End If
                End If
            Next
        End If
        If Me.gbNonIndiv.Enabled = True Then
            For Each ctrl In Me.gbNonIndiv.Controls
                If TypeOf ctrl Is TextBox Then
                    If HasNullValue(ctrl) = True Then
                        Return True
                        Exit Function
                    End If
                End If
            Next
        End If
        For Each ctrl In Me.pnlAddress.Controls
            If TypeOf ctrl Is TextBox Then
                If HasNullValue(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
        For Each ctrl In Me.gbContacts.Controls
            If TypeOf ctrl Is TextBox Then
                If HasNullValue(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Function HasAlreadyExistsIndiv(ByVal strFirstName As String, ByVal strLastName As String) As Boolean
        Try
            sqlStr = "SELECT * FROM Customers WHERE first_name = '" & strFirstName & "' " & _
            "AND last_name = '" & strLastName & "'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Name already exists!", "Duplicate Found!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function HasAlreadyExistsNonIndiv(ByVal strCompanyName As String) As Boolean
        Try
            sqlStr = "SELECT * FROM Customers WHERE company_name = '" & strCompanyName & "'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Name already exists!", "Duplicate Found!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Sub CleaningUpObjects()
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
    End Sub
End Class
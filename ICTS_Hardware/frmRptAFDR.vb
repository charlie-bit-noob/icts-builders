Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptAFDR

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmRptAFDR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.MdiParent = MDI
            Me.GetAF()
            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            Me.RptViewer.ZoomMode = ZoomMode.Percent
            Me.RptViewer.ZoomPercent = 100
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetAF()
        Try
            Dim valVAT As Decimal

            With My.Forms.frmlistDR

                If .cbVATX.CheckState = CheckState.Checked Then
                    Me.sqlStr = "SELECT TOP 1 vat FROM Library WHERE vat IS NOT NULL"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    If Me.sqlReader.Read() Then
                        valVAT = Me.sqlReader(0)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        MessageBox.Show("Value not set for VAT items.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()

                        Dim strInputB As String = InputBox("Enter the value of VAT", _
                        "Please contact your system administator A.S.A.P.", _
                        "1.12")
                        Convert.ToDecimal(strInputB)

                        If IsNumeric(strInputB) Then
                            valVAT = strInputB
                        Else
                            MessageBox.Show("The value entered is invalid. The " & Me.Text & " will now close.", _
                            "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Me.Close()
                        End If
                    End If
                End If

                Me.dsExtractAF.Tables("tblDR").Clear()

                Me.sqlStr = "SELECT date_issued, (SELECT short_name FROM AFs WHERE af_id = af.af_id) AS saf, af_num, " & _
                "cust_id, af_cust = CASE " & _
                "WHEN cust_id <> '' THEN (SELECT first_name + ' ' + last_name + company_name AS flc FROM Customers WHERE cust_id = af.cust_id) " & _
                "ELSE non_cust " & _
                "End, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = af.item_id) AS Particular, " & _
                "(SELECT types FROM Library l, ItemMasterlist im WHERE im.item_id = af.item_id AND l.lib_id = im.type_lib_id) AS ItemType, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS Serial, " & _
                "COALESCE(qty,0) AS qty, COALESCE(cost,0.00) AS cost, " & _
                "(SELECT remarks FROM AFsRemarks WHERE run_id = af.run_id AND branch_id = af.branch_id AND af_id = af.af_id AND af_num = af.af_num) AS Remarks " & _
                "FROM AFsOut af WHERE " & .txtSQLpara.Text & " ORDER BY Particular "

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsExtractAF.Tables("tblDR").NewRow

                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("DeliveredTo") = Me.sqlReader("af_cust")
                    tblRow("Reference") = Me.sqlReader("saf") + " " + TrimAFNumber(Me.sqlReader("af_num").ToString)
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("ItemType") = Me.sqlReader("ItemType")
                    tblRow("Serial") = Me.sqlReader("Serial")
                    tblRow("Qty") = Me.sqlReader("qty")
                    tblRow("Cost") = Me.sqlReader("Cost")
                    tblRow("TotalCost") = tblRow("Cost") * tblRow("Qty")

                    If .cbVATX.CheckState = CheckState.Checked Then
                        If Not Me.sqlReader("ItemType").ToString.Contains("WITHOUT") Then tblRow("TotalCost") = tblRow("TotalCost") / valVAT
                    End If

                    If .txtSpecific.Text = "" Then
                        If .mtxtFrom.MaskFull = True Then
                            tblRow("Period") = "Period: " + Format(Convert.ToDateTime(.mtxtFrom.Text), "MMM. dd, yyyy") + " - " + Format(Convert.ToDateTime(.mtxtTo.Text), "MMM. dd, yyyy")
                        Else
                            tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                        End If
                        If .mtxtFrom.Text = .mtxtTo.Text Then _
                        tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                    End If

                    tblRow("ReportHeader") = .txtHeader.Text
                    tblRow("ReportFooter") = .txtFooter.Text
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsExtractAF.Tables("tblDR").Rows.Add(tblRow)
                End While
            End With

            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
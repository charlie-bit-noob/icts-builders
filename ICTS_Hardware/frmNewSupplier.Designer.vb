<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewSupplier
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelTelNo = New System.Windows.Forms.Label
        Me.LabelMobileNo = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.LabelEmail = New System.Windows.Forms.Label
        Me.txtWebsite = New System.Windows.Forms.TextBox
        Me.LabelWebsite = New System.Windows.Forms.Label
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.mtxtMobile = New System.Windows.Forms.MaskedTextBox
        Me.mtxtTelephone = New System.Windows.Forms.MaskedTextBox
        Me.SuspendLayout()
        '
        'LabelTelNo
        '
        Me.LabelTelNo.AutoSize = True
        Me.LabelTelNo.Location = New System.Drawing.Point(10, 105)
        Me.LabelTelNo.Name = "LabelTelNo"
        Me.LabelTelNo.Size = New System.Drawing.Size(45, 13)
        Me.LabelTelNo.TabIndex = 6
        Me.LabelTelNo.Text = "&Tel. No."
        '
        'LabelMobileNo
        '
        Me.LabelMobileNo.AutoSize = True
        Me.LabelMobileNo.Location = New System.Drawing.Point(343, 105)
        Me.LabelMobileNo.Name = "LabelMobileNo"
        Me.LabelMobileNo.Size = New System.Drawing.Size(57, 13)
        Me.LabelMobileNo.TabIndex = 10
        Me.LabelMobileNo.Text = "&Mobile No."
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(81, 75)
        Me.txtEmail.MaxLength = 30
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(221, 21)
        Me.txtEmail.TabIndex = 5
        Me.txtEmail.Text = "N/A"
        '
        'LabelEmail
        '
        Me.LabelEmail.AutoSize = True
        Me.LabelEmail.Location = New System.Drawing.Point(10, 78)
        Me.LabelEmail.Name = "LabelEmail"
        Me.LabelEmail.Size = New System.Drawing.Size(39, 13)
        Me.LabelEmail.TabIndex = 4
        Me.LabelEmail.Text = "&E-mail:"
        '
        'txtWebsite
        '
        Me.txtWebsite.Location = New System.Drawing.Point(406, 75)
        Me.txtWebsite.MaxLength = 30
        Me.txtWebsite.Name = "txtWebsite"
        Me.txtWebsite.Size = New System.Drawing.Size(221, 21)
        Me.txtWebsite.TabIndex = 9
        Me.txtWebsite.Text = "N/A"
        '
        'LabelWebsite
        '
        Me.LabelWebsite.AutoSize = True
        Me.LabelWebsite.Location = New System.Drawing.Point(343, 78)
        Me.LabelWebsite.Name = "LabelWebsite"
        Me.LabelWebsite.Size = New System.Drawing.Size(50, 13)
        Me.LabelWebsite.TabIndex = 8
        Me.LabelWebsite.Text = "&Website:"
        '
        'txtAddress
        '
        Me.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAddress.Location = New System.Drawing.Point(81, 48)
        Me.txtAddress.MaxLength = 100
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddress.Size = New System.Drawing.Size(548, 21)
        Me.txtAddress.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "A&ddress:"
        '
        'txtName
        '
        Me.txtName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtName.Location = New System.Drawing.Point(81, 21)
        Me.txtName.MaxLength = 100
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(548, 21)
        Me.txtName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Name:"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(521, 163)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(106, 23)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(406, 163)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(106, 23)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-3, 143)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(651, 2)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'mtxtMobile
        '
        Me.mtxtMobile.Location = New System.Drawing.Point(406, 102)
        Me.mtxtMobile.Mask = "####-###-##-##"
        Me.mtxtMobile.Name = "mtxtMobile"
        Me.mtxtMobile.Size = New System.Drawing.Size(221, 21)
        Me.mtxtMobile.TabIndex = 11
        Me.mtxtMobile.Text = "00000000000"
        '
        'mtxtTelephone
        '
        Me.mtxtTelephone.Location = New System.Drawing.Point(81, 102)
        Me.mtxtTelephone.Mask = "000-0000"
        Me.mtxtTelephone.Name = "mtxtTelephone"
        Me.mtxtTelephone.Size = New System.Drawing.Size(221, 21)
        Me.mtxtTelephone.TabIndex = 7
        Me.mtxtTelephone.Text = "0000000"
        '
        'frmNewSupplier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(643, 199)
        Me.Controls.Add(Me.mtxtTelephone)
        Me.Controls.Add(Me.mtxtMobile)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.LabelTelNo)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.LabelMobileNo)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.LabelEmail)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtWebsite)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LabelWebsite)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmNewSupplier"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Supplier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtWebsite As System.Windows.Forms.TextBox
    Friend WithEvents LabelWebsite As System.Windows.Forms.Label
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LabelTelNo As System.Windows.Forms.Label
    Friend WithEvents LabelMobileNo As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents LabelEmail As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents mtxtMobile As System.Windows.Forms.MaskedTextBox
    Friend WithEvents mtxtTelephone As System.Windows.Forms.MaskedTextBox
End Class

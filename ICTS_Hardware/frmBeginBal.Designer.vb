<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBeginBal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.gbMustFilled = New System.Windows.Forms.GroupBox
        Me.dtDate = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnApply = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblPanel = New System.Windows.Forms.Label
        Me.ProgressStatus = New System.Windows.Forms.ProgressBar
        Me.lblPrgressStatus = New System.Windows.Forms.Label
        Me.pnlProgress = New System.Windows.Forms.Panel
        Me.gbMustFilled.SuspendLayout()
        Me.pnlProgress.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbMustFilled
        '
        Me.gbMustFilled.Controls.Add(Me.dtDate)
        Me.gbMustFilled.Controls.Add(Me.Label8)
        Me.gbMustFilled.Controls.Add(Me.cmbBranch)
        Me.gbMustFilled.Controls.Add(Me.Label3)
        Me.gbMustFilled.Location = New System.Drawing.Point(12, 12)
        Me.gbMustFilled.Name = "gbMustFilled"
        Me.gbMustFilled.Size = New System.Drawing.Size(350, 88)
        Me.gbMustFilled.TabIndex = 92
        Me.gbMustFilled.TabStop = False
        Me.gbMustFilled.Text = "Must be filled up"
        '
        'dtDate
        '
        Me.dtDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDate.Location = New System.Drawing.Point(112, 26)
        Me.dtDate.Name = "dtDate"
        Me.dtDate.Size = New System.Drawing.Size(226, 21)
        Me.dtDate.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Location = New System.Drawing.Point(15, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(85, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "&Inventory Date:"
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(112, 53)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(226, 21)
        Me.cmbBranch.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Location = New System.Drawing.Point(15, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "&Branch:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 120)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 102
        Me.Label4.Text = "Remarks:"
        '
        'txtRemarks
        '
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Location = New System.Drawing.Point(124, 117)
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ReadOnly = True
        Me.txtRemarks.Size = New System.Drawing.Size(226, 21)
        Me.txtRemarks.TabIndex = 101
        Me.txtRemarks.Text = "BEGINNING BALANCE"
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(208, 187)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(154, 23)
        Me.btnCancel.TabIndex = 97
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnApply
        '
        Me.btnApply.Location = New System.Drawing.Point(30, 187)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(172, 23)
        Me.btnApply.TabIndex = 96
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(-9, 164)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(398, 2)
        Me.GroupBox2.TabIndex = 98
        Me.GroupBox2.TabStop = False
        '
        'lblPanel
        '
        Me.lblPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPanel.Location = New System.Drawing.Point(-1, 4)
        Me.lblPanel.Name = "lblPanel"
        Me.lblPanel.Size = New System.Drawing.Size(390, 29)
        Me.lblPanel.TabIndex = 103
        '
        'ProgressStatus
        '
        Me.ProgressStatus.Location = New System.Drawing.Point(12, 8)
        Me.ProgressStatus.Name = "ProgressStatus"
        Me.ProgressStatus.Size = New System.Drawing.Size(260, 15)
        Me.ProgressStatus.TabIndex = 104
        '
        'lblPrgressStatus
        '
        Me.lblPrgressStatus.Location = New System.Drawing.Point(278, 10)
        Me.lblPrgressStatus.Name = "lblPrgressStatus"
        Me.lblPrgressStatus.Size = New System.Drawing.Size(101, 13)
        Me.lblPrgressStatus.TabIndex = 105
        '
        'pnlProgress
        '
        Me.pnlProgress.Controls.Add(Me.lblPrgressStatus)
        Me.pnlProgress.Controls.Add(Me.ProgressStatus)
        Me.pnlProgress.Controls.Add(Me.lblPanel)
        Me.pnlProgress.Location = New System.Drawing.Point(-7, 227)
        Me.pnlProgress.Name = "pnlProgress"
        Me.pnlProgress.Size = New System.Drawing.Size(387, 34)
        Me.pnlProgress.TabIndex = 106
        '
        'frmBeginBal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(374, 253)
        Me.ControlBox = False
        Me.Controls.Add(Me.pnlProgress)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.txtRemarks)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.gbMustFilled)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmBeginBal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Beginning Balance Editor"
        Me.gbMustFilled.ResumeLayout(False)
        Me.gbMustFilled.PerformLayout()
        Me.pnlProgress.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbMustFilled As System.Windows.Forms.GroupBox
    Friend WithEvents dtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPanel As System.Windows.Forms.Label
    Friend WithEvents ProgressStatus As System.Windows.Forms.ProgressBar
    Friend WithEvents lblPrgressStatus As System.Windows.Forms.Label
    Friend WithEvents pnlProgress As System.Windows.Forms.Panel
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCorrectingTilesStock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button_Reencode = New System.Windows.Forms.Button
        Me.Button_Balancing = New System.Windows.Forms.Button
        Me.Label_rows = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Button_Reencode
        '
        Me.Button_Reencode.Location = New System.Drawing.Point(12, 12)
        Me.Button_Reencode.Name = "Button_Reencode"
        Me.Button_Reencode.Size = New System.Drawing.Size(277, 23)
        Me.Button_Reencode.TabIndex = 0
        Me.Button_Reencode.Text = "Reencode items on Stock Fragile"
        Me.Button_Reencode.UseVisualStyleBackColor = True
        '
        'Button_Balancing
        '
        Me.Button_Balancing.Location = New System.Drawing.Point(12, 41)
        Me.Button_Balancing.Name = "Button_Balancing"
        Me.Button_Balancing.Size = New System.Drawing.Size(277, 23)
        Me.Button_Balancing.TabIndex = 2
        Me.Button_Balancing.Text = "Stock Balancing"
        Me.Button_Balancing.UseVisualStyleBackColor = True
        '
        'Label_rows
        '
        Me.Label_rows.AutoSize = True
        Me.Label_rows.Location = New System.Drawing.Point(255, 89)
        Me.Label_rows.Name = "Label_rows"
        Me.Label_rows.Size = New System.Drawing.Size(34, 13)
        Me.Label_rows.TabIndex = 3
        Me.Label_rows.Text = "Rows"
        Me.Label_rows.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'frmCorrectingTilesStock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(301, 114)
        Me.Controls.Add(Me.Label_rows)
        Me.Controls.Add(Me.Button_Balancing)
        Me.Controls.Add(Me.Button_Reencode)
        Me.Name = "frmCorrectingTilesStock"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Correcting Tiles Stock"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button_Reencode As System.Windows.Forms.Button
    Friend WithEvents Button_Balancing As System.Windows.Forms.Button
    Friend WithEvents Label_rows As System.Windows.Forms.Label
End Class

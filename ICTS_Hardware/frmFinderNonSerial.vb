Imports System.Data
Imports System.Data.SqlClient

Public Class frmFinderNonSerial

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function GetSrcAFID(ByVal af_short As String) As Integer
        Dim retID As Integer

        sqlStr = "SELECT af_id FROM AFs WHERE short_name = '" & af_short & "'"
        sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
        sqlDBconn.Open()
        sqlReader = sqlCmd.ExecuteReader()

        sqlReader.Read()
        retID = sqlReader(0)

        sqlReader.Close()
        sqlDBconn.Close()

        Return retID
    End Function

    Private Function IsClassInvalidOrder() As Boolean
        Try
            Dim A, B, C, X, Y As Integer

            A = 3
            B = 2
            C = 1

            Select Case Me.lblClass.Text
                Case "A"
                    X = A
                Case "B"
                    X = B
                Case "C"
                    X = C
            End Select

            Dim Row As DataGridViewRow

            For Each Row In Me.dgvNonSerials.Rows
                With Row
                    If .Cells("select").Value = True Then
                        Select Case .Cells("class").Value
                            Case "A"
                                Y = A
                            Case "B"
                                Y = B
                            Case "C"
                                Y = C
                        End Select
                        If X > Y Then
                            MessageBox.Show("The current item's class overlaps the source's class that you have selected. Please select a source that can contain the current Item's class. " & vbNewLine & "" & vbNewLine & " " & _
                            "Example: Current Item = Class B, Selected Source = can be Class A or Class B. Cannot select class " & .Cells("class").Value & " since class " & Me.lblClass.Text & " overlaps " & _
                            "Class " & .Cells("class").Value & " as a source.", "Pag sure oi!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Return True
                            Exit For
                        End If
                    End If
                End With
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_run_id As DataColumn = New DataColumn("run_id")
        col_run_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_run_id)

        Dim col_root_id As DataColumn = New DataColumn("root_id")
        col_root_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_root_id)

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_select As DataColumn = New DataColumn("select")
        col_select.DataType = System.Type.GetType("System.Boolean")
        col_select.DefaultValue = False
        TableTemp.Columns.Add(col_select)

        Dim col_date As DataColumn = New DataColumn("date")
        col_date.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_date)

        Dim col_af As DataColumn = New DataColumn("af")
        col_af.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_af)

        Dim col_number As DataColumn = New DataColumn("number")
        col_number.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_number)

        Dim col_trans As DataColumn = New DataColumn("trans")
        col_trans.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_trans)

        Dim col_branch As DataColumn = New DataColumn("branch")
        col_branch.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_branch)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        TableTemp.DefaultView.Sort = "date ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDone.Click
        Try
            If Me.dgvNonSerials.RowCount < 1 Then Exit Sub

            If Me.lblClass.Text <> "N/A" Then
                If Me.IsClassInvalidOrder = True Then Exit Sub
            End If

            selectedCount = Nothing
            Dim Row As DataGridViewRow

            For Each Row In Me.dgvNonSerials.Rows
                With Row
                    If .Cells("select").Value = True Then
                        selectedCount += 1

                        rootList.Add(.Cells("root_id").Value)
                        runList.Add(.Cells("run_id").Value)
                        afList.Add(Me.GetSrcAFID(.Cells("af").Value))
                        afnumList.Add(.Cells("number").Value)
                        IsFreeList.Add(.Cells("free").Value)
                        classList.Add(.Cells("class").Value)
                        qtyList.Add(.Cells("qty").Value)
                        costList.Add(.Cells("cost").Value)
                    End If
                End With
            Next Row

            MessageBox.Show("" & selectedCount & " source(s) selected!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvNonSerials_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvNonSerials.CellClick
        Try
            Dim index As Integer = Me.dgvNonSerials.CurrentCell.RowIndex
            Dim Bal As Integer = Me.dgvNonSerials.Item("qty", index).Value

            If Me.dgvNonSerials.Item("select", index).Value = False Then
                Me.dgvNonSerials.Item("select", index).Value = True
                Me.lblTotalSelQty.Text = Val(Me.lblTotalSelQty.Text) + Bal
            Else
                Me.dgvNonSerials.Item("select", index).Value = False
                Me.lblTotalSelQty.Text = Val(Me.lblTotalSelQty.Text) - Bal
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvNonSerials()
        Try
            TempTable = MakeDataTableTemp()

            Me.sqlStr = "SELECT root_id, run_id, order_id, item_id, date_issued, af_id, " & _
            "(SELECT short_name FROM AFs WHERE af_id = tns.af_id) AS tns_short, " & _
            "af_num, trans_id, branch_id, " & _
            "(SELECT long_name FROM Branches WHERE branch_id = tns.branch_id) AS tns_branch, " & _
            "is_free, class, cost, qty_bal FROM TypeNonSerial AS tns WHERE branch_id = " & SelectedSrcBranchID & " " & _
            "AND item_id = '" & SelectedItemID & "' AND qty_bal > 0 " & _
            "AND af_id NOT IN (SELECT af_id FROM AFs WHERE af_type = 'Out') " & _
            "AND tns.item_id IN (SELECT item_id FROM ItemMasterlist WHERE item_id = tns.item_id " & _
            "AND non_stock = 'False')"

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("root_id") = sqlReader("root_id")
                Row("run_id") = sqlReader("run_id")
                Row("pop_id") = sqlReader("order_id")
                Row("date") = sqlReader("date_issued")
                Row("af") = sqlReader("tns_short")
                Row("number") = sqlReader("af_num")
                Row("trans") = sqlReader("trans_id")
                Row("branch") = sqlReader("tns_branch")
                Row("free") = sqlReader("is_free")
                Row("class") = sqlReader("class")
                Row("qty") = sqlReader("qty_bal")
                Row("cost") = sqlReader("cost")

                TempTable.Rows.Add(Row)
            End While

            Me.sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "date ASC, pop_id ASC"
            Me.dgvNonSerials.DataSource = TempDataView

            Dim nsrow As DataGridViewRow
            Dim af, trans As String

            For Each nsrow In Me.dgvNonSerials.Rows
                af = nsrow.Cells(5).Value
                trans = nsrow.Cells(7).Value

                sqlStr = "SELECT " & af & " FROM Transactions WHERE trans_id = " & trans & ""
                sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
                sqlDBconn.Open()

                sqlReader = sqlCmd.ExecuteReader()
                sqlReader.Read()
                nsrow.Cells(7).Value = sqlReader(0)

                sqlReader.Close()
                sqlDBconn.Close()
            Next

            Dim Col As DataGridViewColumn
            Dim ColName As String

            For Each Col In Me.dgvNonSerials.Columns
                ColName = Col.Name
                With Col
                    Select Case ColName
                        Case "run_id"
                            .Visible = False
                        Case "root_id"
                            .Visible = False
                        Case "pop_id"
                            .Visible = False
                        Case "select"
                            .HeaderText = "Select"
                            .Width = 50
                        Case "date"
                            .HeaderText = "Date"
                            .Width = 100
                        Case "af"
                            .HeaderText = "Acctbl. Form"
                            .Width = 100
                        Case "number"
                            .HeaderText = "Number"
                            .Width = 100
                        Case "trans"
                            .HeaderText = "Transaction"
                            .Width = 100
                        Case "branch"
                            .HeaderText = "Branch"
                            .Width = 200
                        Case "free"
                            .HeaderText = "Free?"
                            .Width = 50
                        Case "class"
                            .HeaderText = "Class"
                            .Width = 50
                        Case "qty"
                            .HeaderText = "Qty."
                            .Width = 50
                        Case "cost"
                            .HeaderText = "Cost"
                            .Width = 100
                            .DefaultCellStyle.Format = "#,###,###.###"
                    End Select
                End With
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmFinderNonSerial_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CleanUpListOfString()
        Me.FilldgvNonSerials()
        Me.lblTotalSelQty.Text = "0"
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmAuthorize

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmAuthorize_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txtPassword.Clear()
        Me.txtUserName.Clear()
    End Sub

    Private Sub txtUserName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserName.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtPassword.Focus()
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnOK.PerformClick()
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            Me.SQLstr = "SELECT * FROM UserAccounts WHERE username = '" & Me.txtUserName.Text.Trim & "' " & _
            "AND password = '" & Me.txtPassword.Text.Trim & "' AND access_level_lib_id = 3"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read Then
                Me.sqlReader.Close()
                sqlDBconn.Close()

                'My.Forms.MDI.mnuAdjustments.Enabled = True
                Me.Close()
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Account has been denied!", "Failed!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Me.txtUserName.SelectAll()
                Me.txtUserName.Focus()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class
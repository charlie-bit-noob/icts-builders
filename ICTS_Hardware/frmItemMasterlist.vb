Imports System.Data
Imports System.Data.SqlClient

Public Class frmItemMasterlist

    Private DuplicateCounter As Integer = 0

    Private ItemsDataView As DataView
    Private ItemsTable As DataTable
    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String
    Private strItemID As String

    Private Function HasAlreadyExists(ByVal intType As Integer, ByVal strBrand As String, ByVal strProduct As String, ByVal strModel As String, ByVal strDesc As String) As Boolean
        Try
            sqlStr = "SELECT * FROM ItemMasterlist WHERE type_lib_id = " & intType & " " & _
            "AND brand = '" & strBrand.Trim & "' AND product = '" & strProduct.Trim & "' " & _
            "AND model = '" & strModel.Trim & "' AND description = '" & strDesc.Trim & "'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Item already exists!", "Duplicate Found!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function HasInvalidInputs() As Boolean
        Dim ctrl As Control

        If Me.cmbItemType.Enabled = False Then
            Return False
            Exit Function
        End If

        For Each ctrl In Me.pnlInputs.Controls
            If TypeOf ctrl Is ComboBox Then
                If HasInvalidIndex(ctrl) = True And ctrl.Enabled = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Function MakeDataTableItems() As DataTable
        Dim TableItems As DataTable
        TableItems = New DataTable("ItemMasterlist")

        Dim col_item_id As DataColumn = New DataColumn("item_id")
        col_item_id.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_item_id)

        Dim col_type As DataColumn = New DataColumn("type")
        col_type.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_type)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_model)

        Dim col_description As DataColumn = New DataColumn("description")
        col_description.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_description)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableItems.Columns.Add(col_cost)

        Dim col_code As DataColumn = New DataColumn("code")
        col_code.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_code)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_class)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_unit)

        Dim col_nonstock As DataColumn = New DataColumn("non_stock")
        col_nonstock.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_nonstock)

        TableItems.DefaultView.Sort = "brand ASC"

        MakeDataTableItems = TableItems
    End Function

    Private Sub AutoGenItemID()
        Try
            Dim prefix As String = "CNM"
            Dim LastID, NewID As String


            sqlStr = "SELECT TOP 1 item_id FROM ItemMasterlist ORDER BY item_id DESC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader
            If sqlReader.HasRows Then
                sqlReader.Close()

                LastID = sqlCmd.ExecuteScalar
                LastID = LastID.Replace(prefix, "")

                NewID = prefix + Format(Val(LastID) + 1, "00000000")
                Me.lblItemID.Text = NewID

                sqlDBconn.Close()
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                Me.lblItemID.Text = prefix + "00000001"
            End If

        Catch ex As Exception
            sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btn_EDMSG_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_EDMSG.Click
        Select Case Me.dgvItems.MultiSelect
            Case False
                Me.dgvItems.MultiSelect = True
            Case True
                Me.dgvItems.MultiSelect = False
        End Select
    End Sub

    Private Sub btnApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            ItemsDataView.RowFilter = ""

            If HasInvalidInputs() = True Then Exit Sub
            CallTrimmer()
            If HasAlreadyExists(Me.cmbItemType.SelectedIndex, Me.txtBrand.Text, Me.txtProduct.Text, Me.txtModel.Text, Me.txtDescription.Text) = True Then
                ShowThisItem(Me.cmbItemType.Text, Me.txtBrand.Text, Me.txtProduct.Text, Me.txtModel.Text, Me.txtDescription.Text)
                Exit Sub
            End If

            sqlStr = "UPDATE ItemMasterlist SET brand = @brand, product = @product, model = @model, description = @description, " & _
            "unit_lib_id = @unit_lib_id, group_id = @group_id, non_stock = @non_stock, " & _
            "ua_id = @ua_id, date_updated = getdate() " & _
            "WHERE item_id = '" & strItemID & "'"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr
            With sqlCmd.Parameters
                .Add("@brand", SqlDbType.NVarChar).Value = Me.txtBrand.Text.Trim
                .Add("@product", SqlDbType.NVarChar).Value = Me.txtProduct.Text.Trim
                .Add("@model", SqlDbType.NVarChar).Value = Me.txtModel.Text.Trim
                .Add("@description", SqlDbType.NVarChar).Value = Me.txtDescription.Text.Trim
                .Add("@unit_lib_id", SqlDbType.Int).Value = Me.cmbUnit.SelectedIndex
                .Add("@group_id", SqlDbType.Int).Value = Me.cmbGroup.SelectedIndex
                .Add("@non_stock", SqlDbType.Bit).Value = ConvertState(Me.cbNonStock.CheckState)
                .Add("@ua_id", SqlDbType.Int).Value = Val(MDI.stbUserID.Text)
            End With
            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            'Me.HandlerAddOnTextBox()

            ItemsTable.Clear()
            FilldgvItems()

            ShowThisItem(Me.cmbItemType.Text, Me.txtBrand.Text, Me.txtProduct.Text, Me.txtModel.Text, Me.txtDescription.Text)
            ResetControls()

            MessageBox.Show("Changes have been applied!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChange.Click
        Try
            Dim RowIndex As Integer = dgvItems.CurrentCell.RowIndex

            strItemID = dgvItems.Item("item_id", RowIndex).Value.ToString
            ItemsDataView.RowFilter = "item_id = '" & strItemID & "'"

            'Me.HandlerSuspendOnTextBox()

            Me.cmbItemType.Enabled = False

            Me.rbNonTile.Checked = False
            Me.rbTile.Checked = False
            Me.rbNonTile.Enabled = False
            Me.rbTile.Enabled = False

            Me.txtBrand.Focus()
            Me.btnVerify.Enabled = False
            Me.btnApply.Enabled = True
            Me.btnChange.Enabled = False

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CleaningUpObjects()
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.HasInvalidInputs() = True Then Exit Sub
            If Me.rbNonTile.Checked = False And Me.rbTile.Checked = False Then
                MessageBox.Show("Please select either Tile or Non-Tile for this item.", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If
            Me.AutoGenItemID()

            sqlStr = "INSERT INTO ItemMasterlist(item_id, type_lib_id, brand, product, model, " & _
            "description, unit_lib_id, group_id, non_stock, ua_id, date_updated)" & _
            "VALUES(@item_id, @type_lib_id, @brand, @product, @model, " & _
            "@description, @unit_lib_id, @group_id, @non_stock, @ua_id, getdate())"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr
            With sqlCmd.Parameters
                .Add(New SqlParameter("@item_id", SqlDbType.NVarChar)).Value = Me.lblItemID.Text
                .Add(New SqlParameter("@type_lib_id", SqlDbType.Int)).Value = Me.cmbItemType.SelectedIndex
                .Add(New SqlParameter("@brand", SqlDbType.NVarChar)).Value = Me.txtBrand.Text.Trim
                .Add(New SqlParameter("@product", SqlDbType.NVarChar)).Value = Me.txtProduct.Text.Trim
                .Add(New SqlParameter("@model", SqlDbType.NVarChar)).Value = Me.txtModel.Text.Trim
                .Add(New SqlParameter("@description", SqlDbType.NVarChar)).Value = Me.txtDescription.Text.Trim
                .Add(New SqlParameter("@unit_lib_id", SqlDbType.Int)).Value = Me.cmbUnit.SelectedIndex
                .Add(New SqlParameter("@group_id", SqlDbType.Int)).Value = Me.cmbGroup.SelectedIndex
                .Add(New SqlParameter("@non_stock", SqlDbType.Bit)).Value = ConvertState(Me.cbNonStock.CheckState)
                .Add(New SqlParameter("@ua_id", SqlDbType.Int)).Value = Val(MDI.stbUserID.Text)
            End With
            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            If Me.rbNonTile.Checked = True Then
                sqlStr = "INSERT INTO StockDurable(item_id)VALUES(@item_id)"
                sqlCmd = New SqlCommand
                sqlCmd.Connection = sqlDBconn
                sqlCmd.CommandText = sqlStr
                With sqlCmd.Parameters
                    .Add(New SqlParameter("@item_id", SqlDbType.NVarChar)).Value = Me.lblItemID.Text
                End With
                sqlDBconn.Open()
                sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                Dim ClassList(2) As String
                Dim strClass As String

                ClassList(0) = "A"
                ClassList(1) = "B"
                ClassList(2) = "C"

                For Each strClass In ClassList
                    sqlStr = "INSERT INTO StockFragile(item_id, class)VALUES(@item_id, @class)"
                    sqlCmd = New SqlCommand
                    sqlCmd.Connection = sqlDBconn
                    sqlCmd.CommandText = sqlStr
                    With sqlCmd.Parameters
                        .Add(New SqlParameter("@item_id", SqlDbType.NVarChar)).Value = Me.lblItemID.Text
                        .Add(New SqlParameter("@class", SqlDbType.NVarChar)).Value = strClass
                    End With
                    sqlDBconn.Open()
                    sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                Next strClass
            End If

            Me.ItemsTable.Clear()
            Me.FilldgvItems()

            'ShowThisItem(cmbItemType.Text, txtBrand.Text, txtProduct.Text, txtModel.Text)
            Me.ResetControls()

            MessageBox.Show("New item saved!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        ItemsDataView.RowFilter = ""

        If DuplicateCounter > 0 Then
            If MessageBox.Show("Duplicate has been detected. Do you still want to save this item?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Me.btnSave.Enabled = True
                Me.btnSave.Focus()
                Me.btnVerify.Enabled = False
                Exit Sub
            Else
                ResetControls()
                DuplicateCounter = 0
                Exit Sub
            End If
        End If

        If HasInvalidInputs() = True Then Exit Sub
        If Me.rbNonTile.Checked = False And Me.rbTile.Checked = False Then
            MessageBox.Show("Please select either Tile or Non-Tile for this item.", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        CallTrimmer()
        If HasAlreadyExists(Me.cmbItemType.SelectedIndex, Me.txtBrand.Text, Me.txtProduct.Text, Me.txtModel.Text, Me.txtDescription.Text) = True Then
            DuplicateCounter += 1
            ShowThisItem(Me.cmbItemType.Text, Me.txtBrand.Text, Me.txtProduct.Text, Me.txtModel.Text, Me.txtDescription.Text)
            Exit Sub
        Else
            Me.btnSave.Enabled = True
            Me.btnVerify.Enabled = False
        End If
    End Sub

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.pnlInputs.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Sub CleaningUpObjects()
        ItemsDataView = Nothing
        ItemsTable.Clear()
        Row = Nothing
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
        DuplicateCounter = Nothing
        strItemID = Nothing
    End Sub

    Private Sub cmbGroup_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbGroup.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub cmbItemType_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbItemType.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbUnit.Focus()
    End Sub

    Private Sub cmbUnit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbUnit.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbGroup.Focus()
    End Sub

    Private Sub dgvItems_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellDoubleClick
        If Me.dgvItems.RowCount < 1 Then Exit Sub

        Dim ColHeader As String = dgvItems.CurrentCell.OwningColumn.HeaderText
        Dim RowIndex As Integer = dgvItems.CurrentCell.RowIndex

        With dgvItems
            Select Case ColHeader
                Case "Brand"
                    Me.txtBrand.Text = .Item("brand", RowIndex).Value
                    Me.txtBrand.Focus()
                Case "Product"
                    Me.txtProduct.Text = .Item("product", RowIndex).Value
                    Me.txtProduct.Focus()
                Case "Model"
                    Me.txtModel.Text = .Item("model", RowIndex).Value
                    Me.txtModel.Focus()
                Case "Description"
                    Me.txtDescription.Text = .Item("description", RowIndex).Value
                    Me.txtDescription.Focus()
            End Select
        End With
    End Sub

    Private Sub dgvItemsConfig()
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgvItems.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "item_id"
                        .HeaderText = "Item ID"
                        .Width = 100
                    Case "type"
                        .HeaderText = "Type"
                        .Width = 150
                    Case "brand"
                        .HeaderText = "Brand"
                        .Width = 200
                    Case "product"
                        .HeaderText = "Product"
                        .Width = 200
                    Case "model"
                        .HeaderText = "Model"
                        .Width = 200
                    Case "description"
                        .HeaderText = "Description"
                        .Width = 200
                    Case "cost"
                        .HeaderText = "Cost"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "code"
                        .HeaderText = "Group"
                        .Width = 100
                    Case "class"
                        .HeaderText = "Class"
                        .Width = 100
                    Case "unit"
                        .HeaderText = "Unit"
                        .Width = 100
                    Case "non_stock"
                        .HeaderText = "NonStock"
                        .Width = 100
                End Select
            End With
        Next
    End Sub

    Private Sub FilldgvItems()
        Try
            ItemsTable = MakeDataTableItems()

            sqlStr = "SELECT item_id, (SELECT types FROM Library " & _
            "WHERE lib_id = im.type_lib_id) AS imType, brand, product, " & _
            "model, description, cost, (SELECT code FROM ItemGroups WHERE group_id = im.group_id) AS imCode, " & _
            "(SELECT class FROM ItemGroups WHERE group_id = im.group_id) AS imClass, " & _
            "(SELECT units FROM Library WHERE lib_id = im.unit_lib_id) AS imUnit, non_stock " & _
            "FROM ItemMasterlist AS im"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = ItemsTable.NewRow()
                Row("item_id") = sqlReader("item_id")
                Row("type") = sqlReader("imType")
                Row("brand") = sqlReader("brand")
                Row("product") = sqlReader("product")
                Row("model") = sqlReader("model")
                Row("description") = sqlReader("description")
                Row("cost") = sqlReader("cost")
                Row("code") = sqlReader("imCode")
                Row("class") = sqlReader("imClass")
                Row("unit") = sqlReader("imUnit")
                Row("non_stock") = sqlReader("non_stock")

                ItemsTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            ItemsDataView = New DataView
            ItemsDataView.Table = ItemsTable
            ItemsDataView.Sort = "Brand ASC"
            dgvItems.DataSource = ItemsDataView

            dgvItemsConfig()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillGroup()
        Try
            sqlStr = "SELECT product FROM ItemGroups ORDER BY group_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbGroup.Items.Clear()
            Me.cmbGroup.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbGroup.Items.Add(sqlReader("product"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbGroup.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillType()
        Try
            sqlStr = "SELECT Types FROM Library WHERE Types IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbItemType.Items.Clear()
            Me.cmbItemType.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbItemType.Items.Add(sqlReader("Types"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbItemType.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillUnit()
        Try
            sqlStr = "SELECT Units FROM Library WHERE Units IS NOT NULL ORDER BY lib_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbUnit.Items.Clear()
            Me.cmbUnit.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbUnit.Items.Add(sqlReader("Units"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbUnit.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmItemMasterlist_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtBrand.Focus()
    End Sub

    Private Sub frmItemMasterlist_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub HandlerAddOnTextBox()
        AddHandler Me.txtBrand.TextChanged, AddressOf Me.txtBrand_TextChanged
        AddHandler Me.txtProduct.TextChanged, AddressOf Me.txtProduct_TextChanged
        AddHandler Me.txtModel.TextChanged, AddressOf Me.txtModel_TextChanged
        AddHandler Me.txtDescription.TextChanged, AddressOf Me.txtDescription_TextChanged
    End Sub

    Private Sub HandlerSuspendOnTextBox()
        RemoveHandler Me.txtBrand.TextChanged, AddressOf Me.txtBrand_TextChanged
        RemoveHandler Me.txtProduct.TextChanged, AddressOf Me.txtProduct_TextChanged
        RemoveHandler Me.txtModel.TextChanged, AddressOf Me.txtModel_TextChanged
        RemoveHandler Me.txtDescription.TextChanged, AddressOf Me.txtDescription_TextChanged
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FilldgvItems()
        Me.FillType()
        Me.FillUnit()
        Me.FillGroup()
        'Me.HandlerAddOnTextBox()
        Me.ResetControls()
    End Sub

    Private Sub ResetControls()
        Dim ctrl As Control

        For Each ctrl In Me.pnlInputs.Controls
            If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
            If TypeOf ctrl Is ComboBox Then ReIndexComboBox(ctrl)
        Next
        Me.cbNonStock.CheckState = CheckState.Unchecked

        Me.rbNonTile.Enabled = True
        Me.rbTile.Enabled = True
        Me.rbTile.Checked = False
        Me.rbNonTile.Checked = False

        Me.cmbItemType.Enabled = True

        Me.btnVerify.Enabled = True
        Me.btnChange.Enabled = True
        Me.btnApply.Enabled = False
        Me.btnSave.Enabled = False
        Me.btnClose.Focus()
    End Sub

    Private Sub ShowThisItem(ByVal strType As String, ByVal strBrand As String, ByVal strProduct As String, ByVal strModel As String, ByVal strDesc As String)
        Try
            Dim dgvRow As DataGridViewRow
            'Dim dupIndex As Integer

            For Each dgvRow In dgvItems.Rows
                With dgvRow
                    dgvItems.CurrentCell = dgvItems.Rows(dgvRow.Index).Cells("item_id")
                    If .Cells("type").Value.ToString = strType And .Cells("brand").Value.ToString = strBrand And .Cells("product").Value.ToString = strProduct And .Cells("model").Value.ToString = strModel And .Cells("description").Value.ToString = strDesc Then
                        Exit For
                    End If
                End With
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub txtBrand_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBrand.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtProduct.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtBrand_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBrand.TextChanged
        Try
            If Not Me.txtBrand.Text = "" Then
                ItemsDataView.RowFilter = "brand Like '%" & Me.txtBrand.Text & "%'"
            Else
                ItemsDataView.RowFilter = ""
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.cmbItemType.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged
        Try
            If Not Me.txtDescription.Text = "" Then
                ItemsDataView.RowFilter = "description Like '%" & Me.txtDescription.Text & "%'"
            Else
                ItemsDataView.RowFilter = ""
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub txtModel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtModel.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtDescription.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtModel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtModel.TextChanged
        Try
            If Not Me.txtModel.Text = "" Then
                ItemsDataView.RowFilter = "model Like '%" & Me.txtModel.Text & "%'"
            Else
                ItemsDataView.RowFilter = ""
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub txtProduct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProduct.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtModel.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProduct.TextChanged
        Try
            If Not Me.txtProduct.Text = "" Then
                ItemsDataView.RowFilter = "product Like '%" & Me.txtProduct.Text & "%'"
            Else
                ItemsDataView.RowFilter = ""
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub
End Class
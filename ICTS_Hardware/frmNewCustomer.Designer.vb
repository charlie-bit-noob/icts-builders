<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.txtAddress = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.gbIndiv = New System.Windows.Forms.GroupBox
        Me.gbNonIndiv = New System.Windows.Forms.GroupBox
        Me.txtComName = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.mtxtMobile = New System.Windows.Forms.MaskedTextBox
        Me.mtxtTelephone = New System.Windows.Forms.MaskedTextBox
        Me.lblCustID = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cmbCustType = New System.Windows.Forms.ComboBox
        Me.gbContacts = New System.Windows.Forms.GroupBox
        Me.pnlAddress = New System.Windows.Forms.Panel
        Me.gbIndiv.SuspendLayout()
        Me.gbNonIndiv.SuspendLayout()
        Me.gbContacts.SuspendLayout()
        Me.pnlAddress.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-10, 318)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(630, 2)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(405, 335)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(94, 23)
        Me.btnSave.TabIndex = 11
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(505, 335)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(97, 23)
        Me.btnClose.TabIndex = 12
        Me.btnClose.Text = "&Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtAddress
        '
        Me.txtAddress.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAddress.Location = New System.Drawing.Point(76, 8)
        Me.txtAddress.MaxLength = 98
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtAddress.Size = New System.Drawing.Size(508, 21)
        Me.txtAddress.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&First Name:"
        '
        'txtFirstName
        '
        Me.txtFirstName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFirstName.Location = New System.Drawing.Point(83, 20)
        Me.txtFirstName.MaxLength = 30
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(276, 21)
        Me.txtFirstName.TabIndex = 1
        '
        'txtLastName
        '
        Me.txtLastName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLastName.Location = New System.Drawing.Point(83, 47)
        Me.txtLastName.MaxLength = 30
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(276, 21)
        Me.txtLastName.TabIndex = 3
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(101, 47)
        Me.txtEmail.MaxLength = 50
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(481, 21)
        Me.txtEmail.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 50)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "&Last Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "A&ddress:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(81, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Telephone No.:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(318, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "&Mobile No.:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(15, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(81, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "&E-Mail Address:"
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(397, 32)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(208, 21)
        Me.cmbBranch.TabIndex = 1
        '
        'gbIndiv
        '
        Me.gbIndiv.Controls.Add(Me.txtFirstName)
        Me.gbIndiv.Controls.Add(Me.Label1)
        Me.gbIndiv.Controls.Add(Me.txtLastName)
        Me.gbIndiv.Controls.Add(Me.Label3)
        Me.gbIndiv.Enabled = False
        Me.gbIndiv.Location = New System.Drawing.Point(12, 12)
        Me.gbIndiv.Name = "gbIndiv"
        Me.gbIndiv.Size = New System.Drawing.Size(375, 81)
        Me.gbIndiv.TabIndex = 4
        Me.gbIndiv.TabStop = False
        Me.gbIndiv.Text = "Individual"
        '
        'gbNonIndiv
        '
        Me.gbNonIndiv.Controls.Add(Me.txtComName)
        Me.gbNonIndiv.Controls.Add(Me.Label8)
        Me.gbNonIndiv.Enabled = False
        Me.gbNonIndiv.Location = New System.Drawing.Point(12, 99)
        Me.gbNonIndiv.Name = "gbNonIndiv"
        Me.gbNonIndiv.Size = New System.Drawing.Size(375, 55)
        Me.gbNonIndiv.TabIndex = 5
        Me.gbNonIndiv.TabStop = False
        Me.gbNonIndiv.Text = "Non-Individual"
        '
        'txtComName
        '
        Me.txtComName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtComName.Location = New System.Drawing.Point(83, 20)
        Me.txtComName.MaxLength = 30
        Me.txtComName.Name = "txtComName"
        Me.txtComName.Size = New System.Drawing.Size(276, 21)
        Me.txtComName.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(15, 23)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 13)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Name:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(394, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "&Branch:"
        '
        'mtxtMobile
        '
        Me.mtxtMobile.Location = New System.Drawing.Point(385, 20)
        Me.mtxtMobile.Mask = "####-###-##-##"
        Me.mtxtMobile.Name = "mtxtMobile"
        Me.mtxtMobile.Size = New System.Drawing.Size(197, 21)
        Me.mtxtMobile.TabIndex = 3
        Me.mtxtMobile.Text = "00000000000"
        '
        'mtxtTelephone
        '
        Me.mtxtTelephone.Location = New System.Drawing.Point(100, 20)
        Me.mtxtTelephone.Mask = "000-0000"
        Me.mtxtTelephone.Name = "mtxtTelephone"
        Me.mtxtTelephone.Size = New System.Drawing.Size(191, 21)
        Me.mtxtTelephone.TabIndex = 1
        Me.mtxtTelephone.Text = "0000000"
        '
        'lblCustID
        '
        Me.lblCustID.AutoSize = True
        Me.lblCustID.Enabled = False
        Me.lblCustID.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblCustID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustID.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCustID.Location = New System.Drawing.Point(64, 340)
        Me.lblCustID.Name = "lblCustID"
        Me.lblCustID.Size = New System.Drawing.Size(70, 16)
        Me.lblCustID.TabIndex = 10
        Me.lblCustID.Text = "HW-0000000"
        Me.lblCustID.UseCompatibleTextRendering = True
        Me.lblCustID.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Enabled = False
        Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(12, 340)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 16)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "New ID:"
        Me.Label10.UseCompatibleTextRendering = True
        Me.Label10.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(394, 103)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "Ty&pe:"
        '
        'cmbCustType
        '
        Me.cmbCustType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbCustType.Enabled = False
        Me.cmbCustType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbCustType.FormattingEnabled = True
        Me.cmbCustType.Location = New System.Drawing.Point(397, 119)
        Me.cmbCustType.Name = "cmbCustType"
        Me.cmbCustType.Size = New System.Drawing.Size(208, 21)
        Me.cmbCustType.TabIndex = 3
        '
        'gbContacts
        '
        Me.gbContacts.Controls.Add(Me.mtxtTelephone)
        Me.gbContacts.Controls.Add(Me.Label4)
        Me.gbContacts.Controls.Add(Me.mtxtMobile)
        Me.gbContacts.Controls.Add(Me.Label5)
        Me.gbContacts.Controls.Add(Me.txtEmail)
        Me.gbContacts.Controls.Add(Me.Label6)
        Me.gbContacts.Enabled = False
        Me.gbContacts.Location = New System.Drawing.Point(12, 214)
        Me.gbContacts.Name = "gbContacts"
        Me.gbContacts.Size = New System.Drawing.Size(593, 80)
        Me.gbContacts.TabIndex = 7
        Me.gbContacts.TabStop = False
        Me.gbContacts.Text = "Contacts"
        '
        'pnlAddress
        '
        Me.pnlAddress.Controls.Add(Me.txtAddress)
        Me.pnlAddress.Controls.Add(Me.Label2)
        Me.pnlAddress.Enabled = False
        Me.pnlAddress.Location = New System.Drawing.Point(19, 160)
        Me.pnlAddress.Name = "pnlAddress"
        Me.pnlAddress.Size = New System.Drawing.Size(591, 36)
        Me.pnlAddress.TabIndex = 6
        '
        'frmNewCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(616, 370)
        Me.Controls.Add(Me.pnlAddress)
        Me.Controls.Add(Me.gbContacts)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cmbCustType)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblCustID)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.gbNonIndiv)
        Me.Controls.Add(Me.gbIndiv)
        Me.Controls.Add(Me.cmbBranch)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(622, 323)
        Me.Name = "frmNewCustomer"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "New Customer"
        Me.gbIndiv.ResumeLayout(False)
        Me.gbIndiv.PerformLayout()
        Me.gbNonIndiv.ResumeLayout(False)
        Me.gbNonIndiv.PerformLayout()
        Me.gbContacts.ResumeLayout(False)
        Me.gbContacts.PerformLayout()
        Me.pnlAddress.ResumeLayout(False)
        Me.pnlAddress.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents gbIndiv As System.Windows.Forms.GroupBox
    Friend WithEvents gbNonIndiv As System.Windows.Forms.GroupBox
    Friend WithEvents txtComName As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents mtxtMobile As System.Windows.Forms.MaskedTextBox
    Friend WithEvents mtxtTelephone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents lblCustID As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbCustType As System.Windows.Forms.ComboBox
    Friend WithEvents gbContacts As System.Windows.Forms.GroupBox
    Friend WithEvents pnlAddress As System.Windows.Forms.Panel
End Class

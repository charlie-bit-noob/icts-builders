<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockCard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvIn = New System.Windows.Forms.DataGridView
        Me.dgvOut = New System.Windows.Forms.DataGridView
        Me.btnItemFinder = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.txtParticular = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtTotalIn = New System.Windows.Forms.TextBox
        Me.txtTotalOut = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnExecute = New System.Windows.Forms.Button
        Me.txtItemID = New System.Windows.Forms.TextBox
        Me.btnFilter = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cbHasBalance = New System.Windows.Forms.CheckBox
        Me.panel_ParaIN = New System.Windows.Forms.Panel
        Me.txtDateIn = New System.Windows.Forms.MaskedTextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.txtSource = New System.Windows.Forms.TextBox
        Me.txtQtyIn = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtSerialIn = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtNumberIn = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtRefIn = New System.Windows.Forms.TextBox
        Me.panel_ParaOUT = New System.Windows.Forms.Panel
        Me.txtDateOut = New System.Windows.Forms.MaskedTextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtDeliveredTo = New System.Windows.Forms.TextBox
        Me.txtQtyOut = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtSerialOut = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtNumberOut = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtRefOut = New System.Windows.Forms.TextBox
        Me.btnCaptureItem = New System.Windows.Forms.Button
        Me.txtUnit = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.dgvIn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvOut, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel_ParaIN.SuspendLayout()
        Me.panel_ParaOUT.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvIn
        '
        Me.dgvIn.AllowUserToAddRows = False
        Me.dgvIn.AllowUserToDeleteRows = False
        Me.dgvIn.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvIn.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvIn.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvIn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvIn.Location = New System.Drawing.Point(15, 91)
        Me.dgvIn.MultiSelect = False
        Me.dgvIn.Name = "dgvIn"
        Me.dgvIn.ReadOnly = True
        Me.dgvIn.RowHeadersVisible = False
        Me.dgvIn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIn.Size = New System.Drawing.Size(967, 185)
        Me.dgvIn.TabIndex = 186
        '
        'dgvOut
        '
        Me.dgvOut.AllowUserToAddRows = False
        Me.dgvOut.AllowUserToDeleteRows = False
        Me.dgvOut.AllowUserToResizeRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvOut.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvOut.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvOut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvOut.Location = New System.Drawing.Point(12, 322)
        Me.dgvOut.MultiSelect = False
        Me.dgvOut.Name = "dgvOut"
        Me.dgvOut.ReadOnly = True
        Me.dgvOut.RowHeadersVisible = False
        Me.dgvOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOut.Size = New System.Drawing.Size(970, 203)
        Me.dgvOut.TabIndex = 187
        '
        'btnItemFinder
        '
        Me.btnItemFinder.Location = New System.Drawing.Point(645, 9)
        Me.btnItemFinder.Name = "btnItemFinder"
        Me.btnItemFinder.Size = New System.Drawing.Size(24, 23)
        Me.btnItemFinder.TabIndex = 188
        Me.btnItemFinder.Text = "..."
        Me.btnItemFinder.UseCompatibleTextRendering = True
        Me.btnItemFinder.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 190
        Me.Label1.Text = "&Branch:"
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(68, 39)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(202, 21)
        Me.cmbBranch.TabIndex = 191
        '
        'txtParticular
        '
        Me.txtParticular.Location = New System.Drawing.Point(218, 11)
        Me.txtParticular.Name = "txtParticular"
        Me.txtParticular.ReadOnly = True
        Me.txtParticular.Size = New System.Drawing.Size(429, 21)
        Me.txtParticular.TabIndex = 193
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 13)
        Me.Label2.TabIndex = 194
        Me.Label2.Text = "Particular:"
        '
        'txtTotalIn
        '
        Me.txtTotalIn.Location = New System.Drawing.Point(921, 288)
        Me.txtTotalIn.Name = "txtTotalIn"
        Me.txtTotalIn.ReadOnly = True
        Me.txtTotalIn.Size = New System.Drawing.Size(61, 21)
        Me.txtTotalIn.TabIndex = 202
        Me.txtTotalIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalOut
        '
        Me.txtTotalOut.Location = New System.Drawing.Point(921, 539)
        Me.txtTotalOut.Name = "txtTotalOut"
        Me.txtTotalOut.ReadOnly = True
        Me.txtTotalOut.Size = New System.Drawing.Size(61, 21)
        Me.txtTotalOut.TabIndex = 204
        Me.txtTotalOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-12, 71)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1008, 2)
        Me.GroupBox1.TabIndex = 208
        Me.GroupBox1.TabStop = False
        '
        'btnExecute
        '
        Me.btnExecute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExecute.Location = New System.Drawing.Point(675, 9)
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(75, 23)
        Me.btnExecute.TabIndex = 209
        Me.btnExecute.Text = "&Execute!"
        Me.btnExecute.UseVisualStyleBackColor = True
        '
        'txtItemID
        '
        Me.txtItemID.Location = New System.Drawing.Point(68, 12)
        Me.txtItemID.Name = "txtItemID"
        Me.txtItemID.ReadOnly = True
        Me.txtItemID.Size = New System.Drawing.Size(120, 21)
        Me.txtItemID.TabIndex = 212
        '
        'btnFilter
        '
        Me.btnFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFilter.Location = New System.Drawing.Point(276, 38)
        Me.btnFilter.Name = "btnFilter"
        Me.btnFilter.Size = New System.Drawing.Size(75, 23)
        Me.btnFilter.TabIndex = 213
        Me.btnFilter.Text = "&Filter"
        Me.btnFilter.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(870, 542)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(45, 13)
        Me.Label8.TabIndex = 205
        Me.Label8.Text = "Tt. OUT"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(880, 291)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 203
        Me.Label7.Text = "Tt. IN"
        '
        'cbHasBalance
        '
        Me.cbHasBalance.AutoSize = True
        Me.cbHasBalance.Location = New System.Drawing.Point(838, 48)
        Me.cbHasBalance.Name = "cbHasBalance"
        Me.cbHasBalance.Size = New System.Drawing.Size(144, 17)
        Me.cbHasBalance.TabIndex = 214
        Me.cbHasBalance.Text = "Reference(s) w/ &Balance"
        Me.cbHasBalance.UseVisualStyleBackColor = True
        '
        'panel_ParaIN
        '
        Me.panel_ParaIN.Controls.Add(Me.txtDateIn)
        Me.panel_ParaIN.Controls.Add(Me.Label18)
        Me.panel_ParaIN.Controls.Add(Me.Label17)
        Me.panel_ParaIN.Controls.Add(Me.Label16)
        Me.panel_ParaIN.Controls.Add(Me.txtSource)
        Me.panel_ParaIN.Controls.Add(Me.txtQtyIn)
        Me.panel_ParaIN.Controls.Add(Me.Label9)
        Me.panel_ParaIN.Controls.Add(Me.Label10)
        Me.panel_ParaIN.Controls.Add(Me.txtSerialIn)
        Me.panel_ParaIN.Controls.Add(Me.Label5)
        Me.panel_ParaIN.Controls.Add(Me.Label6)
        Me.panel_ParaIN.Controls.Add(Me.txtNumberIn)
        Me.panel_ParaIN.Controls.Add(Me.Label4)
        Me.panel_ParaIN.Controls.Add(Me.txtRefIn)
        Me.panel_ParaIN.Location = New System.Drawing.Point(15, 280)
        Me.panel_ParaIN.Name = "panel_ParaIN"
        Me.panel_ParaIN.Size = New System.Drawing.Size(846, 36)
        Me.panel_ParaIN.TabIndex = 236
        '
        'txtDateIn
        '
        Me.txtDateIn.BeepOnError = True
        Me.txtDateIn.Location = New System.Drawing.Point(456, 8)
        Me.txtDateIn.Mask = "00/00/0000"
        Me.txtDateIn.Name = "txtDateIn"
        Me.txtDateIn.Size = New System.Drawing.Size(72, 21)
        Me.txtDateIn.TabIndex = 238
        Me.txtDateIn.ValidatingType = GetType(Date)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Enabled = False
        Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label18.Location = New System.Drawing.Point(535, 11)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(11, 13)
        Me.Label18.TabIndex = 226
        Me.Label18.Text = "|"
        Me.Label18.UseMnemonic = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Enabled = False
        Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label17.Location = New System.Drawing.Point(629, 11)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(11, 13)
        Me.Label17.TabIndex = 225
        Me.Label17.Text = "|"
        Me.Label17.UseMnemonic = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Enabled = False
        Me.Label16.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label16.Location = New System.Drawing.Point(403, 11)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(11, 13)
        Me.Label16.TabIndex = 224
        Me.Label16.Text = "|"
        Me.Label16.UseMnemonic = False
        '
        'txtSource
        '
        Me.txtSource.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSource.Location = New System.Drawing.Point(58, 8)
        Me.txtSource.Name = "txtSource"
        Me.txtSource.Size = New System.Drawing.Size(197, 21)
        Me.txtSource.TabIndex = 223
        '
        'txtQtyIn
        '
        Me.txtQtyIn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQtyIn.Location = New System.Drawing.Point(588, 8)
        Me.txtQtyIn.Name = "txtQtyIn"
        Me.txtQtyIn.Size = New System.Drawing.Size(35, 21)
        Me.txtQtyIn.TabIndex = 221
        Me.txtQtyIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(549, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(33, 13)
        Me.Label9.TabIndex = 220
        Me.Label9.Text = "Qty.:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(8, 11)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 222
        Me.Label10.Text = "Source:"
        '
        'txtSerialIn
        '
        Me.txtSerialIn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerialIn.Location = New System.Drawing.Point(673, 8)
        Me.txtSerialIn.Name = "txtSerialIn"
        Me.txtSerialIn.Size = New System.Drawing.Size(164, 21)
        Me.txtSerialIn.TabIndex = 219
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(417, 11)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 13)
        Me.Label5.TabIndex = 193
        Me.Label5.Text = "Date:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(643, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 13)
        Me.Label6.TabIndex = 218
        Me.Label6.Text = "SN:"
        '
        'txtNumberIn
        '
        Me.txtNumberIn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumberIn.Location = New System.Drawing.Point(335, 8)
        Me.txtNumberIn.Name = "txtNumberIn"
        Me.txtNumberIn.Size = New System.Drawing.Size(62, 21)
        Me.txtNumberIn.TabIndex = 192
        Me.txtNumberIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(261, 11)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 191
        Me.Label4.Text = "Ref:"
        '
        'txtRefIn
        '
        Me.txtRefIn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRefIn.Location = New System.Drawing.Point(295, 8)
        Me.txtRefIn.Name = "txtRefIn"
        Me.txtRefIn.Size = New System.Drawing.Size(39, 21)
        Me.txtRefIn.TabIndex = 1
        Me.txtRefIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'panel_ParaOUT
        '
        Me.panel_ParaOUT.Controls.Add(Me.txtDateOut)
        Me.panel_ParaOUT.Controls.Add(Me.Label11)
        Me.panel_ParaOUT.Controls.Add(Me.Label12)
        Me.panel_ParaOUT.Controls.Add(Me.Label13)
        Me.panel_ParaOUT.Controls.Add(Me.txtDeliveredTo)
        Me.panel_ParaOUT.Controls.Add(Me.txtQtyOut)
        Me.panel_ParaOUT.Controls.Add(Me.Label14)
        Me.panel_ParaOUT.Controls.Add(Me.Label15)
        Me.panel_ParaOUT.Controls.Add(Me.txtSerialOut)
        Me.panel_ParaOUT.Controls.Add(Me.Label19)
        Me.panel_ParaOUT.Controls.Add(Me.Label20)
        Me.panel_ParaOUT.Controls.Add(Me.txtNumberOut)
        Me.panel_ParaOUT.Controls.Add(Me.Label21)
        Me.panel_ParaOUT.Controls.Add(Me.txtRefOut)
        Me.panel_ParaOUT.Location = New System.Drawing.Point(15, 531)
        Me.panel_ParaOUT.Name = "panel_ParaOUT"
        Me.panel_ParaOUT.Size = New System.Drawing.Size(846, 36)
        Me.panel_ParaOUT.TabIndex = 237
        '
        'txtDateOut
        '
        Me.txtDateOut.BeepOnError = True
        Me.txtDateOut.Location = New System.Drawing.Point(457, 8)
        Me.txtDateOut.Mask = "00/00/0000"
        Me.txtDateOut.Name = "txtDateOut"
        Me.txtDateOut.Size = New System.Drawing.Size(72, 21)
        Me.txtDateOut.TabIndex = 252
        Me.txtDateOut.ValidatingType = GetType(Date)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Enabled = False
        Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label11.Location = New System.Drawing.Point(536, 11)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(11, 13)
        Me.Label11.TabIndex = 251
        Me.Label11.Text = "|"
        Me.Label11.UseMnemonic = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Enabled = False
        Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label12.Location = New System.Drawing.Point(630, 11)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(11, 13)
        Me.Label12.TabIndex = 250
        Me.Label12.Text = "|"
        Me.Label12.UseMnemonic = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Enabled = False
        Me.Label13.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label13.Location = New System.Drawing.Point(404, 11)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(11, 13)
        Me.Label13.TabIndex = 249
        Me.Label13.Text = "|"
        Me.Label13.UseMnemonic = False
        '
        'txtDeliveredTo
        '
        Me.txtDeliveredTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDeliveredTo.Location = New System.Drawing.Point(87, 8)
        Me.txtDeliveredTo.Name = "txtDeliveredTo"
        Me.txtDeliveredTo.Size = New System.Drawing.Size(169, 21)
        Me.txtDeliveredTo.TabIndex = 248
        '
        'txtQtyOut
        '
        Me.txtQtyOut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQtyOut.Location = New System.Drawing.Point(589, 8)
        Me.txtQtyOut.Name = "txtQtyOut"
        Me.txtQtyOut.Size = New System.Drawing.Size(35, 21)
        Me.txtQtyOut.TabIndex = 246
        Me.txtQtyOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(550, 11)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(33, 13)
        Me.Label14.TabIndex = 245
        Me.Label14.Text = "Qty.:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 11)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(69, 13)
        Me.Label15.TabIndex = 247
        Me.Label15.Text = "Delivered to:"
        '
        'txtSerialOut
        '
        Me.txtSerialOut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerialOut.Location = New System.Drawing.Point(674, 8)
        Me.txtSerialOut.Name = "txtSerialOut"
        Me.txtSerialOut.Size = New System.Drawing.Size(164, 21)
        Me.txtSerialOut.TabIndex = 244
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(418, 11)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(34, 13)
        Me.Label19.TabIndex = 242
        Me.Label19.Text = "Date:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(644, 11)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(24, 13)
        Me.Label20.TabIndex = 243
        Me.Label20.Text = "SN:"
        '
        'txtNumberOut
        '
        Me.txtNumberOut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumberOut.Location = New System.Drawing.Point(336, 8)
        Me.txtNumberOut.Name = "txtNumberOut"
        Me.txtNumberOut.Size = New System.Drawing.Size(62, 21)
        Me.txtNumberOut.TabIndex = 241
        Me.txtNumberOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(262, 11)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(28, 13)
        Me.Label21.TabIndex = 240
        Me.Label21.Text = "Ref:"
        '
        'txtRefOut
        '
        Me.txtRefOut.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRefOut.Location = New System.Drawing.Point(296, 8)
        Me.txtRefOut.Name = "txtRefOut"
        Me.txtRefOut.Size = New System.Drawing.Size(39, 21)
        Me.txtRefOut.TabIndex = 239
        Me.txtRefOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnCaptureItem
        '
        Me.btnCaptureItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCaptureItem.Location = New System.Drawing.Point(186, 10)
        Me.btnCaptureItem.Name = "btnCaptureItem"
        Me.btnCaptureItem.Size = New System.Drawing.Size(25, 23)
        Me.btnCaptureItem.TabIndex = 238
        Me.btnCaptureItem.Text = "&<"
        Me.btnCaptureItem.UseVisualStyleBackColor = True
        '
        'txtUnit
        '
        Me.txtUnit.Location = New System.Drawing.Point(545, 38)
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.ReadOnly = True
        Me.txtUnit.Size = New System.Drawing.Size(124, 21)
        Me.txtUnit.TabIndex = 239
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(513, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(30, 13)
        Me.Label3.TabIndex = 240
        Me.Label3.Text = "Unit:"
        '
        'frmStockCard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 570)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtUnit)
        Me.Controls.Add(Me.btnCaptureItem)
        Me.Controls.Add(Me.panel_ParaOUT)
        Me.Controls.Add(Me.panel_ParaIN)
        Me.Controls.Add(Me.cbHasBalance)
        Me.Controls.Add(Me.btnFilter)
        Me.Controls.Add(Me.btnItemFinder)
        Me.Controls.Add(Me.btnExecute)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtTotalOut)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtTotalIn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtParticular)
        Me.Controls.Add(Me.cmbBranch)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvOut)
        Me.Controls.Add(Me.dgvIn)
        Me.Controls.Add(Me.txtItemID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStockCard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Card"
        CType(Me.dgvIn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvOut, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel_ParaIN.ResumeLayout(False)
        Me.panel_ParaIN.PerformLayout()
        Me.panel_ParaOUT.ResumeLayout(False)
        Me.panel_ParaOUT.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvIn As System.Windows.Forms.DataGridView
    Friend WithEvents dgvOut As System.Windows.Forms.DataGridView
    Friend WithEvents btnItemFinder As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents txtParticular As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtTotalIn As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalOut As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnExecute As System.Windows.Forms.Button
    Friend WithEvents txtItemID As System.Windows.Forms.TextBox
    Friend WithEvents btnFilter As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cbHasBalance As System.Windows.Forms.CheckBox
    Friend WithEvents panel_ParaIN As System.Windows.Forms.Panel
    Friend WithEvents txtRefIn As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNumberIn As System.Windows.Forms.TextBox
    Friend WithEvents txtSerialIn As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtQtyIn As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSource As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents panel_ParaOUT As System.Windows.Forms.Panel
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtDateIn As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtDateOut As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtDeliveredTo As System.Windows.Forms.TextBox
    Friend WithEvents txtQtyOut As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtSerialOut As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtNumberOut As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtRefOut As System.Windows.Forms.TextBox
    Friend WithEvents btnCaptureItem As System.Windows.Forms.Button
    Friend WithEvents txtUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class

Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptViewerStockOnFile

    Private strDate As String

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmRptStockOnFile_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.AskForReportDate()
        Me.GetStockOnFile()
        Me.RptViewer.RefreshReport()
        Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
        RptViewer.ZoomMode = ZoomMode.Percent
        RptViewer.ZoomPercent = 100
    End Sub

    Private Sub AskForReportDate()
        Dim strDT As String

        strDT = InputBox("Report Date", "Enter Report Date", "December 25, 2012")
        Me.strDate = strDT
    End Sub

    Private Sub GetStockOnFile()
        Try
            With My.Forms.frmSelectBranch
                Me.dsCountSheet.Tables(1).Clear()

                '---Fetch Serial Items
                Me.sqlStr = "SELECT ts.date_issued, ts.item_id, im.group_id, im.type_lib_id, ig.product, " & _
                "im.brand + ' ' + im.product + ' ' + im.model + ' ' + im.description AS Particular, " & _
                "ts.serial_num, li.units, ts.cost " & _
                "FROM TypeSerial ts, ItemMasterlist im, ItemGroups ig, Library li " & _
                "WHERE im.item_id = ts.item_id AND ig.group_id = im.group_id AND li.lib_id = im.unit_lib_id " & _
                "AND ts.branch_id = " & .cmbBranch.SelectedIndex & " AND ts.has_bal = 'True'"

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsCountSheet.Tables(1).NewRow

                    tblRow("Branch") = .cmbBranch.Text
                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("Product") = Me.sqlReader("product")
                    tblRow("Serial") = Me.sqlReader("serial_num")
                    tblRow("Qty") = 1
                    tblRow("Unit") = Me.sqlReader("units")
                    tblRow("Cost") = Me.sqlReader("cost")
                    tblRow("Total") = tblRow("Cost") * tblRow("Qty")
                    tblRow("ReportDate") = Me.strDate
                    tblRow("Encoder") = MDI.stbUserID.Text

                    Me.dsCountSheet.Tables(1).Rows.Add(tblRow)
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()

                '--Fetch NonSerial Items
                Me.sqlStr = "SELECT tns.date_issued, tns.item_id, im.group_id, im.type_lib_id, ig.product, " & _
                "im.brand + ' ' + im.product + ' ' + im.model + ' ' + im.description AS Particular, " & _
                "tns.qty_bal, li.units, tns.cost " & _
                "FROM TypeNonSerial tns, ItemMasterlist im, ItemGroups ig, Library li " & _
                "WHERE im.item_id = tns.item_id AND ig.group_id = im.group_id AND li.lib_id = im.unit_lib_id " & _
                "AND tns.branch_id = " & .cmbBranch.SelectedIndex & " AND tns.qty_bal > 0"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsCountSheet.Tables(1).NewRow

                    tblRow("Branch") = .cmbBranch.Text
                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("Product") = Me.sqlReader("product")
                    tblRow("Serial") = ""
                    tblRow("Qty") = Me.sqlReader("qty_bal")
                    tblRow("Units") = Me.sqlReader("units")
                    tblRow("Cost") = Me.sqlReader("cost")
                    tblRow("Total") = Convert.ToDouble(tblRow("Cost")) * tblRow("Qty")
                    tblRow("ReportDate") = Me.strDate
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsCountSheet.Tables(1).Rows.Add(tblRow)
                End While
                Me.sqlReader.Close()
                sqlDBconn.Close()
            End With

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptViewerCountSheet

    Private sqlCmd As SqlCommand

    Private sqlConn As SqlConnection
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private strConn As String = "Data Source=DIMDICTR\SQLExpress,2301;Initial Catalog=HardwareCountSheetDB;User ID=sa;Password=JesusChristILuvU"

    Private Sub CleanUpObjects()
        Me.sqlCmd = Nothing
        Me.sqlReader = Nothing
        Me.sqlStr = Nothing
        Me.sqlConn.Dispose()
    End Sub

    Private Sub frmRptViewerCountSheet_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.CleanUpObjects()
    End Sub

    Private Sub frmRptViewerCountSheet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.GetCountSheet()
            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            RptViewer.ZoomMode = ZoomMode.Percent

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetCountSheet()
        Try
            Me.sqlConn = New SqlConnection(Me.strConn)

            With My.Forms.frmCountSheet
                Me.dsCountSheet.Tables(0).Clear()

                Me.sqlStr = "SELECT (SELECT brand + ' ' + product + ' ' + model + ' ' + description " & _
                "FROM HardwareDB.dbo.ItemMasterlist WHERE item_id = cs.item_id) AS particular, " & _
                "(SELECT product FROM HardwareDB.dbo.ItemGroups AS ig WHERE group_id = cs.group_id) AS product, " & _
                "(SELECT cost FROM HardwareDB.dbo.ItemMasterlist WHERE item_id = cs.item_id) AS cost, " & _
                "qty, serial_num, class FROM [" & .cmbBranch.SelectedIndex & "] AS cs WHERE cs_id = '" & .txtCSID.Text & "'"
                If .cbSetGroup.CheckState = CheckState.Checked Then Me.sqlStr = Me.sqlStr + " AND group_id = " & .txtGroupID.Text & ""

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, Me.sqlConn)
                Me.sqlConn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.DScountsheet.Tables(0).NewRow

                    tblRow("date") = .txtRptDate.Text
                    tblRow("branch") = .cmbBranch.Text
                    tblRow("particular") = Me.sqlReader("particular")
                    tblRow("product") = Me.sqlReader("product")
                    tblRow("serial_num") = Me.sqlReader("serial_num")
                    tblRow("cost") = Me.sqlReader("cost")
                    tblRow("qty") = Me.sqlReader("qty")
                    tblRow("total") = tblRow("cost") * tblRow("qty")
                    tblRow("class") = Me.sqlReader("class")
                    tblRow("encoder") = MDI.stbUserName.Text.ToUpper

                    Me.dsCountSheet.Tables(0).Rows.Add(tblRow)
                End While
            End With

            Me.sqlReader.Close()
            Me.sqlConn.Close()

        Catch ex As Exception
            Me.sqlConn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
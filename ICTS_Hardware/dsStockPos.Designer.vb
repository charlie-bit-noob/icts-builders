﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.42
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Imports System


<System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0"),  _
 Serializable(),  _
 System.ComponentModel.DesignerCategoryAttribute("code"),  _
 System.ComponentModel.ToolboxItem(true),  _
 System.Xml.Serialization.XmlSchemaProviderAttribute("GetTypedDataSetSchema"),  _
 System.Xml.Serialization.XmlRootAttribute("dsStockPos"),  _
 System.ComponentModel.Design.HelpKeywordAttribute("vs.data.DataSet")>  _
Partial Public Class dsStockPos
    Inherits System.Data.DataSet
    
    Private tabletblStockPos As tblStockPosDataTable
    
    Private _schemaSerializationMode As System.Data.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Public Sub New()
        MyBase.New
        Me.BeginInit
        Me.InitClass
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler MyBase.Tables.CollectionChanged, schemaChangedHandler
        AddHandler MyBase.Relations.CollectionChanged, schemaChangedHandler
        Me.EndInit
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context, false)
        If (Me.IsBinarySerialized(info, context) = true) Then
            Me.InitVars(false)
            Dim schemaChangedHandler1 As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
            AddHandler Me.Tables.CollectionChanged, schemaChangedHandler1
            AddHandler Me.Relations.CollectionChanged, schemaChangedHandler1
            Return
        End If
        Dim strSchema As String = CType(info.GetValue("XmlSchema", GetType(String)),String)
        If (Me.DetermineSchemaSerializationMode(info, context) = System.Data.SchemaSerializationMode.IncludeSchema) Then
            Dim ds As System.Data.DataSet = New System.Data.DataSet
            ds.ReadXmlSchema(New System.Xml.XmlTextReader(New System.IO.StringReader(strSchema)))
            If (Not (ds.Tables("tblStockPos")) Is Nothing) Then
                MyBase.Tables.Add(New tblStockPosDataTable(ds.Tables("tblStockPos")))
            End If
            Me.DataSetName = ds.DataSetName
            Me.Prefix = ds.Prefix
            Me.Namespace = ds.Namespace
            Me.Locale = ds.Locale
            Me.CaseSensitive = ds.CaseSensitive
            Me.EnforceConstraints = ds.EnforceConstraints
            Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
            Me.InitVars
        Else
            Me.ReadXmlSchema(New System.Xml.XmlTextReader(New System.IO.StringReader(strSchema)))
        End If
        Me.GetSerializationData(info, context)
        Dim schemaChangedHandler As System.ComponentModel.CollectionChangeEventHandler = AddressOf Me.SchemaChanged
        AddHandler MyBase.Tables.CollectionChanged, schemaChangedHandler
        AddHandler Me.Relations.CollectionChanged, schemaChangedHandler
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     System.ComponentModel.Browsable(false),  _
     System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Content)>  _
    Public ReadOnly Property tblStockPos() As tblStockPosDataTable
        Get
            Return Me.tabletblStockPos
        End Get
    End Property
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     System.ComponentModel.BrowsableAttribute(true),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Visible)>  _
    Public Overrides Property SchemaSerializationMode() As System.Data.SchemaSerializationMode
        Get
            Return Me._schemaSerializationMode
        End Get
        Set
            Me._schemaSerializationMode = value
        End Set
    End Property
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Hidden)>  _
    Public Shadows ReadOnly Property Tables() As System.Data.DataTableCollection
        Get
            Return MyBase.Tables
        End Get
    End Property
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Hidden)>  _
    Public Shadows ReadOnly Property Relations() As System.Data.DataRelationCollection
        Get
            Return MyBase.Relations
        End Get
    End Property
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Overrides Sub InitializeDerivedDataSet()
        Me.BeginInit
        Me.InitClass
        Me.EndInit
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Public Overrides Function Clone() As System.Data.DataSet
        Dim cln As dsStockPos = CType(MyBase.Clone,dsStockPos)
        cln.InitVars
        cln.SchemaSerializationMode = Me.SchemaSerializationMode
        Return cln
    End Function
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Overrides Function ShouldSerializeTables() As Boolean
        Return false
    End Function
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Overrides Function ShouldSerializeRelations() As Boolean
        Return false
    End Function
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Overrides Sub ReadXmlSerializable(ByVal reader As System.Xml.XmlReader)
        If (Me.DetermineSchemaSerializationMode(reader) = System.Data.SchemaSerializationMode.IncludeSchema) Then
            Me.Reset
            Dim ds As System.Data.DataSet = New System.Data.DataSet
            ds.ReadXml(reader)
            If (Not (ds.Tables("tblStockPos")) Is Nothing) Then
                MyBase.Tables.Add(New tblStockPosDataTable(ds.Tables("tblStockPos")))
            End If
            Me.DataSetName = ds.DataSetName
            Me.Prefix = ds.Prefix
            Me.Namespace = ds.Namespace
            Me.Locale = ds.Locale
            Me.CaseSensitive = ds.CaseSensitive
            Me.EnforceConstraints = ds.EnforceConstraints
            Me.Merge(ds, false, System.Data.MissingSchemaAction.Add)
            Me.InitVars
        Else
            Me.ReadXml(reader)
            Me.InitVars
        End If
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Protected Overrides Function GetSchemaSerializable() As System.Xml.Schema.XmlSchema
        Dim stream As System.IO.MemoryStream = New System.IO.MemoryStream
        Me.WriteXmlSchema(New System.Xml.XmlTextWriter(stream, Nothing))
        stream.Position = 0
        Return System.Xml.Schema.XmlSchema.Read(New System.Xml.XmlTextReader(stream), Nothing)
    End Function
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Friend Overloads Sub InitVars()
        Me.InitVars(true)
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Friend Overloads Sub InitVars(ByVal initTable As Boolean)
        Me.tabletblStockPos = CType(MyBase.Tables("tblStockPos"),tblStockPosDataTable)
        If (initTable = true) Then
            If (Not (Me.tabletblStockPos) Is Nothing) Then
                Me.tabletblStockPos.InitVars
            End If
        End If
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Private Sub InitClass()
        Me.DataSetName = "dsStockPos"
        Me.Prefix = ""
        Me.Namespace = "http://tempuri.org/dsStockPos.xsd"
        Me.EnforceConstraints = true
        Me.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        Me.tabletblStockPos = New tblStockPosDataTable
        MyBase.Tables.Add(Me.tabletblStockPos)
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Private Function ShouldSerializetblStockPos() As Boolean
        Return false
    End Function
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Private Sub SchemaChanged(ByVal sender As Object, ByVal e As System.ComponentModel.CollectionChangeEventArgs)
        If (e.Action = System.ComponentModel.CollectionChangeAction.Remove) Then
            Me.InitVars
        End If
    End Sub
    
    <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
    Public Shared Function GetTypedDataSetSchema(ByVal xs As System.Xml.Schema.XmlSchemaSet) As System.Xml.Schema.XmlSchemaComplexType
        Dim ds As dsStockPos = New dsStockPos
        Dim type As System.Xml.Schema.XmlSchemaComplexType = New System.Xml.Schema.XmlSchemaComplexType
        Dim sequence As System.Xml.Schema.XmlSchemaSequence = New System.Xml.Schema.XmlSchemaSequence
        xs.Add(ds.GetSchemaSerializable)
        Dim any As System.Xml.Schema.XmlSchemaAny = New System.Xml.Schema.XmlSchemaAny
        any.Namespace = ds.Namespace
        sequence.Items.Add(any)
        type.Particle = sequence
        Return type
    End Function
    
    Public Delegate Sub tblStockPosRowChangeEventHandler(ByVal sender As Object, ByVal e As tblStockPosRowChangeEvent)
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0"),  _
     System.Serializable(),  _
     System.Xml.Serialization.XmlSchemaProviderAttribute("GetTypedTableSchema")>  _
    Partial Public Class tblStockPosDataTable
        Inherits System.Data.DataTable
        Implements System.Collections.IEnumerable
        
        Private columnBranch As System.Data.DataColumn
        
        Private columnDate As System.Data.DataColumn
        
        Private columnParticular As System.Data.DataColumn
        
        Private columnBalance As System.Data.DataColumn
        
        Private columnA As System.Data.DataColumn
        
        Private columnB As System.Data.DataColumn
        
        Private columnC As System.Data.DataColumn
        
        Private columnUnit As System.Data.DataColumn
        
        Private columnEncoder As System.Data.DataColumn
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub New()
            MyBase.New
            Me.TableName = "tblStockPos"
            Me.BeginInit
            Me.InitClass
            Me.EndInit
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Friend Sub New(ByVal table As System.Data.DataTable)
            MyBase.New
            Me.TableName = table.TableName
            If (table.CaseSensitive <> table.DataSet.CaseSensitive) Then
                Me.CaseSensitive = table.CaseSensitive
            End If
            If (table.Locale.ToString <> table.DataSet.Locale.ToString) Then
                Me.Locale = table.Locale
            End If
            If (table.Namespace <> table.DataSet.Namespace) Then
                Me.Namespace = table.Namespace
            End If
            Me.Prefix = table.Prefix
            Me.MinimumCapacity = table.MinimumCapacity
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
            MyBase.New(info, context)
            Me.InitVars
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property BranchColumn() As System.Data.DataColumn
            Get
                Return Me.columnBranch
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property DateColumn() As System.Data.DataColumn
            Get
                Return Me.columnDate
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property ParticularColumn() As System.Data.DataColumn
            Get
                Return Me.columnParticular
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property BalanceColumn() As System.Data.DataColumn
            Get
                Return Me.columnBalance
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property AColumn() As System.Data.DataColumn
            Get
                Return Me.columnA
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property BColumn() As System.Data.DataColumn
            Get
                Return Me.columnB
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property CColumn() As System.Data.DataColumn
            Get
                Return Me.columnC
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property UnitColumn() As System.Data.DataColumn
            Get
                Return Me.columnUnit
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property EncoderColumn() As System.Data.DataColumn
            Get
                Return Me.columnEncoder
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
         System.ComponentModel.Browsable(false)>  _
        Public ReadOnly Property Count() As Integer
            Get
                Return Me.Rows.Count
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Default ReadOnly Property Item(ByVal index As Integer) As tblStockPosRow
            Get
                Return CType(Me.Rows(index),tblStockPosRow)
            End Get
        End Property
        
        Public Event tblStockPosRowChanging As tblStockPosRowChangeEventHandler
        
        Public Event tblStockPosRowChanged As tblStockPosRowChangeEventHandler
        
        Public Event tblStockPosRowDeleting As tblStockPosRowChangeEventHandler
        
        Public Event tblStockPosRowDeleted As tblStockPosRowChangeEventHandler
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Overloads Sub AddtblStockPosRow(ByVal row As tblStockPosRow)
            Me.Rows.Add(row)
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Overloads Function AddtblStockPosRow(ByVal Branch As String, ByVal _Date As Date, ByVal Particular As String, ByVal Balance As Integer, ByVal A As Integer, ByVal B As Integer, ByVal C As Double, ByVal Unit As String, ByVal Encoder As String) As tblStockPosRow
            Dim rowtblStockPosRow As tblStockPosRow = CType(Me.NewRow,tblStockPosRow)
            rowtblStockPosRow.ItemArray = New Object() {Branch, _Date, Particular, Balance, A, B, C, Unit, Encoder}
            Me.Rows.Add(rowtblStockPosRow)
            Return rowtblStockPosRow
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Overridable Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.Rows.GetEnumerator
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Overrides Function Clone() As System.Data.DataTable
            Dim cln As tblStockPosDataTable = CType(MyBase.Clone,tblStockPosDataTable)
            cln.InitVars
            Return cln
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Function CreateInstance() As System.Data.DataTable
            Return New tblStockPosDataTable
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Friend Sub InitVars()
            Me.columnBranch = MyBase.Columns("Branch")
            Me.columnDate = MyBase.Columns("Date")
            Me.columnParticular = MyBase.Columns("Particular")
            Me.columnBalance = MyBase.Columns("Balance")
            Me.columnA = MyBase.Columns("A")
            Me.columnB = MyBase.Columns("B")
            Me.columnC = MyBase.Columns("C")
            Me.columnUnit = MyBase.Columns("Unit")
            Me.columnEncoder = MyBase.Columns("Encoder")
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Private Sub InitClass()
            Me.columnBranch = New System.Data.DataColumn("Branch", GetType(String), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnBranch)
            Me.columnDate = New System.Data.DataColumn("Date", GetType(Date), Nothing, System.Data.MappingType.Element)
            Me.columnDate.ExtendedProperties.Add("Generator_ColumnPropNameInRow", "_Date")
            Me.columnDate.ExtendedProperties.Add("Generator_UserColumnName", "Date")
            MyBase.Columns.Add(Me.columnDate)
            Me.columnParticular = New System.Data.DataColumn("Particular", GetType(String), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnParticular)
            Me.columnBalance = New System.Data.DataColumn("Balance", GetType(Integer), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnBalance)
            Me.columnA = New System.Data.DataColumn("A", GetType(Integer), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnA)
            Me.columnB = New System.Data.DataColumn("B", GetType(Integer), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnB)
            Me.columnC = New System.Data.DataColumn("C", GetType(Double), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnC)
            Me.columnUnit = New System.Data.DataColumn("Unit", GetType(String), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnUnit)
            Me.columnEncoder = New System.Data.DataColumn("Encoder", GetType(String), Nothing, System.Data.MappingType.Element)
            MyBase.Columns.Add(Me.columnEncoder)
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function NewtblStockPosRow() As tblStockPosRow
            Return CType(Me.NewRow,tblStockPosRow)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Function NewRowFromBuilder(ByVal builder As System.Data.DataRowBuilder) As System.Data.DataRow
            Return New tblStockPosRow(builder)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Function GetRowType() As System.Type
            Return GetType(tblStockPosRow)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Sub OnRowChanged(ByVal e As System.Data.DataRowChangeEventArgs)
            MyBase.OnRowChanged(e)
            If (Not (Me.tblStockPosRowChangedEvent) Is Nothing) Then
                RaiseEvent tblStockPosRowChanged(Me, New tblStockPosRowChangeEvent(CType(e.Row,tblStockPosRow), e.Action))
            End If
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Sub OnRowChanging(ByVal e As System.Data.DataRowChangeEventArgs)
            MyBase.OnRowChanging(e)
            If (Not (Me.tblStockPosRowChangingEvent) Is Nothing) Then
                RaiseEvent tblStockPosRowChanging(Me, New tblStockPosRowChangeEvent(CType(e.Row,tblStockPosRow), e.Action))
            End If
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Sub OnRowDeleted(ByVal e As System.Data.DataRowChangeEventArgs)
            MyBase.OnRowDeleted(e)
            If (Not (Me.tblStockPosRowDeletedEvent) Is Nothing) Then
                RaiseEvent tblStockPosRowDeleted(Me, New tblStockPosRowChangeEvent(CType(e.Row,tblStockPosRow), e.Action))
            End If
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Protected Overrides Sub OnRowDeleting(ByVal e As System.Data.DataRowChangeEventArgs)
            MyBase.OnRowDeleting(e)
            If (Not (Me.tblStockPosRowDeletingEvent) Is Nothing) Then
                RaiseEvent tblStockPosRowDeleting(Me, New tblStockPosRowChangeEvent(CType(e.Row,tblStockPosRow), e.Action))
            End If
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub RemovetblStockPosRow(ByVal row As tblStockPosRow)
            Me.Rows.Remove(row)
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Shared Function GetTypedTableSchema(ByVal xs As System.Xml.Schema.XmlSchemaSet) As System.Xml.Schema.XmlSchemaComplexType
            Dim type As System.Xml.Schema.XmlSchemaComplexType = New System.Xml.Schema.XmlSchemaComplexType
            Dim sequence As System.Xml.Schema.XmlSchemaSequence = New System.Xml.Schema.XmlSchemaSequence
            Dim ds As dsStockPos = New dsStockPos
            xs.Add(ds.GetSchemaSerializable)
            Dim any1 As System.Xml.Schema.XmlSchemaAny = New System.Xml.Schema.XmlSchemaAny
            any1.Namespace = "http://www.w3.org/2001/XMLSchema"
            any1.MinOccurs = New Decimal(0)
            any1.MaxOccurs = Decimal.MaxValue
            any1.ProcessContents = System.Xml.Schema.XmlSchemaContentProcessing.Lax
            sequence.Items.Add(any1)
            Dim any2 As System.Xml.Schema.XmlSchemaAny = New System.Xml.Schema.XmlSchemaAny
            any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1"
            any2.MinOccurs = New Decimal(1)
            any2.ProcessContents = System.Xml.Schema.XmlSchemaContentProcessing.Lax
            sequence.Items.Add(any2)
            Dim attribute1 As System.Xml.Schema.XmlSchemaAttribute = New System.Xml.Schema.XmlSchemaAttribute
            attribute1.Name = "namespace"
            attribute1.FixedValue = ds.Namespace
            type.Attributes.Add(attribute1)
            Dim attribute2 As System.Xml.Schema.XmlSchemaAttribute = New System.Xml.Schema.XmlSchemaAttribute
            attribute2.Name = "tableTypeName"
            attribute2.FixedValue = "tblStockPosDataTable"
            type.Attributes.Add(attribute2)
            type.Particle = sequence
            Return type
        End Function
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")>  _
    Partial Public Class tblStockPosRow
        Inherits System.Data.DataRow
        
        Private tabletblStockPos As tblStockPosDataTable
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Friend Sub New(ByVal rb As System.Data.DataRowBuilder)
            MyBase.New(rb)
            Me.tabletblStockPos = CType(Me.Table,tblStockPosDataTable)
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property Branch() As String
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.BranchColumn),String)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Branch' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.BranchColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property _Date() As Date
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.DateColumn),Date)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Date' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.DateColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property Particular() As String
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.ParticularColumn),String)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Particular' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.ParticularColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property Balance() As Integer
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.BalanceColumn),Integer)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Balance' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.BalanceColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property A() As Integer
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.AColumn),Integer)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'A' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.AColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property B() As Integer
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.BColumn),Integer)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'B' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.BColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property C() As Double
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.CColumn),Double)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'C' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.CColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property Unit() As String
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.UnitColumn),String)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Unit' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.UnitColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Property Encoder() As String
            Get
                Try 
                    Return CType(Me(Me.tabletblStockPos.EncoderColumn),String)
                Catch e As System.InvalidCastException
                    Throw New System.Data.StrongTypingException("The value for column 'Encoder' in table 'tblStockPos' is DBNull.", e)
                End Try
            End Get
            Set
                Me(Me.tabletblStockPos.EncoderColumn) = value
            End Set
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsBranchNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.BranchColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetBranchNull()
            Me(Me.tabletblStockPos.BranchColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function Is_DateNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.DateColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub Set_DateNull()
            Me(Me.tabletblStockPos.DateColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsParticularNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.ParticularColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetParticularNull()
            Me(Me.tabletblStockPos.ParticularColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsBalanceNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.BalanceColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetBalanceNull()
            Me(Me.tabletblStockPos.BalanceColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsANull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.AColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetANull()
            Me(Me.tabletblStockPos.AColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsBNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.BColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetBNull()
            Me(Me.tabletblStockPos.BColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsCNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.CColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetCNull()
            Me(Me.tabletblStockPos.CColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsUnitNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.UnitColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetUnitNull()
            Me(Me.tabletblStockPos.UnitColumn) = System.Convert.DBNull
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function IsEncoderNull() As Boolean
            Return Me.IsNull(Me.tabletblStockPos.EncoderColumn)
        End Function
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub SetEncoderNull()
            Me(Me.tabletblStockPos.EncoderColumn) = System.Convert.DBNull
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")>  _
    Public Class tblStockPosRowChangeEvent
        Inherits System.EventArgs
        
        Private eventRow As tblStockPosRow
        
        Private eventAction As System.Data.DataRowAction
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub New(ByVal row As tblStockPosRow, ByVal action As System.Data.DataRowAction)
            MyBase.New
            Me.eventRow = row
            Me.eventAction = action
        End Sub
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property Row() As tblStockPosRow
            Get
                Return Me.eventRow
            End Get
        End Property
        
        <System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public ReadOnly Property Action() As System.Data.DataRowAction
            Get
                Return Me.eventAction
            End Get
        End Property
    End Class
End Class

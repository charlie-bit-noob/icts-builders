<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParaOutstanding
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbGroup = New System.Windows.Forms.RadioButton
        Me.btnItemFinderTo = New System.Windows.Forms.Button
        Me.btnItemFinderFrom = New System.Windows.Forms.Button
        Me.cmbGroupTo = New System.Windows.Forms.ComboBox
        Me.cmbGroupFrom = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtModelTo = New System.Windows.Forms.TextBox
        Me.txtModelFrom = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtSQLpara = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtSumTitle = New System.Windows.Forms.TextBox
        Me.txtItemIDTo = New System.Windows.Forms.TextBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.txtItemIDFrom = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnApply = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.rbModel = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.cbVATX = New System.Windows.Forms.CheckBox
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.mtxtTo = New System.Windows.Forms.MaskedTextBox
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbGroup
        '
        Me.rbGroup.AutoSize = True
        Me.rbGroup.Checked = True
        Me.rbGroup.Location = New System.Drawing.Point(6, -1)
        Me.rbGroup.Name = "rbGroup"
        Me.rbGroup.Size = New System.Drawing.Size(103, 17)
        Me.rbGroup.TabIndex = 9
        Me.rbGroup.TabStop = True
        Me.rbGroup.Text = "Range by &Group"
        Me.rbGroup.UseVisualStyleBackColor = True
        '
        'btnItemFinderTo
        '
        Me.btnItemFinderTo.Location = New System.Drawing.Point(634, 50)
        Me.btnItemFinderTo.Name = "btnItemFinderTo"
        Me.btnItemFinderTo.Size = New System.Drawing.Size(25, 21)
        Me.btnItemFinderTo.TabIndex = 14
        Me.btnItemFinderTo.Text = ">"
        Me.btnItemFinderTo.UseCompatibleTextRendering = True
        Me.btnItemFinderTo.UseVisualStyleBackColor = True
        '
        'btnItemFinderFrom
        '
        Me.btnItemFinderFrom.Location = New System.Drawing.Point(634, 23)
        Me.btnItemFinderFrom.Name = "btnItemFinderFrom"
        Me.btnItemFinderFrom.Size = New System.Drawing.Size(25, 21)
        Me.btnItemFinderFrom.TabIndex = 13
        Me.btnItemFinderFrom.Text = ">"
        Me.btnItemFinderFrom.UseCompatibleTextRendering = True
        Me.btnItemFinderFrom.UseVisualStyleBackColor = True
        '
        'cmbGroupTo
        '
        Me.cmbGroupTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbGroupTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbGroupTo.FormattingEnabled = True
        Me.cmbGroupTo.Location = New System.Drawing.Point(50, 50)
        Me.cmbGroupTo.Name = "cmbGroupTo"
        Me.cmbGroupTo.Size = New System.Drawing.Size(113, 21)
        Me.cmbGroupTo.TabIndex = 12
        '
        'cmbGroupFrom
        '
        Me.cmbGroupFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbGroupFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbGroupFrom.FormattingEnabled = True
        Me.cmbGroupFrom.Location = New System.Drawing.Point(50, 23)
        Me.cmbGroupFrom.Name = "cmbGroupFrom"
        Me.cmbGroupFrom.Size = New System.Drawing.Size(113, 21)
        Me.cmbGroupFrom.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 28)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 107
        Me.Label5.Text = "From:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(181, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 13)
        Me.Label7.TabIndex = 107
        Me.Label7.Text = "From:"
        '
        'txtModelTo
        '
        Me.txtModelTo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelTo.Location = New System.Drawing.Point(222, 50)
        Me.txtModelTo.Name = "txtModelTo"
        Me.txtModelTo.ReadOnly = True
        Me.txtModelTo.ShortcutsEnabled = False
        Me.txtModelTo.Size = New System.Drawing.Size(432, 21)
        Me.txtModelTo.TabIndex = 113
        '
        'txtModelFrom
        '
        Me.txtModelFrom.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModelFrom.Location = New System.Drawing.Point(222, 23)
        Me.txtModelFrom.Name = "txtModelFrom"
        Me.txtModelFrom.ReadOnly = True
        Me.txtModelFrom.ShortcutsEnabled = False
        Me.txtModelFrom.Size = New System.Drawing.Size(432, 21)
        Me.txtModelFrom.TabIndex = 110
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(585, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(44, 13)
        Me.Label14.TabIndex = 260
        Me.Label14.Text = "&Branch:"
        '
        'txtSQLpara
        '
        Me.txtSQLpara.Location = New System.Drawing.Point(12, 223)
        Me.txtSQLpara.Name = "txtSQLpara"
        Me.txtSQLpara.ReadOnly = True
        Me.txtSQLpara.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSQLpara.Size = New System.Drawing.Size(671, 21)
        Me.txtSQLpara.TabIndex = 272
        Me.txtSQLpara.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(117, 9)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(78, 13)
        Me.Label8.TabIndex = 262
        Me.Label8.Text = "Summary &Title:"
        '
        'txtSumTitle
        '
        Me.txtSumTitle.Location = New System.Drawing.Point(120, 25)
        Me.txtSumTitle.Name = "txtSumTitle"
        Me.txtSumTitle.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtSumTitle.Size = New System.Drawing.Size(462, 21)
        Me.txtSumTitle.TabIndex = 263
        '
        'txtItemIDTo
        '
        Me.txtItemIDTo.Location = New System.Drawing.Point(335, 52)
        Me.txtItemIDTo.Name = "txtItemIDTo"
        Me.txtItemIDTo.ReadOnly = True
        Me.txtItemIDTo.Size = New System.Drawing.Size(109, 21)
        Me.txtItemIDTo.TabIndex = 271
        Me.txtItemIDTo.Visible = False
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(701, 162)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(115, 23)
        Me.btnClose.TabIndex = 266
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtItemIDFrom
        '
        Me.txtItemIDFrom.Location = New System.Drawing.Point(450, 52)
        Me.txtItemIDFrom.Name = "txtItemIDFrom"
        Me.txtItemIDFrom.ReadOnly = True
        Me.txtItemIDFrom.Size = New System.Drawing.Size(132, 21)
        Me.txtItemIDFrom.TabIndex = 270
        Me.txtItemIDFrom.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(23, 13)
        Me.Label3.TabIndex = 109
        Me.Label3.Text = "To:"
        '
        'btnApply
        '
        Me.btnApply.Location = New System.Drawing.Point(701, 133)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(115, 23)
        Me.btnApply.TabIndex = 265
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbGroup)
        Me.GroupBox3.Controls.Add(Me.btnItemFinderTo)
        Me.GroupBox3.Controls.Add(Me.btnItemFinderFrom)
        Me.GroupBox3.Controls.Add(Me.cmbGroupTo)
        Me.GroupBox3.Controls.Add(Me.txtModelTo)
        Me.GroupBox3.Controls.Add(Me.cmbGroupFrom)
        Me.GroupBox3.Controls.Add(Me.txtModelFrom)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.rbModel)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 111)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(671, 84)
        Me.GroupBox3.TabIndex = 268
        Me.GroupBox3.TabStop = False
        '
        'rbModel
        '
        Me.rbModel.AutoSize = True
        Me.rbModel.Location = New System.Drawing.Point(184, -1)
        Me.rbModel.Name = "rbModel"
        Me.rbModel.Size = New System.Drawing.Size(102, 17)
        Me.rbModel.TabIndex = 12
        Me.rbModel.Text = "Range by &Model"
        Me.rbModel.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(181, 53)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(23, 13)
        Me.Label6.TabIndex = 109
        Me.Label6.Text = "To:"
        '
        'cbVATX
        '
        Me.cbVATX.AutoSize = True
        Me.cbVATX.Location = New System.Drawing.Point(729, 61)
        Me.cbVATX.Name = "cbVATX"
        Me.cbVATX.Size = New System.Drawing.Size(92, 17)
        Me.cbVATX.TabIndex = 264
        Me.cbVATX.Text = "&VAT Exclusive"
        Me.cbVATX.UseVisualStyleBackColor = True
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(588, 25)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(228, 21)
        Me.cmbBranch.TabIndex = 261
        '
        'GroupBox5
        '
        Me.GroupBox5.Location = New System.Drawing.Point(-3, 91)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(850, 2)
        Me.GroupBox5.TabIndex = 269
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "GroupBox5"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(34, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Date:"
        '
        'mtxtTo
        '
        Me.mtxtTo.BeepOnError = True
        Me.mtxtTo.Location = New System.Drawing.Point(12, 25)
        Me.mtxtTo.Mask = "00/00/0000"
        Me.mtxtTo.Name = "mtxtTo"
        Me.mtxtTo.Size = New System.Drawing.Size(102, 21)
        Me.mtxtTo.TabIndex = 217
        Me.mtxtTo.ValidatingType = GetType(Date)
        '
        'frmParaOutstanding
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(828, 209)
        Me.Controls.Add(Me.mtxtTo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtSQLpara)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtSumTitle)
        Me.Controls.Add(Me.txtItemIDTo)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtItemIDFrom)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.cbVATX)
        Me.Controls.Add(Me.cmbBranch)
        Me.Controls.Add(Me.GroupBox5)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmParaOutstanding"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Outstanding Stocks Summary Parameters"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbGroup As System.Windows.Forms.RadioButton
    Friend WithEvents btnItemFinderTo As System.Windows.Forms.Button
    Friend WithEvents btnItemFinderFrom As System.Windows.Forms.Button
    Friend WithEvents cmbGroupTo As System.Windows.Forms.ComboBox
    Friend WithEvents cmbGroupFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtModelTo As System.Windows.Forms.TextBox
    Friend WithEvents txtModelFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtSQLpara As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtSumTitle As System.Windows.Forms.TextBox
    Friend WithEvents txtItemIDTo As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtItemIDFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbModel As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cbVATX As System.Windows.Forms.CheckBox
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents mtxtTo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

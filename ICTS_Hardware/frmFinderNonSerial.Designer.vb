<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinderNonSerial
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnDone = New System.Windows.Forms.Button
        Me.dgvNonSerials = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblTotalSelQty = New System.Windows.Forms.Label
        Me.lblClass = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        CType(Me.dgvNonSerials, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnDone
        '
        Me.btnDone.Location = New System.Drawing.Point(678, 383)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(138, 23)
        Me.btnDone.TabIndex = 22
        Me.btnDone.Text = "&Done"
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'dgvNonSerials
        '
        Me.dgvNonSerials.AllowUserToAddRows = False
        Me.dgvNonSerials.AllowUserToDeleteRows = False
        Me.dgvNonSerials.AllowUserToResizeRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvNonSerials.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvNonSerials.BackgroundColor = System.Drawing.Color.White
        Me.dgvNonSerials.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvNonSerials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNonSerials.Location = New System.Drawing.Point(12, 12)
        Me.dgvNonSerials.MultiSelect = False
        Me.dgvNonSerials.Name = "dgvNonSerials"
        Me.dgvNonSerials.ReadOnly = True
        Me.dgvNonSerials.RowHeadersVisible = False
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvNonSerials.RowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvNonSerials.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNonSerials.Size = New System.Drawing.Size(804, 358)
        Me.dgvNonSerials.TabIndex = 20
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Enabled = False
        Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(12, 388)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(134, 16)
        Me.Label2.TabIndex = 175
        Me.Label2.Text = "Total of selected quantity:"
        Me.Label2.UseCompatibleTextRendering = True
        '
        'lblTotalSelQty
        '
        Me.lblTotalSelQty.AutoSize = True
        Me.lblTotalSelQty.Enabled = False
        Me.lblTotalSelQty.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblTotalSelQty.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalSelQty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblTotalSelQty.Location = New System.Drawing.Point(152, 388)
        Me.lblTotalSelQty.Name = "lblTotalSelQty"
        Me.lblTotalSelQty.Size = New System.Drawing.Size(13, 16)
        Me.lblTotalSelQty.TabIndex = 176
        Me.lblTotalSelQty.Text = "0"
        Me.lblTotalSelQty.UseCompatibleTextRendering = True
        '
        'lblClass
        '
        Me.lblClass.AutoSize = True
        Me.lblClass.Enabled = False
        Me.lblClass.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.lblClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblClass.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblClass.Location = New System.Drawing.Point(368, 388)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(25, 16)
        Me.lblClass.TabIndex = 178
        Me.lblClass.Text = "N/A"
        Me.lblClass.UseCompatibleTextRendering = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Enabled = False
        Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(282, 388)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 177
        Me.Label3.Text = "Selected Class:"
        Me.Label3.UseCompatibleTextRendering = True
        '
        'frmFinderNonSerial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(830, 418)
        Me.Controls.Add(Me.lblClass)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblTotalSelQty)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.dgvNonSerials)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinderNonSerial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Non-Serial Item Finder"
        CType(Me.dgvNonSerials, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents dgvNonSerials As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblTotalSelQty As System.Windows.Forms.Label
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class

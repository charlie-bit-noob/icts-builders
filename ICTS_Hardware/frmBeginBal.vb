Imports System.Data
Imports System.Data.SqlClient

Public Class frmBeginBal

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function IsEmptySourceTable() As Boolean
        Try
            Dim RecCount As Integer

            Me.sqlStr = "SELECT Count(*) AS RecCount FROM ImportPhysicalCount"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()

            RecCount = CInt(Me.sqlCmd.ExecuteScalar)

            If RecCount < 1 Then
                MessageBox.Show("Source table is empty. Please contact your system administrator.", _
                "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                sqlDBconn.Close()
                Return True
            Else
                sqlDBconn.Close()
                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Function IsInventoryDateExists() As Boolean
        Try
            Dim strDateCovered As String = Me.dtDate.Value.ToString("d")

            Me.sqlStr = "SELECT * FROM ActualCount WHERE date_cutoff = '" & strDateCovered & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read() Then
                MessageBox.Show("Inventory date already exists!. Please enter date correctly.", "Stop!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Me.dtDate.Focus()
                Return True
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        If Me.dtDate.Value = Date.Today Or Me.cmbBranch.SelectedIndex < 1 Then Exit Sub

        If Me.IsEmptySourceTable() = True Then
            Me.btnCancel.Focus()
            Exit Sub
        End If

        If Me.IsInventoryDateExists = True Then Exit Sub

        If MessageBox.Show("Click Yes to begin the process and click No to cancel. Please DO NOT make any other " & _
        "task to avoid discrepancy.", "Please Read!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then
            Me.btnCancel.Focus()
            Exit Sub
        End If

        Me.gbMustFilled.Enabled = False
        Me.pnlProgress.Visible = True
        Me.btnApply.Enabled = False
        Me.btnCancel.Enabled = False

        Try
            'Kwaon pila ang number sa rows
            Dim intRowCount As Integer

            Me.btnApply.Text = "Acquiring rows..."
            Me.btnApply.Refresh()

            Me.sqlStr = "SELECT Count(*) AS Result FROM ImportPhysicalCount"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()
            intRowCount = Me.sqlReader("Result")
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.ProgressStatus.Maximum = intRowCount

            Dim intIndexID, intTransID, intIsFree As Integer
            Dim strAFNumber, strSerial, strClass, strAFType, PrevRunID, NextRunID As String
            Dim dtDateIssued As Date
            Dim dobQty As Double
            Dim decCost As Decimal

            For intIndexID = 1 To Me.ProgressStatus.Maximum
                'Fetch each row from table source
                Me.btnApply.Text = "Fetching..."
                Me.btnApply.Refresh()

                Me.sqlStr = "SELECT * FROM ImportPhysicalCount WHERE ID = " & intIndexID & ""
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()

                Me.sqlReader.Read()
                GenAFID = Me.sqlReader("af_id")

                strAFNumber = Me.sqlReader("af_num")
                dtDateIssued = Me.sqlReader("date_issued")

                intTransID = Me.sqlReader("trans_id")

                SelectedItemID = Me.sqlReader("item_id")
                intIsFree = Me.sqlReader("is_free")
                strSerial = Me.sqlReader("serial_num").ToString
                dobQty = Me.sqlReader("qty")
                strClass = Me.sqlReader("class").ToString
                decCost = Me.sqlReader("cost")

                Me.sqlReader.Close()
                sqlDBconn.Close()

                'Get AF table using intAFID
                Me.btnApply.Text = "Get table..."
                Me.btnApply.Refresh()

                Me.sqlStr = "SELECT af_type, short_name FROM AFs WHERE af_id = " & GenAFID & ""
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()

                Me.sqlReader.Read()
                strAFType = Me.sqlReader(0)
                GenShortAF = Me.sqlReader(1)
                Me.sqlReader.Close()
                sqlDBconn.Close()

                'Acquire an order_id to be assigned to the current row
                Me.btnApply.Text = "Acquiring Pop ID..."
                Me.btnApply.Refresh()

                Me.sqlStr = "SELECT order_id FROM AFs" & strAFType & " WHERE branch_id = " & Me.cmbBranch.SelectedIndex & " " & _
                "AND af_id = " & GenAFID & " AND af_num = '" & strAFNumber & "' " & _
                "ORDER BY order_id DESC"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()

                If Me.sqlReader.Read Then
                    GenPopID = Me.sqlReader(0) + 1
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                    GenPopID = 1
                End If

                'Acquire an run_rid to be assigned to the current row
                Me.btnApply.Text = "Acquiring Run ID..."
                Me.btnApply.Refresh()

                Me.sqlStr = "SELECT run_id FROM AFs" & strAFType & " WHERE af_id = " & GenAFID & " " & _
                "AND af_num = '" & strAFNumber & "' AND run_id IS NOT NULL"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader()

                If Me.sqlReader.Read Then
                    GenRunID = Me.sqlReader(0)
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()

                    Me.sqlStr = "SELECT TOP 1 run_id FROM AFs" & strAFType & " WHERE af_id = " & GenAFID & " " & _
                    "AND run_id IS NOT NULL ORDER BY run_id DESC"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader

                    If Me.sqlReader.Read Then
                        PrevRunID = Me.sqlReader(0).ToString.Remove(0, GenShortAF.Length)
                        NextRunID = GenShortAF + Format(Val(PrevRunID) + 1, "000000000000")

                        GenRunID = NextRunID

                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        GenRunID = GenShortAF + "000000000001"

                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    End If
                End If

                'Insert to ActualCount table
                Me.btnApply.Text = "Saving to AC..."
                Me.btnApply.Refresh()

                Me.sqlStr = "INSERT INTO ActualCount(date_cutoff, branch_id, af_id, af_num, date_purchased, item_id, " & _
                "qty, serial_num, class, is_free, cost, ua_id)" & _
                "VALUES('" & Me.dtDate.Value & "', " & Me.cmbBranch.SelectedIndex & ", " & GenAFID & ", '" & strAFNumber & "', '" & dtDateIssued & "', '" & SelectedItemID & "', " & _
                "" & dobQty & ", '" & strSerial & "', '" & strClass & "', " & intIsFree & ", " & decCost & ", " & MDI.stbUserID.Text & ")"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                'Insert to AF table
                Me.btnApply.Text = "Acquiring Root ID..."
                Me.btnApply.Refresh()
                'Acquire root_id to be assigned
                AutoGenRootID()

                Me.btnApply.Text = "Updating RootBank..."
                Me.btnApply.Refresh()
                'Update BankRootID with the Acquired root_id
                Me.sqlStr = "INSERT INTO BankRootID(root_id) VALUES('" & GenRootID & "')"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.btnApply.Text = "Updating Stocks..."
                Me.btnApply.Refresh()
                If strClass <> "" Then
                    Me.sqlStr = "UPDATE StockFragile " & _
                    "SET [" & Me.cmbBranch.SelectedIndex & "] = ([" & Me.cmbBranch.SelectedIndex & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & SelectedItemID & "' AND class = '" & strClass & "'"
                Else
                    Me.sqlStr = "UPDATE StockDurable " & _
                    "SET [" & Me.cmbBranch.SelectedIndex & "] = ([" & Me.cmbBranch.SelectedIndex & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & SelectedItemID & "'"
                End If

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.btnApply.Text = "Updating IM..."
                Me.btnApply.Refresh()
                'Update Cost in ItemMasterlist
                Me.sqlStr = "UPDATE ItemMasterlist SET cost = " & decCost & ", ua_id = " & MDI.stbUserID.Text & ", date_updated = GETDATE() " & _
                "WHERE item_id = '" & SelectedItemID & "'"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.btnApply.Text = "Saving to AFs..."
                Me.btnApply.Refresh()
                'Insert to In if AF is Incoming
                If strAFType = "In" Then
                    Me.sqlStr = "INSERT INTO AFsIn(root_id, run_id, order_id, item_id, class, is_free, qty, cost, " & _
                    "date_issued, branch_id, af_id, af_num, trans_id, cust_id, supplier_id, ua_id)" & _
                    "VALUES('" & GenRootID & "', '" & GenRunID & "', " & GenPopID & ", '" & SelectedItemID & "', '" & strClass & "', " & intIsFree & ", " & dobQty & ", " & decCost & ", " & _
                    "'" & dtDateIssued & "', " & Me.cmbBranch.SelectedIndex & ", " & GenAFID & ", '" & strAFNumber & "', " & intTransID & ", '', 0, " & MDI.stbUserID.Text & ")"
                Else
                    Me.sqlStr = "INSERT INTO AFsTransfer(root_id, run_id, order_id, item_id, class, is_free, qty, cost, " & _
                    "date_issued, src_branch_id, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, ua_id)" & _
                    "VALUES('" & GenRootID & "', '" & GenRunID & "', " & GenPopID & ", '" & SelectedItemID & "', '" & strClass & "', " & intIsFree & ", " & dobQty & ", " & decCost & ", " & _
                    "'" & dtDateIssued & "', 0, " & Me.cmbBranch.SelectedIndex & ", 0, '', " & GenAFID & ", '" & strAFNumber & "', " & intTransID & ", " & MDI.stbUserID.Text & ")"
                End If

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.btnApply.Text = "Saving to TT..."
                Me.btnApply.Refresh()

                If strSerial <> "" Then
                    Me.sqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, " & _
                    "ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id)" & _
                    "VALUES('" & GenRootID & "', '" & GenRunID & "', " & GenPopID & ", '" & SelectedItemID & "', " & intIsFree & ", '" & strSerial & "', " & decCost & ", " & _
                    "0, '', " & GenAFID & ", '" & strAFNumber & "', " & Me.cmbBranch.SelectedIndex & ", '" & dtDateIssued & "', " & intTransID & ")"
                Else
                    Me.sqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, qty_in, " & _
                    "qty_bal, ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id)" & _
                    "VALUES('" & GenRootID & "', '" & GenRunID & "', " & GenPopID & ", '" & SelectedItemID & "', " & intIsFree & ", '" & strClass & "', " & decCost & ", " & dobQty & ", " & _
                    "" & dobQty & ", 0, '', " & GenAFID & ", '" & strAFNumber & "', " & Me.cmbBranch.SelectedIndex & ", '" & dtDateIssued & "', " & intTransID & ")"
                End If
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr

                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                'Save Remarks to table
                Me.btnApply.Text = "Saving Remarks..."
                Me.btnApply.Refresh()
                'Insert if not exist
                Me.sqlStr = "SELECT * FROM AFsRemarks WHERE branch_id = " & Me.cmbBranch.SelectedIndex & " " & _
                "AND af_id = " & GenAFID & " AND af_num = '" & strAFNumber & "'"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader

                If Me.sqlReader.Read Then
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()

                    Me.sqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks)" & _
                    "VALUES('" & GenRunID & "', " & Me.cmbBranch.SelectedIndex & ", '" & dtDateIssued & "', " & GenAFID & ", '" & strAFNumber & "', '" & Me.txtRemarks.Text & "')"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                End If

                Me.ProgressStatus.Value = intIndexID
                Me.lblPrgressStatus.Text = Int(Me.ProgressStatus.Value * 100 / Me.ProgressStatus.Maximum).ToString + " % Completed"
                Me.lblPrgressStatus.Refresh()
            Next

            Me.gbMustFilled.Enabled = True
            Me.btnApply.Text = "&Apply"
            Me.btnApply.Enabled = True
            Me.btnCancel.Text = "E&xit"
            Me.btnCancel.Enabled = True

            'MessageBox.Show("Beginning Balance has been successfully applied!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()
            Me.btnCancel.Enabled = True
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
    End Sub

    Private Sub dtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles dtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbBranch.Focus()
    End Sub

    Private Sub FillBranch()
        Try
            Me.sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            Me.sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While Me.sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmBeginBal_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.sqlCmd = Nothing
        Me.sqlReader = Nothing
        Me.sqlStr = Nothing
    End Sub

    Private Sub frmBeginBal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FillBranch()
        Me.dtDate.Value = Date.Today
        Me.dtDate.Focus()
    End Sub
End Class
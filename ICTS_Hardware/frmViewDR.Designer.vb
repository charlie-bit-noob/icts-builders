<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewDR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtCustomer = New System.Windows.Forms.TextBox
        Me.txtBranch = New System.Windows.Forms.TextBox
        Me.txtTransaction = New System.Windows.Forms.TextBox
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgvView = New System.Windows.Forms.DataGridView
        Me.btnclose = New System.Windows.Forms.Button
        Me.txtCustID = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtOffice = New System.Windows.Forms.TextBox
        Me.lblstockBal = New System.Windows.Forms.Label
        Me.lbldesc = New System.Windows.Forms.Label
        Me.cbVATXview = New System.Windows.Forms.CheckBox
        Me.txtTotal = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmDR = New System.Windows.Forms.ContextMenu
        Me.mnuSendtoPUR = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuSendtoCI = New System.Windows.Forms.MenuItem
        Me.mnuSendtoCA = New System.Windows.Forms.MenuItem
        Me.btnMakeChanges = New System.Windows.Forms.Button
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtRemarks
        '
        Me.txtRemarks.BackColor = System.Drawing.SystemColors.Control
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.Location = New System.Drawing.Point(83, 96)
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ReadOnly = True
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(549, 21)
        Me.txtRemarks.TabIndex = 230
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(9, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 231
        Me.Label4.Text = "Remarks:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(8, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 229
        Me.Label2.Text = "Transaction:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(304, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 228
        Me.Label3.Text = "Branch:"
        '
        'txtCustomer
        '
        Me.txtCustomer.BackColor = System.Drawing.SystemColors.Control
        Me.txtCustomer.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCustomer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustomer.Location = New System.Drawing.Point(467, 42)
        Me.txtCustomer.MaxLength = 100
        Me.txtCustomer.Name = "txtCustomer"
        Me.txtCustomer.ReadOnly = True
        Me.txtCustomer.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCustomer.Size = New System.Drawing.Size(298, 21)
        Me.txtCustomer.TabIndex = 246
        '
        'txtBranch
        '
        Me.txtBranch.BackColor = System.Drawing.SystemColors.Control
        Me.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBranch.Location = New System.Drawing.Point(380, 15)
        Me.txtBranch.Name = "txtBranch"
        Me.txtBranch.ReadOnly = True
        Me.txtBranch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBranch.Size = New System.Drawing.Size(290, 21)
        Me.txtBranch.TabIndex = 244
        '
        'txtTransaction
        '
        Me.txtTransaction.BackColor = System.Drawing.SystemColors.Control
        Me.txtTransaction.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransaction.Location = New System.Drawing.Point(83, 69)
        Me.txtTransaction.Name = "txtTransaction"
        Me.txtTransaction.ReadOnly = True
        Me.txtTransaction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTransaction.Size = New System.Drawing.Size(194, 21)
        Me.txtTransaction.TabIndex = 242
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.SystemColors.Control
        Me.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(83, 42)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDate.Size = New System.Drawing.Size(194, 21)
        Me.txtDate.TabIndex = 241
        '
        'txtNumber
        '
        Me.txtNumber.BackColor = System.Drawing.SystemColors.Control
        Me.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(83, 15)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.ReadOnly = True
        Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNumber.Size = New System.Drawing.Size(194, 21)
        Me.txtNumber.TabIndex = 240
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvHistory.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Location = New System.Drawing.Point(12, 339)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHistory.Size = New System.Drawing.Size(754, 169)
        Me.dgvHistory.TabIndex = 222
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-1, 321)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(779, 2)
        Me.GroupBox1.TabIndex = 238
        Me.GroupBox1.TabStop = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 226
        Me.Label9.Text = "Number:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(304, 45)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 13)
        Me.Label6.TabIndex = 224
        Me.Label6.Text = "Delivered to:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 223
        Me.Label1.Text = "Date:"
        '
        'dgvView
        '
        Me.dgvView.AllowUserToAddRows = False
        Me.dgvView.AllowUserToDeleteRows = False
        Me.dgvView.AllowUserToResizeRows = False
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvView.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvView.Location = New System.Drawing.Point(11, 123)
        Me.dgvView.MultiSelect = False
        Me.dgvView.Name = "dgvView"
        Me.dgvView.ReadOnly = True
        Me.dgvView.RowHeadersVisible = False
        Me.dgvView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvView.Size = New System.Drawing.Size(755, 149)
        Me.dgvView.TabIndex = 221
        '
        'btnclose
        '
        Me.btnclose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnclose.Location = New System.Drawing.Point(12, 166)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(59, 21)
        Me.btnclose.TabIndex = 227
        Me.btnclose.TabStop = False
        Me.btnclose.Text = "Close"
        Me.btnclose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'txtCustID
        '
        Me.txtCustID.BackColor = System.Drawing.SystemColors.Control
        Me.txtCustID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCustID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCustID.Location = New System.Drawing.Point(380, 42)
        Me.txtCustID.Name = "txtCustID"
        Me.txtCustID.ReadOnly = True
        Me.txtCustID.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtCustID.Size = New System.Drawing.Size(81, 21)
        Me.txtCustID.TabIndex = 245
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(206, 293)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 14)
        Me.Label7.TabIndex = 248
        Me.Label7.Text = "To office:"
        '
        'txtOffice
        '
        Me.txtOffice.BackColor = System.Drawing.SystemColors.Control
        Me.txtOffice.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtOffice.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOffice.Location = New System.Drawing.Point(281, 290)
        Me.txtOffice.Name = "txtOffice"
        Me.txtOffice.ReadOnly = True
        Me.txtOffice.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOffice.Size = New System.Drawing.Size(252, 22)
        Me.txtOffice.TabIndex = 249
        '
        'lblstockBal
        '
        Me.lblstockBal.BackColor = System.Drawing.SystemColors.Control
        Me.lblstockBal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblstockBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstockBal.Location = New System.Drawing.Point(67, 290)
        Me.lblstockBal.Name = "lblstockBal"
        Me.lblstockBal.Size = New System.Drawing.Size(60, 21)
        Me.lblstockBal.TabIndex = 259
        Me.lblstockBal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbldesc
        '
        Me.lbldesc.AutoSize = True
        Me.lbldesc.BackColor = System.Drawing.SystemColors.Control
        Me.lbldesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldesc.Location = New System.Drawing.Point(7, 294)
        Me.lbldesc.Name = "lbldesc"
        Me.lbldesc.Size = New System.Drawing.Size(54, 13)
        Me.lbldesc.TabIndex = 258
        Me.lbldesc.Text = "Balance:"
        '
        'cbVATXview
        '
        Me.cbVATXview.AutoSize = True
        Me.cbVATXview.Location = New System.Drawing.Point(673, 98)
        Me.cbVATXview.Name = "cbVATXview"
        Me.cbVATXview.Size = New System.Drawing.Size(93, 17)
        Me.cbVATXview.TabIndex = 257
        Me.cbVATXview.Text = "&Non-VAT view"
        Me.cbVATXview.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.SystemColors.Control
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(590, 291)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTotal.Size = New System.Drawing.Size(176, 21)
        Me.txtTotal.TabIndex = 261
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.SystemColors.Control
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(539, 292)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 16)
        Me.Label5.TabIndex = 260
        Me.Label5.Text = "Total:"
        '
        'cmDR
        '
        Me.cmDR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSendtoPUR, Me.MenuItem1, Me.mnuSendtoCI, Me.mnuSendtoCA})
        '
        'mnuSendtoPUR
        '
        Me.mnuSendtoPUR.Index = 0
        Me.mnuSendtoPUR.Text = "Send to PU&R"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 1
        Me.MenuItem1.Text = "-"
        '
        'mnuSendtoCI
        '
        Me.mnuSendtoCI.Index = 2
        Me.mnuSendtoCI.Text = "Send to C&I"
        '
        'mnuSendtoCA
        '
        Me.mnuSendtoCA.Index = 3
        Me.mnuSendtoCA.Text = "Send to C&A"
        '
        'btnMakeChanges
        '
        Me.btnMakeChanges.Location = New System.Drawing.Point(676, 12)
        Me.btnMakeChanges.Name = "btnMakeChanges"
        Me.btnMakeChanges.Size = New System.Drawing.Size(90, 23)
        Me.btnMakeChanges.TabIndex = 265
        Me.btnMakeChanges.Text = "&Make Changes"
        Me.btnMakeChanges.UseVisualStyleBackColor = True
        '
        'frmViewDR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 522)
        Me.Controls.Add(Me.btnMakeChanges)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblstockBal)
        Me.Controls.Add(Me.lbldesc)
        Me.Controls.Add(Me.cbVATXview)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtOffice)
        Me.Controls.Add(Me.txtRemarks)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCustomer)
        Me.Controls.Add(Me.txtBranch)
        Me.Controls.Add(Me.txtTransaction)
        Me.Controls.Add(Me.txtDate)
        Me.Controls.Add(Me.txtNumber)
        Me.Controls.Add(Me.dgvHistory)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvView)
        Me.Controls.Add(Me.btnclose)
        Me.Controls.Add(Me.txtCustID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmViewDR"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Delivery Receipt"
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCustomer As System.Windows.Forms.TextBox
    Friend WithEvents txtBranch As System.Windows.Forms.TextBox
    Friend WithEvents txtTransaction As System.Windows.Forms.TextBox
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvView As System.Windows.Forms.DataGridView
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents txtCustID As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtOffice As System.Windows.Forms.TextBox
    Friend WithEvents lblstockBal As System.Windows.Forms.Label
    Friend WithEvents lbldesc As System.Windows.Forms.Label
    Friend WithEvents cbVATXview As System.Windows.Forms.CheckBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmDR As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuSendtoPUR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSendtoCI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSendtoCA As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents btnMakeChanges As System.Windows.Forms.Button
End Class

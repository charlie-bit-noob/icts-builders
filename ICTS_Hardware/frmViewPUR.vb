Imports System.Data
Imports System.Data.SqlClient

Public Class frmViewPUR

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_run_id As DataColumn = New DataColumn("run_id")
        col_run_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_run_id)

        Dim col_root_id As DataColumn = New DataColumn("root_id")
        col_root_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_root_id)

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_item_id As DataColumn = New DataColumn("item_id")
        col_item_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_item_id)

        Dim col_ref_af As DataColumn = New DataColumn("ref_af")
        col_ref_af.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_ref_af)

        Dim col_ref_af_num As DataColumn = New DataColumn("ref_af_num")
        col_ref_af_num.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_ref_af_num)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("desc")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_class)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_qty As DataColumn = New DataColumn("qty")
        col_qty.DataType = System.Type.GetType("System.Double")
        TableTemp.Columns.Add(col_qty)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_unit)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        Dim col_user As DataColumn = New DataColumn("user")
        col_user.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_user)

        Dim col_datepost As DataColumn = New DataColumn("datepost")
        col_datepost.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_datepost)

        TableTemp.DefaultView.Sort = "pop_id ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnMakeChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeChanges.Click
        My.Forms.frmAFChanges.strNumber = Me.txtNumber.Text

        My.Forms.frmAFChanges.frmList = My.Forms.frmlistPUR
        My.Forms.frmAFChanges.frmView = Me
        My.Forms.frmAFChanges.ShowDialog()
    End Sub

    Private Sub cbVATXview_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbVATXview.CheckStateChanged
        Try
            If Me.dgvView.RowCount < 1 Then Exit Sub
            If Me.cbVATXview.CheckState = CheckState.Checked Then
                VATXview(Me.dgvView, Me.txtTotal)
            Else
                Me.VATview()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvHistory_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvHistory.CellDoubleClick
        ViewThis(Me, Me.dgvHistory)
    End Sub

    Private Sub dgvView_CellMouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvView.CellMouseClick
        If Me.dgvView.RowCount < 1 Then Exit Sub
        If e.Button = Windows.Forms.MouseButtons.Right Then Me.dgvView.CurrentCell = Me.dgvView.Rows(e.RowIndex).Cells(e.ColumnIndex)
        GenHistory(Me.txtNumber.Text, Me.dgvView, Me.dgvHistory, Me.lblstockBal)
    End Sub

    Private Sub dgvView_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvView.MouseUp
        Dim indexPos As Integer = Me.dgvView.CurrentCell.RowIndex
        Dim itemID As String = Me.dgvView.Item("item_id", indexPos).Value.ToString

        If e.Button = Windows.Forms.MouseButtons.Right And Not itemID = "" Then Me.cmCost.Show(Me.dgvView, e.Location, LeftRightAlignment.Right)
    End Sub

    Private Sub FillAFdata()
        Try
            TempTable = MakeDataTableTemp()

            Me.sqlStr = "SELECT run_id, branch_id, af_id, date_issued, " & _
            "(SELECT " & GenShortAF & " FROM Transactions WHERE trans_id = af.trans_id) AS af_trans, " & _
            "(SELECT remarks FROM AFsRemarks WHERE run_id = af.run_id AND branch_id = af.branch_id AND af_id = af.af_id AND af_num = '" & Me.txtNumber.Text & "') AS af_remarks, " & _
            "cust_id, af_cust = CASE " & _
            "WHEN cust_id <> '' THEN (SELECT first_name + ' ' + last_name + company_name AS flc FROM Customers WHERE cust_id = af.cust_id) " & _
            "WHEN cust_id = '' THEN non_cust " & _
            "End " & _
            "FROM AFsIn AS af WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' AND branch_id = " & GenBranchID & ""

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()

            Me.txtDate.Text = Format(Me.sqlReader("date_issued"), "MMMM dd, yyyy")
            Me.txtTransaction.Text = Me.sqlReader("af_trans")
            Me.txtRemarks.Text = Me.sqlReader("af_remarks")
            Me.txtCustID.Text = Me.sqlReader("cust_id").ToString
            Me.txtSource.Text = Me.sqlReader("af_cust").ToString

            Me.sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            Me.dgvView.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvView()
        Try
            TempTable = MakeDataTableTemp()

            sqlStr = "SELECT run_id, root_id, order_id, date_issued, af_id, af_num, item_id, " & _
            "(SELECT short_name FROM AFs WHERE af_id = af.ref_af_id) AS af_ref_af, ref_af_num, " & _
            "(SELECT brand FROM ItemMasterlist AS im WHERE im.item_id = af.item_id) AS af_brand, " & _
            "(SELECT product FROM ItemMasterlist AS im1 WHERE im1.item_id = af.item_id) AS af_product, " & _
            "(SELECT model FROM ItemMasterlist AS im2 WHERE im2.item_id = af.item_id) AS af_model, " & _
            "(SELECT description FROM ItemMasterlist AS im3 WHERE im3.item_id = af.item_id) AS af_desc, class, " & _
            "is_free, (SELECT serial_num FROM TypeSerial WHERE item_id = af.item_id AND af_id = af.af_id " & _
            "AND af_num = af.af_num AND date_issued = af.date_issued AND order_id = af.order_id) AS af_serial, qty, " & _
            "(SELECT units FROM Library WHERE lib_id = (SELECT unit_lib_id FROM ItemMasterlist WHERE item_id = af.item_id)) AS af_unit, " & _
            "cost, ua_id, (SELECT username FROM UserAccounts WHERE ua_id = af.ua_id) AS af_user, date_updated FROM AFsIn AS af " & _
            "WHERE branch_id = " & GenBranchID & " AND af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "'"

            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("run_id") = sqlReader("run_id")
                Row("root_id") = sqlReader("root_id")
                Row("pop_id") = sqlReader("order_id")
                Row("item_id") = sqlReader("item_id")
                Row("ref_af") = sqlReader("af_ref_af")
                Row("ref_af_num") = sqlReader("ref_af_num")
                Row("brand") = sqlReader("af_brand")
                Row("product") = sqlReader("af_product")
                Row("model") = sqlReader("af_model")
                Row("desc") = sqlReader("af_desc")
                Row("class") = sqlReader("class")
                Row("free") = sqlReader("is_free")
                Row("serial") = sqlReader("af_serial")
                Row("qty") = sqlReader("qty")
                Row("unit") = sqlReader("af_unit")
                Row("cost") = sqlReader("cost")
                Row("user") = sqlReader("af_user")
                Row("datepost") = sqlReader("date_updated")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "pop_id ASC"
            dgvView.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmViewPUR_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.LoadDateAndRefresh()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub LoadDateAndRefresh()
        GetAFID(Me.Text.ToUpper)
        GetBranchID(Me.txtBranch.Text)
        GetShortAF(Me.Text.ToUpper)

        FillAFdata()
        FilldgvView()
        dgvViewConfig(Me.dgvView)
        ComputeCost(Me.dgvView, Me.txtTotal)
    End Sub

    Private Sub mnuUpdateCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUpdateCost.Click
        Try
            Dim strInputB As String = InputBox("Cost:", "To cancel, input nothing", "0.000")
            Dim decCost As Decimal

            Convert.ToDecimal(strInputB.Replace(",", ""))

            If IsNumeric(strInputB) Then
                decCost = strInputB
            Else
                MessageBox.Show("The value entered is invalid. No changes took place.", _
                "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            If MessageBox.Show("Are you sure the value is correct?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Me.UpdateItemCost(decCost)

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateItemCost(ByVal cost As Decimal)
        Try
            sqlStr = "UPDATE TOP (1) AFsIn SET cost = " & cost & ", " & _
            "ua_id = " & MDI.stbUserID.Text & ", date_updated = getdate() " & _
            "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' AND " & _
            "branch_id = " & GenBranchID & " AND item_id = '" & SelectedItemID & "' " & _
            "AND order_id = " & Me.dgvView.Item("pop_id", Me.dgvView.CurrentCell.RowIndex).Value & ""

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr
            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            If Me.dgvView.Item("serial", Me.dgvView.CurrentCell.RowIndex).Value = "" Then

                Me.sqlStr = "UPDATE TypeNonSerial " & _
                "SET cost = " & cost & " " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' AND " & _
                "branch_id = " & GenBranchID & " AND item_id = '" & SelectedItemID & "' " & _
                "AND order_id = " & Me.dgvView.Item("pop_id", Me.dgvView.CurrentCell.RowIndex).Value & ""

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                Me.sqlStr = "UPDATE TypeSerial " & _
                "SET cost = " & cost & " " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' AND " & _
                "branch_id = " & GenBranchID & " AND item_id = '" & SelectedItemID & "' " & _
                "AND serial_num = '" & Me.dgvView.Item("serial", Me.dgvView.CurrentCell.RowIndex).Value & "'"

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            End If

            MessageBox.Show("New unit cost successfully applied!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub VATview()
        Try
            Me.FilldgvView()
            ComputeCost(Me.dgvView, Me.txtTotal)
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMakeRR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.rbCancel = New System.Windows.Forms.RadioButton
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.btnNew = New System.Windows.Forms.Button
        Me.txtBalance = New System.Windows.Forms.TextBox
        Me.btnPop = New System.Windows.Forms.Button
        Me.btnDone = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.dgvMake = New System.Windows.Forms.DataGridView
        Me.Label5 = New System.Windows.Forms.Label
        Me.gbGiven = New System.Windows.Forms.GroupBox
        Me.txtFreight = New System.Windows.Forms.TextBox
        Me.txtTotalAmount = New System.Windows.Forms.TextBox
        Me.btnCompute = New System.Windows.Forms.Button
        Me.Label11 = New System.Windows.Forms.Label
        Me.lbltotalInAmt = New System.Windows.Forms.Label
        Me.txtArrastre = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtDiscount = New System.Windows.Forms.TextBox
        Me.txtAmountPerModel = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.lblNumber = New System.Windows.Forms.Label
        Me.pnlAFDetails = New System.Windows.Forms.Panel
        Me.cmbTransaction = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.mtxtDate = New System.Windows.Forms.MaskedTextBox
        Me.cmbSupplier = New System.Windows.Forms.ComboBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.pnlNonItem = New System.Windows.Forms.Panel
        Me.btnNewModel = New System.Windows.Forms.Button
        Me.pnlItemDetails = New System.Windows.Forms.Panel
        Me.cbFreeItem = New System.Windows.Forms.CheckBox
        Me.btnCaptureItem = New System.Windows.Forms.Button
        Me.btnItemFinder = New System.Windows.Forms.Button
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtUnit = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtSerialNumber = New System.Windows.Forms.TextBox
        Me.txtQuantity = New System.Windows.Forms.TextBox
        Me.cmbClass = New System.Windows.Forms.ComboBox
        Me.txtItemName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtItemID = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        CType(Me.dgvMake, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbGiven.SuspendLayout()
        Me.pnlAFDetails.SuspendLayout()
        Me.pnlNonItem.SuspendLayout()
        Me.pnlItemDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbCancel
        '
        Me.rbCancel.AutoSize = True
        Me.rbCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rbCancel.Location = New System.Drawing.Point(76, 3)
        Me.rbCancel.Name = "rbCancel"
        Me.rbCancel.Size = New System.Drawing.Size(71, 17)
        Me.rbCancel.TabIndex = 0
        Me.rbCancel.Text = "C&ancelled"
        Me.rbCancel.UseVisualStyleBackColor = True
        '
        'txtCost
        '
        Me.txtCost.BackColor = System.Drawing.SystemColors.Control
        Me.txtCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCost.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCost.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtCost.Location = New System.Drawing.Point(750, 374)
        Me.txtCost.MaxLength = 12
        Me.txtCost.Name = "txtCost"
        Me.txtCost.ReadOnly = True
        Me.txtCost.Size = New System.Drawing.Size(214, 22)
        Me.txtCost.TabIndex = 6
        Me.txtCost.TabStop = False
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(687, 377)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(39, 14)
        Me.Label15.TabIndex = 5
        Me.Label15.Text = "Cost:"
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Location = New System.Drawing.Point(450, 473)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(98, 23)
        Me.btnNew.TabIndex = 10
        Me.btnNew.Text = "&New"
        Me.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'txtBalance
        '
        Me.txtBalance.BackColor = System.Drawing.SystemColors.Control
        Me.txtBalance.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBalance.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtBalance.Location = New System.Drawing.Point(750, 402)
        Me.txtBalance.MaxLength = 12
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.ReadOnly = True
        Me.txtBalance.Size = New System.Drawing.Size(214, 22)
        Me.txtBalance.TabIndex = 8
        Me.txtBalance.TabStop = False
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnPop
        '
        Me.btnPop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPop.Location = New System.Drawing.Point(554, 473)
        Me.btnPop.Name = "btnPop"
        Me.btnPop.Size = New System.Drawing.Size(98, 23)
        Me.btnPop.TabIndex = 13
        Me.btnPop.Text = "&Pop in"
        Me.btnPop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPop.UseVisualStyleBackColor = True
        '
        'btnDone
        '
        Me.btnDone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDone.Location = New System.Drawing.Point(658, 473)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(98, 23)
        Me.btnDone.TabIndex = 14
        Me.btnDone.Text = "&Done"
        Me.btnDone.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(762, 473)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(98, 23)
        Me.btnSave.TabIndex = 15
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(866, 473)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(98, 23)
        Me.btnClose.TabIndex = 16
        Me.btnClose.Text = "&Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvMake
        '
        Me.dgvMake.AllowUserToAddRows = False
        Me.dgvMake.AllowUserToDeleteRows = False
        Me.dgvMake.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvMake.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMake.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvMake.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvMake.Location = New System.Drawing.Point(12, 74)
        Me.dgvMake.MultiSelect = False
        Me.dgvMake.Name = "dgvMake"
        Me.dgvMake.ReadOnly = True
        Me.dgvMake.RowHeadersVisible = False
        Me.dgvMake.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMake.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMake.Size = New System.Drawing.Size(952, 166)
        Me.dgvMake.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(687, 405)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 14)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Balance:"
        '
        'gbGiven
        '
        Me.gbGiven.Controls.Add(Me.txtFreight)
        Me.gbGiven.Controls.Add(Me.txtTotalAmount)
        Me.gbGiven.Controls.Add(Me.btnCompute)
        Me.gbGiven.Controls.Add(Me.Label11)
        Me.gbGiven.Controls.Add(Me.lbltotalInAmt)
        Me.gbGiven.Controls.Add(Me.txtArrastre)
        Me.gbGiven.Controls.Add(Me.Label17)
        Me.gbGiven.Controls.Add(Me.Label18)
        Me.gbGiven.Controls.Add(Me.txtDiscount)
        Me.gbGiven.Controls.Add(Me.txtAmountPerModel)
        Me.gbGiven.Controls.Add(Me.Label20)
        Me.gbGiven.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbGiven.Location = New System.Drawing.Point(12, 321)
        Me.gbGiven.Name = "gbGiven"
        Me.gbGiven.Size = New System.Drawing.Size(653, 115)
        Me.gbGiven.TabIndex = 3
        Me.gbGiven.TabStop = False
        '
        'txtFreight
        '
        Me.txtFreight.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFreight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFreight.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFreight.Location = New System.Drawing.Point(247, 33)
        Me.txtFreight.MaxLength = 12
        Me.txtFreight.Name = "txtFreight"
        Me.txtFreight.Size = New System.Drawing.Size(185, 21)
        Me.txtFreight.TabIndex = 3
        Me.txtFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTotalAmount.Location = New System.Drawing.Point(18, 33)
        Me.txtTotalAmount.MaxLength = 12
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.Size = New System.Drawing.Size(223, 21)
        Me.txtTotalAmount.TabIndex = 1
        Me.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnCompute
        '
        Me.btnCompute.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompute.Location = New System.Drawing.Point(438, 78)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(202, 23)
        Me.btnCompute.TabIndex = 10
        Me.btnCompute.Text = "C&ompute"
        Me.btnCompute.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(15, 62)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Arrastre:"
        '
        'lbltotalInAmt
        '
        Me.lbltotalInAmt.AutoSize = True
        Me.lbltotalInAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotalInAmt.Location = New System.Drawing.Point(15, 17)
        Me.lbltotalInAmt.Name = "lbltotalInAmt"
        Me.lbltotalInAmt.Size = New System.Drawing.Size(75, 13)
        Me.lbltotalInAmt.TabIndex = 0
        Me.lbltotalInAmt.Text = "Total Amount:"
        '
        'txtArrastre
        '
        Me.txtArrastre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtArrastre.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArrastre.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtArrastre.Location = New System.Drawing.Point(18, 78)
        Me.txtArrastre.MaxLength = 12
        Me.txtArrastre.Name = "txtArrastre"
        Me.txtArrastre.Size = New System.Drawing.Size(223, 21)
        Me.txtArrastre.TabIndex = 7
        Me.txtArrastre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(244, 17)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Freight:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(435, 17)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Discount:"
        '
        'txtDiscount
        '
        Me.txtDiscount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiscount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiscount.Location = New System.Drawing.Point(438, 33)
        Me.txtDiscount.MaxLength = 12
        Me.txtDiscount.Name = "txtDiscount"
        Me.txtDiscount.Size = New System.Drawing.Size(202, 21)
        Me.txtDiscount.TabIndex = 5
        Me.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmountPerModel
        '
        Me.txtAmountPerModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtAmountPerModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountPerModel.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtAmountPerModel.Location = New System.Drawing.Point(247, 78)
        Me.txtAmountPerModel.MaxLength = 12
        Me.txtAmountPerModel.Name = "txtAmountPerModel"
        Me.txtAmountPerModel.Size = New System.Drawing.Size(185, 21)
        Me.txtAmountPerModel.TabIndex = 9
        Me.txtAmountPerModel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(244, 62)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(98, 13)
        Me.Label20.TabIndex = 8
        Me.Label20.Text = "Amount Per Model:"
        '
        'txtNumber
        '
        Me.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumber.Location = New System.Drawing.Point(496, 37)
        Me.txtNumber.MaxLength = 10
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNumber.Size = New System.Drawing.Size(120, 21)
        Me.txtNumber.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Supplier:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(627, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Remarks:"
        '
        'txtRemarks
        '
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRemarks.Location = New System.Drawing.Point(685, 36)
        Me.txtRemarks.MaxLength = 100
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(286, 21)
        Me.txtRemarks.TabIndex = 11
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(630, 9)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(341, 21)
        Me.cmbBranch.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(580, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Branch:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(278, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Date:"
        '
        'lblNumber
        '
        Me.lblNumber.AutoSize = True
        Me.lblNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumber.Location = New System.Drawing.Point(442, 40)
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Size = New System.Drawing.Size(48, 13)
        Me.lblNumber.TabIndex = 8
        Me.lblNumber.Text = "Number:"
        '
        'pnlAFDetails
        '
        Me.pnlAFDetails.Controls.Add(Me.cmbTransaction)
        Me.pnlAFDetails.Controls.Add(Me.Label10)
        Me.pnlAFDetails.Controls.Add(Me.Label4)
        Me.pnlAFDetails.Controls.Add(Me.txtNumber)
        Me.pnlAFDetails.Controls.Add(Me.mtxtDate)
        Me.pnlAFDetails.Controls.Add(Me.txtRemarks)
        Me.pnlAFDetails.Controls.Add(Me.cmbSupplier)
        Me.pnlAFDetails.Controls.Add(Me.lblNumber)
        Me.pnlAFDetails.Controls.Add(Me.cmbBranch)
        Me.pnlAFDetails.Controls.Add(Me.Label3)
        Me.pnlAFDetails.Controls.Add(Me.Label1)
        Me.pnlAFDetails.Controls.Add(Me.Label9)
        Me.pnlAFDetails.Location = New System.Drawing.Point(-7, 3)
        Me.pnlAFDetails.Name = "pnlAFDetails"
        Me.pnlAFDetails.Size = New System.Drawing.Size(990, 65)
        Me.pnlAFDetails.TabIndex = 0
        '
        'cmbTransaction
        '
        Me.cmbTransaction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbTransaction.FormattingEnabled = True
        Me.cmbTransaction.Location = New System.Drawing.Point(89, 37)
        Me.cmbTransaction.Name = "cmbTransaction"
        Me.cmbTransaction.Size = New System.Drawing.Size(183, 21)
        Me.cmbTransaction.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(16, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Transaction:"
        '
        'mtxtDate
        '
        Me.mtxtDate.Location = New System.Drawing.Point(318, 37)
        Me.mtxtDate.Mask = "##/##/####"
        Me.mtxtDate.Name = "mtxtDate"
        Me.mtxtDate.Size = New System.Drawing.Size(118, 21)
        Me.mtxtDate.TabIndex = 7
        '
        'cmbSupplier
        '
        Me.cmbSupplier.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbSupplier.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbSupplier.FormattingEnabled = True
        Me.cmbSupplier.Location = New System.Drawing.Point(89, 10)
        Me.cmbSupplier.Name = "cmbSupplier"
        Me.cmbSupplier.Size = New System.Drawing.Size(485, 21)
        Me.cmbSupplier.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(-7, 313)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(990, 2)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-7, 453)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(990, 2)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        '
        'pnlNonItem
        '
        Me.pnlNonItem.Controls.Add(Me.rbCancel)
        Me.pnlNonItem.Location = New System.Drawing.Point(809, 321)
        Me.pnlNonItem.Name = "pnlNonItem"
        Me.pnlNonItem.Size = New System.Drawing.Size(175, 22)
        Me.pnlNonItem.TabIndex = 4
        '
        'btnNewModel
        '
        Me.btnNewModel.Location = New System.Drawing.Point(889, 217)
        Me.btnNewModel.Name = "btnNewModel"
        Me.btnNewModel.Size = New System.Drawing.Size(75, 23)
        Me.btnNewModel.TabIndex = 11
        Me.btnNewModel.Text = "New Model"
        Me.btnNewModel.UseVisualStyleBackColor = True
        '
        'pnlItemDetails
        '
        Me.pnlItemDetails.Controls.Add(Me.cbFreeItem)
        Me.pnlItemDetails.Controls.Add(Me.btnCaptureItem)
        Me.pnlItemDetails.Controls.Add(Me.btnItemFinder)
        Me.pnlItemDetails.Controls.Add(Me.Label24)
        Me.pnlItemDetails.Controls.Add(Me.Label8)
        Me.pnlItemDetails.Controls.Add(Me.txtUnit)
        Me.pnlItemDetails.Controls.Add(Me.Label6)
        Me.pnlItemDetails.Controls.Add(Me.Label25)
        Me.pnlItemDetails.Controls.Add(Me.txtSerialNumber)
        Me.pnlItemDetails.Controls.Add(Me.txtQuantity)
        Me.pnlItemDetails.Controls.Add(Me.cmbClass)
        Me.pnlItemDetails.Controls.Add(Me.txtItemName)
        Me.pnlItemDetails.Controls.Add(Me.Label2)
        Me.pnlItemDetails.Controls.Add(Me.txtItemID)
        Me.pnlItemDetails.Controls.Add(Me.Label7)
        Me.pnlItemDetails.Location = New System.Drawing.Point(-7, 246)
        Me.pnlItemDetails.Name = "pnlItemDetails"
        Me.pnlItemDetails.Size = New System.Drawing.Size(990, 60)
        Me.pnlItemDetails.TabIndex = 17
        '
        'cbFreeItem
        '
        Me.cbFreeItem.AutoSize = True
        Me.cbFreeItem.Location = New System.Drawing.Point(892, 10)
        Me.cbFreeItem.Name = "cbFreeItem"
        Me.cbFreeItem.Size = New System.Drawing.Size(73, 17)
        Me.cbFreeItem.TabIndex = 6
        Me.cbFreeItem.Text = "&Free Item"
        Me.cbFreeItem.UseVisualStyleBackColor = True
        '
        'btnCaptureItem
        '
        Me.btnCaptureItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCaptureItem.Location = New System.Drawing.Point(199, 4)
        Me.btnCaptureItem.Name = "btnCaptureItem"
        Me.btnCaptureItem.Size = New System.Drawing.Size(25, 23)
        Me.btnCaptureItem.TabIndex = 3
        Me.btnCaptureItem.Text = "&<"
        Me.btnCaptureItem.UseVisualStyleBackColor = True
        '
        'btnItemFinder
        '
        Me.btnItemFinder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItemFinder.Location = New System.Drawing.Point(173, 4)
        Me.btnItemFinder.Name = "btnItemFinder"
        Me.btnItemFinder.Size = New System.Drawing.Size(25, 23)
        Me.btnItemFinder.TabIndex = 2
        Me.btnItemFinder.Text = "&>"
        Me.btnItemFinder.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(780, 36)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 13)
        Me.Label24.TabIndex = 13
        Me.Label24.Text = "Unit:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(453, 36)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(77, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Serial Number:"
        '
        'txtUnit
        '
        Me.txtUnit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUnit.Location = New System.Drawing.Point(816, 33)
        Me.txtUnit.MaxLength = 30
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.ReadOnly = True
        Me.txtUnit.Size = New System.Drawing.Size(155, 21)
        Me.txtUnit.TabIndex = 14
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(227, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Qty."
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(16, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Class:"
        '
        'txtSerialNumber
        '
        Me.txtSerialNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerialNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSerialNumber.Location = New System.Drawing.Point(536, 33)
        Me.txtSerialNumber.MaxLength = 50
        Me.txtSerialNumber.Name = "txtSerialNumber"
        Me.txtSerialNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSerialNumber.Size = New System.Drawing.Size(226, 21)
        Me.txtSerialNumber.TabIndex = 12
        '
        'txtQuantity
        '
        Me.txtQuantity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuantity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuantity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtQuantity.Location = New System.Drawing.Point(272, 33)
        Me.txtQuantity.MaxLength = 20
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(175, 21)
        Me.txtQuantity.TabIndex = 10
        Me.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cmbClass
        '
        Me.cmbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClass.FormattingEnabled = True
        Me.cmbClass.Location = New System.Drawing.Point(71, 33)
        Me.cmbClass.Name = "cmbClass"
        Me.cmbClass.Size = New System.Drawing.Size(150, 21)
        Me.cmbClass.TabIndex = 8
        '
        'txtItemName
        '
        Me.txtItemName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtItemName.Location = New System.Drawing.Point(272, 6)
        Me.txtItemName.MaxLength = 30
        Me.txtItemName.Name = "txtItemName"
        Me.txtItemName.ReadOnly = True
        Me.txtItemName.Size = New System.Drawing.Size(606, 21)
        Me.txtItemName.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(230, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Name:"
        '
        'txtItemID
        '
        Me.txtItemID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtItemID.Location = New System.Drawing.Point(71, 6)
        Me.txtItemID.MaxLength = 30
        Me.txtItemID.Name = "txtItemID"
        Me.txtItemID.ReadOnly = True
        Me.txtItemID.Size = New System.Drawing.Size(101, 21)
        Me.txtItemID.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Item ID:"
        '
        'frmMakeRR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(976, 509)
        Me.Controls.Add(Me.pnlItemDetails)
        Me.Controls.Add(Me.pnlNonItem)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.btnPop)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.pnlAFDetails)
        Me.Controls.Add(Me.gbGiven)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtBalance)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.dgvMake)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.btnNewModel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmMakeRR"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receiving Report"
        CType(Me.dgvMake, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbGiven.ResumeLayout(False)
        Me.gbGiven.PerformLayout()
        Me.pnlAFDetails.ResumeLayout(False)
        Me.pnlAFDetails.PerformLayout()
        Me.pnlNonItem.ResumeLayout(False)
        Me.pnlNonItem.PerformLayout()
        Me.pnlItemDetails.ResumeLayout(False)
        Me.pnlItemDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbCancel As System.Windows.Forms.RadioButton
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents btnPop As System.Windows.Forms.Button
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dgvMake As System.Windows.Forms.DataGridView
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents gbGiven As System.Windows.Forms.GroupBox
    Friend WithEvents txtFreight As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAmount As System.Windows.Forms.TextBox
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lbltotalInAmt As System.Windows.Forms.Label
    Friend WithEvents txtArrastre As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtDiscount As System.Windows.Forms.TextBox
    Friend WithEvents txtAmountPerModel As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents pnlAFDetails As System.Windows.Forms.Panel
    Friend WithEvents cmbSupplier As System.Windows.Forms.ComboBox
    Friend WithEvents mtxtDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents pnlNonItem As System.Windows.Forms.Panel
    Friend WithEvents btnNewModel As System.Windows.Forms.Button
    Friend WithEvents pnlItemDetails As System.Windows.Forms.Panel
    Friend WithEvents cbFreeItem As System.Windows.Forms.CheckBox
    Friend WithEvents btnCaptureItem As System.Windows.Forms.Button
    Friend WithEvents btnItemFinder As System.Windows.Forms.Button
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtSerialNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents cmbClass As System.Windows.Forms.ComboBox
    Friend WithEvents txtItemName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtItemID As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbTransaction As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
End Class

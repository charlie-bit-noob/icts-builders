Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptAFCI

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmRptAFCI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.MdiParent = MDI
            Me.GetAF()
            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            Me.RptViewer.ZoomMode = ZoomMode.Percent
            Me.RptViewer.ZoomPercent = 100
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetAF()
        Try
            Dim valVAT As Decimal

            With My.Forms.frmlistCI

                If .cbVATX.CheckState = CheckState.Checked Then
                    Me.sqlStr = "SELECT TOP 1 vat FROM Library WHERE vat IS NOT NULL"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    If Me.sqlReader.Read() Then
                        valVAT = Me.sqlReader(0)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        MessageBox.Show("Value not set for VAT items.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()

                        Dim strInputB As String = InputBox("Enter the value of VAT", _
                        "Please contact your system administator A.S.A.P.", _
                        "1.12")
                        Convert.ToDecimal(strInputB)

                        If IsNumeric(strInputB) Then
                            valVAT = strInputB
                        Else
                            MessageBox.Show("The value entered is invalid. The " & Me.Text & " will now close.", _
                            "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Me.Close()
                        End If
                    End If
                End If

                Me.dsExtractAF.Tables("tblCI").Clear()

                Me.sqlStr = "SELECT run_id, root_id, item_id, branch_id, af_num, date_issued, (SELECT short_name FROM AFs WHERE af_id = af.af_id) AS saf, af_num, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = af.item_id) AS Particular, " & _
                "(SELECT types FROM Library l, ItemMasterlist im WHERE im.item_id = af.item_id AND l.lib_id = im.type_lib_id) AS ItemType, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS Serial, " & _
                "COALESCE(qty,0) AS qty, COALESCE(cost,0.00) AS cost, COALESCE(price,0.00) AS price FROM AFsOut AS af WHERE " & .txtSQLpara.Text & " ORDER BY Particular "

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsExtractAF.Tables("tblCI").NewRow

                    tblRow("RunID") = Me.sqlReader("run_id")
                    tblRow("RootID") = Me.sqlReader("root_id")
                    tblRow("ItemID") = Me.sqlReader("item_id")
                    tblRow("BranchID") = Me.sqlReader("branch_id")
                    tblRow("Number") = Me.sqlReader("af_num")
                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("Reference") = Me.sqlReader("saf") + " " + Me.sqlReader("af_num").ToString
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("ItemType") = Me.sqlReader("ItemType")
                    tblRow("Serial") = Me.sqlReader("Serial")
                    tblRow("Qty") = Me.sqlReader("qty")

                    If .cbVATX.CheckState = CheckState.Checked Then
                        If Not Me.sqlReader("ItemType").ToString.Contains("WITHOUT") Then
                            tblRow("Cost") = Me.sqlReader("cost") / valVAT
                        Else
                            tblRow("Cost") = Me.sqlReader("cost")
                        End If
                    Else
                        tblRow("Cost") = Me.sqlReader("cost")
                    End If

                    tblRow("Price") = Me.sqlReader("price")
                    tblRow("TotalCost") = tblRow("Cost") * tblRow("Qty")
                    tblRow("TotalInvPrice") = tblRow("Price") * tblRow("Qty")

                    If .txtSpecific.Text = "" Then
                        If .mtxtFrom.MaskFull = True Then
                            tblRow("Period") = "Period: " + Format(Convert.ToDateTime(.mtxtFrom.Text), "MMM. dd, yyyy") + " - " + Format(Convert.ToDateTime(.mtxtTo.Text), "MMM. dd, yyyy")
                        Else
                            tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                        End If
                        If .mtxtFrom.Text = .mtxtTo.Text Then _
                        tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                    End If

                    tblRow("ReportHeader") = .txtHeader.Text
                    tblRow("ReportFooter") = .txtFooter.Text
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsExtractAF.Tables("tblCI").Rows.Add(tblRow)
                End While
            End With

            Me.sqlReader.Close()
            sqlDBconn.Close()

            'Get appropriate PUR transactions
            Me.sqlStr = "SELECT PUR FROM Transactions WHERE trans_id <= 2"
            Dim strPURTrans As New List(Of String)

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader
            While Me.sqlReader.Read()
                strPURTrans.Add(Me.sqlReader(0))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            'Check each row for PUR transaction
            For Each row As DataRow In Me.dsExtractAF.Tables("tblCI").Rows
                If Not row("ItemType").ToString = "" Then
                    Me.sqlStr = "SELECT type_tables FROM Library WHERE types = '" & row("ItemType").ToString & "'"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    Me.sqlReader.Read()
                    Dim strTypeTable As String = Me.sqlReader(0).ToString
                    Me.sqlReader.Close()
                    sqlDBconn.Close()

                    Me.sqlStr = "SELECT COALESCE(trans_id, 0 ) AS transid FROM " & strTypeTable & " " & _
                    "WHERE branch_id = " & row("BranchID") & " AND ref_af_id = " & GenAFID & " AND ref_af_num = '" & row("Number") & "' AND " & _
                    "item_id = '" & row("ItemID") & "' AND run_id = '" & row("RunID") & "' " & _
                    "AND root_id = '" & row("RootID") & "'"

                    Dim intPURTransID As Integer

                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    If Me.sqlReader.Read() Then
                        intPURTransID = Me.sqlReader(0) 'Invalid attempt to read when no data is present
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        intPURTransID = 0
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    End If

                    If strPURTrans.Contains(intPURTransID.ToString) Then
                        Me.sqlStr = "SELECT af_num, qty FROM AFsOut " & _
                        "WHERE item_id = '" & row("ItemID") & "' AND run_id = '" & row("RunID") & "' AND " & _
                        "ref_af_id = " & GenAFID & " AND ref_af_num = '" & row("Number") & "' AND " & _
                        "root_id = '" & row("RootID") & "'"
                        Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                        sqlDBconn.Open()
                        Me.sqlReader = Me.sqlCmd.ExecuteReader
                        Me.sqlReader.Read()
                        row("PUR") = Me.sqlReader(0).ToString
                        Dim intQty As Integer = Me.sqlReader(1)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()

                        If strTypeTable.Contains("Non") Then
                            row("Cost") = 0.0
                            row("Cost") = 0.0
                            row("TotalInvPrice") = 0.0
                        Else
                            If intQty > 1 Then
                                row("TotalCost") = row("TotalCost") - (row("Cost") * intQty)
                                row("TotalInvPrice") = row("TotalInvPrice") - (row("Price") * intQty)
                            Else
                                row("Cost") = 0.0
                                row("Cost") = 0.0
                                row("TotalInvPrice") = 0.0
                            End If
                        End If
                    End If
                End If
            Next

            'Remove prefix code
            For Each row As DataRow In Me.dsExtractAF.Tables("tblCI").Rows
                row("Reference") = "CI " + TrimAFNumber(row("Reference").ToString.Remove(0, 3))

                If row("PUR").ToString <> "" Then
                    row("PUR") = TrimAFNumber(row("PUR").ToString)
                End If
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

End Class
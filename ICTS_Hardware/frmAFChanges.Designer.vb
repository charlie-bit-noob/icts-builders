<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAFChanges
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rbDate = New System.Windows.Forms.RadioButton
        Me.rbRemarks = New System.Windows.Forms.RadioButton
        Me.btnClose = New System.Windows.Forms.Button
        Me.mtxtDate = New System.Windows.Forms.MaskedTextBox
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.btnApply = New System.Windows.Forms.Button
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.rbTransaction = New System.Windows.Forms.RadioButton
        Me.cmbTransaction = New System.Windows.Forms.ComboBox
        Me.rbNumber = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'rbDate
        '
        Me.rbDate.AutoSize = True
        Me.rbDate.Location = New System.Drawing.Point(12, 12)
        Me.rbDate.Name = "rbDate"
        Me.rbDate.Size = New System.Drawing.Size(48, 17)
        Me.rbDate.TabIndex = 12
        Me.rbDate.Text = "&Date"
        Me.rbDate.UseVisualStyleBackColor = True
        '
        'rbRemarks
        '
        Me.rbRemarks.AutoSize = True
        Me.rbRemarks.Location = New System.Drawing.Point(12, 162)
        Me.rbRemarks.Name = "rbRemarks"
        Me.rbRemarks.Size = New System.Drawing.Size(66, 17)
        Me.rbRemarks.TabIndex = 19
        Me.rbRemarks.Text = "&Remarks"
        Me.rbRemarks.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(335, 41)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(103, 23)
        Me.btnClose.TabIndex = 21
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'mtxtDate
        '
        Me.mtxtDate.Location = New System.Drawing.Point(30, 35)
        Me.mtxtDate.Mask = "00/00/0000"
        Me.mtxtDate.Name = "mtxtDate"
        Me.mtxtDate.Size = New System.Drawing.Size(283, 21)
        Me.mtxtDate.TabIndex = 13
        Me.mtxtDate.ValidatingType = GetType(Date)
        '
        'txtRemarks
        '
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Location = New System.Drawing.Point(30, 185)
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(408, 21)
        Me.txtRemarks.TabIndex = 18
        '
        'btnApply
        '
        Me.btnApply.Location = New System.Drawing.Point(335, 12)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(103, 23)
        Me.btnApply.TabIndex = 20
        Me.btnApply.Text = "&Apply"
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'txtNumber
        '
        Me.txtNumber.Location = New System.Drawing.Point(30, 85)
        Me.txtNumber.MaxLength = 10
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.Size = New System.Drawing.Size(283, 21)
        Me.txtNumber.TabIndex = 14
        '
        'rbTransaction
        '
        Me.rbTransaction.AutoSize = True
        Me.rbTransaction.Location = New System.Drawing.Point(12, 112)
        Me.rbTransaction.Name = "rbTransaction"
        Me.rbTransaction.Size = New System.Drawing.Size(81, 17)
        Me.rbTransaction.TabIndex = 17
        Me.rbTransaction.Text = "&Transaction"
        Me.rbTransaction.UseVisualStyleBackColor = True
        '
        'cmbTransaction
        '
        Me.cmbTransaction.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbTransaction.FormattingEnabled = True
        Me.cmbTransaction.Location = New System.Drawing.Point(30, 135)
        Me.cmbTransaction.Name = "cmbTransaction"
        Me.cmbTransaction.Size = New System.Drawing.Size(283, 21)
        Me.cmbTransaction.TabIndex = 15
        '
        'rbNumber
        '
        Me.rbNumber.AutoSize = True
        Me.rbNumber.Location = New System.Drawing.Point(12, 62)
        Me.rbNumber.Name = "rbNumber"
        Me.rbNumber.Size = New System.Drawing.Size(62, 17)
        Me.rbNumber.TabIndex = 16
        Me.rbNumber.Text = "&Number"
        Me.rbNumber.UseVisualStyleBackColor = True
        '
        'frmAFChanges
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 218)
        Me.Controls.Add(Me.rbDate)
        Me.Controls.Add(Me.rbRemarks)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.mtxtDate)
        Me.Controls.Add(Me.txtRemarks)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.txtNumber)
        Me.Controls.Add(Me.rbTransaction)
        Me.Controls.Add(Me.cmbTransaction)
        Me.Controls.Add(Me.rbNumber)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAFChanges"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "What to change?"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbDate As System.Windows.Forms.RadioButton
    Friend WithEvents rbRemarks As System.Windows.Forms.RadioButton
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents mtxtDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents rbTransaction As System.Windows.Forms.RadioButton
    Friend WithEvents cmbTransaction As System.Windows.Forms.ComboBox
    Friend WithEvents rbNumber As System.Windows.Forms.RadioButton
End Class

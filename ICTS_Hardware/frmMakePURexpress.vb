Imports System.Data
Imports System.Data.SqlClient

Public Class frmMakePURexpress

    '---Reference's Info
    Private intRefAF, intRefBranch, intRefQty As Integer

    Private orderid As Integer
    Private RefDGV As DataGridView

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String
    Private strRefNumber, strRefCustID, strRefNonCust As String

    Private Function IsInValidItem() As Boolean
        If SelectedItemID = "" Then Return True
    End Function

    Private Sub AutoOrderID()
        Try
            Me.sqlStr = "SELECT order_id FROM AFsIn " & _
            "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' " & _
            "AND branch_id = " & Me.cmbBranch.SelectedIndex & " ORDER BY order_id DESC"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read Then
                orderid = Me.sqlReader(0) + 1
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Else
                orderid = 1
                Me.sqlReader.Close()
                sqlDBconn.Close()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '---Trapping Part
        If Me.cmbBranch.SelectedIndex < 1 Or _
        Me.cmbTransaction.SelectedIndex < 1 Or _
        Me.mtxtDate.Text = "  /  /    " Or _
        Me.mtxtDate.MaskFull = False Or _
        Me.txtNumber.Text.Trim = "" Or _
        Me.txtRemarks.Text.Trim = "" Or _
        Me.txtQuantity.Text = "" Then
            Me.ReturnTrapMsg()
            Exit Sub
        End If

        '---Saving Part
        Try
            Dim curindx As Integer = Me.RefDGV.CurrentCell.RowIndex

            Me.AutoOrderID()
            Dim DecCost As Decimal = Me.txtCost.Text.Replace(",", "")

            With Me.RefDGV

                Me.sqlStr = "INSERT INTO AFsIn(root_id, run_id, order_id, item_id, class, is_free, qty, cost, date_issued, branch_id, " & _
                "ref_af_id, ref_af_num, af_id, af_num, trans_id, cust_id, non_cust, supplier_id, ua_id) " & _
                "VALUES('" & GenRootID & "', " & _
                "'" & GenRunID & "', " & _
                "" & Me.orderid & ", " & _
                "'" & SelectedItemID & "', " & _
                "'" & .Item("class", curindx).Value & "', " & _
                "'" & .Item("free", curindx).Value & "', " & _
                "" & .Item("qty", curindx).Value & ", " & _
                "" & DecCost & ", " & _
                "'" & Me.mtxtDate.Text & "', " & _
                "" & Me.cmbBranch.SelectedIndex & ", " & _
                "" & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & _
                "" & Me.cmbTransaction.SelectedIndex & ", " & _
                "'" & Me.strRefCustID & "', '" & Me.strRefNonCust & "', 0, " & _
                "" & MDI.stbUserID.Text & ")"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr

                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                If Me.txtSerialNumber.Text.Trim <> "" Then
                    Me.sqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, " & _
                    "ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) " & _
                    "VALUES('" & GenRootID & "', " & _
                    "'" & GenRunID & "', " & _
                    "" & Me.orderid & ", " & _
                    "'" & SelectedItemID & "', " & _
                    "'" & .Item("free", curindx).Value & "', " & _
                    "'" & .Item("serial", curindx).Value & "', " & _
                    "" & DecCost & ", " & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                    "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & Me.cmbBranch.SelectedIndex & ", " & _
                    "'" & Me.mtxtDate.Text & "', " & Me.cmbTransaction.SelectedIndex & ")"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    Me.sqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, qty_in, " & _
                    "qty_bal, ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) " & _
                    "VALUES('" & GenRootID & "', " & _
                    "'" & GenRunID & "', " & _
                    "" & Me.orderid & ", " & _
                    "'" & SelectedItemID & "', " & _
                    "'" & .Item("free", curindx).Value & "', " & _
                    "'" & .Item("class", curindx).Value & "', " & _
                    "" & DecCost & ", " & Me.txtQuantity.Text & ", " & _
                    "" & Me.txtQuantity.Text & ", " & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                    "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & Me.cmbBranch.SelectedIndex & ", " & _
                    "'" & Me.mtxtDate.Text & "', " & Me.cmbTransaction.SelectedIndex & ")"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                End If

                'Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                'sqlDBconn.Open()
                'Me.sqlReader = Me.sqlCmd.ExecuteReader()
                'Me.sqlReader.Read()

                'Save Remarks to table
                Me.sqlStr = "SELECT * FROM AFsRemarks WHERE run_id = '" & GenRunID & "' AND branch_id = " & Me.intRefBranch & " " & _
                "AND date_issued = '" & Me.mtxtDate.Text & "' AND af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "'"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                If Not Me.sqlReader.Read Then
                    Me.sqlReader.Close()
                    sqlDBconn.Close()

                    Me.sqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
                    "VALUES('" & GenRunID & "', " & _
                    "" & Me.cmbBranch.SelectedIndex & ", " & _
                    "'" & Me.mtxtDate.Text & "', " & GenAFID & ", '" & Me.txtNumber.Text & "', " & _
                    "'" & Me.txtRemarks.Text & "')"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                End If
                'End of Remarks

                '---Update Reference Part
                If .Item("class", curindx).Value.ToString <> "" Then
                    '---Update Stock on File by Adding
                    Me.sqlStr = "UPDATE StockFragile " & _
                    "SET [" & Me.cmbBranch.SelectedIndex & "] = ([" & Me.cmbBranch.SelectedIndex & "] + " & Me.txtQuantity.Text & ") " & _
                    "WHERE item_id = '" & SelectedItemID & "' AND class = '" & .Item("class", curindx).Value.ToString & "'"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_out and qty_bal of a selected item
                    Me.sqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_out = (qty_out + " & Me.txtQuantity.Text & ") " & _
                    "WHERE branch_id = " & Me.intRefBranch & " " & _
                    "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                    "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & " " & _
                    "AND class = '" & .Item("class", curindx).Value.ToString & "'"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_bal of a selected item
                    Me.sqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_bal = (qty_in - qty_out) , trans_pur = " & Me.cmbTransaction.SelectedIndex & " " & _
                    "WHERE branch_id = " & Me.intRefBranch & " " & _
                    "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                    "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & " " & _
                    "AND class = '" & .Item("class", curindx).Value.ToString & "'"

                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    Me.sqlStr = "UPDATE StockDurable " & _
                    "SET [" & Me.cmbBranch.SelectedIndex & "] = ([" & Me.cmbBranch.SelectedIndex & "] + " & Me.txtQuantity.Text & ") " & _
                    "WHERE item_id = '" & SelectedItemID & "'"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    If GetTypeLibID(SelectedItemID) > 2 Then
                        '---Update qty_out of a selected item
                        Me.sqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_out = (qty_out + " & Me.txtQuantity.Text & ") " & _
                        "WHERE branch_id = " & Me.intRefBranch & " " & _
                        "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                        "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & ""

                        Me.sqlCmd = New SqlCommand
                        Me.sqlCmd.Connection = sqlDBconn
                        Me.sqlCmd.CommandText = Me.sqlStr
                        sqlDBconn.Open()
                        Me.sqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()

                        '---Update qty_bal of a selected item
                        Me.sqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_bal = (qty_in - qty_out), trans_pur = " & Me.cmbTransaction.SelectedIndex & " " & _
                        "WHERE branch_id = " & Me.intRefBranch & " " & _
                        "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                        "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & ""

                        Me.sqlCmd = New SqlCommand
                        Me.sqlCmd.Connection = sqlDBconn
                        Me.sqlCmd.CommandText = Me.sqlStr
                        sqlDBconn.Open()
                        Me.sqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    Else
                        Me.sqlStr = "UPDATE TypeSerial SET trans_pur = " & Me.cmbTransaction.SelectedIndex & " " & _
                        "WHERE branch_id = " & Me.intRefBranch & " AND serial_num = '" & Me.txtSerialNumber.Text & "' " & _
                        "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                        "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & ""

                        Me.sqlCmd = New SqlCommand
                        Me.sqlCmd.Connection = sqlDBconn
                        Me.sqlCmd.CommandText = Me.sqlStr
                        sqlDBconn.Open()
                        Me.sqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    End If
                End If
            End With

            MessageBox.Show("PUR " & Me.txtNumber.Text & " saved succesfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CleaningUpObjects()
        'Global
        SelectedItemID = Nothing
        GenRootID = Nothing
        GenRunID = Nothing
        GenPopID = Nothing
        GenAFID = Nothing

        'Local
        Me.sqlCmd = Nothing
        Me.sqlReader = Nothing
        Me.sqlStr = Nothing
        Me.intRefAF = Nothing
        Me.intRefBranch = Nothing
        Me.strRefNumber = Nothing
        Me.strRefCustID = Nothing
        Me.intRefQty = Nothing
        Me.RefDGV = Nothing
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.cmbTransaction.Focus()
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        IsInvalidBranch(Me.cmbBranch.SelectedIndex, Me.cmbBranch)
    End Sub

    Private Sub cmbTransaction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTransaction.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtDate.Focus()
    End Sub

    Private Sub FillBranch()
        Try
            sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = Me.intRefBranch

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillTransaction()
        Try
            sqlStr = "SELECT PUR FROM Transactions WHERE PUR IS NOT NULL ORDER BY trans_id ASC"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()

            Me.cmbTransaction.Items.Clear()
            Me.cmbTransaction.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbTransaction.Items.Add(sqlReader(0))
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbTransaction.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub FillupRefVars(ByVal intAFID As Integer, ByVal strNumber As String, ByVal strCustID As String, ByVal strNonCust As String, ByVal dgv As DataGridView)
        Me.intRefAF = intAFID
        Me.intRefBranch = GenBranchID
        Me.strRefNumber = strNumber
        Me.strRefCustID = strCustID
        Me.strRefNonCust = strNonCust
        Me.RefDGV = dgv

        With Me.RefDGV
            Me.intRefQty = .Item("qty", .CurrentCell.RowIndex).Value
            Me.txtQuantity.Text = Me.intRefQty
            Me.txtSerialNumber.Text = .Item("serial", .CurrentCell.RowIndex).Value.ToString
            Me.sslblparticular.Text = .Item("brand", .CurrentCell.RowIndex).Value + " " + .Item("product", .CurrentCell.RowIndex).Value + " " + .Item("model", .CurrentCell.RowIndex).Value + " " + .Item("desc", .CurrentCell.RowIndex).Value
        End With
    End Sub

    Private Sub frmMakePURexpress_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.CleaningUpObjects()
    End Sub

    Private Sub frmMakePURexpress_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsInValidItem = True Then Me.Close()
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FillBranch()
        Me.FillTransaction()
        Me.txtCost.Text = "0.000"
        GetAFID(Me.Text.Replace(" Express", ""))
        GetShortAF(Me.Text.Replace(" Express", "").ToUpper)
        Me.cmbBranch.Focus()
    End Sub

    Private Sub mtxtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtNumber.Focus()
    End Sub

    Private Sub ReturnTrapMsg()
        MessageBox.Show("Invalid or Incomplete input!", "Be Cautious!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    End Sub

    Private Sub txtCost_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCost.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.btnSave.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtCost_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCost.Leave
        OnLeaveFormat(Me.txtCost)
    End Sub

    Private Sub txtCost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCost.TextChanged
        InputFormat(Me.txtCost)
    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtRemarks.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.Leave
        If Me.txtNumber.Text.ToString.Trim <> "" Then IsNumberExists("AFsIn", Me.cmbBranch.SelectedIndex, Me.txtNumber.Text, Me.txtNumber)
    End Sub

    Private Sub txtQuantity_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQuantity.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtCost.Focus()

        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuantity.TextChanged
        If Me.txtQuantity.Text <> "" Then
            If Val(Me.txtQuantity.Text) > Me.intRefQty Or Val(Me.txtQuantity.Text) < 1 Then Me.txtQuantity.Clear()
        End If
    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtQuantity.Focus()
    End Sub
End Class
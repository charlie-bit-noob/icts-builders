Imports System.Data
Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class MDI

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function HasListOpen() As Boolean
        For Each frm As Form In Me.MdiChildren
            If frm.Name.Contains("list") Then
                MessageBox.Show("Please close search " & frm.Text.Replace("&", "") & " form prior to open " & _
                "another document search.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
                Exit For
            End If
        Next
    End Function

    Private Sub CleaningUpObjects()
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
        sqlDBconn.Dispose()
    End Sub

    'Unused temporarily
    Private Sub DisableAFsMenus(ByVal strAF As String)
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Text <> strAF Then mnu.Enabled = False
            If mnu.Text = "-" Then Exit For
        Next
    End Sub

    Private Sub MDI_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        If Not Me.stbUserName.Text = "" Then
            SetOffline("Offline", Me.stbUserName.Text)
            Me.stbUserName.Text = ""
        End If
    End Sub

    Private Sub MDI_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If MessageBox.Show("Are you sure want to exit application? " & _
            "Transactions unsaved will be discarded.", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                If Me.stbUserName.Text = "WHchaz1027" Then Me.stbUserName.Text = ""
                If Not Me.stbUserName.Text = "" Then
                    SetOffline("Offline", Me.stbUserName.Text)
                    Me.stbUserName.Text = ""
                End If
                Me.CleaningUpObjects()
                End
            Else
                e.Cancel = True
                Exit Sub
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub MDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Login.ShowDialog()
        'Me.mnuAdjustments.Enabled = False
    End Sub

    Private Sub mnuAuthors_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAuthors.Click
        My.Forms.frmAboutAuthors.ShowDialog()
    End Sub

    Private Sub mnuBeginBalEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuBeginBalEdit.Click
        My.Forms.frmBeginBal.ShowDialog()
    End Sub

    Private Sub mnuChangeSerial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuChangeSerial.Click
        My.Forms.frmAdjustmentSerial.ShowDialog()
    End Sub

    Private Sub mnuCountSheet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCountSheet.Click
        My.Forms.frmCountSheet.Show()
    End Sub

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Me.Close()
    End Sub

    Private Sub mnuIncomingStocks_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuIncomingStocks.Click
        For Each mnu As MenuItem In Me.mnuSummary.MenuItems
            If mnu.Enabled = False Then
                MessageBox.Show("Close " & mnu.Text & " prior to open " & Me.mnuIncomingStocks.Text & " summary.", _
                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Enabled = True Then
                For Each submnu As MenuItem In mnu.MenuItems
                    If submnu.Enabled = False Then
                        For Each childfrm As Form In Me.MdiChildren
                            If childfrm.Text = mnu.Text.Replace("&", "") Then
                                MessageBox.Show("Close " & mnu.Text.Replace("&", "") & " prior to open " & Me.mnuIncomingStocks.Text & ".", _
                                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Me.mnuIncomingStocks.Enabled = False
        My.Forms.frmParaIncoming.Show()
    End Sub

    Private Sub mnuItemMasterlist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuItemMasterlist.Click
        My.Forms.frmItemMasterlist.ShowDialog()
    End Sub

    Private Sub mnuLock_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLock.Click
        My.Forms.Lock.ShowDialog()
    End Sub

    Private Sub mnuNewCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewCA.Click
        'Me.DisableAFsMenus("C&ash Invoice")
        ResetAFsMenusDisableMDI()
        frmMakeCA.Show()
    End Sub

    Private Sub mnuNewCI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewCI.Click
        'Me.DisableAFsMenus("&Credit Invoice")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeCI.Show()
    End Sub

    Private Sub mnuNewCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewCustomer.Click
        My.Forms.frmNewCustomer.ShowDialog()
    End Sub

    Private Sub mnuNewDR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewDR.Click
        'Me.DisableAFsMenus("&Delivery Receipt")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeDR.Show()
    End Sub

    Private Sub mnuNewPUR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewPUR.Click
        'Me.DisableAFsMenus("&Pick-up Receipt")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakePUR.Show()
    End Sub

    Private Sub mnuNewRR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewRR.Click
        If Me.mnuPurchases.Enabled = False Then
            MessageBox.Show("Please close " & My.Forms.frmParaPurchases.Text & " in order to proceed.", Me.Text, MessageBoxButtons.OK)
            Exit Sub
        End If
        'Me.DisableAFsMenus("&Receiving Report")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeRR.Show()
    End Sub

    Private Sub mnuNewSRS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewSRS.Click
        'Me.DisableAFsMenus("&Stock Requisition Slip")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeSRS.Show()
    End Sub

    Private Sub mnuNewSTR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewSTR.Click
        'Me.DisableAFsMenus("Stock &Transfer Receipt")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeSTR.Show()
    End Sub

    Private Sub mnuNewSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewSupplier.Click
        My.Forms.frmNewSupplier.Show()
    End Sub

    Private Sub mnuNewUserAcct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewUserAcct.Click
        My.Forms.frmNewUserAccount.Show()
    End Sub

    Private Sub mnuNewWR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewWR.Click
        If Me.mnuPurchases.Enabled = False Then
            MessageBox.Show("Please close " & My.Forms.frmParaPurchases.Text & " in order to proceed.", Me.Text, MessageBoxButtons.OK)
            Exit Sub
        End If
        'Me.DisableAFsMenus("&Warehouse Receipt")
        ResetAFsMenusDisableMDI()
        My.Forms.frmMakeWR.Show()
    End Sub

    Private Sub mnuOutgoingStocks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOutgoingStocks.Click
        For Each mnu As MenuItem In Me.mnuSummary.MenuItems
            If mnu.Enabled = False Then
                MessageBox.Show("Close " & mnu.Text & " prior to open " & Me.mnuOutgoingStocks.Text & " summary.", _
                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Enabled = True Then
                For Each submnu As MenuItem In mnu.MenuItems
                    If submnu.Enabled = False Then
                        For Each childfrm As Form In Me.MdiChildren
                            If childfrm.Text = mnu.Text.Replace("&", "") Then
                                MessageBox.Show("Close " & mnu.Text.Replace("&", "") & " prior to open " & Me.mnuOutgoingStocks.Text & ".", _
                                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Me.mnuOutgoingStocks.Enabled = False
        My.Forms.frmParaOutgoing.Show()
    End Sub

    Private Sub mnuOutstandingBalances_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOutstandingBalances.Click
        For Each mnu As MenuItem In Me.mnuSummary.MenuItems
            If mnu.Enabled = False Then
                MessageBox.Show("Close " & mnu.Text & " prior to open " & Me.mnuOutstandingBalances.Text & " summary.", _
                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Enabled = True Then
                For Each submnu As MenuItem In mnu.MenuItems
                    If submnu.Enabled = False Then
                        For Each childfrm As Form In Me.MdiChildren
                            If childfrm.Text = mnu.Text.Replace("&", "") Then
                                MessageBox.Show("Close " & mnu.Text.Replace("&", "") & " prior to open " & Me.mnuOutstandingBalances.Text & ".", _
                                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Me.mnuOutstandingBalances.Enabled = False
        My.Forms.frmParaOutstanding.Show()
    End Sub

    Private Sub mnuPurchases_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPurchases.Click
        For Each mnu As MenuItem In Me.mnuSummary.MenuItems
            If mnu.Enabled = False Then
                MessageBox.Show("Close " & mnu.Text & " prior to open " & Me.mnuPurchases.Text & " summary.", _
                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Enabled = True Then
                For Each submnu As MenuItem In mnu.MenuItems
                    If submnu.Enabled = False Then
                        For Each childfrm As Form In Me.MdiChildren
                            If childfrm.Text = mnu.Text.Replace("&", "") Then
                                MessageBox.Show("Close " & mnu.Text.Replace("&", "") & " prior to open " & Me.mnuPurchases.Text & ".", _
                                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Me.mnuPurchases.Enabled = False
        My.Forms.frmParaPurchases.Show()
    End Sub

    Private Sub mnuSales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSales.Click
        For Each mnu As MenuItem In Me.mnuSummary.MenuItems
            If mnu.Enabled = False Then
                MessageBox.Show("Close " & mnu.Text & " prior to open " & Me.mnuSales.Text & " summary.", _
                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Next
        For Each mnu As MenuItem In Me.mnuAFs.MenuItems
            If mnu.Enabled = True Then
                For Each submnu As MenuItem In mnu.MenuItems
                    If submnu.Enabled = False Then
                        For Each childfrm As Form In Me.MdiChildren
                            If childfrm.Text = mnu.Text.Replace("&", "") Then
                                MessageBox.Show("Close " & mnu.Text.Replace("&", "") & " prior to open " & Me.mnuSales.Text & ".", _
                                Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Exit Sub
                            End If
                        Next
                    End If
                Next
            End If
        Next
        Me.mnuSales.Enabled = False
        My.Forms.frmParaSales.Show()
    End Sub

    Private Sub mnuSearchCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchCA.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistCA.Show()
    End Sub

    Private Sub mnuSearchCI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchCI.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistCI.Show()
    End Sub

    Private Sub mnuSearchCustomer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchCustomer.Click
        My.Forms.frmFinderCustomer.ShowDialog()
    End Sub

    Private Sub mnuSearchDR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchDR.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistDR.Show()
    End Sub

    Private Sub mnuSearchPUR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchPUR.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistPUR.Show()
    End Sub

    Private Sub mnuSearchRR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchRR.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistRR.Show()
    End Sub

    Private Sub mnuSearchSRS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchSRS.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistSRS.Show()
    End Sub

    Private Sub mnuSearchSTR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchSTR.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistSTR.Show()
    End Sub

    Private Sub mnuSearchWR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearchWR.Click
        If Me.HasListOpen = True Then Exit Sub
        My.Forms.frmlistWR.Show()
    End Sub

    Private Sub mnuStockCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStockCard.Click
        My.Forms.frmStockCard.Show()
    End Sub

    Private Sub mnuStockPosition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStockPosition.Click
        My.Forms.frmStockPosition.Show()
    End Sub

    Private Sub SetOffline(ByVal stat As String, ByVal strUserName As String)
        Try
            sqlStr = "UPDATE UserAccounts SET Status = '" & stat & "' " & _
            "WHERE username = '" & strUserName & "'"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr

            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub mnuStockOnFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStockOnFile.Click
        My.Forms.frmSelectBranch.Show()
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnUserReset = New System.Windows.Forms.Button
        Me.MyLabel = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.lblpassword = New System.Windows.Forms.Label
        Me.txtpassword = New System.Windows.Forms.TextBox
        Me.lblusername = New System.Windows.Forms.Label
        Me.btnOK = New System.Windows.Forms.Button
        Me.txtusername = New System.Windows.Forms.TextBox
        Me.btnChangeServer = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Enabled = False
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(13, 117)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 16)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Credits to Jesus Christ"
        Me.Label1.UseCompatibleTextRendering = True
        '
        'btnUserReset
        '
        Me.btnUserReset.Location = New System.Drawing.Point(416, 33)
        Me.btnUserReset.Name = "btnUserReset"
        Me.btnUserReset.Size = New System.Drawing.Size(37, 10)
        Me.btnUserReset.TabIndex = 2
        Me.btnUserReset.UseCompatibleTextRendering = True
        Me.btnUserReset.UseVisualStyleBackColor = True
        Me.btnUserReset.Visible = False
        '
        'MyLabel
        '
        Me.MyLabel.AutoSize = True
        Me.MyLabel.Enabled = False
        Me.MyLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MyLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MyLabel.Location = New System.Drawing.Point(13, 102)
        Me.MyLabel.Name = "MyLabel"
        Me.MyLabel.Size = New System.Drawing.Size(144, 16)
        Me.MyLabel.TabIndex = 9
        Me.MyLabel.Text = "� 2011 Charlie G. Pandacan"
        Me.MyLabel.UseCompatibleTextRendering = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-2, 92)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(484, 2)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(377, 112)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(81, 23)
        Me.btnCancel.TabIndex = 7
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'lblpassword
        '
        Me.lblpassword.AutoSize = True
        Me.lblpassword.Location = New System.Drawing.Point(13, 57)
        Me.lblpassword.Name = "lblpassword"
        Me.lblpassword.Size = New System.Drawing.Size(53, 13)
        Me.lblpassword.TabIndex = 3
        Me.lblpassword.Text = "&Password"
        '
        'txtpassword
        '
        Me.txtpassword.Location = New System.Drawing.Point(100, 54)
        Me.txtpassword.MaxLength = 20
        Me.txtpassword.Name = "txtpassword"
        Me.txtpassword.Size = New System.Drawing.Size(358, 21)
        Me.txtpassword.TabIndex = 4
        Me.txtpassword.UseSystemPasswordChar = True
        '
        'lblusername
        '
        Me.lblusername.AutoSize = True
        Me.lblusername.Location = New System.Drawing.Point(13, 30)
        Me.lblusername.Name = "lblusername"
        Me.lblusername.Size = New System.Drawing.Size(55, 13)
        Me.lblusername.TabIndex = 0
        Me.lblusername.Text = "&Username"
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(283, 112)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(88, 23)
        Me.btnOK.TabIndex = 6
        Me.btnOK.Text = "&OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'txtusername
        '
        Me.txtusername.Location = New System.Drawing.Point(100, 27)
        Me.txtusername.MaxLength = 20
        Me.txtusername.Name = "txtusername"
        Me.txtusername.Size = New System.Drawing.Size(358, 21)
        Me.txtusername.TabIndex = 1
        '
        'btnChangeServer
        '
        Me.btnChangeServer.Location = New System.Drawing.Point(189, 112)
        Me.btnChangeServer.Name = "btnChangeServer"
        Me.btnChangeServer.Size = New System.Drawing.Size(88, 23)
        Me.btnChangeServer.TabIndex = 8
        Me.btnChangeServer.Text = "Change Ser&ver"
        Me.btnChangeServer.UseVisualStyleBackColor = True
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(470, 146)
        Me.Controls.Add(Me.btnChangeServer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnUserReset)
        Me.Controls.Add(Me.MyLabel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.lblpassword)
        Me.Controls.Add(Me.txtpassword)
        Me.Controls.Add(Me.lblusername)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.txtusername)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Login"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Enter a valid username and password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnUserReset As System.Windows.Forms.Button
    Friend WithEvents MyLabel As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents lblpassword As System.Windows.Forms.Label
    Friend WithEvents txtpassword As System.Windows.Forms.TextBox
    Friend WithEvents lblusername As System.Windows.Forms.Label
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents txtusername As System.Windows.Forms.TextBox
    Friend WithEvents btnChangeServer As System.Windows.Forms.Button
End Class

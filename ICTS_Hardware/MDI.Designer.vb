<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MDI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDI))
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuAFs = New System.Windows.Forms.MenuItem
        Me.mnuWR = New System.Windows.Forms.MenuItem
        Me.mnuNewWR = New System.Windows.Forms.MenuItem
        Me.mnuSearchWR = New System.Windows.Forms.MenuItem
        Me.mnuRR = New System.Windows.Forms.MenuItem
        Me.mnuNewRR = New System.Windows.Forms.MenuItem
        Me.mnuSearchRR = New System.Windows.Forms.MenuItem
        Me.mnuSRS = New System.Windows.Forms.MenuItem
        Me.mnuNewSRS = New System.Windows.Forms.MenuItem
        Me.mnuSearchSRS = New System.Windows.Forms.MenuItem
        Me.mnuSTR = New System.Windows.Forms.MenuItem
        Me.mnuNewSTR = New System.Windows.Forms.MenuItem
        Me.mnuSearchSTR = New System.Windows.Forms.MenuItem
        Me.mnuCI = New System.Windows.Forms.MenuItem
        Me.mnuNewCI = New System.Windows.Forms.MenuItem
        Me.mnuSearchCI = New System.Windows.Forms.MenuItem
        Me.mnuCA = New System.Windows.Forms.MenuItem
        Me.mnuNewCA = New System.Windows.Forms.MenuItem
        Me.mnuSearchCA = New System.Windows.Forms.MenuItem
        Me.mnuPUR = New System.Windows.Forms.MenuItem
        Me.mnuNewPUR = New System.Windows.Forms.MenuItem
        Me.mnuSearchPUR = New System.Windows.Forms.MenuItem
        Me.mnuDR = New System.Windows.Forms.MenuItem
        Me.mnuNewDR = New System.Windows.Forms.MenuItem
        Me.mnuSearchDR = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.mnuLock = New System.Windows.Forms.MenuItem
        Me.mnuExit = New System.Windows.Forms.MenuItem
        Me.mnuView = New System.Windows.Forms.MenuItem
        Me.mnuStockPosition = New System.Windows.Forms.MenuItem
        Me.mnuStockCard = New System.Windows.Forms.MenuItem
        Me.mnuSummary = New System.Windows.Forms.MenuItem
        Me.mnuPurchases = New System.Windows.Forms.MenuItem
        Me.mnuSales = New System.Windows.Forms.MenuItem
        Me.mnuIncomingStocks = New System.Windows.Forms.MenuItem
        Me.mnuOutgoingStocks = New System.Windows.Forms.MenuItem
        Me.mnuOutstandingBalances = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuStockOnFile = New System.Windows.Forms.MenuItem
        Me.mnuResources = New System.Windows.Forms.MenuItem
        Me.mnuSupplier = New System.Windows.Forms.MenuItem
        Me.mnuNewSupplier = New System.Windows.Forms.MenuItem
        Me.mnuCustomer = New System.Windows.Forms.MenuItem
        Me.mnuNewCustomer = New System.Windows.Forms.MenuItem
        Me.mnuSearchCustomer = New System.Windows.Forms.MenuItem
        Me.mnuUtilities = New System.Windows.Forms.MenuItem
        Me.mnuItemMasterlist = New System.Windows.Forms.MenuItem
        Me.mnuNewUserAcct = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.mnuChangeSerial = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCountSheet = New System.Windows.Forms.MenuItem
        Me.mnuBeginBalEdit = New System.Windows.Forms.MenuItem
        Me.mnuAbout = New System.Windows.Forms.MenuItem
        Me.mnuAuthors = New System.Windows.Forms.MenuItem
        Me.mnuICTS = New System.Windows.Forms.MenuItem
        Me.stbMDI = New System.Windows.Forms.StatusBar
        Me.StatusBarPanel1 = New System.Windows.Forms.StatusBarPanel
        Me.stbUserID = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel2 = New System.Windows.Forms.StatusBarPanel
        Me.stbUserName = New System.Windows.Forms.StatusBarPanel
        Me.StatusBarPanel3 = New System.Windows.Forms.StatusBarPanel
        Me.stbAccessLevel = New System.Windows.Forms.StatusBarPanel
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stbUserID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stbUserName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stbAccessLevel, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAFs, Me.mnuView, Me.mnuSummary, Me.mnuResources, Me.mnuUtilities, Me.mnuAbout})
        '
        'mnuAFs
        '
        Me.mnuAFs.Index = 0
        Me.mnuAFs.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuWR, Me.mnuRR, Me.mnuSRS, Me.mnuSTR, Me.mnuCI, Me.mnuCA, Me.mnuPUR, Me.mnuDR, Me.MenuItem11, Me.mnuLock, Me.mnuExit})
        Me.mnuAFs.Text = "&Accountable Forms"
        '
        'mnuWR
        '
        Me.mnuWR.Index = 0
        Me.mnuWR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewWR, Me.mnuSearchWR})
        Me.mnuWR.Text = "&Warehouse Receipt"
        '
        'mnuNewWR
        '
        Me.mnuNewWR.Index = 0
        Me.mnuNewWR.Text = "New"
        '
        'mnuSearchWR
        '
        Me.mnuSearchWR.Index = 1
        Me.mnuSearchWR.Text = "Search"
        '
        'mnuRR
        '
        Me.mnuRR.Index = 1
        Me.mnuRR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewRR, Me.mnuSearchRR})
        Me.mnuRR.Text = "&Receiving Report"
        '
        'mnuNewRR
        '
        Me.mnuNewRR.Index = 0
        Me.mnuNewRR.Text = "New"
        '
        'mnuSearchRR
        '
        Me.mnuSearchRR.Index = 1
        Me.mnuSearchRR.Text = "Search"
        '
        'mnuSRS
        '
        Me.mnuSRS.Index = 2
        Me.mnuSRS.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewSRS, Me.mnuSearchSRS})
        Me.mnuSRS.Text = "&Stock Requisition Slip"
        '
        'mnuNewSRS
        '
        Me.mnuNewSRS.Index = 0
        Me.mnuNewSRS.Text = "New"
        '
        'mnuSearchSRS
        '
        Me.mnuSearchSRS.Index = 1
        Me.mnuSearchSRS.Text = "Search"
        '
        'mnuSTR
        '
        Me.mnuSTR.Index = 3
        Me.mnuSTR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewSTR, Me.mnuSearchSTR})
        Me.mnuSTR.Text = "Stock &Transfer Receipt"
        '
        'mnuNewSTR
        '
        Me.mnuNewSTR.Index = 0
        Me.mnuNewSTR.Text = "New"
        '
        'mnuSearchSTR
        '
        Me.mnuSearchSTR.Index = 1
        Me.mnuSearchSTR.Text = "Search"
        '
        'mnuCI
        '
        Me.mnuCI.Index = 4
        Me.mnuCI.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewCI, Me.mnuSearchCI})
        Me.mnuCI.Text = "&Credit Invoice"
        '
        'mnuNewCI
        '
        Me.mnuNewCI.Index = 0
        Me.mnuNewCI.Text = "New"
        '
        'mnuSearchCI
        '
        Me.mnuSearchCI.Index = 1
        Me.mnuSearchCI.Text = "Search"
        '
        'mnuCA
        '
        Me.mnuCA.Index = 5
        Me.mnuCA.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewCA, Me.mnuSearchCA})
        Me.mnuCA.Text = "C&ash Invoice"
        '
        'mnuNewCA
        '
        Me.mnuNewCA.Index = 0
        Me.mnuNewCA.Text = "New"
        '
        'mnuSearchCA
        '
        Me.mnuSearchCA.Index = 1
        Me.mnuSearchCA.Text = "Search"
        '
        'mnuPUR
        '
        Me.mnuPUR.Index = 6
        Me.mnuPUR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewPUR, Me.mnuSearchPUR})
        Me.mnuPUR.Text = "&Pick-up Receipt"
        '
        'mnuNewPUR
        '
        Me.mnuNewPUR.Index = 0
        Me.mnuNewPUR.Text = "New"
        '
        'mnuSearchPUR
        '
        Me.mnuSearchPUR.Index = 1
        Me.mnuSearchPUR.Text = "Search"
        '
        'mnuDR
        '
        Me.mnuDR.Index = 7
        Me.mnuDR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewDR, Me.mnuSearchDR})
        Me.mnuDR.Text = "&Delivery Receipt"
        '
        'mnuNewDR
        '
        Me.mnuNewDR.Index = 0
        Me.mnuNewDR.Text = "New"
        '
        'mnuSearchDR
        '
        Me.mnuSearchDR.Index = 1
        Me.mnuSearchDR.Text = "Search"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 8
        Me.MenuItem11.Text = "-"
        '
        'mnuLock
        '
        Me.mnuLock.Index = 9
        Me.mnuLock.Text = "&Lock"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 10
        Me.mnuExit.Text = "E&xit"
        '
        'mnuView
        '
        Me.mnuView.Index = 1
        Me.mnuView.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuStockPosition, Me.mnuStockCard})
        Me.mnuView.Text = "&View"
        '
        'mnuStockPosition
        '
        Me.mnuStockPosition.Index = 0
        Me.mnuStockPosition.Text = "Stock Position"
        '
        'mnuStockCard
        '
        Me.mnuStockCard.Index = 1
        Me.mnuStockCard.Text = "Stock Card"
        '
        'mnuSummary
        '
        Me.mnuSummary.Index = 2
        Me.mnuSummary.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuPurchases, Me.mnuSales, Me.mnuIncomingStocks, Me.mnuOutgoingStocks, Me.mnuOutstandingBalances, Me.MenuItem1, Me.mnuStockOnFile})
        Me.mnuSummary.Text = "&Summary"
        '
        'mnuPurchases
        '
        Me.mnuPurchases.Index = 0
        Me.mnuPurchases.Text = "Purchases"
        '
        'mnuSales
        '
        Me.mnuSales.Index = 1
        Me.mnuSales.Text = "Sales"
        '
        'mnuIncomingStocks
        '
        Me.mnuIncomingStocks.Index = 2
        Me.mnuIncomingStocks.Text = "Incoming Stocks"
        '
        'mnuOutgoingStocks
        '
        Me.mnuOutgoingStocks.Index = 3
        Me.mnuOutgoingStocks.Text = "Outgoing Stocks"
        '
        'mnuOutstandingBalances
        '
        Me.mnuOutstandingBalances.Index = 4
        Me.mnuOutstandingBalances.Text = "Outstanding Balances"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 5
        Me.MenuItem1.Text = "-"
        '
        'mnuStockOnFile
        '
        Me.mnuStockOnFile.Index = 6
        Me.mnuStockOnFile.Text = "Stock on File Count"
        '
        'mnuResources
        '
        Me.mnuResources.Index = 3
        Me.mnuResources.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSupplier, Me.mnuCustomer})
        Me.mnuResources.Text = "&Resources"
        '
        'mnuSupplier
        '
        Me.mnuSupplier.Index = 0
        Me.mnuSupplier.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewSupplier})
        Me.mnuSupplier.Text = "&Supplier"
        '
        'mnuNewSupplier
        '
        Me.mnuNewSupplier.Index = 0
        Me.mnuNewSupplier.Text = "New"
        '
        'mnuCustomer
        '
        Me.mnuCustomer.Index = 1
        Me.mnuCustomer.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuNewCustomer, Me.mnuSearchCustomer})
        Me.mnuCustomer.Text = "&Customer"
        '
        'mnuNewCustomer
        '
        Me.mnuNewCustomer.Index = 0
        Me.mnuNewCustomer.Text = "New"
        '
        'mnuSearchCustomer
        '
        Me.mnuSearchCustomer.Index = 1
        Me.mnuSearchCustomer.Text = "Search"
        '
        'mnuUtilities
        '
        Me.mnuUtilities.Index = 4
        Me.mnuUtilities.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuItemMasterlist, Me.mnuNewUserAcct, Me.MenuItem2, Me.mnuChangeSerial, Me.MenuItem3, Me.mnuCountSheet, Me.mnuBeginBalEdit})
        Me.mnuUtilities.Text = "&Utilities"
        '
        'mnuItemMasterlist
        '
        Me.mnuItemMasterlist.Index = 0
        Me.mnuItemMasterlist.Text = "Item &Masterlist"
        '
        'mnuNewUserAcct
        '
        Me.mnuNewUserAcct.Index = 1
        Me.mnuNewUserAcct.Text = "&New User Account"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 2
        Me.MenuItem2.Text = "-"
        '
        'mnuChangeSerial
        '
        Me.mnuChangeSerial.Index = 3
        Me.mnuChangeSerial.Text = "Change Se&rial Number"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 4
        Me.MenuItem3.Text = "-"
        '
        'mnuCountSheet
        '
        Me.mnuCountSheet.Index = 5
        Me.mnuCountSheet.Text = "Count Sh&eet"
        '
        'mnuBeginBalEdit
        '
        Me.mnuBeginBalEdit.Index = 6
        Me.mnuBeginBalEdit.Text = "&Beginning Balance Editor"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 5
        Me.mnuAbout.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAuthors, Me.mnuICTS})
        Me.mnuAbout.Text = "About"
        '
        'mnuAuthors
        '
        Me.mnuAuthors.Index = 0
        Me.mnuAuthors.Text = "The Authors"
        '
        'mnuICTS
        '
        Me.mnuICTS.Enabled = False
        Me.mnuICTS.Index = 1
        Me.mnuICTS.Text = "ICTS - Hardware Edition"
        '
        'stbMDI
        '
        Me.stbMDI.Location = New System.Drawing.Point(0, 521)
        Me.stbMDI.Name = "stbMDI"
        Me.stbMDI.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.StatusBarPanel1, Me.stbUserID, Me.StatusBarPanel2, Me.stbUserName, Me.StatusBarPanel3, Me.stbAccessLevel})
        Me.stbMDI.ShowPanels = True
        Me.stbMDI.Size = New System.Drawing.Size(786, 22)
        Me.stbMDI.SizingGrip = False
        Me.stbMDI.TabIndex = 14
        '
        'StatusBarPanel1
        '
        Me.StatusBarPanel1.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel1.Name = "StatusBarPanel1"
        Me.StatusBarPanel1.Text = "ID:"
        Me.StatusBarPanel1.Width = 30
        '
        'stbUserID
        '
        Me.stbUserID.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.stbUserID.Name = "stbUserID"
        '
        'StatusBarPanel2
        '
        Me.StatusBarPanel2.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel2.Name = "StatusBarPanel2"
        Me.StatusBarPanel2.Text = "Username:"
        Me.StatusBarPanel2.Width = 70
        '
        'stbUserName
        '
        Me.stbUserName.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.stbUserName.Name = "stbUserName"
        Me.stbUserName.Width = 200
        '
        'StatusBarPanel3
        '
        Me.StatusBarPanel3.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.StatusBarPanel3.Name = "StatusBarPanel3"
        Me.StatusBarPanel3.Text = "Access Level:"
        Me.StatusBarPanel3.Width = 80
        '
        'stbAccessLevel
        '
        Me.stbAccessLevel.BorderStyle = System.Windows.Forms.StatusBarPanelBorderStyle.None
        Me.stbAccessLevel.Name = "stbAccessLevel"
        Me.stbAccessLevel.Width = 200
        '
        'MDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 543)
        Me.Controls.Add(Me.stbMDI)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Menu = Me.MainMenu1
        Me.Name = "MDI"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inventory Control and Tracking System 1.0.2 - Hardware Edition"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.StatusBarPanel1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stbUserID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stbUserName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusBarPanel3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stbAccessLevel, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuAFs As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewWR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchWR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewRR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchRR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSRS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewSRS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchSRS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSTR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewSTR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchSTR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewCI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchCI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCA As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewCA As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchCA As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPUR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewPUR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchPUR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewDR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSearchDR As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuLock As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuView As System.Windows.Forms.MenuItem
    Friend WithEvents mnuStockPosition As System.Windows.Forms.MenuItem
    Friend WithEvents mnuStockCard As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSummary As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPurchases As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSales As System.Windows.Forms.MenuItem
    Friend WithEvents mnuIncomingStocks As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOutgoingStocks As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOutstandingBalances As System.Windows.Forms.MenuItem
    Friend WithEvents mnuResources As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSupplier As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewSupplier As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCustomer As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewCustomer As System.Windows.Forms.MenuItem
    Friend WithEvents mnuUtilities As System.Windows.Forms.MenuItem
    Friend WithEvents mnuItemMasterlist As System.Windows.Forms.MenuItem
    Friend WithEvents mnuNewUserAcct As System.Windows.Forms.MenuItem
    Friend WithEvents mnuBeginBalEdit As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAuthors As System.Windows.Forms.MenuItem
    Friend WithEvents mnuICTS As System.Windows.Forms.MenuItem
    Friend WithEvents stbMDI As System.Windows.Forms.StatusBar
    Friend WithEvents StatusBarPanel1 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents stbUserID As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel2 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents stbUserName As System.Windows.Forms.StatusBarPanel
    Friend WithEvents StatusBarPanel3 As System.Windows.Forms.StatusBarPanel
    Friend WithEvents stbAccessLevel As System.Windows.Forms.StatusBarPanel
    Friend WithEvents mnuSearchCustomer As System.Windows.Forms.MenuItem
    Friend WithEvents mnuChangeSerial As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCountSheet As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuStockOnFile As System.Windows.Forms.MenuItem

End Class

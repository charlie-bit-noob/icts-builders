Imports System.Data
Imports System.Data.SqlClient

Module modProcedures

    Private ModPsqlCmd As SqlCommand
    Private ModPsqlReader As SqlDataReader
    Private ModPsqlStr As String

    Public Sub AutoGenRootID()
        Try
            Dim strYear As String = Format(Now.Year, "00")
            Dim strMonth As String = Format(Now.Month, "00")
            Dim strDay As String = Format(Now.Day, "00")
            Dim LastID, NewID, LastRoot As String
            Dim CurrentDay As String = strYear + strMonth + strDay

            sqlDBconn.Open()
            ModPsqlCmd = New SqlCommand("SELECT root_id FROM BankRootID ORDER BY root_id DESC", sqlDBconn)
            ModPsqlReader = ModPsqlCmd.ExecuteReader

            If ModPsqlReader.HasRows Then
                ModPsqlReader.Close()

                LastID = ModPsqlCmd.ExecuteScalar
                LastRoot = LastID.Remove(Val(LastID.Length) - 3)
                LastID = LastID.Remove(0, Val(LastRoot.Length))

                If LastRoot = CurrentDay Then
                    NewID = Format(Val(LastID) + 1, "000")
                    NewID = strYear + strMonth + strDay + NewID
                Else
                    NewID = strYear + strMonth + strDay + "001"
                End If

                GenRootID = NewID

                'ModPsqlReader.Close()
                sqlDBconn.Close()
            Else
                GenRootID = strYear + strMonth + strDay + "001"
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub AutoGenRunID()
        Try
            Dim IDcode As String = GenShortAF
            Dim charLen As Integer = GenShortAF.Length
            Dim LastRunID, NewRunID As String

            ModPsqlStr = "SELECT TOP 1 run_id FROM AFsIn WHERE af_id = " & GenAFID & " " & _
            "AND run_id IS NOT NULL ORDER BY run_id DESC"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                LastRunID = ModPsqlReader("run_id")
                LastRunID = LastRunID.Remove(0, charLen)
                NewRunID = IDcode + Format(Val(LastRunID) + 1, "000000000000")

                GenRunID = NewRunID

                ModPsqlReader.Close()
                sqlDBconn.Close()
            Else
                GenRunID = IDcode + "000000000001"

                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub CancelledAF(ByVal strNumber As String, ByVal intBranch As Integer, ByVal dtDateIssued As Date)
        Try
            Dim type As String

            '--Get the tpye of AF
            ModPsqlStr = "SELECT af_type FROM AFs WHERE short_name = '" & GenShortAF & "'"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            type = ModPsqlReader(0)

            ModPsqlReader.Close()
            sqlDBconn.Close()

            ModPsqlStr = "INSERT INTO AFs" & type & "(run_id, order_id, af_id, af_num, branch_id, date_issued, ua_id) " & _
            "VALUES('" & GenRunID & "', 1, " & GenAFID & ", '" & strNumber & "', " & intBranch & ", '" & dtDateIssued & "', " & MDI.stbUserID.Text & ")"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            'Save Remarks to table
            ModPsqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
            "VALUES('" & GenRunID & "', " & intBranch & ", '" & dtDateIssued & "', " & GenAFID & ", '" & strNumber & "', 'CANCELLED')"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()
            'End of Remarks

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub CleanUpListOfString()
        selectedCount = Nothing
        rootList.Clear()
        runList.Clear()
        afList.Clear()
        afnumList.Clear()
        IsFreeList.Clear()
        classList.Clear()
        costList.Clear()
        qtyList.Clear()
    End Sub

    Public Sub ClearTextBox(ByVal txtBox As TextBox)
        Try
            txtBox.Clear()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub CloseMe(ByVal frm1 As Form, ByVal frm2 As Form)
        Try
            If frm1.Visible = True Then frm1.Close()
            If frm2.Visible = True Then frm2.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub ComputeCost(ByVal dgv As DataGridView, ByVal txt As TextBox)
        Try
            If dgv.RowCount < 1 Then Exit Sub

            Dim Row As DataGridViewRow
            Dim DumTotal As Decimal

            For Each Row In dgv.Rows
                Dim strQty As String = Row.Cells("qty").Value.ToString

                If Not strQty = "" Then
                    Dim QtyCell As Integer = Row.Cells("qty").Value
                    Dim CostCell As Decimal = Row.Cells("cost").Value

                    DumTotal = (CostCell * QtyCell) + DumTotal
                End If

            Next Row

            txt.Text = DumTotal.ToString("N3")

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ComputePrice(ByVal dgv As DataGridView, ByVal txt As TextBox)
        Try
            If dgv.RowCount < 1 Then Exit Sub

            Dim Row As DataGridViewRow
            Dim DumTotal As Decimal

            For Each Row In dgv.Rows
                Dim strQty As String = Row.Cells("qty").Value.ToString

                If Not strQty = "" Then
                    Dim QtyCell As Double = Row.Cells("qty").Value
                    Dim CostCell As Decimal = Row.Cells("price").Value

                    DumTotal = (CostCell * QtyCell) + DumTotal
                End If

            Next Row

            txt.Text = DumTotal.ToString("N3")

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ComputeRemainBal(ByVal ItmID As String, ByVal Cost As Decimal, ByVal Arrastre As Decimal, ByVal AmtPerModel As Decimal, ByVal Discount As Decimal, ByVal TBoxBal As TextBox, ByVal FreightOnly As Decimal, ByVal FreightArrastre As Decimal)
        Try
            Cost = Format(Cost, "0.000")

            ModPsqlStr = "SELECT * FROM Temp_AFsIn WHERE item_id = '" & ItmID & "' " & _
            "AND cost = " & Cost & " AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()
            If ModPsqlReader.Read() Then
                ModPsqlReader.Close()
                sqlDBconn.Close()
                Exit Sub
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If
            If Not Arrastre > 0 Then
                Dim getPct As Decimal
                getPct = AmtPerModel * Discount
                TBoxBal.Text = (Val(TBoxBal.Text.Replace(",", "")) - (FreightOnly + getPct))
            Else
                Dim getPct As Decimal
                getPct = AmtPerModel * Discount
                TBoxBal.Text = (Val(TBoxBal.Text.Replace(",", "")) - (FreightArrastre + getPct))
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub CreateNewLink(ByVal listfrm As Form)
        Try
            If MDI.stbAccessLevel.Text = "Guest" Then
                MessageBox.Show("Need to be atleast Encoder level to gain access.", listfrm.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            Dim strlistfrm As String = listfrm.Text

            Select Case strlistfrm

                Case "Warehouse Receipt"
                    With My.Forms.frmViewWR
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewWR.PerformClick()
                    End With

                Case "Receiving Report"
                    With My.Forms.frmViewRR
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewRR.PerformClick()
                    End With

                Case "Pick-up Receipt"
                    With My.Forms.frmViewPUR
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewPUR.PerformClick()
                    End With

                Case "Stock Requisition Slip"
                    With My.Forms.frmViewSRS
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewSRS.PerformClick()
                    End With

                Case "Stock Transfer Receipt"
                    With My.Forms.frmViewSTR
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewSTR.PerformClick()
                    End With

                Case "Credit Invoice"
                    With My.Forms.frmViewCI
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewCI.PerformClick()
                    End With

                Case "Cash Invoice"
                    With My.Forms.frmViewCA
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewCA.PerformClick()
                    End With

                Case "Delivery Receipt"
                    With My.Forms.frmViewDR
                        If .Visible = True Then .Close()
                        listfrm.Close()
                        MDI.mnuNewDR.PerformClick()
                    End With

            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub DelPrevTempIn()
        Try
            ModPsqlStr = "SELECT * FROM Temp_AFsIn WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()
                ModPsqlStr = "DELETE FROM Temp_AFsIn WHERE ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr
                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub DelPrevTempOut()
        Try
            ModPsqlStr = "SELECT * FROM Temp_AFsOut WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()
                ModPsqlStr = "DELETE FROM Temp_AFsOut WHERE ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr
                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub DelPrevTempTransfer()
        Try
            ModPsqlStr = "SELECT * FROM Temp_AFsTransfer WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()
                ModPsqlStr = "DELETE FROM Temp_AFsTransfer WHERE ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr
                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub dgvHistoryConfig(ByVal dgv As DataGridView)
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgv.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "date"
                        .HeaderText = "Date"
                        .Width = 100
                    Case "af"
                        .HeaderText = "Reference"
                        .Width = 100
                    Case "number"
                        .HeaderText = "Number"
                        .Width = 100
                    Case "trans"
                        .HeaderText = "Transaction"
                        .Width = 150
                    Case "branch"
                        .HeaderText = "Branch"
                        .Width = 200
                    Case "cost"
                        .HeaderText = "Cost"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###,###.###"
                    Case "price"
                        .HeaderText = "Price"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###,###.###"
                End Select
            End With
        Next
    End Sub

    Public Sub dgvListConfig(ByVal dgv As DataGridView)
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgv.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "pop_id"
                        .HeaderText = "No."
                        .Width = 30
                    Case "custid"
                        .HeaderText = "Cust.ID"
                        .Width = 100
                    Case "deliveredto"
                        .HeaderText = "Delivered To"
                        .Width = 150
                    Case "date"
                        .HeaderText = "Date"
                        .Width = 100
                    Case "number"
                        .HeaderText = "Number"
                        .Width = 100
                    Case "trans"
                        .HeaderText = "Transaction"
                        .Width = 100
                    Case "supplier"
                        .HeaderText = "Supplier"
                        .Width = 200
                    Case "custid"
                        .HeaderText = "Cust.ID"
                        .Width = 100
                    Case "source"
                        .HeaderText = "Source"
                        .Width = 200
                    Case "branch"
                        .HeaderText = "Branch"
                        .Width = 150
                    Case "brand"
                        .HeaderText = "Brand"
                        .Width = 100
                    Case "product"
                        .HeaderText = "Product"
                        .Width = 100
                    Case "model"
                        .HeaderText = "Model"
                        .Width = 150
                    Case "desc"
                        .HeaderText = "Description"
                        .Width = 100
                    Case "class"
                        .HeaderText = "Class"
                        .Width = 50
                    Case "free"
                        .HeaderText = "Free?"
                        .Width = 50
                    Case "serial"
                        .HeaderText = "Serial Number"
                        .Width = 100
                    Case "qty"
                        .HeaderText = "Qty."
                        .Width = 50
                    Case "unit"
                        .HeaderText = "Unit"
                        .Width = 100
                    Case "cost"
                        .HeaderText = "Cost"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "price"
                        .HeaderText = "Price"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "remarks"
                        .HeaderText = "Remarks"
                        .Width = 200
                    Case "user"
                        .HeaderText = "Encoder"
                        .Width = 100
                    Case "datepost"
                        .HeaderText = "Posted"
                        .Width = 100
                End Select
            End With
        Next
    End Sub

    Public Sub dgvMakeConfig(ByVal dgv As DataGridView)
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgv.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "pop_id"
                        .HeaderText = "No."
                        .Width = 30
                    Case "ref"
                        .HeaderText = "Ref."
                        .Width = 70
                    Case "number"
                        .HeaderText = "Number"
                        .Width = 70
                    Case "brand"
                        .HeaderText = "Brand"
                        .Width = 150
                    Case "product"
                        .HeaderText = "Product"
                        .Width = 150
                    Case "model"
                        .HeaderText = "Model"
                        .Width = 200
                    Case "desc"
                        .HeaderText = "Description"
                        .Width = 200
                    Case "class"
                        .HeaderText = "Class"
                        .Width = 50
                    Case "free"
                        .HeaderText = "Free?"
                        .Width = 50
                    Case "serial"
                        .HeaderText = "Serial Number"
                        .Width = 150
                    Case "qty"
                        .HeaderText = "Qty"
                        .Width = 50
                    Case "unit"
                        .HeaderText = "Unit"
                        .Width = 100
                    Case "cost"
                        .HeaderText = "Cost"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "price"
                        .HeaderText = "Price"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "discount"
                        .HeaderText = "Discount"
                        .Width = 100
                    Case "amtpermodel"
                        .HeaderText = "Amt./Model"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "OfisUse"
                        .HeaderText = "Office Use"
                        .Width = 150
                End Select
            End With
        Next
    End Sub

    Public Sub dgvViewConfig(ByVal dgv As DataGridView)
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgv.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "run_id"
                        .Visible = False
                    Case "root_id"
                        .Visible = False
                    Case "item_id"
                        .Visible = False
                    Case "pop_id"
                        .HeaderText = "No."
                        .Width = 30
                    Case "ref_af"
                        .HeaderText = "Reference"
                        .Width = 100
                    Case "ref_af_num"
                        .HeaderText = "Reference#"
                        .Width = 100
                    Case "brand"
                        .HeaderText = "Brand"
                        .Width = 100
                    Case "product"
                        .HeaderText = "Product"
                        .Width = 100
                    Case "model"
                        .HeaderText = "Model"
                        .Width = 150
                    Case "desc"
                        .HeaderText = "Description"
                        .Width = 100
                    Case "class"
                        .HeaderText = "Class"
                        .Width = 50
                    Case "free"
                        .HeaderText = "Free?"
                        .Width = 50
                    Case "serial"
                        .HeaderText = "Serial Number"
                        .Width = 100
                    Case "qty"
                        .HeaderText = "Qty."
                        .Width = 50
                    Case "unit"
                        .HeaderText = "Unit"
                        .Width = 100
                    Case "cost"
                        .HeaderText = "Cost"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "price"
                        .HeaderText = "Price"
                        .Width = 100
                        .DefaultCellStyle.Format = "#,###.###"
                    Case "ofis_use"
                        .HeaderText = "Office Use"
                        .Width = 150
                    Case "user"
                        .HeaderText = "Encoder"
                        .Width = 100
                    Case "datepost"
                        .HeaderText = "Posted"
                        .Width = 100
                End Select
            End With
        Next
    End Sub

    Public Sub DisplayAF(ByVal strFrm As String, ByVal dgv As DataGridView)
        Try
            If dgv.RowCount < 1 Then Exit Sub
            Dim indexPos As Integer = dgv.CurrentCell.RowIndex

            If dgv.Item("unit", indexPos).Value.ToString = "" Then Exit Sub

            Dim strAF, strNum, strBranch As String
            Dim col As DataGridViewColumn

            GetShortAF(strFrm)

            For Each col In dgv.Columns
                strAF = GenShortAF
                strNum = dgv.Item("number", indexPos).Value
                strBranch = dgv.Item("branch", indexPos).Value
                With col
                    Select Case strAF
                        Case "WR"
                            With My.Forms.frmViewWR
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "RR"
                            With My.Forms.frmViewRR
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "SRS"
                            With My.Forms.frmViewSRS
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "STR"
                            With My.Forms.frmViewSTR
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "CI"
                            With My.Forms.frmViewCI
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "CA"
                            With My.Forms.frmViewCA
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "PUR"
                            With My.Forms.frmViewPUR
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                        Case "DR"
                            With My.Forms.frmViewDR
                                .txtNumber.Text = strNum
                                .txtBranch.Text = strBranch
                                .Show()
                            End With
                    End Select
                End With
            Next

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub FocusThisEntry(ByVal dgv As DataGridView, ByVal intPopID As Integer)
        Try
            Dim dgvRow As DataGridViewRow

            For Each dgvRow In dgv.Rows
                With dgvRow
                    dgv.CurrentCell = dgv.Rows(dgvRow.Index).Cells("pop_id")
                    If .Cells("pop_id").Value = intPopID Then Exit For
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GenHistory(ByVal strNumber As String, ByVal dgv As DataGridView, ByVal dgvH As DataGridView, ByVal txt As Label)
        Try
            Dim strTypeTable As String

            If dgv.RowCount < 1 Then Exit Sub

            Dim indexPos As Integer = dgv.CurrentCell.RowIndex

            SelectedItemID = dgv.Item("item_id", indexPos).Value.ToString

            If SelectedItemID = "" Then Exit Sub

            'ToTrack
            SelectedItemID = dgv.Item("item_id", indexPos).Value
            GenRunID = dgv.Item("run_id", indexPos).Value
            GenRootID = dgv.Item("root_id", indexPos).Value

            'GetITemHistory
            HistoryTable = MakeDataTableHistory()

            ModPsqlStr = "SELECT type_tables FROM Library WHERE lib_id = (SELECT type_lib_id FROM ItemMasterlist WHERE item_id = '" & SelectedItemID & "')"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            ModPsqlReader.Read()
            strTypeTable = ModPsqlReader("type_tables")
            ModPsqlReader.Close()
            sqlDBconn.Close()

            ModPsqlStr = "SELECT type_tables FROM Library WHERE lib_id = (SELECT type_lib_id FROM ItemMasterlist WHERE item_id = '" & SelectedItemID & "')"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            ModPsqlReader.Read()
            strTypeTable = ModPsqlReader("type_tables")
            ModPsqlReader.Close()
            sqlDBconn.Close()

            ModPsqlStr = "SELECT af_id, date_issued, (SELECT short_name FROM AFs " & _
            "WHERE af_id = tt.af_id) AS shortaf, af_num, " & _
            "trans_id, branch_id, (SELECT long_name FROM Branches WHERE branch_id = tt.branch_id) AS branch, " & _
            "cost, price FROM " & strTypeTable & " as tt " & _
            "WHERE item_id = '" & SelectedItemID & "' AND run_id = '" & GenRunID & "' AND " & _
            "root_id = '" & GenRootID & "'"

            If strTypeTable.Contains("Non") Then
                ModPsqlStr = ModPsqlStr + " AND class = '" & dgv.Item("class", dgv.CurrentCell.RowIndex).Value.ToString & "'"
            Else
                ModPsqlStr = ModPsqlStr + " AND serial_num = '" & dgv.Item("serial", dgv.CurrentCell.RowIndex).Value.ToString & "'"
            End If

            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            While ModPsqlReader.Read()
                Row = HistoryTable.NewRow()

                Row("date") = ModPsqlReader("date_issued")
                Row("af") = ModPsqlReader("shortaf")
                Row("number") = ModPsqlReader("af_num")
                Row("trans") = ModPsqlReader("trans_id")
                Row("branch") = ModPsqlReader("branch")
                Row("cost") = ModPsqlReader("cost")
                Row("price") = ModPsqlReader("price")

                HistoryTable.Rows.Add(Row)
            End While
            ModPsqlReader.Close()
            sqlDBconn.Close()

            HistoryDataview = New DataView
            HistoryDataview.Table = HistoryTable
            HistoryDataview.Sort = "date ASC"
            dgvH.DataSource = HistoryDataview

            Dim hRow As DataGridViewRow
            Dim af, trans As String

            For Each hRow In dgvH.Rows
                af = hRow.Cells(1).Value
                trans = hRow.Cells(3).Value

                ModPsqlStr = "SELECT " & af & " FROM Transactions WHERE trans_id = " & trans & ""
                ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
                sqlDBconn.Open()

                ModPsqlReader = ModPsqlCmd.ExecuteReader()
                ModPsqlReader.Read()
                hRow.Cells(3).Value = ModPsqlReader(0)

                ModPsqlReader.Close()
                sqlDBconn.Close()
            Next

            If Not dgv.Columns(0).HeaderText = "Date" Then dgvHistoryConfig(dgvH)

            'GetStockBal()
            Dim fld As String

            txt.Text = ""

            If strTypeTable.Contains("Non") Then
                fld = "qty_bal"
            Else
                fld = "has_bal"
            End If

            ModPsqlStr = "SELECT CAST(" & fld & " as float) AS bal, af_id FROM " & strTypeTable & " AS tt " & _
            "WHERE item_id = '" & SelectedItemID & "' AND run_id = '" & GenRunID & "' " & _
            "AND root_id = '" & GenRootID & "' AND branch_id = " & GenBranchID & " " & _
            "AND af_id = " & GenAFID & " AND af_num = '" & strNumber & "' " & _
            "AND af_id NOT IN (SELECT af_id FROM AFs WHERE af_type = 'Out')"

            If strTypeTable.Contains("Non") Then ModPsqlStr = ModPsqlStr + " AND class = '" & dgv.Item("class", dgv.CurrentCell.RowIndex).Value.ToString & "'"

            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            If ModPsqlReader.Read() Then
                txt.Text = ModPsqlReader("bal")
                ModPsqlReader.Close()
                sqlDBconn.Close()
            Else
                txt.Text = "N/A"
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GetAFID(ByVal strAF As String)
        Try
            ModPsqlStr = "SELECT af_id FROM AFs WHERE long_name = '" & strAF & "'"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            GenAFID = ModPsqlReader("af_id")

            ModPsqlReader.Close()
            sqlDBconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GetBranchID(ByVal strLongName As String)
        Try
            ModPsqlStr = "SELECT branch_id FROM Branches WHERE long_name = '" & strLongName & "'"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            GenBranchID = ModPsqlReader("branch_id")
            ModPsqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GetShortAF(ByVal strLongAF As String)
        Try
            ModPsqlStr = "SELECT short_name FROM AFs WHERE long_name = '" & strLongAF & "'"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            GenShortAF = ModPsqlReader("short_name")

            ModPsqlReader.Close()
            sqlDBconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GetTransIDTransferOnly()
        Try
            ModPsqlStr = "SELECT trans_id FROM Transactions WHERE " & GenShortAF & " IS NOT NULL"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            GenTransIDTransferOnly = ModPsqlReader("trans_id")

            ModPsqlReader.Close()
            sqlDBconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub GetTransIDWarehouseReceiptOnly()
        Try
            ModPsqlStr = "SELECT TOP 1 trans_id FROM Transactions WHERE " & GenShortAF & " IS NOT NULL"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            GenTransIDPurchasesOnly = ModPsqlReader("trans_id")

            ModPsqlReader.Close()
            sqlDBconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub InputFormat(ByVal InputTBox As TextBox)
        Try
            If Val(InputTBox.Text) < 1 Or InputTBox.Text = "" Or InputTBox.Text.Contains(".") Then Exit Sub

            InputTBox.Text = Format(Val(InputTBox.Text.Replace(",", "")), "#,###,###,###,###,###.####")
            InputTBox.SelectionStart = InputTBox.TextLength
            InputTBox.SelectionLength = 0
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub InserterIn(ByVal intBranch As Integer, ByVal intRowCount As Integer, ByVal dtDateIssued As String, ByVal strAFnum As String, ByVal decTotal As Decimal, ByVal decFreight As Decimal, ByVal decDiscount As Decimal, ByVal decArrastre As Decimal, ByVal strRemarks As String)
        Try
            UpdateStockBalanceAndRootOnly(intBranch, intRowCount)

            ModPsqlStr = "INSERT INTO AFsIn(root_id, run_id, order_id, item_id, class, is_free, qty, cost, " & _
            "date_issued, branch_id, af_id, af_num, trans_id, cust_id, supplier_id, ua_id) SELECT root_id, run_id, order_id, " & _
            "item_id, class, is_free, qty, cost, date_issued, branch_id, af_id, af_num, trans_id, cust_id, supplier_id, ua_id " & _
            "FROM Temp_AFsIn WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            ModPsqlStr = "SELECT * FROM Temp_AFsIn WHERE item_id <> '' AND serial_num <> '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, " & _
                "af_id, af_num, branch_id, date_issued, trans_id) SELECT root_id, run_id, order_id, item_id, is_free, " & _
                "serial_num, cost, af_id, af_num, branch_id, date_issued, trans_id FROM Temp_AFsIn " & _
                "WHERE item_id <> '' AND serial_num <> '' AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            ModPsqlStr = "SELECT * FROM Temp_AFsIn WHERE item_id <> '' AND serial_num = '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, qty_in, " & _
                "qty_bal, af_id, af_num, branch_id, date_issued, trans_id) SELECT root_id, run_id, order_id, item_id, is_free, " & _
                "class, cost, qty AS qty_in, qty AS qty_bal, af_id, af_num, branch_id, date_issued, trans_id " & _
                "FROM Temp_AFsIn WHERE item_id <> '' AND serial_num = '' AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            If GenAFID < 3 Then
                'Save Amounts to table
                ModPsqlStr = "INSERT INTO PurchasesAmounts(run_id, date_issued, " & _
                "af_id, af_num, total, freight, discount, arrastre) " & _
                "VALUES('" & GenRunID & "', '" & dtDateIssued & "', " & GenAFID & ", '" & strAFnum & "', " & _
                "" & decTotal & ", " & decFreight & ", " & decDiscount & ", " & decArrastre & ")"
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
                'End of Amounts

                ModPsqlStr = "UPDATE ItemMasterlist " & _
                "SET cost = (SELECT TOP 1 cost FROM Temp_AFsIn WHERE " & _
                "item_id = ItemMasterlist.item_id " & _
                "AND ua_id = " & MDI.stbUserID.Text & " ORDER BY order_id) " & _
                "WHERE EXISTS " & _
                "(SELECT TOP 1 cost FROM Temp_AFsIn WHERE " & _
                "item_id = ItemMasterlist.item_id " & _
                "AND ua_id = " & MDI.stbUserID.Text & " ORDER BY order_id)"

                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr
                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            End If

            'Save Remarks to table
            ModPsqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
            "VALUES('" & GenRunID & "', " & intBranch & ", '" & dtDateIssued & "', " & GenAFID & ", '" & strAFnum & "', '" & strRemarks & "')"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()
            'End of Remarks

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub InserterOut(ByVal intBranch As Integer, ByVal intRowCount As Integer, ByVal strAFnum As String, ByVal strRemarks As String)
        Try
            UpdateStockBalanceBranch(intBranch, intRowCount)

            ModPsqlStr = "INSERT INTO AFsOut(root_id, run_id, order_id, item_id, class, is_free, qty, cost, price, " & _
            "date_issued, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, cust_id, non_cust, section_lib_id, ua_id) " & _
            "SELECT root_id, run_id, order_id, item_id, class, is_free, qty, cost, price, " & _
            "date_issued, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, cust_id, non_cust, section_lib_id, ua_id " & _
            "FROM Temp_AFsout WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            ModPsqlStr = "SELECT * FROM Temp_AFsOut WHERE item_id <> '' AND serial_num <> '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, price, " & _
                "ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id, has_bal) SELECT root_id, " & _
                "run_id, order_id, item_id, is_free, serial_num, cost, price, ref_af_id, ref_af_num, af_id, af_num, branch_id, " & _
                "date_issued, trans_id, 'False' AS has_bal FROM Temp_AFsOut WHERE item_id <> '' AND serial_num <> '' AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            ModPsqlStr = "SELECT * FROM Temp_AFsOut WHERE item_id <> '' AND serial_num = '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, price, qty_in, " & _
                "qty_bal, ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) SELECT root_id, " & _
                "run_id, order_id, item_id, is_free, class, cost, price, qty AS qty_in, qty AS qty_bal, ref_af_id, ref_af_num, af_id, af_num, " & _
                "branch_id, date_issued, trans_id FROM Temp_AFsOut WHERE item_id <> '' AND serial_num = '' " & _
                "AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            'Save Remarks to table
            ModPsqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
            "SELECT TOP 1 run_id, branch_id, date_issued, af_id, af_num, '" & strRemarks & "' AS remarks " & _
            "FROM Temp_AFsOut WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()
            'End of Remarks

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub InserterTransfers(ByVal intSource As Integer, ByVal intBranch As Integer, ByVal intRowCount As Integer, ByVal strAFnum As String, ByVal strRemarks As String)
        Try
            UpdateStockBalanceSourceAndBranch(intSource, intBranch, intRowCount)

            ModPsqlStr = "INSERT INTO AFsTransfer(root_id, run_id, order_id, item_id, class, is_free, qty, cost, " & _
            "date_issued, src_branch_id, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, ua_id) SELECT root_id, run_id, order_id, " & _
            "item_id, class, is_free, qty, cost, date_issued, src_branch_id, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, ua_id " & _
            "FROM Temp_AFsTransfer WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            ModPsqlStr = "SELECT * FROM Temp_AFsTransfer WHERE item_id <> '' AND serial_num <> '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, " & _
                "ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) SELECT root_id, " & _
                "run_id, order_id, item_id, is_free, serial_num, cost, ref_af_id, ref_af_num, af_id, af_num, branch_id, " & _
                "date_issued, trans_id FROM Temp_AFsTransfer WHERE item_id <> '' AND serial_num <> '' AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            ModPsqlStr = "SELECT * FROM Temp_AFsTransfer WHERE item_id <> '' AND serial_num = '' " & _
            "AND ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader
            If ModPsqlReader.Read Then
                ModPsqlReader.Close()
                sqlDBconn.Close()

                ModPsqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, qty_in, " & _
                "qty_bal, ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) SELECT root_id, " & _
                "run_id, order_id, item_id, is_free, class, cost, qty AS qty_in, qty AS qty_bal, ref_af_id, ref_af_num, af_id, af_num, " & _
                "branch_id, date_issued, trans_id FROM Temp_AFsTransfer WHERE item_id <> '' AND serial_num = '' " & _
                "AND ua_id = " & MDI.stbUserID.Text & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                ModPsqlReader.Close()
                sqlDBconn.Close()
            End If

            'Save Remarks to table
            ModPsqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
            "SELECT TOP 1 run_id, branch_id, date_issued, af_id, af_num, '" & strRemarks & "' AS remarks " & _
            "FROM Temp_AFsTransfer WHERE ua_id = " & MDI.stbUserID.Text & ""
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()
            'End of Remarks

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub NumberOfRowsPrompt()
        Try
            Dim intInput As String = InputBox("To cancel, type a valid number then press enter. Close Accountable Form", "Enter the number of items", "0")

            If Not IsInteger(intInput) Then
                MessageBox.Show("Invalid value!", "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                NumberOfRowsPrompt()
            Else
                NumOfItems = Convert.ToInt32(intInput)
            End If
        Catch ex As Exception
            NumberOfRowsPrompt()
        End Try
    End Sub

    Public Sub OnLeaveFormat(ByVal LeaveTbox As TextBox)
        Try
            If LeaveTbox.Text = "" Then LeaveTbox.Text = "0.000"
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub OthersAF(ByVal strNumber As String, ByVal intBranch As Integer, ByVal intTrans As Integer, ByVal dtDateIssued As Date, ByVal strCustID As String, ByVal decPrice As Decimal, ByVal strRemarks As String)
        Try
            Dim type As String

            '--Get the tpye of AF
            ModPsqlStr = "SELECT af_type FROM AFs WHERE short_name = '" & GenShortAF & "'"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            type = ModPsqlReader(0)

            ModPsqlReader.Close()
            sqlDBconn.Close()

            ModPsqlStr = "INSERT INTO AFs" & type & "(run_id, order_id, af_id, af_num, branch_id, trans_id, date_issued, cust_id, price, ua_id) " & _
            "VALUES('" & GenRunID & "', 1, " & GenAFID & ", '" & strNumber & "', " & intBranch & ", " & intTrans & ", '" & dtDateIssued & "', '" & strCustID & "', " & decPrice & ", " & MDI.stbUserID.Text & ")"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            'Save Remarks to table
            ModPsqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
            "VALUES('" & GenRunID & "', " & intBranch & ", '" & dtDateIssued & "', " & GenAFID & ", '" & strNumber & "', '" & strRemarks & "')"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()
            'End of Remarks

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ReIndexComboBox(ByVal cmbBox As ComboBox)
        Try
            cmbBox.SelectedIndex = 0
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ResetAFbutton(ByVal btn As Button)
        Try
            Dim listbtnText As New List(Of String)

            listbtnText.Add("New Model")
            listbtnText.Add("&New")
            listbtnText.Add("&Close")

            If listbtnText.Contains(btn.Text) Then
                btn.Enabled = True
            Else
                btn.Enabled = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ResetAFcontrols(ByVal AFs As Form)
        Try
            Dim ctrl As Control
            Dim subctrl As Control

            For Each ctrl In AFs.Controls
                If TypeOf ctrl Is Panel Or TypeOf ctrl Is GroupBox Then
                    For Each subctrl In ctrl.Controls
                        If TypeOf subctrl Is TextBox Then ClearTextBox(subctrl)
                        If TypeOf subctrl Is Button Then subctrl.Enabled = True
                        If TypeOf subctrl Is ComboBox Then ReIndexComboBox(subctrl)
                        If TypeOf subctrl Is MaskedTextBox Then ResetMaskTextBox(subctrl)
                        If TypeOf subctrl Is CheckBox Then UncheckCB(subctrl)
                        If TypeOf subctrl Is RadioButton Then UncheckRB(subctrl)
                    Next
                    ctrl.Enabled = False
                End If
                If TypeOf ctrl Is TextBox Then
                    ClearTextBox(ctrl)
                End If
                If TypeOf ctrl Is Button Then
                    ResetAFbutton(ctrl)
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ResetAFsMenusMDI()
        Try
            MDI.mnuNewWR.Enabled = True
            MDI.mnuNewRR.Enabled = True
            MDI.mnuNewPUR.Enabled = True
            MDI.mnuNewSRS.Enabled = True
            MDI.mnuNewSTR.Enabled = True
            MDI.mnuNewCI.Enabled = True
            MDI.mnuNewCA.Enabled = True
            MDI.mnuNewDR.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ResetAFsMenusDisableMDI()
        Try
            MDI.mnuNewWR.Enabled = False
            MDI.mnuNewRR.Enabled = False
            MDI.mnuNewPUR.Enabled = False
            MDI.mnuNewSRS.Enabled = False
            MDI.mnuNewSTR.Enabled = False
            MDI.mnuNewCI.Enabled = False
            MDI.mnuNewCA.Enabled = False
            MDI.mnuNewDR.Enabled = False
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ResetMaskTextBox(ByVal mtxtBox As MaskedTextBox)
        Try
            mtxtBox.ResetText()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub TempInserterIn(ByVal strRunID As String, ByVal intOrderID As Integer, _
    ByVal strItemID As String, ByVal intFree As Integer, ByVal strClass As String, ByVal decCost As Decimal, ByVal strSerial As String, _
    ByVal dobQty As Double, ByVal strUnit As String, ByVal decAmtPerModel As Decimal, ByVal decDiscount As Decimal, ByVal dtDateIssued As Date, _
    ByVal intBranch As Integer, ByVal intAF As Integer, ByVal strAFnum As String, ByVal intTrans As Integer, ByVal strCustID As String, _
    ByVal intSupplier As Integer, ByVal intUser As Integer)
        Try
            ModPsqlStr = "INSERT INTO Temp_AFsIn(run_id, order_id, item_id, is_free, class, cost, serial_num, " & _
            "qty, unit, amtpermodel, discount, date_issued, branch_id, af_id, af_num, trans_id, cust_id, supplier_id, ua_id)" & _
            "VALUES(@run_id, @order_id, @item_id, @is_free, @class, @cost, @serial_num, " & _
            "@qty, @unit, @amtpermodel, @discount, @date_issued, @branch_id, @af_id, @af_num, @trans_id, " & _
            "@cust_id, @supplier_id, @ua_id)"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr
            With ModPsqlCmd.Parameters
                .Add(New SqlParameter("@run_id", SqlDbType.NVarChar)).Value = strRunID
                .Add(New SqlParameter("@order_id", SqlDbType.Int)).Value = intOrderID
                .Add(New SqlParameter("@item_id", SqlDbType.NVarChar)).Value = strItemID
                .Add(New SqlParameter("@is_free", SqlDbType.Bit)).Value = intFree
                .Add(New SqlParameter("@class", SqlDbType.NVarChar)).Value = strClass
                .Add(New SqlParameter("@cost", SqlDbType.Decimal)).Value = decCost
                .Add(New SqlParameter("@serial_num", SqlDbType.NVarChar)).Value = strSerial
                If strSerial = "" Then
                    .Add(New SqlParameter("@qty", SqlDbType.Int)).Value = dobQty
                Else
                    .Add(New SqlParameter("@qty", SqlDbType.Int)).Value = 1
                End If
                .Add(New SqlParameter("@unit", SqlDbType.NVarChar)).Value = strUnit
                .Add(New SqlParameter("@amtpermodel", SqlDbType.Decimal)).Value = decAmtPerModel
                .Add(New SqlParameter("@discount", SqlDbType.Decimal)).Value = decDiscount
                .Add(New SqlParameter("@date_issued", SqlDbType.SmallDateTime)).Value = dtDateIssued
                .Add(New SqlParameter("@branch_id", SqlDbType.Int)).Value = intBranch
                .Add(New SqlParameter("@af_id", SqlDbType.Int)).Value = intAF
                .Add(New SqlParameter("@af_num", SqlDbType.NVarChar)).Value = strAFnum
                .Add(New SqlParameter("@trans_id", SqlDbType.Int)).Value = intTrans
                .Add(New SqlParameter("@cust_id", SqlDbType.NVarChar)).Value = strCustID
                .Add(New SqlParameter("@supplier_id", SqlDbType.Int)).Value = intSupplier
                .Add(New SqlParameter("@ua_id", SqlDbType.Int)).Value = intUser
            End With
            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub TempInserterOut(ByVal strRoot As String, ByVal strRun As String, ByVal strItemID As String, _
    ByVal strFree As String, ByVal strPrevClass As String, ByVal strClass As String, ByVal strSerial As String, ByVal dobQty As Double, _
    ByVal strUnit As String, ByVal decCost As Decimal, ByVal decPrice As Decimal, ByVal dtDateIssued As Date, ByVal intBranch As Integer, _
    ByVal intRefAF As Integer, ByVal strRefNum As String, ByVal intAF As Integer, ByVal strNum As String, _
    ByVal intTrans As Integer, ByVal strCust As String, ByVal strNonCust As String, ByVal intSection As Integer, ByVal intUser As Integer)
        Try
            If strCust <> "" And strNonCust.Trim <> "" Then strNonCust = "" 'Clear strNonCust value if it is a Customer

            ModPsqlStr = "INSERT INTO Temp_AFsOut(root_id, run_id, order_id, item_id, is_free, prev_class, class, serial_num, qty, unit, " & _
            "cost, price, date_issued, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, cust_id, non_cust, section_lib_id, ua_id)" & _
            "VALUES('" & strRoot & "', '" & strRun & "', " & GenPopID & ", '" & strItemID & "', '" & strFree & "', '" & strPrevClass & "', " & _
            "'" & strClass & "', '" & strSerial & "', " & dobQty & ", '" & strUnit & "', " & decCost & ", " & decPrice & ", '" & dtDateIssued & "', " & intBranch & ", " & _
            "" & intRefAF & ", '" & strRefNum & "', " & intAF & ", '" & strNum & "', " & intTrans & ", '" & strCust & "', '" & strNonCust & "'," & intSection & "," & intUser & ")"

            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub TempInserterTransfer(ByVal strRoot As String, ByVal strRun As String, ByVal strItemID As String, _
    ByVal strFree As String, ByVal strPrevClass As String, ByVal strClass As String, ByVal decCost As Decimal, ByVal strSerial As String, ByVal dobQty As Double, _
    ByVal strUnit As String, ByVal dtDateIssued As Date, ByVal intSource As Integer, ByVal intBranch As Integer, _
    ByVal intRefAF As Integer, ByVal strRefNum As String, ByVal intAF As Integer, ByVal strNum As String, _
    ByVal intTrans As Integer, ByVal intUser As Integer)
        Try

            ModPsqlStr = "INSERT INTO Temp_AFsTransfer(root_id, run_id, order_id, item_id, is_free, prev_class, class, cost, serial_num, " & _
            "qty, unit, date_issued, src_branch_id, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, ua_id)" & _
            "VALUES('" & strRoot & "', '" & strRun & "', " & GenPopID & ", '" & strItemID & "', '" & strFree & "', " & _
            "'" & strPrevClass & "', '" & strClass & "', " & decCost & ", '" & strSerial & "', " & dobQty & ", '" & strUnit & "', '" & dtDateIssued & "', " & _
            "" & intSource & ", " & intBranch & ", " & intRefAF & ", '" & strRefNum & "', " & intAF & ", '" & strNum & "', " & _
            "" & intTrans & ", " & intUser & ")"
            ModPsqlCmd = New SqlCommand
            ModPsqlCmd.Connection = sqlDBconn
            ModPsqlCmd.CommandText = ModPsqlStr

            sqlDBconn.Open()
            ModPsqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub TrimTextBox(ByVal txtBox As TextBox)
        Try
            txtBox.Text.Trim()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub UncheckCB(ByVal cb As CheckBox)
        Try
            cb.CheckState = CheckState.Unchecked
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub UncheckRB(ByVal cb As RadioButton)
        Try
            cb.Checked = False
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateStockBalanceAndRootOnly(ByVal intBranch As Integer, ByVal intRowCount As Integer)
        Try
            Dim i As Integer
            Dim dobQty As Double
            Dim strItemID, strClass As String

            For i = 1 To intRowCount
                'Acquire root_id
                AutoGenRootID()
                'Update BankRootID with the Acquired root_id
                ModPsqlStr = "INSERT INTO BankRootID(root_id) VALUES('" & GenRootID & "')"
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr

                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
                'Assign root_id on each item in the temp
                ModPsqlStr = "UPDATE Temp_AFsIn SET root_id = '" & GenRootID & "' " & _
                "WHERE ua_id = " & MDI.stbUserID.Text & " AND order_id = " & i & ""
                ModPsqlCmd = New SqlCommand
                ModPsqlCmd.Connection = sqlDBconn
                ModPsqlCmd.CommandText = ModPsqlStr
                sqlDBconn.Open()
                ModPsqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
                'End of root_id

                ModPsqlStr = "SELECT item_id, qty, class FROM Temp_AFsIn WHERE order_id = " & i & ""

                ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
                sqlDBconn.Open()
                ModPsqlReader = ModPsqlCmd.ExecuteReader

                ModPsqlReader.Read()
                strItemID = ModPsqlReader("item_id")
                dobQty = ModPsqlReader("qty")
                strClass = ModPsqlReader("class")
                ModPsqlReader.Close()
                sqlDBconn.Close()

                If strClass <> "" Then
                    ModPsqlStr = "UPDATE StockFragile " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "' AND class = '" & strClass & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    ModPsqlStr = "UPDATE StockDurable " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                End If
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateStockBalanceBranch(ByVal intBranch As Integer, ByVal intRowCount As Integer)
        Try
            Dim i, intRefAFID As Integer
            Dim dobQty As Double
            Dim strRunID, strRootID, strRefnum, strItemID, strPrevclass, strClass, strSerial As String

            For i = 1 To intRowCount
                ModPsqlStr = "SELECT root_id, run_id, ref_af_id, ref_af_num, " & _
                "item_id, qty, prev_class, class, serial_num FROM Temp_AFsOut WHERE order_id = " & i & ""

                ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
                sqlDBconn.Open()
                ModPsqlReader = ModPsqlCmd.ExecuteReader

                ModPsqlReader.Read()
                strRunID = ModPsqlReader("run_id")
                strRootID = ModPsqlReader("root_id")
                intRefAFID = ModPsqlReader("ref_af_id")
                strRefnum = ModPsqlReader("ref_af_num")
                strItemID = ModPsqlReader("item_id")
                dobQty = ModPsqlReader("qty")
                strPrevclass = ModPsqlReader("prev_class")
                strClass = ModPsqlReader("class")
                strSerial = ModPsqlReader("serial_num")
                ModPsqlReader.Close()
                sqlDBconn.Close()

                If strClass <> "" Then
                    '---Update Stock on File by Deducting
                    ModPsqlStr = "UPDATE StockFragile " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] - " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "' AND class = '" & strPrevclass & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_out and qty_bal of a selected item
                    ModPsqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_out = (qty_out + " & dobQty & ") " & _
                    "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                    "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                    "item_id = '" & strItemID & "' AND class = '" & strPrevclass & "' AND " & _
                    "branch_id = " & intBranch & ""
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_bal of a selected item
                    ModPsqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_bal = (qty_in - qty_out) " & _
                    "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                    "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                    "item_id = '" & strItemID & "' AND class = '" & strPrevclass & "' AND " & _
                    "branch_id = " & intBranch & ""
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    ModPsqlStr = "UPDATE StockDurable " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] - " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    If GetTypeLibID(strItemID) > 2 Then
                        '---Update qty_out of a selected item
                        ModPsqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_out = (qty_out + " & dobQty & ") " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND branch_id = " & intBranch & ""
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()

                        '---Update qty_bal of a selected item
                        ModPsqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_bal = (qty_in - qty_out) " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND branch_id = " & intBranch & ""
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    Else
                        ModPsqlStr = "UPDATE TypeSerial SET has_bal = 'False' " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND serial_num = '" & strSerial & "' AND " & _
                        "has_bal = 'True'"
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    End If
                End If
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateStockBalanceSourceAndBranch(ByVal intSource As Integer, ByVal intBranch As Integer, ByVal intRowCount As Integer)
        Try
            Dim i, intRefAFID As Integer
            Dim dobQty As Double
            Dim strRunID, strRootID, strRefnum, strItemID, strPrevclass, strClass, strSerial As String

            For i = 1 To intRowCount
                ModPsqlStr = "SELECT root_id, run_id, ref_af_id, ref_af_num, " & _
                "item_id, qty, prev_class, class, serial_num FROM Temp_AFsTransfer WHERE order_id = " & i & ""

                ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
                sqlDBconn.Open()
                ModPsqlReader = ModPsqlCmd.ExecuteReader

                ModPsqlReader.Read()
                strRunID = ModPsqlReader("run_id")
                strRootID = ModPsqlReader("root_id")
                intRefAFID = ModPsqlReader("ref_af_id")
                strRefnum = ModPsqlReader("ref_af_num")
                strItemID = ModPsqlReader("item_id")
                dobQty = ModPsqlReader("qty")
                strPrevclass = ModPsqlReader("prev_class")
                strClass = ModPsqlReader("class")
                strSerial = ModPsqlReader("serial_num")
                ModPsqlReader.Close()
                sqlDBconn.Close()

                If strClass <> "" Then
                    '---Update Stock on File by Adding
                    ModPsqlStr = "UPDATE StockFragile " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "' AND class = '" & strClass & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update Stock on File by Deducting
                    ModPsqlStr = "UPDATE StockFragile " & _
                    "SET [" & intSource & "] = ([" & intSource & "] - " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "' AND class = '" & strPrevclass & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_out and qty_bal of a selected item
                    ModPsqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_out = (qty_out + " & dobQty & ") " & _
                    "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                    "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                    "item_id = '" & strItemID & "' AND class = '" & strPrevclass & "' AND " & _
                    "branch_id = " & intSource & ""
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    '---Update qty_bal of a selected item
                    ModPsqlStr = "UPDATE TypeNonSerial " & _
                    "SET qty_bal = (qty_in - qty_out) " & _
                    "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                    "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                    "item_id = '" & strItemID & "' AND class = '" & strPrevclass & "' AND " & _
                    "branch_id = " & intSource & ""
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    ModPsqlStr = "UPDATE StockDurable " & _
                    "SET [" & intBranch & "] = ([" & intBranch & "] + " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    ModPsqlStr = "UPDATE StockDurable " & _
                    "SET [" & intSource & "] = ([" & intSource & "] - " & dobQty & ") " & _
                    "WHERE item_id = '" & strItemID & "'"
                    ModPsqlCmd = New SqlCommand
                    ModPsqlCmd.Connection = sqlDBconn
                    ModPsqlCmd.CommandText = ModPsqlStr
                    sqlDBconn.Open()
                    ModPsqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()

                    If GetTypeLibID(strItemID) > 2 Then
                        '---Update qty_out of a selected item
                        ModPsqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_out = (qty_out + " & dobQty & ") " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND branch_id = " & intSource & ""
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()

                        '---Update qty_bal of a selected item
                        ModPsqlStr = "UPDATE TypeNonSerial " & _
                        "SET qty_bal = (qty_in - qty_out) " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND branch_id = " & intSource & ""
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    Else
                        ModPsqlStr = "UPDATE TypeSerial SET has_bal = 'False' " & _
                        "WHERE run_id = '" & strRunID & "' AND root_id = '" & strRootID & "' AND " & _
                        "af_id = " & intRefAFID & " AND af_num = '" & strRefnum & "' AND " & _
                        "item_id = '" & strItemID & "' AND serial_num = '" & strSerial & "' AND " & _
                        "has_bal = 'True'"
                        ModPsqlCmd = New SqlCommand
                        ModPsqlCmd.Connection = sqlDBconn
                        ModPsqlCmd.CommandText = ModPsqlStr
                        sqlDBconn.Open()
                        ModPsqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()
                    End If
                End If
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub VATXview(ByVal dgv As DataGridView, ByVal txt As TextBox)
        Try
            Dim decVAT As Decimal
            Dim Row As DataGridViewRow
            Dim DumTotal As Decimal

            ModPsqlStr = "SELECT vat FROM Library WHERE vat IS NOT NULL"
            ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModPsqlReader = ModPsqlCmd.ExecuteReader()

            ModPsqlReader.Read()
            decVAT = ModPsqlReader(0)

            ModPsqlReader.Close()
            sqlDBconn.Close()

            dgv.Columns("item_id").Visible = True

            For Each Row In dgv.Rows
                If Row.Cells("product").Value.ToString <> "" Then
                    ModPsqlStr = "SELECT im.item_id, im.type_lib_id, lib.types FROM Library lib, ItemMasterlist im " & _
                    "WHERE im.item_id = '" & Row.Cells("item_id").Value & "' " & _
                    "AND lib.lib_id = im.type_lib_id"

                    ModPsqlCmd = New SqlCommand(ModPsqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    ModPsqlReader = ModPsqlCmd.ExecuteReader()
                    ModPsqlReader.Read()

                    If Not ModPsqlReader("Types").ToString.Contains("WITHOUT") Then
                        Row.Cells("cost").Value = Row.Cells("cost").Value / decVAT
                        DumTotal = DumTotal + Row.Cells("cost").Value
                    End If

                    ModPsqlReader.Close()
                    sqlDBconn.Close()
                End If
            Next Row

            ComputeCost(dgv, txt)
            dgvViewConfig(dgv)
            dgv.Columns("item_id").Visible = False

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub ViewThis(ByVal frm As Form, ByVal dgv As DataGridView)
        Try
            Dim index As Integer = dgv.CurrentCell.RowIndex
            Dim strShortAF As String = dgv.Item("af", index).Value
            Dim strNumber As String = dgv.Item("number", index).Value
            Dim strBranch As String = dgv.Item("branch", index).Value

            If strShortAF = GenShortAF Then Exit Sub

            Select Case strShortAF
                Case "WR"
                    With My.Forms.frmViewWR
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "RR"
                    With My.Forms.frmViewRR
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "SRS"
                    With My.Forms.frmViewSRS
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "STR"
                    With My.Forms.frmViewSTR
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "CI"
                    With My.Forms.frmViewCI
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "CA"
                    With My.Forms.frmViewCA
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "PUR"
                    With My.Forms.frmViewPUR
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
                Case "DR"
                    With My.Forms.frmViewDR
                        .txtNumber.Text = strNumber
                        .txtBranch.Text = strBranch
                        .Show()
                        frm.Close()
                    End With
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Module

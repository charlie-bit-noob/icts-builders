<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStockPosition
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtItemID = New System.Windows.Forms.TextBox
        Me.RptViewer = New Microsoft.Reporting.WinForms.ReportViewer
        Me.btnCaptureItem = New System.Windows.Forms.Button
        Me.btnItemFinder = New System.Windows.Forms.Button
        Me.dsTransaction = New ICTS_Hardware.dsTransaction
        Me.tblStockPosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dsTransaction, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tblStockPosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Item ID:"
        '
        'txtItemID
        '
        Me.txtItemID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemID.Location = New System.Drawing.Point(66, 14)
        Me.txtItemID.Name = "txtItemID"
        Me.txtItemID.ReadOnly = True
        Me.txtItemID.Size = New System.Drawing.Size(119, 21)
        Me.txtItemID.TabIndex = 3
        '
        'RptViewer
        '
        Me.RptViewer.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RptViewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        ReportDataSource1.Name = "dsTransaction_tblStockPos"
        ReportDataSource1.Value = Me.tblStockPosBindingSource
        Me.RptViewer.LocalReport.DataSources.Add(ReportDataSource1)
        Me.RptViewer.LocalReport.ReportEmbeddedResource = "ICTS_Hardware.RptStockPos.rdlc"
        Me.RptViewer.Location = New System.Drawing.Point(12, 41)
        Me.RptViewer.Name = "RptViewer"
        Me.RptViewer.Size = New System.Drawing.Size(970, 518)
        Me.RptViewer.TabIndex = 4
        '
        'btnCaptureItem
        '
        Me.btnCaptureItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCaptureItem.Location = New System.Drawing.Point(215, 12)
        Me.btnCaptureItem.Name = "btnCaptureItem"
        Me.btnCaptureItem.Size = New System.Drawing.Size(25, 23)
        Me.btnCaptureItem.TabIndex = 1
        Me.btnCaptureItem.Text = "&<"
        Me.btnCaptureItem.UseVisualStyleBackColor = True
        '
        'btnItemFinder
        '
        Me.btnItemFinder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItemFinder.Location = New System.Drawing.Point(189, 12)
        Me.btnItemFinder.Name = "btnItemFinder"
        Me.btnItemFinder.Size = New System.Drawing.Size(25, 23)
        Me.btnItemFinder.TabIndex = 0
        Me.btnItemFinder.Text = "&>"
        Me.btnItemFinder.UseVisualStyleBackColor = True
        '
        'dsTransaction
        '
        Me.dsTransaction.DataSetName = "dsTransaction"
        Me.dsTransaction.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'tblStockPosBindingSource
        '
        Me.tblStockPosBindingSource.DataMember = "tblStockPos"
        Me.tblStockPosBindingSource.DataSource = Me.dsTransaction
        '
        'frmStockPosition
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(994, 571)
        Me.Controls.Add(Me.btnCaptureItem)
        Me.Controls.Add(Me.btnItemFinder)
        Me.Controls.Add(Me.RptViewer)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtItemID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStockPosition"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Position"
        CType(Me.dsTransaction, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tblStockPosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtItemID As System.Windows.Forms.TextBox
    Friend WithEvents RptViewer As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents btnCaptureItem As System.Windows.Forms.Button
    Friend WithEvents btnItemFinder As System.Windows.Forms.Button
    Friend WithEvents tblStockPosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents dsTransaction As ICTS_Hardware.dsTransaction
End Class

Public Class frmProgress

    Inherits System.Windows.Forms.Form

    Public Overloads Sub Show(ByVal Message As String)
        lblStatus.Text = Message

        Me.Show()
        Application.DoEvents()
    End Sub

End Class
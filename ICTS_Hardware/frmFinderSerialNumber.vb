Imports System.Data
Imports System.Data.SqlClient

Public Class frmFinderSerialNumber

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_run_id As DataColumn = New DataColumn("run_id")
        col_run_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_run_id)

        Dim col_root_id As DataColumn = New DataColumn("root_id")
        col_root_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_root_id)

        Dim col_pop_id As DataColumn = New DataColumn("pop_id")
        col_pop_id.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_pop_id)

        Dim col_date As DataColumn = New DataColumn("date")
        col_date.DataType = System.Type.GetType("System.DateTime")
        TableTemp.Columns.Add(col_date)

        Dim col_af As DataColumn = New DataColumn("af")
        col_af.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_af)

        Dim col_number As DataColumn = New DataColumn("number")
        col_number.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_number)

        Dim col_trans As DataColumn = New DataColumn("trans")
        col_trans.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_trans)

        Dim col_branch As DataColumn = New DataColumn("branch")
        col_branch.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_branch)

        Dim col_free As DataColumn = New DataColumn("free")
        col_free.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_free)

        Dim col_serial As DataColumn = New DataColumn("serial")
        col_serial.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_serial)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        TableTemp.Columns.Add(col_cost)

        TableTemp.DefaultView.Sort = "date ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Function GetSrcAFID(ByVal af_short As String) As Integer
        Dim retID As Integer

        sqlStr = "SELECT af_id FROM AFs WHERE short_name = '" & af_short & "'"
        sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
        sqlDBconn.Open()
        sqlReader = sqlCmd.ExecuteReader()

        sqlReader.Read()
        retID = sqlReader(0)

        sqlReader.Close()
        sqlDBconn.Close()

        Return retID
    End Function

    Private Sub FilldgvSerials()
        Try
            TempTable = MakeDataTableTemp()

            Me.sqlStr = "SELECT root_id, run_id, order_id, item_id, date_issued, af_id, " & _
            "(SELECT short_name FROM AFs WHERE af_id = ts.af_id) AS ts_short, " & _
            "af_num, trans_id, branch_id, " & _
            "(SELECT long_name FROM Branches WHERE branch_id = ts.branch_id) AS ts_branch, " & _
            "is_free, cost, serial_num, has_bal FROM TypeSerial AS ts WHERE branch_id = " & SelectedSrcBranchID & " " & _
            "AND item_id = '" & SelectedItemID & "' AND has_bal = 'True' " & _
            "AND ts.item_id IN (SELECT item_id FROM ItemMasterlist WHERE item_id = ts.item_id " & _
            "AND non_stock = 'False')"

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("root_id") = sqlReader("root_id")
                Row("run_id") = sqlReader("run_id")
                Row("pop_id") = sqlReader("order_id")
                Row("date") = sqlReader("date_issued")
                Row("af") = sqlReader("ts_short")
                Row("number") = sqlReader("af_num")
                Row("trans") = sqlReader("trans_id")
                Row("branch") = sqlReader("ts_branch")
                Row("free") = sqlReader("is_free")
                Row("serial") = sqlReader("serial_num")
                Row("cost") = sqlReader("cost")

                TempTable.Rows.Add(Row)
            End While

            Me.sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "date ASC, pop_id ASC"
            Me.dgvSerialNumbers.DataSource = TempDataView

            Dim srow As DataGridViewRow
            Dim af, trans As String

            For Each srow In Me.dgvSerialNumbers.Rows
                af = srow.Cells(4).Value
                trans = srow.Cells(6).Value

                sqlStr = "SELECT " & af & " FROM Transactions WHERE trans_id = " & trans & ""
                sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
                sqlDBconn.Open()

                sqlReader = sqlCmd.ExecuteReader()
                sqlReader.Read()
                srow.Cells(6).Value = sqlReader(0)

                sqlReader.Close()
                sqlDBconn.Close()
            Next

            Dim Col As DataGridViewColumn
            Dim ColName As String

            For Each Col In Me.dgvSerialNumbers.Columns
                ColName = Col.Name
                With Col
                    Select Case ColName
                        Case "run_id"
                            .Visible = False
                        Case "root_id"
                            .Visible = False
                        Case "pop_id"
                            .Visible = False
                        Case "date"
                            .HeaderText = "Date"
                            .Width = 100
                        Case "af"
                            .HeaderText = "Acctbl. Form"
                            .Width = 100
                        Case "number"
                            .HeaderText = "Number"
                            .Width = 100
                        Case "trans"
                            .HeaderText = "Transaction"
                            .Width = 100
                        Case "branch"
                            .HeaderText = "Branch"
                            .Width = 200
                        Case "free"
                            .HeaderText = "Free?"
                            .Width = 50
                        Case "serial"
                            .HeaderText = "Serial No."
                            .Width = 100
                        Case "cost"
                            .HeaderText = "Cost"
                            .Width = 100
                            .DefaultCellStyle.Format = "#,###,###.###"
                    End Select
                End With
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmFinderSerialNumber_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FilldgvSerials()
    End Sub

    Private Sub dgvSerialNumbers_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSerialNumbers.CellDoubleClick
        Try
            selectedCount = Nothing
            Dim index As Integer = Me.dgvSerialNumbers.CurrentCell.RowIndex

            With Me.dgvSerialNumbers
                rootList.Add(.Item("root_id", index).Value.ToString)
                runList.Add(.Item("run_id", index).Value.ToString)
                afList.Add(Me.GetSrcAFID(.Item("af", index).Value.ToString))
                afnumList.Add(.Item("number", index).Value.ToString)
                IsFreeList.Add(.Item("free", index).Value.ToString)
                costList.Add(.Item("cost", index).Value.ToString)

                My.Settings.Serial = .Item("serial", index).Value.ToString
                selectedCount += 1
            End With

            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmNewSupplier

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmNewSupplier_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtName.Focus()
    End Sub

    Private Sub frmNewSupplier_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.ResetControls()
    End Sub

    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtAddress.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtAddress_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAddress.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtEmail.Focus()
    End Sub

    Private Sub txtEmail_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtEmail.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtTelephone.Focus()
    End Sub

    Private Sub mtxtTelephone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtTelephone.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtWebsite.Focus()
    End Sub

    Private Sub txtWebsite_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWebsite.KeyPress
        If e.KeyChar = Chr(13) Then Me.mtxtMobile.Focus()
    End Sub

    Private Sub mtxtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtMobile.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnSave.Focus()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If Me.HasInvalidInputs = True Then Exit Sub
            CallTrimmer()

            sqlStr = "INSERT INTO Suppliers(name, address, website, email_add, telephone_num, " & _
            "mobile_num, ua_id, date_added)VALUES(@name, @address, @website, @email_add, @telephone_num, " & _
            "@mobile_num, @ua_id, getdate())"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr
            With sqlCmd.Parameters
                .Add(New SqlParameter("@name", SqlDbType.NVarChar)).Value = Me.txtName.Text
                .Add(New SqlParameter("@address", SqlDbType.NVarChar)).Value = Me.txtAddress.Text
                .Add(New SqlParameter("@website", SqlDbType.NVarChar)).Value = Me.txtWebsite.Text
                .Add(New SqlParameter("@email_add", SqlDbType.NVarChar)).Value = Me.txtEmail.Text
                .Add(New SqlParameter("@telephone_num", SqlDbType.NVarChar)).Value = Me.mtxtTelephone.Text
                .Add(New SqlParameter("@mobile_num", SqlDbType.NVarChar)).Value = Me.mtxtMobile.Text
                .Add(New SqlParameter("@ua_id", SqlDbType.Int)).Value = Val(MDI.stbUserID.Text)
            End With
            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            ResetControls()
            MessageBox.Show("New supplier saved!", "Success!", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        CleaningUpObjects()
        Me.Close()
    End Sub

    Private Function HasInvalidInputs() As Boolean
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is TextBox Then
                If HasNullValue(ctrl) = True Then
                    Return True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Sub ResetControls()
        Me.txtName.Clear()
        Me.txtAddress.Clear()
        Me.txtEmail.Text = "N/A"
        Me.txtWebsite.Text = "N/A"
        Me.mtxtTelephone.Text = "000-0000"
        Me.mtxtMobile.Text = "00000000000"
        Me.btnClose.Focus()
    End Sub

    Private Sub CleaningUpObjects()
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
    End Sub
End Class
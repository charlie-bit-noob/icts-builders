<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinderCustomer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtCompany = New System.Windows.Forms.TextBox
        Me.txtTelephone = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.panelIndividual = New System.Windows.Forms.Panel
        Me.txtFirstname = New System.Windows.Forms.TextBox
        Me.txtLastname = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.panelNonIndividual = New System.Windows.Forms.Panel
        Me.MyLabel = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.LinkCreate = New System.Windows.Forms.LinkLabel
        Me.panelContacts = New System.Windows.Forms.Panel
        Me.txtMobile = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.dgvCustomers = New System.Windows.Forms.DataGridView
        Me.btnClose = New System.Windows.Forms.Button
        Me.panelIndividual.SuspendLayout()
        Me.panelNonIndividual.SuspendLayout()
        Me.panelContacts.SuspendLayout()
        CType(Me.dgvCustomers, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(5, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(34, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Name"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Enabled = False
        Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(453, 346)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 16)
        Me.Label8.TabIndex = 184
        Me.Label8.Text = "  Non-Individual"
        Me.Label8.UseCompatibleTextRendering = True
        '
        'txtCompany
        '
        Me.txtCompany.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCompany.Location = New System.Drawing.Point(6, 19)
        Me.txtCompany.MaxLength = 50
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(178, 21)
        Me.txtCompany.TabIndex = 9
        '
        'txtTelephone
        '
        Me.txtTelephone.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTelephone.Location = New System.Drawing.Point(120, 23)
        Me.txtTelephone.MaxLength = 15
        Me.txtTelephone.Name = "txtTelephone"
        Me.txtTelephone.Size = New System.Drawing.Size(108, 21)
        Me.txtTelephone.TabIndex = 13
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-3, 354)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(847, 2)
        Me.GroupBox1.TabIndex = 179
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'panelIndividual
        '
        Me.panelIndividual.Controls.Add(Me.txtFirstname)
        Me.panelIndividual.Controls.Add(Me.txtLastname)
        Me.panelIndividual.Controls.Add(Me.Label2)
        Me.panelIndividual.Controls.Add(Me.Label1)
        Me.panelIndividual.Location = New System.Drawing.Point(12, 371)
        Me.panelIndividual.Name = "panelIndividual"
        Me.panelIndividual.Size = New System.Drawing.Size(374, 46)
        Me.panelIndividual.TabIndex = 177
        '
        'txtFirstname
        '
        Me.txtFirstname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFirstname.Location = New System.Drawing.Point(190, 19)
        Me.txtFirstname.MaxLength = 50
        Me.txtFirstname.Name = "txtFirstname"
        Me.txtFirstname.Size = New System.Drawing.Size(178, 21)
        Me.txtFirstname.TabIndex = 7
        '
        'txtLastname
        '
        Me.txtLastname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtLastname.Location = New System.Drawing.Point(6, 19)
        Me.txtLastname.MaxLength = 50
        Me.txtLastname.Name = "txtLastname"
        Me.txtLastname.Size = New System.Drawing.Size(178, 21)
        Me.txtLastname.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "&Last Name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(187, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "&First Name"
        '
        'panelNonIndividual
        '
        Me.panelNonIndividual.Controls.Add(Me.txtCompany)
        Me.panelNonIndividual.Controls.Add(Me.Label7)
        Me.panelNonIndividual.Location = New System.Drawing.Point(392, 371)
        Me.panelNonIndividual.Name = "panelNonIndividual"
        Me.panelNonIndividual.Size = New System.Drawing.Size(190, 46)
        Me.panelNonIndividual.TabIndex = 178
        '
        'MyLabel
        '
        Me.MyLabel.AutoSize = True
        Me.MyLabel.Enabled = False
        Me.MyLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.MyLabel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MyLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.MyLabel.Location = New System.Drawing.Point(102, 346)
        Me.MyLabel.Name = "MyLabel"
        Me.MyLabel.Size = New System.Drawing.Size(59, 16)
        Me.MyLabel.TabIndex = 183
        Me.MyLabel.Text = "  Individual"
        Me.MyLabel.UseCompatibleTextRendering = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "&Mobile No."
        '
        'LinkCreate
        '
        Me.LinkCreate.AutoSize = True
        Me.LinkCreate.Location = New System.Drawing.Point(733, 330)
        Me.LinkCreate.Name = "LinkCreate"
        Me.LinkCreate.Size = New System.Drawing.Size(96, 13)
        Me.LinkCreate.TabIndex = 182
        Me.LinkCreate.TabStop = True
        Me.LinkCreate.Text = "Add new customer"
        '
        'panelContacts
        '
        Me.panelContacts.Controls.Add(Me.txtTelephone)
        Me.panelContacts.Controls.Add(Me.txtMobile)
        Me.panelContacts.Controls.Add(Me.Label5)
        Me.panelContacts.Controls.Add(Me.Label3)
        Me.panelContacts.Location = New System.Drawing.Point(588, 367)
        Me.panelContacts.Name = "panelContacts"
        Me.panelContacts.Size = New System.Drawing.Size(235, 49)
        Me.panelContacts.TabIndex = 181
        '
        'txtMobile
        '
        Me.txtMobile.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtMobile.Location = New System.Drawing.Point(6, 23)
        Me.txtMobile.MaxLength = 15
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(108, 21)
        Me.txtMobile.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(117, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(77, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "&Telephone No."
        '
        'dgvCustomers
        '
        Me.dgvCustomers.AllowUserToAddRows = False
        Me.dgvCustomers.AllowUserToDeleteRows = False
        Me.dgvCustomers.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvCustomers.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCustomers.BackgroundColor = System.Drawing.Color.White
        Me.dgvCustomers.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvCustomers.Location = New System.Drawing.Point(12, 12)
        Me.dgvCustomers.MultiSelect = False
        Me.dgvCustomers.Name = "dgvCustomers"
        Me.dgvCustomers.RowHeadersVisible = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvCustomers.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCustomers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCustomers.Size = New System.Drawing.Size(815, 303)
        Me.dgvCustomers.TabIndex = 175
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(507, 292)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 176
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmFinderCustomer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(841, 427)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.MyLabel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.panelIndividual)
        Me.Controls.Add(Me.panelNonIndividual)
        Me.Controls.Add(Me.LinkCreate)
        Me.Controls.Add(Me.panelContacts)
        Me.Controls.Add(Me.dgvCustomers)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinderCustomer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Customer Finder"
        Me.panelIndividual.ResumeLayout(False)
        Me.panelIndividual.PerformLayout()
        Me.panelNonIndividual.ResumeLayout(False)
        Me.panelNonIndividual.PerformLayout()
        Me.panelContacts.ResumeLayout(False)
        Me.panelContacts.PerformLayout()
        CType(Me.dgvCustomers, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCompany As System.Windows.Forms.TextBox
    Friend WithEvents txtTelephone As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents panelIndividual As System.Windows.Forms.Panel
    Friend WithEvents txtFirstname As System.Windows.Forms.TextBox
    Friend WithEvents txtLastname As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents panelNonIndividual As System.Windows.Forms.Panel
    Friend WithEvents MyLabel As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LinkCreate As System.Windows.Forms.LinkLabel
    Friend WithEvents panelContacts As System.Windows.Forms.Panel
    Friend WithEvents txtMobile As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvCustomers As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class

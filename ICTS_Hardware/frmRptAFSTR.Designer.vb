<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptAFSTR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource
        Me.RptViewer = New Microsoft.Reporting.WinForms.ReportViewer
        Me.dsExtractAF = New ICTS_Hardware.dsExtractAF
        Me.tblSTRBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dsExtractAF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tblSTRBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RptViewer
        '
        Me.RptViewer.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.RptViewer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.RptViewer.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dsExtractAF_tblSTR"
        ReportDataSource1.Value = Me.tblSTRBindingSource
        Me.RptViewer.LocalReport.DataSources.Add(ReportDataSource1)
        Me.RptViewer.LocalReport.ReportEmbeddedResource = "ICTS_Hardware.RptAFSTR.rdlc"
        Me.RptViewer.Location = New System.Drawing.Point(0, 0)
        Me.RptViewer.Name = "RptViewer"
        Me.RptViewer.Size = New System.Drawing.Size(942, 558)
        Me.RptViewer.TabIndex = 10
        '
        'dsExtractAF
        '
        Me.dsExtractAF.DataSetName = "dsExtractAF"
        Me.dsExtractAF.EnforceConstraints = False
        Me.dsExtractAF.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'tblSTRBindingSource
        '
        Me.tblSTRBindingSource.DataMember = "tblSTR"
        Me.tblSTRBindingSource.DataSource = Me.dsExtractAF
        '
        'frmRptAFSTR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(942, 558)
        Me.Controls.Add(Me.RptViewer)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "frmRptAFSTR"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Transfer Receipt Summary"
        CType(Me.dsExtractAF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tblSTRBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RptViewer As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents tblSTRBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents dsExtractAF As ICTS_Hardware.dsExtractAF
End Class

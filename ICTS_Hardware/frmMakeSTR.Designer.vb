<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMakeSTR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.rbCancel = New System.Windows.Forms.RadioButton
        Me.btnNew = New System.Windows.Forms.Button
        Me.btnPop = New System.Windows.Forms.Button
        Me.btnDone = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.dgvMake = New System.Windows.Forms.DataGridView
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.pnlNonItem = New System.Windows.Forms.Panel
        Me.btnNewModel = New System.Windows.Forms.Button
        Me.txtTotal = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.pnlItemDetails = New System.Windows.Forms.Panel
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtSerialNumber = New System.Windows.Forms.TextBox
        Me.txtQuantity = New System.Windows.Forms.TextBox
        Me.btnSourceFinder = New System.Windows.Forms.Button
        Me.txtItemName = New System.Windows.Forms.TextBox
        Me.btnCaptureItem = New System.Windows.Forms.Button
        Me.btnItemFinder = New System.Windows.Forms.Button
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtUnit = New System.Windows.Forms.TextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.cmbClass = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtItemID = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.pnlAFDetails = New System.Windows.Forms.Panel
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.mtxtDate = New System.Windows.Forms.MaskedTextBox
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.cmbSource = New System.Windows.Forms.ComboBox
        Me.lblNumber = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        CType(Me.dgvMake, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlNonItem.SuspendLayout()
        Me.pnlItemDetails.SuspendLayout()
        Me.pnlAFDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'rbCancel
        '
        Me.rbCancel.AutoSize = True
        Me.rbCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbCancel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.rbCancel.Location = New System.Drawing.Point(26, 3)
        Me.rbCancel.Name = "rbCancel"
        Me.rbCancel.Size = New System.Drawing.Size(71, 17)
        Me.rbCancel.TabIndex = 0
        Me.rbCancel.Text = "C&ancelled"
        Me.rbCancel.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.Location = New System.Drawing.Point(450, 391)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(98, 23)
        Me.btnNew.TabIndex = 11
        Me.btnNew.Text = "&New"
        Me.btnNew.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnPop
        '
        Me.btnPop.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPop.Location = New System.Drawing.Point(554, 391)
        Me.btnPop.Name = "btnPop"
        Me.btnPop.Size = New System.Drawing.Size(98, 23)
        Me.btnPop.TabIndex = 14
        Me.btnPop.Text = "&Pop in"
        Me.btnPop.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnPop.UseVisualStyleBackColor = True
        '
        'btnDone
        '
        Me.btnDone.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDone.Location = New System.Drawing.Point(658, 391)
        Me.btnDone.Name = "btnDone"
        Me.btnDone.Size = New System.Drawing.Size(98, 23)
        Me.btnDone.TabIndex = 15
        Me.btnDone.Text = "&Done"
        Me.btnDone.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDone.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.Location = New System.Drawing.Point(762, 391)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(98, 23)
        Me.btnSave.TabIndex = 16
        Me.btnSave.Text = "&Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(866, 391)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(98, 23)
        Me.btnClose.TabIndex = 17
        Me.btnClose.Text = "&Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'dgvMake
        '
        Me.dgvMake.AllowUserToAddRows = False
        Me.dgvMake.AllowUserToDeleteRows = False
        Me.dgvMake.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvMake.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvMake.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvMake.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvMake.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvMake.Location = New System.Drawing.Point(12, 74)
        Me.dgvMake.MultiSelect = False
        Me.dgvMake.Name = "dgvMake"
        Me.dgvMake.ReadOnly = True
        Me.dgvMake.RowHeadersVisible = False
        Me.dgvMake.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvMake.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMake.Size = New System.Drawing.Size(952, 166)
        Me.dgvMake.TabIndex = 1
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(-7, 313)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(990, 2)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-7, 371)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(990, 2)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'pnlNonItem
        '
        Me.pnlNonItem.Controls.Add(Me.rbCancel)
        Me.pnlNonItem.Location = New System.Drawing.Point(-16, 331)
        Me.pnlNonItem.Name = "pnlNonItem"
        Me.pnlNonItem.Size = New System.Drawing.Size(174, 22)
        Me.pnlNonItem.TabIndex = 5
        '
        'btnNewModel
        '
        Me.btnNewModel.Location = New System.Drawing.Point(889, 217)
        Me.btnNewModel.Name = "btnNewModel"
        Me.btnNewModel.Size = New System.Drawing.Size(75, 23)
        Me.btnNewModel.TabIndex = 12
        Me.btnNewModel.Text = "New Model"
        Me.btnNewModel.UseVisualStyleBackColor = True
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.SystemColors.Control
        Me.txtTotal.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtTotal.Location = New System.Drawing.Point(717, 333)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.Size = New System.Drawing.Size(247, 22)
        Me.txtTotal.TabIndex = 239
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(669, 335)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(42, 14)
        Me.Label16.TabIndex = 238
        Me.Label16.Text = "Total:"
        '
        'pnlItemDetails
        '
        Me.pnlItemDetails.Controls.Add(Me.Label8)
        Me.pnlItemDetails.Controls.Add(Me.Label6)
        Me.pnlItemDetails.Controls.Add(Me.txtSerialNumber)
        Me.pnlItemDetails.Controls.Add(Me.txtQuantity)
        Me.pnlItemDetails.Controls.Add(Me.btnSourceFinder)
        Me.pnlItemDetails.Controls.Add(Me.txtItemName)
        Me.pnlItemDetails.Controls.Add(Me.btnCaptureItem)
        Me.pnlItemDetails.Controls.Add(Me.btnItemFinder)
        Me.pnlItemDetails.Controls.Add(Me.Label24)
        Me.pnlItemDetails.Controls.Add(Me.txtUnit)
        Me.pnlItemDetails.Controls.Add(Me.Label25)
        Me.pnlItemDetails.Controls.Add(Me.cmbClass)
        Me.pnlItemDetails.Controls.Add(Me.Label2)
        Me.pnlItemDetails.Controls.Add(Me.txtItemID)
        Me.pnlItemDetails.Controls.Add(Me.Label7)
        Me.pnlItemDetails.Location = New System.Drawing.Point(-7, 246)
        Me.pnlItemDetails.Name = "pnlItemDetails"
        Me.pnlItemDetails.Size = New System.Drawing.Size(990, 60)
        Me.pnlItemDetails.TabIndex = 241
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(380, 36)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(77, 13)
        Me.Label8.TabIndex = 239
        Me.Label8.Text = "Serial Number:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(230, 36)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(29, 13)
        Me.Label6.TabIndex = 237
        Me.Label6.Text = "Qty."
        '
        'txtSerialNumber
        '
        Me.txtSerialNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerialNumber.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ICTS_Hardware.My.MySettings.Default, "Serial", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtSerialNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSerialNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSerialNumber.Location = New System.Drawing.Point(461, 33)
        Me.txtSerialNumber.MaxLength = 50
        Me.txtSerialNumber.Name = "txtSerialNumber"
        Me.txtSerialNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSerialNumber.Size = New System.Drawing.Size(209, 21)
        Me.txtSerialNumber.TabIndex = 240
        Me.txtSerialNumber.Text = Global.ICTS_Hardware.My.MySettings.Default.Serial
        '
        'txtQuantity
        '
        Me.txtQuantity.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtQuantity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtQuantity.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtQuantity.Location = New System.Drawing.Point(272, 33)
        Me.txtQuantity.MaxLength = 20
        Me.txtQuantity.Name = "txtQuantity"
        Me.txtQuantity.Size = New System.Drawing.Size(102, 21)
        Me.txtQuantity.TabIndex = 238
        Me.txtQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnSourceFinder
        '
        Me.btnSourceFinder.Location = New System.Drawing.Point(873, 31)
        Me.btnSourceFinder.Name = "btnSourceFinder"
        Me.btnSourceFinder.Size = New System.Drawing.Size(98, 23)
        Me.btnSourceFinder.TabIndex = 236
        Me.btnSourceFinder.Text = "Source Finder"
        Me.btnSourceFinder.UseVisualStyleBackColor = True
        '
        'txtItemName
        '
        Me.txtItemName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemName.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtItemName.Location = New System.Drawing.Point(272, 6)
        Me.txtItemName.MaxLength = 30
        Me.txtItemName.Name = "txtItemName"
        Me.txtItemName.ReadOnly = True
        Me.txtItemName.Size = New System.Drawing.Size(699, 21)
        Me.txtItemName.TabIndex = 234
        '
        'btnCaptureItem
        '
        Me.btnCaptureItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCaptureItem.Location = New System.Drawing.Point(199, 4)
        Me.btnCaptureItem.Name = "btnCaptureItem"
        Me.btnCaptureItem.Size = New System.Drawing.Size(25, 23)
        Me.btnCaptureItem.TabIndex = 3
        Me.btnCaptureItem.Text = "&<"
        Me.btnCaptureItem.UseVisualStyleBackColor = True
        '
        'btnItemFinder
        '
        Me.btnItemFinder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItemFinder.Location = New System.Drawing.Point(173, 4)
        Me.btnItemFinder.Name = "btnItemFinder"
        Me.btnItemFinder.Size = New System.Drawing.Size(25, 23)
        Me.btnItemFinder.TabIndex = 2
        Me.btnItemFinder.Text = "&>"
        Me.btnItemFinder.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(676, 36)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(30, 13)
        Me.Label24.TabIndex = 13
        Me.Label24.Text = "Unit:"
        '
        'txtUnit
        '
        Me.txtUnit.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtUnit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUnit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtUnit.Location = New System.Drawing.Point(712, 33)
        Me.txtUnit.MaxLength = 30
        Me.txtUnit.Name = "txtUnit"
        Me.txtUnit.ReadOnly = True
        Me.txtUnit.Size = New System.Drawing.Size(155, 21)
        Me.txtUnit.TabIndex = 14
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(16, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(36, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Class:"
        '
        'cmbClass
        '
        Me.cmbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClass.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbClass.FormattingEnabled = True
        Me.cmbClass.Location = New System.Drawing.Point(71, 33)
        Me.cmbClass.Name = "cmbClass"
        Me.cmbClass.Size = New System.Drawing.Size(150, 21)
        Me.cmbClass.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(230, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Name:"
        '
        'txtItemID
        '
        Me.txtItemID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemID.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtItemID.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtItemID.Location = New System.Drawing.Point(71, 6)
        Me.txtItemID.MaxLength = 30
        Me.txtItemID.Name = "txtItemID"
        Me.txtItemID.ReadOnly = True
        Me.txtItemID.Size = New System.Drawing.Size(101, 21)
        Me.txtItemID.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(16, 9)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Item ID:"
        '
        'pnlAFDetails
        '
        Me.pnlAFDetails.Controls.Add(Me.Label4)
        Me.pnlAFDetails.Controls.Add(Me.txtNumber)
        Me.pnlAFDetails.Controls.Add(Me.mtxtDate)
        Me.pnlAFDetails.Controls.Add(Me.txtRemarks)
        Me.pnlAFDetails.Controls.Add(Me.cmbSource)
        Me.pnlAFDetails.Controls.Add(Me.lblNumber)
        Me.pnlAFDetails.Controls.Add(Me.cmbBranch)
        Me.pnlAFDetails.Controls.Add(Me.Label3)
        Me.pnlAFDetails.Controls.Add(Me.Label1)
        Me.pnlAFDetails.Controls.Add(Me.Label9)
        Me.pnlAFDetails.Location = New System.Drawing.Point(-7, 3)
        Me.pnlAFDetails.Name = "pnlAFDetails"
        Me.pnlAFDetails.Size = New System.Drawing.Size(990, 65)
        Me.pnlAFDetails.TabIndex = 242
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(537, 40)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Remarks:"
        '
        'txtNumber
        '
        Me.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumber.Location = New System.Drawing.Point(367, 37)
        Me.txtNumber.MaxLength = 10
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNumber.Size = New System.Drawing.Size(164, 21)
        Me.txtNumber.TabIndex = 7
        '
        'mtxtDate
        '
        Me.mtxtDate.Location = New System.Drawing.Point(71, 37)
        Me.mtxtDate.Mask = "##/##/####"
        Me.mtxtDate.Name = "mtxtDate"
        Me.mtxtDate.Size = New System.Drawing.Size(236, 21)
        Me.mtxtDate.TabIndex = 5
        '
        'txtRemarks
        '
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRemarks.Location = New System.Drawing.Point(595, 37)
        Me.txtRemarks.MaxLength = 100
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(376, 21)
        Me.txtRemarks.TabIndex = 9
        '
        'cmbSource
        '
        Me.cmbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSource.FormattingEnabled = True
        Me.cmbSource.Location = New System.Drawing.Point(71, 10)
        Me.cmbSource.Name = "cmbSource"
        Me.cmbSource.Size = New System.Drawing.Size(460, 21)
        Me.cmbSource.TabIndex = 1
        '
        'lblNumber
        '
        Me.lblNumber.AutoSize = True
        Me.lblNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumber.Location = New System.Drawing.Point(313, 40)
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Size = New System.Drawing.Size(48, 13)
        Me.lblNumber.TabIndex = 6
        Me.lblNumber.Text = "Number:"
        '
        'cmbBranch
        '
        Me.cmbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(595, 9)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(376, 21)
        Me.cmbBranch.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(537, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(44, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Branch:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Date:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(16, 13)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(44, 13)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Source:"
        '
        'frmMakeSTR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(976, 427)
        Me.Controls.Add(Me.pnlAFDetails)
        Me.Controls.Add(Me.pnlItemDetails)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.pnlNonItem)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.btnDone)
        Me.Controls.Add(Me.btnPop)
        Me.Controls.Add(Me.btnNew)
        Me.Controls.Add(Me.dgvMake)
        Me.Controls.Add(Me.btnNewModel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmMakeSTR"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stock Transfer Receipt"
        CType(Me.dgvMake, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlNonItem.ResumeLayout(False)
        Me.pnlNonItem.PerformLayout()
        Me.pnlItemDetails.ResumeLayout(False)
        Me.pnlItemDetails.PerformLayout()
        Me.pnlAFDetails.ResumeLayout(False)
        Me.pnlAFDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rbCancel As System.Windows.Forms.RadioButton
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents btnPop As System.Windows.Forms.Button
    Friend WithEvents btnDone As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents dgvMake As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents pnlNonItem As System.Windows.Forms.Panel
    Friend WithEvents btnNewModel As System.Windows.Forms.Button
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents pnlItemDetails As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtSerialNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtQuantity As System.Windows.Forms.TextBox
    Friend WithEvents btnSourceFinder As System.Windows.Forms.Button
    Friend WithEvents txtItemName As System.Windows.Forms.TextBox
    Friend WithEvents btnCaptureItem As System.Windows.Forms.Button
    Friend WithEvents btnItemFinder As System.Windows.Forms.Button
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtUnit As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cmbClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtItemID As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents pnlAFDetails As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents mtxtDate As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents cmbSource As System.Windows.Forms.ComboBox
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
End Class

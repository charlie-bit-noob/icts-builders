<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFinderItem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.txtProduct = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.btnNewItem = New System.Windows.Forms.Button
        Me.txtDescription = New System.Windows.Forms.TextBox
        Me.txtBrand = New System.Windows.Forms.TextBox
        Me.txtModel = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgvItems = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.PanelSearch = New System.Windows.Forms.Panel
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelSearch.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtProduct
        '
        Me.txtProduct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProduct.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ICTS_Hardware.My.MySettings.Default, "Set_Product", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtProduct.Location = New System.Drawing.Point(63, 40)
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.Size = New System.Drawing.Size(262, 21)
        Me.txtProduct.TabIndex = 3
        Me.txtProduct.Text = Global.ICTS_Hardware.My.MySettings.Default.Set_Product
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(341, 43)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "&Description:"
        '
        'btnNewItem
        '
        Me.btnNewItem.Location = New System.Drawing.Point(777, 431)
        Me.btnNewItem.Name = "btnNewItem"
        Me.btnNewItem.Size = New System.Drawing.Size(59, 23)
        Me.btnNewItem.TabIndex = 17
        Me.btnNewItem.Text = "&New"
        Me.btnNewItem.UseVisualStyleBackColor = True
        '
        'txtDescription
        '
        Me.txtDescription.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDescription.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ICTS_Hardware.My.MySettings.Default, "Set_Description", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtDescription.Location = New System.Drawing.Point(411, 40)
        Me.txtDescription.MaxLength = 100
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(341, 21)
        Me.txtDescription.TabIndex = 7
        Me.txtDescription.Text = Global.ICTS_Hardware.My.MySettings.Default.Set_Description
        '
        'txtBrand
        '
        Me.txtBrand.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBrand.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ICTS_Hardware.My.MySettings.Default, "Set_Brand", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtBrand.Location = New System.Drawing.Point(63, 13)
        Me.txtBrand.MaxLength = 50
        Me.txtBrand.Name = "txtBrand"
        Me.txtBrand.Size = New System.Drawing.Size(262, 21)
        Me.txtBrand.TabIndex = 1
        Me.txtBrand.Text = Global.ICTS_Hardware.My.MySettings.Default.Set_Brand
        '
        'txtModel
        '
        Me.txtModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Global.ICTS_Hardware.My.MySettings.Default, "Set_Model", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me.txtModel.Location = New System.Drawing.Point(411, 13)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(341, 21)
        Me.txtModel.TabIndex = 5
        Me.txtModel.Text = Global.ICTS_Hardware.My.MySettings.Default.Set_Model
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(7, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "&Brand:"
        '
        'dgvItems
        '
        Me.dgvItems.AllowUserToAddRows = False
        Me.dgvItems.AllowUserToDeleteRows = False
        Me.dgvItems.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvItems.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvItems.BackgroundColor = System.Drawing.Color.White
        Me.dgvItems.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvItems.Location = New System.Drawing.Point(12, 12)
        Me.dgvItems.MultiSelect = False
        Me.dgvItems.Name = "dgvItems"
        Me.dgvItems.ReadOnly = True
        Me.dgvItems.RowHeadersVisible = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvItems.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvItems.Size = New System.Drawing.Size(822, 400)
        Me.dgvItems.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(7, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "&Product:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(341, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(39, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "&Model:"
        '
        'PanelSearch
        '
        Me.PanelSearch.Controls.Add(Me.txtBrand)
        Me.PanelSearch.Controls.Add(Me.txtProduct)
        Me.PanelSearch.Controls.Add(Me.Label6)
        Me.PanelSearch.Controls.Add(Me.txtModel)
        Me.PanelSearch.Controls.Add(Me.txtDescription)
        Me.PanelSearch.Controls.Add(Me.Label1)
        Me.PanelSearch.Controls.Add(Me.Label2)
        Me.PanelSearch.Controls.Add(Me.Label3)
        Me.PanelSearch.Location = New System.Drawing.Point(12, 418)
        Me.PanelSearch.Name = "PanelSearch"
        Me.PanelSearch.Size = New System.Drawing.Size(759, 71)
        Me.PanelSearch.TabIndex = 18
        '
        'frmFinderItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 496)
        Me.Controls.Add(Me.btnNewItem)
        Me.Controls.Add(Me.dgvItems)
        Me.Controls.Add(Me.PanelSearch)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFinderItem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Item Finder"
        CType(Me.dgvItems, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelSearch.ResumeLayout(False)
        Me.PanelSearch.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtProduct As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnNewItem As System.Windows.Forms.Button
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox
    Friend WithEvents txtBrand As System.Windows.Forms.TextBox
    Friend WithEvents txtModel As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvItems As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PanelSearch As System.Windows.Forms.Panel
End Class

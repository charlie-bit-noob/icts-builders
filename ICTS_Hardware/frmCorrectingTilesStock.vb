Imports System.Data
Imports System.Data.SqlClient

Public Class frmCorrectingTilesStock

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private sqlCmd2 As SqlCommand
    Private sqlReader2 As SqlDataReader
    Private sqlStr2 As String
    Private sqlDBconn2 As SqlConnection

    Private Sub Button_Reencode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Reencode.Click
        Try
            sqlDBconn2 = New SqlConnection(strDBconn)
            Dim ClassList(2) As String
            Dim strClass As String

            ClassList(0) = "A"
            ClassList(1) = "B"
            ClassList(2) = "C"

            Me.sqlStr = "SELECT item_id FROM ItemMasterlist WHERE group_id = 124"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            While Me.sqlReader.Read
                For Each strClass In classList
                    sqlStr2 = "INSERT INTO StockFragile(item_id, class)VALUES(@item_id, @class)"
                    sqlCmd2 = New SqlCommand
                    sqlCmd2.Connection = Me.sqlDBconn2
                    sqlCmd2.CommandText = sqlStr2
                    With sqlCmd2.Parameters
                        .Add(New SqlParameter("@item_id", SqlDbType.NVarChar)).Value = Me.sqlReader(0)
                        .Add(New SqlParameter("@class", SqlDbType.NVarChar)).Value = strClass
                    End With
                    Me.sqlDBconn2.Open()
                    sqlCmd2.ExecuteNonQuery()
                    Me.sqlDBconn2.Close()
                Next strClass
            End While

            Me.sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show("Done!")

        Catch ex As Exception
            sqlDBconn.Close()
            Me.sqlDBconn2.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub Button_Balancing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Balancing.Click
        Try
            Me.Button_Balancing.Enabled = False

            sqlDBconn2 = New SqlConnection(strDBconn)

            Dim RowsCount As Integer = Nothing

            Me.sqlStr = "SELECT item_id, class, qty_in, branch_id FROM TypeNonSerial " & _
            "WHERE item_id IN (SELECT DISTINCT(item_id) FROM StockFragile) AND af_id = 6"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            While Me.sqlReader.Read
                Me.sqlStr2 = "UPDATE StockFragile SET [" & Me.sqlReader("branch_id") & "] = [" & Me.sqlReader("branch_id") & "] - " & Me.sqlReader("qty_in") & " " & _
                "WHERE item_id = '" & Me.sqlReader("item_id") & "' AND class = '" & Me.sqlReader("class") & "'"
                Me.sqlCmd2 = New SqlCommand
                Me.sqlCmd2.Connection = Me.sqlDBconn2
                Me.sqlCmd2.CommandText = Me.sqlStr2
                Me.sqlDBconn2.Open()
                Me.sqlCmd2.ExecuteNonQuery()
                Me.sqlDBconn2.Close()

                RowsCount += 1

                Me.Label_rows.Text = RowsCount
                Me.Label_rows.Refresh()
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            MessageBox.Show("Done!")
            Me.Button_Balancing.Enabled = True


        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
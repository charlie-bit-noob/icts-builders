<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmViewRR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.cbVATXview = New System.Windows.Forms.CheckBox
        Me.txtArrastre = New System.Windows.Forms.TextBox
        Me.txtFreight = New System.Windows.Forms.TextBox
        Me.txtTotal = New System.Windows.Forms.TextBox
        Me.txtTransaction = New System.Windows.Forms.TextBox
        Me.txtDate = New System.Windows.Forms.TextBox
        Me.txtNumber = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.dgvHistory = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lblstockBal = New System.Windows.Forms.Label
        Me.lbldesc = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtRemarks = New System.Windows.Forms.TextBox
        Me.dgvView = New System.Windows.Forms.DataGridView
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtSource = New System.Windows.Forms.TextBox
        Me.txtBranch = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmRR = New System.Windows.Forms.ContextMenu
        Me.mnuUpdateCost = New System.Windows.Forms.MenuItem
        Me.mnuChangeItem = New System.Windows.Forms.MenuItem
        Me.btnMakeChanges = New System.Windows.Forms.Button
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbVATXview
        '
        Me.cbVATXview.AutoSize = True
        Me.cbVATXview.Location = New System.Drawing.Point(674, 96)
        Me.cbVATXview.Name = "cbVATXview"
        Me.cbVATXview.Size = New System.Drawing.Size(93, 17)
        Me.cbVATXview.TabIndex = 250
        Me.cbVATXview.Text = "&Non-VAT view"
        Me.cbVATXview.UseVisualStyleBackColor = True
        '
        'txtArrastre
        '
        Me.txtArrastre.BackColor = System.Drawing.SystemColors.Control
        Me.txtArrastre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtArrastre.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArrastre.Location = New System.Drawing.Point(643, 352)
        Me.txtArrastre.Name = "txtArrastre"
        Me.txtArrastre.ReadOnly = True
        Me.txtArrastre.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtArrastre.Size = New System.Drawing.Size(121, 21)
        Me.txtArrastre.TabIndex = 249
        Me.txtArrastre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFreight
        '
        Me.txtFreight.BackColor = System.Drawing.SystemColors.Control
        Me.txtFreight.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFreight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFreight.Location = New System.Drawing.Point(441, 352)
        Me.txtFreight.Name = "txtFreight"
        Me.txtFreight.ReadOnly = True
        Me.txtFreight.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFreight.Size = New System.Drawing.Size(123, 21)
        Me.txtFreight.TabIndex = 248
        Me.txtFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotal
        '
        Me.txtTotal.BackColor = System.Drawing.SystemColors.Control
        Me.txtTotal.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(252, 352)
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.ReadOnly = True
        Me.txtTotal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTotal.Size = New System.Drawing.Size(127, 21)
        Me.txtTotal.TabIndex = 247
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTransaction
        '
        Me.txtTransaction.BackColor = System.Drawing.SystemColors.Control
        Me.txtTransaction.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTransaction.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTransaction.Location = New System.Drawing.Point(88, 67)
        Me.txtTransaction.Name = "txtTransaction"
        Me.txtTransaction.ReadOnly = True
        Me.txtTransaction.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtTransaction.Size = New System.Drawing.Size(194, 21)
        Me.txtTransaction.TabIndex = 243
        '
        'txtDate
        '
        Me.txtDate.BackColor = System.Drawing.SystemColors.Control
        Me.txtDate.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(88, 40)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.ReadOnly = True
        Me.txtDate.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDate.Size = New System.Drawing.Size(194, 21)
        Me.txtDate.TabIndex = 242
        '
        'txtNumber
        '
        Me.txtNumber.BackColor = System.Drawing.SystemColors.Control
        Me.txtNumber.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtNumber.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumber.Location = New System.Drawing.Point(88, 13)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.ReadOnly = True
        Me.txtNumber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtNumber.Size = New System.Drawing.Size(194, 21)
        Me.txtNumber.TabIndex = 241
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(159, 355)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(87, 13)
        Me.Label11.TabIndex = 240
        Me.Label11.Text = "Total Amount:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(579, 355)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(58, 13)
        Me.Label13.TabIndex = 239
        Me.Label13.Text = "Arrastre:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(385, 355)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 13)
        Me.Label14.TabIndex = 238
        Me.Label14.Text = "Freight:"
        '
        'dgvHistory
        '
        Me.dgvHistory.AllowUserToAddRows = False
        Me.dgvHistory.AllowUserToDeleteRows = False
        Me.dgvHistory.AllowUserToResizeRows = False
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvHistory.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvHistory.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvHistory.Location = New System.Drawing.Point(12, 408)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvHistory.Size = New System.Drawing.Size(755, 140)
        Me.dgvHistory.TabIndex = 237
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-2, 388)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(789, 2)
        Me.GroupBox1.TabIndex = 236
        Me.GroupBox1.TabStop = False
        '
        'lblstockBal
        '
        Me.lblstockBal.BackColor = System.Drawing.SystemColors.Control
        Me.lblstockBal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblstockBal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblstockBal.Location = New System.Drawing.Point(70, 351)
        Me.lblstockBal.Name = "lblstockBal"
        Me.lblstockBal.Size = New System.Drawing.Size(60, 21)
        Me.lblstockBal.TabIndex = 235
        Me.lblstockBal.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lbldesc
        '
        Me.lbldesc.AutoSize = True
        Me.lbldesc.BackColor = System.Drawing.SystemColors.Control
        Me.lbldesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldesc.Location = New System.Drawing.Point(10, 354)
        Me.lbldesc.Name = "lbldesc"
        Me.lbldesc.Size = New System.Drawing.Size(54, 13)
        Me.lbldesc.TabIndex = 234
        Me.lbldesc.Text = "Balance:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(9, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(48, 13)
        Me.Label9.TabIndex = 232
        Me.Label9.Text = "Number:"
        '
        'txtRemarks
        '
        Me.txtRemarks.BackColor = System.Drawing.SystemColors.Control
        Me.txtRemarks.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.Location = New System.Drawing.Point(88, 94)
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ReadOnly = True
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemarks.Size = New System.Drawing.Size(499, 21)
        Me.txtRemarks.TabIndex = 226
        '
        'dgvView
        '
        Me.dgvView.AllowUserToAddRows = False
        Me.dgvView.AllowUserToDeleteRows = False
        Me.dgvView.AllowUserToResizeRows = False
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dgvView.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvView.BackgroundColor = System.Drawing.SystemColors.Window
        Me.dgvView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvView.Location = New System.Drawing.Point(12, 121)
        Me.dgvView.MultiSelect = False
        Me.dgvView.Name = "dgvView"
        Me.dgvView.ReadOnly = True
        Me.dgvView.RowHeadersVisible = False
        Me.dgvView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvView.Size = New System.Drawing.Size(755, 221)
        Me.dgvView.TabIndex = 224
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(9, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 227
        Me.Label3.Text = "Transaction:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(9, 97)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(52, 13)
        Me.Label4.TabIndex = 229
        Me.Label4.Text = "Remarks:"
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(12, 152)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(66, 21)
        Me.btnClose.TabIndex = 233
        Me.btnClose.TabStop = False
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 13)
        Me.Label1.TabIndex = 225
        Me.Label1.Text = "Date:"
        '
        'txtSource
        '
        Me.txtSource.BackColor = System.Drawing.SystemColors.Control
        Me.txtSource.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSource.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSource.Location = New System.Drawing.Point(371, 40)
        Me.txtSource.Name = "txtSource"
        Me.txtSource.ReadOnly = True
        Me.txtSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSource.Size = New System.Drawing.Size(287, 21)
        Me.txtSource.TabIndex = 257
        '
        'txtBranch
        '
        Me.txtBranch.BackColor = System.Drawing.SystemColors.Control
        Me.txtBranch.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBranch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBranch.Location = New System.Drawing.Point(371, 13)
        Me.txtBranch.Name = "txtBranch"
        Me.txtBranch.ReadOnly = True
        Me.txtBranch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBranch.Size = New System.Drawing.Size(287, 21)
        Me.txtBranch.TabIndex = 258
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(295, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 253
        Me.Label2.Text = "Supplier:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(295, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 254
        Me.Label6.Text = "Branch:"
        '
        'cmRR
        '
        Me.cmRR.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUpdateCost, Me.mnuChangeItem})
        '
        'mnuUpdateCost
        '
        Me.mnuUpdateCost.Index = 0
        Me.mnuUpdateCost.Text = "&Update Costing"
        '
        'mnuChangeItem
        '
        Me.mnuChangeItem.Index = 1
        Me.mnuChangeItem.Text = "Chan&ge Item(s)"
        '
        'btnMakeChanges
        '
        Me.btnMakeChanges.Location = New System.Drawing.Point(676, 11)
        Me.btnMakeChanges.Name = "btnMakeChanges"
        Me.btnMakeChanges.Size = New System.Drawing.Size(90, 23)
        Me.btnMakeChanges.TabIndex = 259
        Me.btnMakeChanges.Text = "&Make Changes"
        Me.btnMakeChanges.UseVisualStyleBackColor = True
        '
        'frmViewRR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(778, 560)
        Me.Controls.Add(Me.btnMakeChanges)
        Me.Controls.Add(Me.txtSource)
        Me.Controls.Add(Me.txtBranch)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cbVATXview)
        Me.Controls.Add(Me.txtArrastre)
        Me.Controls.Add(Me.txtFreight)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.txtTransaction)
        Me.Controls.Add(Me.txtDate)
        Me.Controls.Add(Me.txtNumber)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.dgvHistory)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblstockBal)
        Me.Controls.Add(Me.lbldesc)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtRemarks)
        Me.Controls.Add(Me.dgvView)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmViewRR"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receiving Report"
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbVATXview As System.Windows.Forms.CheckBox
    Friend WithEvents txtArrastre As System.Windows.Forms.TextBox
    Friend WithEvents txtFreight As System.Windows.Forms.TextBox
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtTransaction As System.Windows.Forms.TextBox
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents txtNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblstockBal As System.Windows.Forms.Label
    Friend WithEvents lbldesc As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents dgvView As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSource As System.Windows.Forms.TextBox
    Friend WithEvents txtBranch As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmRR As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUpdateCost As System.Windows.Forms.MenuItem
    Friend WithEvents mnuChangeItem As System.Windows.Forms.MenuItem
    Friend WithEvents btnMakeChanges As System.Windows.Forms.Button
End Class

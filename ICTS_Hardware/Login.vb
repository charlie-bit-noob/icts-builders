Imports System.Data
Imports System.Data.SqlClient

Public Class Login

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function HasInvalidInputs() As Boolean
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is TextBox Then
                If HasNullValue(ctrl) = True Then
                    Return True
                    Exit Function
                End If
                If ctrl.Text.EndsWith("-") Then
                    MessageBox.Show("Please provide a valid username or password", ">:-(", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return True
                    Exit Function
                End If
            End If
        Next
    End Function

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        CleaningUpObjects()
        End
    End Sub

    Private Sub btnChangeServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnChangeServer.Click
        Dim strServerName As String

        strServerName = InputBox("Server Name", "Enter Server Name", "")
        strDBconn = strDBconn.Replace("DIMDICTR", strServerName)
        sqlDBconn = New SqlConnection(strDBconn)
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            If Me.HasInvalidInputs() = True Then Exit Sub

            If Me.txtpassword.Text = "showresetbutton" Then
                Me.btnUserReset.Visible = True
                ResetTextBoxes()
                Me.txtusername.Focus()
                Exit Sub
            End If
            If Me.txtusername.Text = "whchaz1027" And Me.txtpassword.Text = "startx" Then
                MDI.stbUserName.Text = txtusername.Text
                MDI.stbAccessLevel.Text = "Administrator"
                MessageBox.Show("Welcome Charlie Sensei!", "Greetings", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
                Exit Sub
            End If

            Me.CallTrimmer()

            sqlStr = "SELECT ua_id, status, username, access_level_lib_id, (SELECT access_levels FROM Library " & _
            "WHERE lib_id = ua.access_level_lib_id) AS access_level FROM UserAccounts AS ua " & _
            "WHERE username = '" & Me.txtusername.Text & "' AND password = '" & Me.txtpassword.Text & "'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                If Not sqlReader("status") = "Online" Then
                    MDI.stbUserID.Text = sqlReader("ua_id").ToString
                    MDI.stbUserName.Text = sqlReader("username").ToString
                    MDI.stbAccessLevel.Text = sqlReader("access_level").ToString

                    sqlReader.Close()
                    sqlDBconn.Close()

                    Me.SetOnline("Online", Val(MDI.stbUserID.Text))
                Else
                    MessageBox.Show("Username '" & txtusername.Text & "' is already running, Type the username correctly.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
                    sqlReader.Close()
                    sqlDBconn.Close()

                    txtusername.Focus()
                    txtusername.SelectAll()
                    Exit Sub
                End If
            Else
                sqlReader.Close()
                sqlDBconn.Close()

                MessageBox.Show("Invalid username or password. Please check your spelling and if the capslock key is on.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)

                txtpassword.Clear()
                txtusername.Focus()
                txtusername.SelectAll()
                Exit Sub
            End If

            With My.Forms.MDI

                If .stbAccessLevel.Text = "Encoder" Then
                    .mnuNewUserAcct.Enabled = False
                    .mnuBeginBalEdit.Enabled = False
                End If
                If .stbAccessLevel.Text = "Guest" Then
                    .mnuUtilities.Enabled = False
                    .mnuResources.Enabled = False
                    '.mnuAdjustments.Enabled = False

                    For Each mnu As MenuItem In .mnuAFs.MenuItems
                        For Each submnu As MenuItem In mnu.MenuItems
                            If submnu.Text = "New" Then
                                submnu.Enabled = False
                            End If
                        Next
                    Next
                End If

            End With

            Me.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnUserReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUserReset.Click
        Try
            sqlStr = "SELECT ua_id FROM UserAccounts WHERE username = '" & Trim(Me.txtusername.Text) & "' AND status = 'Online'"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            If sqlReader.Read Then
                sqlReader.Close()
                sqlDBconn.Close()
            Else
                MessageBox.Show("No need to reset " & txtusername.Text & "'s status.", "User Stats", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.txtpassword.Clear()
                Me.txtusername.Focus()

                sqlReader.Close()
                sqlDBconn.Close()

                Me.btnUserReset.Visible = False
                Exit Sub
            End If

            sqlStr = "UPDATE UserAccounts SET Status = 'Offline' " & _
            "WHERE username = '" & Me.txtusername.Text & "' " & _
            "AND Status = 'Online'"
            sqlCmd = New SqlCommand
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr

            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            MessageBox.Show("The status of " & Me.txtusername.Text & " has been reset.", "User Stats", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnUserReset.Visible = False

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CallTrimmer()
        Dim ctrlToTrim As Control

        For Each ctrlToTrim In Me.Controls
            If TypeOf ctrlToTrim Is TextBox Then TrimTextBox(ctrlToTrim)
        Next
    End Sub

    Private Sub CleaningUpObjects()
        sqlStr = Nothing
        sqlCmd = Nothing
        sqlReader = Nothing
    End Sub

    Private Sub Login_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtusername.Focus()
    End Sub

    Private Sub Login_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If Not MDI.stbUserName.Text = "" Then
                e.Cancel = False
            Else
                btnCancel_Click(sender, e)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub Login_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'System.Diagnostics.Process.Start("C:\Program Files\chazware\ICTS 1.0.2\synctime.exe")
            ResetTextBoxes()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub ResetTextBoxes()
        Dim ctrl As Control

        For Each ctrl In Me.Controls
            If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
        Next
    End Sub

    Private Sub SetOnline(ByVal strStat As String, ByVal intUA_ID As Integer)
        Try
            sqlStr = "UPDATE UserAccounts SET Status = '" & strStat & "' " & _
            "WHERE ua_id = " & intUA_ID & ""
            sqlCmd.Connection = sqlDBconn
            sqlCmd.CommandText = sqlStr

            sqlDBconn.Open()
            sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub txtpassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpassword.KeyPress
        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then btnOK.PerformClick()
    End Sub

    Private Sub txtusername_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtusername.KeyPress
        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then
            If Me.btnUserReset.Visible = False Then
                txtpassword.Focus()
            Else
                btnUserReset.PerformClick()
            End If
        End If
    End Sub
End Class

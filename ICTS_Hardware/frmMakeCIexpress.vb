Imports System.Data
Imports System.Data.SqlClient

Public Class frmMakeCIexpress

    '---Reference's Info
    Private intRefAF, intRefBranch, intRefQty As Integer
    Private IsValid As Boolean

    Private orderid As Integer
    Private RefDGV As DataGridView

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String
    Private strRefNumber, strRefCustID As String

    Private Function IsInValidItem() As Boolean
        If SelectedItemID = "" Or _
        Me.IsValid = False Then Return True
    End Function

    Private Sub AutoOrderID()
        Try
            Me.sqlStr = "SELECT order_id FROM AFsOut " & _
            "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "' " & _
            "AND branch_id = " & Me.intRefBranch & " ORDER BY order_id DESC"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read Then
                orderid = Me.sqlReader(0) + 1
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Else
                orderid = 1
                Me.sqlReader.Close()
                sqlDBconn.Close()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '---Trapping Part
        If Me.mtxtDate.Text = "  /  /    " Or _
        Me.mtxtDate.MaskFull = False Or _
        Me.txtNumber.Text.Trim = "" Or _
        Me.txtRemarks.Text.Trim = "" Or _
        Me.txtQuantity.Text = "" Or _
        Me.txtPrice.Text = "" Then
            Me.ReturnTrapMsg()
            Exit Sub
        End If

        For Each ctrl As Control In Me.Controls
            If TypeOf ctrl Is TextBox Then TrimTextBox(ctrl)
        Next

        '---Saving Part
        Try
            Dim curindx As Integer = Me.RefDGV.CurrentCell.RowIndex

            Me.btnSave.Text = "Wait..."
            Me.AutoOrderID()

            Dim DecPrice As Decimal = Me.txtPrice.Text.Replace(",", "")

            With Me.RefDGV

                Me.sqlStr = "INSERT INTO AFsOut(root_id, run_id, order_id, item_id, class, is_free, qty, cost, price, " & _
                "date_issued, branch_id, ref_af_id, ref_af_num, af_id, af_num, trans_id, cust_id, non_cust, section_lib_id, ua_id) " & _
                "VALUES ('" & GenRootID & "', " & _
                "'" & GenRunID & "', " & _
                "" & Me.orderid & ", " & _
                "'" & SelectedItemID & "', " & _
                "'" & .Item("class", curindx).Value & "', " & _
                "'" & .Item("free", curindx).Value & "', " & _
                "" & Me.txtQuantity.Text & ", " & _
                "" & 0 & ", " & _
                "" & DecPrice & ", " & _
                "'" & Me.mtxtDate.Text & "', " & _
                "" & Me.intRefBranch & ", " & _
                "" & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & _
                "" & 1 & ", '" & Me.strRefCustID & "', '', " & _
                "" & 0 & ", " & MDI.stbUserID.Text & ")"

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr

                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                If Me.txtSerialNumber.Text.Trim <> "" Then
                    Me.sqlStr = "INSERT INTO TypeSerial(root_id, run_id, order_id, item_id, is_free, serial_num, cost, price, " & _
                    "ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id, has_bal) " & _
                    "VALUES ('" & GenRootID & "', " & _
                    "'" & GenRunID & "', " & _
                    "" & Me.orderid & ", " & _
                    "'" & SelectedItemID & "', " & _
                    "'" & .Item("free", curindx).Value & "', " & _
                    "'" & .Item("serial", curindx).Value & "', " & _
                    "" & 0 & ", " & _
                    "" & DecPrice & ", " & _
                    "" & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                    "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & _
                    "" & Me.intRefBranch & ", " & _
                    "'" & Me.mtxtDate.Text & "', " & _
                    "" & 1 & ", 'False')"

                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    Me.sqlStr = "INSERT INTO TypeNonSerial(root_id, run_id, order_id, item_id, is_free, class, cost, price, qty_in, " & _
                    "qty_bal, ref_af_id, ref_af_num, af_id, af_num, branch_id, date_issued, trans_id) " & _
                    "VALUES ('" & GenRootID & "', " & _
                    "'" & GenRunID & "', " & _
                    "" & Me.orderid & ", " & _
                    "'" & SelectedItemID & "', " & _
                    "'" & .Item("free", curindx).Value & "', " & _
                    "'" & .Item("class", curindx).Value & "', " & _
                    "" & 0 & ", " & _
                    "" & DecPrice & ", " & _
                    "" & Me.txtQuantity.Text & ", " & _
                    "" & Me.txtQuantity.Text & ", " & _
                    "" & Me.intRefAF & ", '" & Me.strRefNumber & "', " & _
                    "" & GenAFID & ", '" & Me.txtNumber.Text & "', " & _
                    "" & Me.intRefBranch & ", " & _
                    "'" & Me.mtxtDate.Text & "', " & _
                    "" & 1 & ")"

                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                End If

                '--Update Source DR
                Me.sqlStr = "UPDATE TOP (1) AFsOut " & _
                "SET istransposed = 'True' " & _
                "WHERE branch_id = " & Me.intRefBranch & " " & _
                "AND af_id = " & Me.intRefAF & " AND af_num = '" & Me.strRefNumber & "' " & _
                "AND item_id = '" & SelectedItemID & "' AND order_id = " & Me.orderid & ""

                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr

                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
                '--End of Update

                '--Save Remarks to table
                Me.sqlStr = "SELECT * FROM AFsRemarks WHERE run_id = '" & GenRunID & "' AND branch_id = " & Me.intRefBranch & " " & _
                "AND date_issued = '" & Me.mtxtDate.Text & "' AND af_id = " & GenAFID & " AND af_num = '" & Me.txtNumber.Text & "'"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                If Not Me.sqlReader.Read Then
                    Me.sqlReader.Close()
                    sqlDBconn.Close()

                    Me.sqlStr = "INSERT INTO AFsRemarks(run_id, branch_id, date_issued, af_id, af_num, remarks) " & _
                    "VALUES('" & GenRunID & "', " & Me.intRefBranch & ", '" & Me.mtxtDate.Text & "', " & GenAFID & ", " & _
                    "'" & Me.txtNumber.Text & "', '" & Me.txtRemarks.Text & "')"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr

                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                End If
                '---End of Remarks

            End With

            Me.btnSave.Text = "&Save"
            MessageBox.Show("DR " & Me.strRefNumber & " has been successfully transposed into CI " & Me.txtNumber.Text & "!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub CleaningUpObjects()
        'Global
        SelectedItemID = Nothing
        GenRootID = Nothing
        GenRunID = Nothing
        GenPopID = Nothing
        GenAFID = Nothing

        'Local
        Me.sqlCmd = Nothing
        Me.sqlReader = Nothing
        Me.sqlStr = Nothing
        Me.intRefAF = Nothing
        Me.IsValid = Nothing
        Me.intRefBranch = Nothing
        Me.strRefNumber = Nothing
        Me.strRefCustID = Nothing
        Me.intRefQty = Nothing
        Me.RefDGV = Nothing
    End Sub

    Public Sub FillupRefVars(ByVal intAFID As Integer, ByVal strNumber As String, ByVal strCustID As String, ByVal bolValidTrans As Boolean, ByVal dgv As DataGridView)
        Me.intRefAF = intAFID
        Me.intRefBranch = GenBranchID
        Me.strRefNumber = strNumber
        Me.strRefCustID = strCustID
        Me.IsValid = bolValidTrans
        Me.RefDGV = dgv

        With Me.RefDGV
            Me.intRefQty = .Item("qty", .CurrentCell.RowIndex).Value
            Me.txtQuantity.Text = Me.intRefQty
            Me.txtSerialNumber.Text = .Item("serial", .CurrentCell.RowIndex).Value.ToString
            Me.sslblparticular.Text = .Item("brand", .CurrentCell.RowIndex).Value + " " + .Item("product", .CurrentCell.RowIndex).Value + " " + .Item("model", .CurrentCell.RowIndex).Value + " " + .Item("desc", .CurrentCell.RowIndex).Value
        End With
    End Sub

    Private Sub frmMakeCIexpress_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtNumber.Focus()
    End Sub

    Private Sub frmMakeCIexpress_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.CleaningUpObjects()
    End Sub

    Private Sub frmMakeCIexpress_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsInValidItem = True Then Me.Close()
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.txtPrice.Text = "0.000"
        GetAFID(Me.Text.Replace(" Express", "").ToUpper)
        Me.txtNumber.Focus()
    End Sub

    Private Sub mtxtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtRemarks.Focus()
    End Sub

    Private Sub ReturnTrapMsg()
        MessageBox.Show("Invalid or Incomplete input!", "Be Cautious!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
    End Sub

    Private Sub txtNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.mtxtDate.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtNumber_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNumber.Leave
        If Me.txtNumber.Text.ToString.Trim <> "" Then IsNumberExists("AFsOut", Me.intRefBranch, Me.txtNumber.Text, Me.txtNumber)
    End Sub

    Private Sub txtPrice_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrice.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.btnSave.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtPrice_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrice.Leave
        OnLeaveFormat(Me.txtPrice)
    End Sub

    Private Sub txtPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPrice.TextChanged
        InputFormat(Me.txtPrice)
    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtPrice.Focus()
    End Sub
End Class
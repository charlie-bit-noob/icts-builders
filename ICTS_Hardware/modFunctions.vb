Imports System.Data
Imports System.Data.SqlClient

Module modFunctions

    Private ModFsqlCmd As SqlCommand
    Private ModFsqlReader As SqlDataReader
    Private ModFsqlStr As String

    Public Function ConvertState(ByVal cbState As Integer) As Integer
        If cbState = 1 Then
            Return 1
        Else
            Return 0
        End If
    End Function

    Public Function CountModelsInTempTable(ByVal strTempTable As String) As Integer
        Dim nsi, si, count As Integer
        Try
            ModFsqlStr = "SELECT COUNT(DISTINCT(item_id)) AS modelcount FROM " & strTempTable & " " & _
            "WHERE ua_id = " & MDI.stbUserID.Text & " AND serial_num = ''"

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader

            ModFsqlReader.Read()
            nsi = ModFsqlReader(0)

            ModFsqlReader.Close()
            sqlDBconn.Close()

            ModFsqlStr = "SELECT COUNT(item_id) AS modelcount FROM " & strTempTable & " " & _
            "WHERE ua_id = " & MDI.stbUserID.Text & " AND serial_num <> ''"

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader

            ModFsqlReader.Read()
            si = ModFsqlReader(0)

            ModFsqlReader.Close()
            sqlDBconn.Close()

            count = nsi + si

            Return count

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function GetTypeLibID(ByVal strItemID As String) As Integer
        Try
            Dim intID As String

            ModFsqlStr = "SELECT type_lib_id FROM ItemMasterlist WHERE item_id = '" & strItemID & "'"
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            ModFsqlReader.Read()
            intID = ModFsqlReader(0)

            ModFsqlReader.Close()
            sqlDBconn.Close()

            Return intID

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function HasInvalidIndex(ByVal cmbBox As ComboBox) As Boolean
        Try
            If cmbBox.SelectedIndex < 1 Then
                MessageBox.Show("You've selected an invalid value!", "Invalid Value", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cmbBox.Focus()
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function HasNullValue(ByVal txtBox As TextBox) As Boolean
        Try
            If txtBox.Text = "" And txtBox.Name = "txtCustID" And GenShortAF = "DR" Then
                Return False
                Exit Function
            End If
            If txtBox.Text.Trim = "" Then
                MessageBox.Show("Cannot proceed when there is an empty field!", "No blank is allowed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtBox.Focus()
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function HasOpenItem(ByVal strTable As String, ByVal strItemID As String, ByVal btnItem As Button, ByVal btnCapture As Button) As Boolean
        Try
            ModFsqlStr = "SELECT item_id FROM " & strTable & " WHERE item_id = '" & strItemID & "' " & _
            "AND ref_af_id = 0 AND ua_id = " & My.Forms.MDI.stbUserID.Text & ""
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                MessageBox.Show("The system allows only single open item for non-serials", "An open item has already been popped!", MessageBoxButtons.OK, MessageBoxIcon.Stop)

                btnItem.Enabled = True
                btnCapture.Enabled = True

                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function InCompleteInAF(ByVal pnl As Panel) As Boolean
        Try
            Dim ctrl As Control

            For Each ctrl In pnl.Controls
                If TypeOf ctrl Is TextBox Then
                    If HasNullValue(ctrl) = True Then
                        Return True
                        Exit Function
                    End If
                End If
                If TypeOf ctrl Is ComboBox Then
                    If HasInvalidIndex(ctrl) = True Then
                        Return True
                        Exit Function
                    End If
                End If
                If TypeOf ctrl Is MaskedTextBox Then
                    If IsNotLogical(ctrl) = True Then
                        Return True
                        Exit Function
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function InCompleteInItemDetails(ByVal txtid As TextBox, ByVal txtqty As TextBox, ByVal txtserial As TextBox) As Boolean
        Try
            If txtid.Text.Trim = "" Or txtqty.Text.Trim = "" Then
                MessageBox.Show("Incomplete in Item Details!", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Return True
                Exit Function
            End If

            If txtserial.Enabled = True Then
                If txtserial.Text.Trim = "" Then Return True
            End If

        Catch ex As Exception

        End Try
    End Function

    Public Function InvalidCancelledAF(ByVal strNumber As String, ByVal intBranch As Integer, ByVal strDate As String) As Boolean
        If strNumber.Trim = "" Or _
        intBranch < 1 Or _
        Not IsDate(strDate) Then Return True
    End Function

    Public Function InvalidOthersAF(ByVal strNumber As String, ByVal intBranch As Integer, ByVal intTrans As Integer, ByVal strDateIssued As String, ByVal strCustID As String, ByVal strPrice As String, ByVal strRemarks As String) As Boolean
        If strNumber.Trim = "" Or _
        intBranch < 1 Or _
        intTrans < 1 Or _
        Not IsDate(strDateIssued) Or _
        strCustID = "" Or _
        strPrice = "" Or _
        strRemarks.Trim = "" Then Return True
    End Function

    Public Function IsAFNumberAlreadyExists(ByVal strAFTable As String, ByVal intBranchID As Integer, ByVal strAFNumber As String, ByVal txt As TextBox) As Boolean
        Try
            ModFsqlStr = "SELECT * FROM " & strAFTable & " WHERE branch_id = " & intBranchID & " AND af_id = " & GenAFID & " AND af_num = '" & strAFNumber & "'"

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                MessageBox.Show("" & GenShortAF & " " & strAFNumber & " already exists!", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txt.Focus()

                ModFsqlReader.Close()
                sqlDBconn.Close()

                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsEmptyMakeGrid(ByVal dgv As DataGridView) As Boolean
        Try
            If dgv.RowCount < 1 Then
                MessageBox.Show("Grid is empty. The form will now close.", "Be Cautious!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsInteger(ByVal v As VariantType) As Boolean
        If VarType(v) = VariantType.Integer Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function IsInvalidBranch(ByVal intBranch As Integer, ByVal cmb As ComboBox) As Boolean
        Try
            If intBranch = 0 Then Exit Function

            ModFsqlStr = "SELECT COUNT(short_name) AS 'Switch' FROM Branches " & _
            "WHERE branch_id = " & intBranch & " AND short_af LIKE '%" & GenShortAF & "%'"

            If GenShortAF = "CI" Or GenShortAF = "CA" Then ModFsqlStr = ModFsqlStr.Replace("%" & GenShortAF & "%", "%RR%")

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            ModFsqlReader.Read()

            If ModFsqlReader(0) < 1 Then
                MessageBox.Show("" & cmb.Text & " is an invalid branch for " & GenShortAF & " form", "Invalid Branch!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                cmb.SelectedIndex = 0

                ModFsqlReader.Close()
                sqlDBconn.Close()

                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsNotLogical(ByVal mtxtBox As MaskedTextBox) As Boolean
        Try
            If mtxtBox.MaskFull = False Then
                MessageBox.Show("Cannot proceed with an invalid value!", "Value Not Recognize!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                mtxtBox.Focus()
                Return True
            Else
                If IsDate(mtxtBox.Text) = False Then
                    MessageBox.Show("Invalid date!", "Value Not Recognize!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    mtxtBox.Focus()
                    Return True
                    Exit Function
                End If
                If Convert.ToDateTime(mtxtBox.Text).Year < 1900 Then
                    MessageBox.Show("Invalid range of date!", "Value Not Recognize!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    mtxtBox.Focus()
                    Return True
                    Exit Function
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsNumberExists(ByVal strTable As String, ByVal intBranch As Integer, ByVal strNumber As String, ByVal txt As TextBox) As Boolean
        Try
            ModFsqlStr = "SELECT * FROM " & strTable & " WHERE branch_id = " & intBranch & " " & _
            "AND af_id = " & GenAFID & " AND af_num = '" & strNumber & "'"

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader

            If ModFsqlReader.Read Then
                MessageBox.Show("Number " & strNumber & " already exists! Please be cautious.", "Hoy! Pag sure.", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                txt.Focus()

                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsSerialExists(ByVal TBoxSerial As TextBox, ByVal intBranch As String) As Boolean
        Try
            ModFsqlStr = "SELECT serial_num FROM TypeSerial " & _
            "WHERE serial_num = '" & TBoxSerial.Text & "' AND item_id = '" & SelectedItemID & "' " & _
            "AND branch_id = " & intBranch & " AND has_bal = 'True' AND af_id = " & GenAFID & ""
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                ModFsqlReader.Close()
                sqlDBconn.Close()
                MessageBox.Show("Serial Number already exists!", "Oopps!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                TBoxSerial.Focus()
                TBoxSerial.SelectAll()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsSerialExistsOnTempIn(ByVal TBoxSerial As TextBox, ByVal intUser As Integer) As Boolean
        Try
            ModFsqlStr = "SELECT serial_num FROM Temp_AFsIn WHERE serial_num = '" & TBoxSerial.Text & "' " & _
            "AND ua_id = " & intUser & ""
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                ModFsqlReader.Close()
                sqlDBconn.Close()
                MessageBox.Show("Serial Number has already been popped or you did not provide one!", "Oopps!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                TBoxSerial.Focus()
                TBoxSerial.SelectAll()
                CleanUpListOfString()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsSerialExistsOnTempOut(ByVal TBoxSerial As TextBox, ByVal intUser As Integer) As Boolean
        Try
            ModFsqlStr = "SELECT serial_num FROM Temp_AFsOut WHERE serial_num = '" & TBoxSerial.Text & "' " & _
            "AND ua_id = " & intUser & ""
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                ModFsqlReader.Close()
                sqlDBconn.Close()
                MessageBox.Show("Serial Number has already been popped or you did not provide one!", "Oopps!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                TBoxSerial.Focus()
                TBoxSerial.SelectAll()
                CleanUpListOfString()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsSerialExistsOnTempTransfer(ByVal TBoxSerial As TextBox, ByVal intUser As Integer) As Boolean
        Try
            ModFsqlStr = "SELECT serial_num FROM Temp_AFsTransfer WHERE serial_num = '" & TBoxSerial.Text & "' " & _
            "AND ua_id = " & intUser & ""
            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader()

            If ModFsqlReader.Read Then
                ModFsqlReader.Close()
                sqlDBconn.Close()
                MessageBox.Show("Serial Number has already been popped or you did not provide one!", "Oopps!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                TBoxSerial.Focus()
                TBoxSerial.SelectAll()
                CleanUpListOfString()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function IsTile(ByVal strItemID As String) As Boolean
        ModFsqlStr = "SELECT * FROM StockFragile WHERE item_id = '" & strItemID & "'"
        ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
        sqlDBconn.Open()
        ModFsqlReader = ModFsqlCmd.ExecuteReader()

        If ModFsqlReader.Read() Then
            ModFsqlReader.Close()
            sqlDBconn.Close()

            Return True
        Else
            ModFsqlReader.Close()
            sqlDBconn.Close()

            Return False
        End If
    End Function

    Public Function IsTransposed(ByVal intBranch As Integer, ByVal intAF As Integer, ByVal strNumber As String, ByVal strItem As String, ByVal intPop As Integer) As Boolean
        Try
            ModFsqlStr = "SELECT * FROM AFsOut WHERE branch_id = " & intBranch & " AND af_id = " & intAF & " " & _
            "AND af_num = '" & strNumber & "' AND item_id = '" & strItem & "' AND order_id = " & intPop & " " & _
            "AND istransposed = 'True'"

            ModFsqlCmd = New SqlCommand(ModFsqlStr, sqlDBconn)
            sqlDBconn.Open()
            ModFsqlReader = ModFsqlCmd.ExecuteReader

            If ModFsqlReader.Read Then
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return True
            Else
                ModFsqlReader.Close()
                sqlDBconn.Close()
                Return False
            End If
        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function MakeDataTableHistory() As DataTable
        Dim HistoryTable As DataTable
        HistoryTable = New DataTable("History")

        Dim col_date As DataColumn = New DataColumn("date")
        col_date.DataType = System.Type.GetType("System.DateTime")
        HistoryTable.Columns.Add(col_date)

        Dim col_af As DataColumn = New DataColumn("af")
        col_af.DataType = System.Type.GetType("System.String")
        HistoryTable.Columns.Add(col_af)

        Dim col_number As DataColumn = New DataColumn("number")
        col_number.DataType = System.Type.GetType("System.String")
        HistoryTable.Columns.Add(col_number)

        Dim col_trans As DataColumn = New DataColumn("trans")
        col_trans.DataType = System.Type.GetType("System.String")
        HistoryTable.Columns.Add(col_trans)

        Dim col_branch_Source As DataColumn = New DataColumn("branch")
        col_branch_Source.DataType = System.Type.GetType("System.String")
        HistoryTable.Columns.Add(col_branch_Source)

        Dim col_cost As DataColumn = New DataColumn("cost")
        col_cost.DataType = System.Type.GetType("System.Decimal")
        HistoryTable.Columns.Add(col_cost)

        Dim col_price As DataColumn = New DataColumn("price")
        col_price.DataType = System.Type.GetType("System.Decimal")
        HistoryTable.Columns.Add(col_price)

        HistoryTable.DefaultView.Sort = "date ASC"

        MakeDataTableHistory = HistoryTable

    End Function

    Public Function TileNoClassDetected(ByVal ItemName As String, ByVal cmb As ComboBox) As Boolean
        Try
            'ItemName has been idled due to some exceptions
            If cmb.Enabled = True Then
                If cmb.SelectedIndex = 0 Then
                    MessageBox.Show("Cannot proceed when a class for this item is not assigned.", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return True
                End If

                'This is an exception. Use to detect tile item w/out source 
                'but due this is already been fixed by assigning ClassPresume = classALT
                'Dim ShortAFList As New List(Of String)

                'ShortAFList.Add("WR")
                'ShortAFList.Add("RR")
                'ShortAFList.Add("PUR")

                'If Not ShortAFList.Contains(GenShortAF) Then
                'Dim i As Integer = 0

                'For Each str As String In classList
                'If Str() <> "" Then i += 1
                'Next

                'If i < 1 Then
                'MessageBox.Show("Item Tiles should not be encoded as null source.", "Tiles Restriction!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                'Return True
                'End If
                'End If

            Else
                Return False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Public Function TrimAFNumber(ByVal num As String) As String
        Dim realNum As String

        If num.Length = 10 Then
            realNum = num.Remove(0, 2)
            realNum = realNum.TrimStart("0")

            Return realNum
        Else
            Return num
        End If
    End Function
End Module

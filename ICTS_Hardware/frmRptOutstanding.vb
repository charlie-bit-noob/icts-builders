Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptOutstanding

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmRptOutstanding_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.GetOutstanding()
            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            Me.RptViewer.ZoomMode = ZoomMode.Percent
            Me.RptViewer.ZoomPercent = 100
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetOutstanding()
        Try
            Dim valVAT As Decimal

            With frmParaOutstanding

                If .cbVATX.CheckState = CheckState.Checked Then
                    Me.sqlStr = "SELECT TOP 1 vat FROM Library WHERE vat IS NOT NULL"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    If Me.sqlReader.Read() Then
                        valVAT = Me.sqlReader(0)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        MessageBox.Show("Value not set for VAT items.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()

                        Dim strInputB As String = InputBox("Enter the value of VAT", _
                        "Please contact your system administator A.S.A.P.", _
                        "1.12")
                        Convert.ToDecimal(strInputB)

                        If IsNumeric(strInputB) Then
                            valVAT = strInputB
                        Else
                            MessageBox.Show("The value entered is invalid. The " & Me.Text & " will now close.", _
                            "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Me.Close()
                        End If
                    End If
                End If

                Me.dsTransaction.Tables("tblOutstanding").Clear()

                Me.sqlStr = "SELECT date_issued, (SELECT short_name FROM Branches WHERE branch_id = ts.branch_id) AS Branch, " & _
                "(SELECT short_name FROM AFs WHERE af_id = ts.af_id) AS saf, af_num, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = ts.item_id) AS Particular, " & _
                "(SELECT types FROM Library l, ItemMasterlist im WHERE im.item_id = ts.item_id AND l.lib_id = im.type_lib_id) AS ItemType, " & _
                "serial_num, qty_bal = 1, cost " & _
                "FROM TypeSerial ts " & _
                "WHERE has_bal = 'TRUE' AND " & .txtSQLpara.Text & " " & _
                "UNION ALL " & _
                "SELECT date_issued, (SELECT short_name FROM Branches WHERE branch_id = tns.branch_id) AS Branch, " & _
                "(SELECT short_name FROM AFs WHERE af_id = tns.af_id) AS saf, af_num, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = tns.item_id) AS Particular, " & _
                "(SELECT types FROM Library l, ItemMasterlist im WHERE im.item_id = tns.item_id AND l.lib_id = im.type_lib_id) AS ItemType, " & _
                "serial_num = '', qty_bal, cost " & _
                "FROM TypeNonSerial tns " & _
                "WHERE qty_bal > 0 AND af_id NOT IN (SELECT af_id FROM AFs WHERE af_type = 'OUT') " & _
                "AND " & .txtSQLpara.Text & ""

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsTransaction.Tables("tblOutstanding").NewRow

                    tblRow("Branch") = Me.sqlReader("Branch")
                    tblRow("Reference") = Me.sqlReader("saf") + " " + TrimAFNumber(Me.sqlReader("af_num").ToString)
                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("ItemType") = Me.sqlReader("ItemType")
                    tblRow("Serial") = Me.sqlReader("serial_num")
                    tblRow("Qty") = Me.sqlReader("qty_bal")

                    If .cbVATX.CheckState = CheckState.Checked Then
                        If Not Me.sqlReader("ItemType").ToString.Contains("WITHOUT") Then
                            tblRow("Cost") = Me.sqlReader("cost") / valVAT
                        Else
                            tblRow("Cost") = Me.sqlReader("cost")
                        End If
                    Else
                        tblRow("Cost") = Me.sqlReader("cost")
                    End If

                    tblRow("Total") = tblRow("Cost") * tblRow("Qty")

                    tblRow("Period") = Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                    tblRow("ReportHeader") = .txtSumTitle.Text
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsTransaction.Tables("tblOutstanding").Rows.Add(tblRow)
                End While
            End With

            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCountSheet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.txtModel = New System.Windows.Forms.TextBox
        Me.pnlItemFinder = New System.Windows.Forms.Panel
        Me.txtProduct = New System.Windows.Forms.TextBox
        Me.txtBrand = New System.Windows.Forms.TextBox
        Me.btnClose = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cbSetGroup = New System.Windows.Forms.CheckBox
        Me.lblRptDate = New System.Windows.Forms.Label
        Me.txtRptDate = New System.Windows.Forms.TextBox
        Me.gbParameter = New System.Windows.Forms.GroupBox
        Me.cbParameter = New System.Windows.Forms.CheckBox
        Me.gbDetails = New System.Windows.Forms.GroupBox
        Me.txtGroupCode = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtTotalRow = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtTotalQty = New System.Windows.Forms.TextBox
        Me.txtItemID = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnView = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.gbInputs = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtClass = New System.Windows.Forms.TextBox
        Me.lblSerial = New System.Windows.Forms.Label
        Me.lblQty = New System.Windows.Forms.Label
        Me.txtSerial = New System.Windows.Forms.TextBox
        Me.txtQty = New System.Windows.Forms.TextBox
        Me.txtCSID = New System.Windows.Forms.TextBox
        Me.DGVfinder = New System.Windows.Forms.DataGridView
        Me.gbScope = New System.Windows.Forms.GroupBox
        Me.btnConfirm = New System.Windows.Forms.Button
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmbBranch = New System.Windows.Forms.ComboBox
        Me.txtGroupID = New System.Windows.Forms.TextBox
        Me.btnCheckSerial = New System.Windows.Forms.Button
        Me.pnlItemFinder.SuspendLayout()
        Me.gbParameter.SuspendLayout()
        Me.gbDetails.SuspendLayout()
        Me.gbInputs.SuspendLayout()
        CType(Me.DGVfinder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbScope.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(529, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "&Description:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(359, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "&Model:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(189, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "&Product:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(22, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(39, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "&Brand:"
        '
        'txtDesc
        '
        Me.txtDesc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDesc.Location = New System.Drawing.Point(532, 19)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(201, 21)
        Me.txtDesc.TabIndex = 7
        '
        'txtModel
        '
        Me.txtModel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtModel.Location = New System.Drawing.Point(362, 19)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(164, 21)
        Me.txtModel.TabIndex = 5
        '
        'pnlItemFinder
        '
        Me.pnlItemFinder.Controls.Add(Me.Label7)
        Me.pnlItemFinder.Controls.Add(Me.Label6)
        Me.pnlItemFinder.Controls.Add(Me.Label5)
        Me.pnlItemFinder.Controls.Add(Me.Label4)
        Me.pnlItemFinder.Controls.Add(Me.txtDesc)
        Me.pnlItemFinder.Controls.Add(Me.txtModel)
        Me.pnlItemFinder.Controls.Add(Me.txtProduct)
        Me.pnlItemFinder.Controls.Add(Me.txtBrand)
        Me.pnlItemFinder.Location = New System.Drawing.Point(-10, 218)
        Me.pnlItemFinder.Name = "pnlItemFinder"
        Me.pnlItemFinder.Size = New System.Drawing.Size(754, 46)
        Me.pnlItemFinder.TabIndex = 11
        '
        'txtProduct
        '
        Me.txtProduct.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtProduct.Location = New System.Drawing.Point(192, 19)
        Me.txtProduct.Name = "txtProduct"
        Me.txtProduct.Size = New System.Drawing.Size(164, 21)
        Me.txtProduct.TabIndex = 3
        '
        'txtBrand
        '
        Me.txtBrand.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtBrand.Location = New System.Drawing.Point(22, 19)
        Me.txtBrand.Name = "txtBrand"
        Me.txtBrand.Size = New System.Drawing.Size(164, 21)
        Me.txtBrand.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(540, 524)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 23)
        Me.btnClose.TabIndex = 17
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(-3, 507)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(740, 2)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "GroupBox3"
        '
        'cbSetGroup
        '
        Me.cbSetGroup.AutoSize = True
        Me.cbSetGroup.Enabled = False
        Me.cbSetGroup.Location = New System.Drawing.Point(322, 23)
        Me.cbSetGroup.Name = "cbSetGroup"
        Me.cbSetGroup.Size = New System.Drawing.Size(74, 17)
        Me.cbSetGroup.TabIndex = 7
        Me.cbSetGroup.Text = "Set &Group"
        Me.cbSetGroup.UseVisualStyleBackColor = True
        '
        'lblRptDate
        '
        Me.lblRptDate.AutoSize = True
        Me.lblRptDate.Enabled = False
        Me.lblRptDate.Location = New System.Drawing.Point(43, 23)
        Me.lblRptDate.Name = "lblRptDate"
        Me.lblRptDate.Size = New System.Drawing.Size(70, 13)
        Me.lblRptDate.TabIndex = 6
        Me.lblRptDate.Text = "Report Date:"
        '
        'txtRptDate
        '
        Me.txtRptDate.Enabled = False
        Me.txtRptDate.Location = New System.Drawing.Point(129, 20)
        Me.txtRptDate.Name = "txtRptDate"
        Me.txtRptDate.Size = New System.Drawing.Size(186, 21)
        Me.txtRptDate.TabIndex = 2
        '
        'gbParameter
        '
        Me.gbParameter.Controls.Add(Me.cbSetGroup)
        Me.gbParameter.Controls.Add(Me.lblRptDate)
        Me.gbParameter.Controls.Add(Me.txtRptDate)
        Me.gbParameter.Controls.Add(Me.cbParameter)
        Me.gbParameter.Location = New System.Drawing.Point(15, 387)
        Me.gbParameter.Name = "gbParameter"
        Me.gbParameter.Size = New System.Drawing.Size(407, 54)
        Me.gbParameter.TabIndex = 19
        Me.gbParameter.TabStop = False
        '
        'cbParameter
        '
        Me.cbParameter.AutoSize = True
        Me.cbParameter.Location = New System.Drawing.Point(9, 0)
        Me.cbParameter.Name = "cbParameter"
        Me.cbParameter.Size = New System.Drawing.Size(100, 17)
        Me.cbParameter.TabIndex = 0
        Me.cbParameter.Text = "Set Parameters"
        Me.cbParameter.UseVisualStyleBackColor = True
        '
        'gbDetails
        '
        Me.gbDetails.Controls.Add(Me.txtGroupCode)
        Me.gbDetails.Controls.Add(Me.Label3)
        Me.gbDetails.Controls.Add(Me.txtTotalRow)
        Me.gbDetails.Controls.Add(Me.Label2)
        Me.gbDetails.Controls.Add(Me.Label1)
        Me.gbDetails.Controls.Add(Me.txtTotalQty)
        Me.gbDetails.Controls.Add(Me.txtItemID)
        Me.gbDetails.Location = New System.Drawing.Point(441, 387)
        Me.gbDetails.Name = "gbDetails"
        Me.gbDetails.Size = New System.Drawing.Size(282, 109)
        Me.gbDetails.TabIndex = 14
        Me.gbDetails.TabStop = False
        Me.gbDetails.Text = "Item Details"
        '
        'txtGroupCode
        '
        Me.txtGroupCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtGroupCode.Location = New System.Drawing.Point(225, 20)
        Me.txtGroupCode.Name = "txtGroupCode"
        Me.txtGroupCode.Size = New System.Drawing.Size(41, 21)
        Me.txtGroupCode.TabIndex = 9
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Total Row:"
        '
        'txtTotalRow
        '
        Me.txtTotalRow.Location = New System.Drawing.Point(81, 75)
        Me.txtTotalRow.Name = "txtTotalRow"
        Me.txtTotalRow.Size = New System.Drawing.Size(185, 21)
        Me.txtTotalRow.TabIndex = 5
        Me.txtTotalRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Total Qty.:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Item ID:"
        '
        'txtTotalQty
        '
        Me.txtTotalQty.Location = New System.Drawing.Point(81, 48)
        Me.txtTotalQty.Name = "txtTotalQty"
        Me.txtTotalQty.Size = New System.Drawing.Size(185, 21)
        Me.txtTotalQty.TabIndex = 3
        Me.txtTotalQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtItemID
        '
        Me.txtItemID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtItemID.Location = New System.Drawing.Point(81, 20)
        Me.txtItemID.Name = "txtItemID"
        Me.txtItemID.Size = New System.Drawing.Size(138, 21)
        Me.txtItemID.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-3, 209)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(740, 2)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'btnView
        '
        Me.btnView.Location = New System.Drawing.Point(636, 524)
        Me.btnView.Name = "btnView"
        Me.btnView.Size = New System.Drawing.Size(87, 23)
        Me.btnView.TabIndex = 18
        Me.btnView.Text = "Vie&w"
        Me.btnView.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(441, 524)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(93, 23)
        Me.btnSave.TabIndex = 16
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbInputs
        '
        Me.gbInputs.Controls.Add(Me.btnCheckSerial)
        Me.gbInputs.Controls.Add(Me.Label9)
        Me.gbInputs.Controls.Add(Me.txtClass)
        Me.gbInputs.Controls.Add(Me.lblSerial)
        Me.gbInputs.Controls.Add(Me.lblQty)
        Me.gbInputs.Controls.Add(Me.txtSerial)
        Me.gbInputs.Controls.Add(Me.txtQty)
        Me.gbInputs.Location = New System.Drawing.Point(12, 268)
        Me.gbInputs.Name = "gbInputs"
        Me.gbInputs.Size = New System.Drawing.Size(407, 108)
        Me.gbInputs.TabIndex = 13
        Me.gbInputs.TabStop = False
        Me.gbInputs.Text = "Inputs"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(17, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(32, 13)
        Me.Label9.TabIndex = 7
        Me.Label9.Text = "Cl&ass"
        '
        'txtClass
        '
        Me.txtClass.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtClass.Location = New System.Drawing.Point(129, 75)
        Me.txtClass.MaxLength = 1
        Me.txtClass.Name = "txtClass"
        Me.txtClass.Size = New System.Drawing.Size(262, 21)
        Me.txtClass.TabIndex = 8
        '
        'lblSerial
        '
        Me.lblSerial.AutoSize = True
        Me.lblSerial.Location = New System.Drawing.Point(17, 51)
        Me.lblSerial.Name = "lblSerial"
        Me.lblSerial.Size = New System.Drawing.Size(37, 13)
        Me.lblSerial.TabIndex = 5
        Me.lblSerial.Text = "Se&rial:"
        '
        'lblQty
        '
        Me.lblQty.AutoSize = True
        Me.lblQty.Location = New System.Drawing.Point(17, 23)
        Me.lblQty.Name = "lblQty"
        Me.lblQty.Size = New System.Drawing.Size(33, 13)
        Me.lblQty.TabIndex = 3
        Me.lblQty.Text = "&Qty.:"
        '
        'txtSerial
        '
        Me.txtSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSerial.Location = New System.Drawing.Point(129, 48)
        Me.txtSerial.Name = "txtSerial"
        Me.txtSerial.Size = New System.Drawing.Size(205, 21)
        Me.txtSerial.TabIndex = 6
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(129, 20)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(262, 21)
        Me.txtQty.TabIndex = 4
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCSID
        '
        Me.txtCSID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCSID.Location = New System.Drawing.Point(81, 48)
        Me.txtCSID.MaxLength = 8
        Me.txtCSID.Name = "txtCSID"
        Me.txtCSID.Size = New System.Drawing.Size(185, 21)
        Me.txtCSID.TabIndex = 2
        '
        'DGVfinder
        '
        Me.DGVfinder.AllowUserToAddRows = False
        Me.DGVfinder.AllowUserToDeleteRows = False
        Me.DGVfinder.AllowUserToResizeRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DGVfinder.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVfinder.BackgroundColor = System.Drawing.Color.White
        Me.DGVfinder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.DGVfinder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVfinder.Location = New System.Drawing.Point(12, 12)
        Me.DGVfinder.MultiSelect = False
        Me.DGVfinder.Name = "DGVfinder"
        Me.DGVfinder.ReadOnly = True
        Me.DGVfinder.RowHeadersVisible = False
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DGVfinder.RowsDefaultCellStyle = DataGridViewCellStyle2
        Me.DGVfinder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVfinder.Size = New System.Drawing.Size(711, 183)
        Me.DGVfinder.TabIndex = 12
        '
        'gbScope
        '
        Me.gbScope.Controls.Add(Me.btnConfirm)
        Me.gbScope.Controls.Add(Me.Label11)
        Me.gbScope.Controls.Add(Me.Label10)
        Me.gbScope.Controls.Add(Me.cmbBranch)
        Me.gbScope.Controls.Add(Me.txtCSID)
        Me.gbScope.Location = New System.Drawing.Point(441, 268)
        Me.gbScope.Name = "gbScope"
        Me.gbScope.Size = New System.Drawing.Size(282, 108)
        Me.gbScope.TabIndex = 20
        Me.gbScope.TabStop = False
        Me.gbScope.Text = "Scope"
        '
        'btnConfirm
        '
        Me.btnConfirm.Location = New System.Drawing.Point(81, 74)
        Me.btnConfirm.Name = "btnConfirm"
        Me.btnConfirm.Size = New System.Drawing.Size(185, 23)
        Me.btnConfirm.TabIndex = 4
        Me.btnConfirm.Text = "Confirm / Change"
        Me.btnConfirm.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(6, 51)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 3
        Me.Label11.Text = "CS&ID"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(6, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(40, 13)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "&Branch"
        '
        'cmbBranch
        '
        Me.cmbBranch.FormattingEnabled = True
        Me.cmbBranch.Location = New System.Drawing.Point(81, 20)
        Me.cmbBranch.Name = "cmbBranch"
        Me.cmbBranch.Size = New System.Drawing.Size(185, 21)
        Me.cmbBranch.TabIndex = 0
        '
        'txtGroupID
        '
        Me.txtGroupID.Location = New System.Drawing.Point(352, 526)
        Me.txtGroupID.Name = "txtGroupID"
        Me.txtGroupID.Size = New System.Drawing.Size(69, 21)
        Me.txtGroupID.TabIndex = 21
        Me.txtGroupID.Visible = False
        '
        'btnCheckSerial
        '
        Me.btnCheckSerial.Location = New System.Drawing.Point(340, 46)
        Me.btnCheckSerial.Name = "btnCheckSerial"
        Me.btnCheckSerial.Size = New System.Drawing.Size(51, 23)
        Me.btnCheckSerial.TabIndex = 9
        Me.btnCheckSerial.Text = "Chec&k"
        Me.btnCheckSerial.UseVisualStyleBackColor = True
        '
        'frmCountSheet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(735, 559)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtGroupID)
        Me.Controls.Add(Me.gbScope)
        Me.Controls.Add(Me.pnlItemFinder)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.gbParameter)
        Me.Controls.Add(Me.gbDetails)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnView)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.gbInputs)
        Me.Controls.Add(Me.DGVfinder)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmCountSheet"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Count Sheet"
        Me.pnlItemFinder.ResumeLayout(False)
        Me.pnlItemFinder.PerformLayout()
        Me.gbParameter.ResumeLayout(False)
        Me.gbParameter.PerformLayout()
        Me.gbDetails.ResumeLayout(False)
        Me.gbDetails.PerformLayout()
        Me.gbInputs.ResumeLayout(False)
        Me.gbInputs.PerformLayout()
        CType(Me.DGVfinder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbScope.ResumeLayout(False)
        Me.gbScope.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents txtModel As System.Windows.Forms.TextBox
    Friend WithEvents pnlItemFinder As System.Windows.Forms.Panel
    Friend WithEvents txtProduct As System.Windows.Forms.TextBox
    Friend WithEvents txtBrand As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cbSetGroup As System.Windows.Forms.CheckBox
    Friend WithEvents lblRptDate As System.Windows.Forms.Label
    Friend WithEvents txtRptDate As System.Windows.Forms.TextBox
    Friend WithEvents gbParameter As System.Windows.Forms.GroupBox
    Friend WithEvents cbParameter As System.Windows.Forms.CheckBox
    Friend WithEvents gbDetails As System.Windows.Forms.GroupBox
    Friend WithEvents txtGroupCode As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtTotalRow As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalQty As System.Windows.Forms.TextBox
    Friend WithEvents txtItemID As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents gbInputs As System.Windows.Forms.GroupBox
    Friend WithEvents txtCSID As System.Windows.Forms.TextBox
    Friend WithEvents lblSerial As System.Windows.Forms.Label
    Friend WithEvents lblQty As System.Windows.Forms.Label
    Friend WithEvents txtSerial As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents DGVfinder As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtClass As System.Windows.Forms.TextBox
    Friend WithEvents gbScope As System.Windows.Forms.GroupBox
    Friend WithEvents btnConfirm As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbBranch As System.Windows.Forms.ComboBox
    Friend WithEvents txtGroupID As System.Windows.Forms.TextBox
    Friend WithEvents btnCheckSerial As System.Windows.Forms.Button
End Class

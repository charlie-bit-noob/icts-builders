Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WinForms

Public Class frmRptIncoming

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub frmRptIncoming_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.MdiParent = MDI
            Me.GetIncoming()
            Me.RptViewer.RefreshReport()
            Me.RptViewer.SetDisplayMode(DisplayMode.PrintLayout)
            Me.RptViewer.ZoomMode = ZoomMode.Percent
            Me.RptViewer.ZoomPercent = 100
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetIncoming()
        Try
            Dim valVAT As Decimal

            With frmParaIncoming

                If .cbVATX.CheckState = CheckState.Checked Then
                    Me.sqlStr = "SELECT TOP 1 vat FROM Library WHERE vat IS NOT NULL"
                    Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                    sqlDBconn.Open()
                    Me.sqlReader = Me.sqlCmd.ExecuteReader
                    If Me.sqlReader.Read() Then
                        valVAT = Me.sqlReader(0)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()
                    Else
                        MessageBox.Show("Value not set for VAT items.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.sqlReader.Close()
                        sqlDBconn.Close()

                        Dim strInputB As String = InputBox("Enter the value of VAT", _
                        "Please contact your system administator A.S.A.P.", _
                        "1.12")
                        Convert.ToDecimal(strInputB)

                        If IsNumeric(strInputB) Then
                            valVAT = strInputB
                        Else
                            MessageBox.Show("The value entered is invalid. The " & Me.Text & " will now close.", _
                            "Pastilan!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                            Me.Close()
                        End If
                    End If
                End If

                Me.dsTransaction.Tables("tblIncoming").Clear()

                Me.sqlStr = "SELECT date_issued, (SELECT short_name FROM Branches WHERE branch_id = af.branch_id) AS Branch, " & _
                "(SELECT short_name FROM AFs WHERE af_id = af.af_id) AS saf, af_num, cust_id, " & _
                "Source = CASE " & _
                "WHEN af_id < 3 THEN (SELECT name FROM Suppliers WHERE supplier_id = af.supplier_id) " & _
                "WHEN af_id = 3 THEN (SELECT first_name + ' ' + last_name + company_name AS flc FROM Customers WHERE cust_id = af.cust_id) + non_cust " & _
                "END, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = af.item_id) AS Particular, " & _
                "(SELECT types FROM Library l, ItemMasterlist im WHERE im.item_id = af.item_id AND l.lib_id = im.type_lib_id) AS ItemType, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af.branch_id AND af_id = af.af_id AND af_num = af.af_num AND item_id = af.item_id AND order_id = af.order_id) AS Serial, " & _
                "qty, cost " & _
                "FROM AFsIn af WHERE " & .txtSQLpara.Text & " " & _
                "UNION ALL " & _
                "SELECT date_issued, (SELECT short_name FROM Branches WHERE branch_id = af2.branch_id) AS Branch, " & _
                "(SELECT short_name FROM AFs WHERE af_id = af2.af_id) AS saf, af_num, cust_id = '', " & _
                "(SELECT short_name FROM Branches WHERE branch_id = af2.src_branch_id) AS Source, " & _
                "(SELECT brand + ' ' + product + ' ' + model + ' ' + description FROM ItemMasterlist WHERE item_id = af2.item_id) AS Particular, " & _
                "(SELECT types FROM Library l2, ItemMasterlist im2 WHERE im2.item_id = af2.item_id AND l2.lib_id = im2.type_lib_id) AS ItemType, " & _
                "(SELECT serial_num FROM TypeSerial WHERE branch_id = af2.branch_id AND af_id = af2.af_id AND af_num = af2.af_num AND item_id = af2.item_id AND order_id = af2.order_id) AS Serial, " & _
                "qty, cost " & _
                "FROM AFsTransfer af2 WHERE " & .txtSQLpara.Text & " ORDER BY Particular "

                Dim tblRow As DataRow

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader
                While Me.sqlReader.Read()
                    tblRow = Me.dsTransaction.Tables("tblIncoming").NewRow

                    tblRow("Branch") = Me.sqlReader("Branch")
                    tblRow("Reference") = Me.sqlReader("saf") + " " + TrimAFNumber(Me.sqlReader("af_num").ToString)
                    tblRow("Date") = Me.sqlReader("date_issued")
                    tblRow("Source") = Me.sqlReader("Source")
                    tblRow("Particular") = Me.sqlReader("Particular")
                    tblRow("ItemType") = Me.sqlReader("ItemType")
                    tblRow("Serial") = Me.sqlReader("Serial")
                    tblRow("Qty") = Me.sqlReader("qty")

                    If .cbVATX.CheckState = CheckState.Checked Then
                        If Not Me.sqlReader("ItemType").ToString.Contains("WITHOUT") Then
                            tblRow("Cost") = Me.sqlReader("cost") / valVAT
                        Else
                            tblRow("Cost") = Me.sqlReader("cost")
                            End If
                    Else
                        tblRow("Cost") = Me.sqlReader("cost")
                        End If

                    tblRow("Total") = tblRow("Cost") * tblRow("Qty")

                    If .mtxtFrom.MaskFull = True Then
                        tblRow("Period") = "Period: " + Format(Convert.ToDateTime(.mtxtFrom.Text), "MMM. dd, yyyy") + " - " + Format(Convert.ToDateTime(.mtxtTo.Text), "MMM. dd, yyyy")
                    Else
                        tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                        End If
                    If .mtxtFrom.Text = .mtxtTo.Text Then _
                    tblRow("Period") = "Period: " + Convert.ToDateTime(.mtxtTo.Text).ToLongDateString
                    tblRow("ReportHeader") = .txtSumTitle.Text
                    tblRow("Encoder") = MDI.stbUserName.Text

                    Me.dsTransaction.Tables("tblIncoming").Rows.Add(tblRow)
                    End While
            End With

            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class
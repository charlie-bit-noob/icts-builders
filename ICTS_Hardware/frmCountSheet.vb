Imports System.Data
Imports System.Data.SqlClient

Public Class frmCountSheet

    Private Row As DataRow
    Private sqlCmd As SqlCommand

    Private sqlConn As SqlConnection
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private strConn As String = "Data Source=DIMDICTR\SQLExpress,2301;Initial Catalog=HardwareCountSheetDB;User ID=sa;Password=JesusChristILuvU"

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function IsInvalid() As Boolean
        If Me.txtItemID.Text = "" Then Return True
        If Me.txtQty.Text = "" Or Val(Me.txtQty.Text) < 1 Then Return True
        If Me.txtSerial.ReadOnly = False And txtSerial.Text.Trim = "" Then Return True
        If Me.cmbBranch.Enabled = True Then Return True
        If Me.txtCSID.ReadOnly = False Then Return True
    End Function

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_itemid As DataColumn = New DataColumn("item_id")
        col_itemid.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_itemid)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_model)

        Dim col_desc As DataColumn = New DataColumn("description")
        col_desc.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_desc)

        Dim col_groupcode As DataColumn = New DataColumn("group_code")
        col_groupcode.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_groupcode)

        Dim col_group As DataColumn = New DataColumn("group_id")
        col_group.DataType = System.Type.GetType("System.Int32")
        TableTemp.Columns.Add(col_group)

        Dim col_itemtype As DataColumn = New DataColumn("item_type")
        col_itemtype.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_itemtype)

        Dim col_nonstock As DataColumn = New DataColumn("non_stock")
        col_nonstock.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_nonstock)

        TableTemp.DefaultView.Sort = "brand ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Private Sub btnCheckSerial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckSerial.Click
        Try
            Me.sqlConn = New SqlConnection(Me.strConn)

            Me.sqlStr = "SELECT * FROM [" & Me.cmbBranch.SelectedIndex & "] WHERE cs_id = '" & Me.txtCSID.Text & "' AND serial_num = '" & Me.txtSerial.Text & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, Me.sqlConn)
            Me.sqlConn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader

            If Me.sqlReader.Read() Then
                MessageBox.Show("Serial Number already exists!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.sqlReader.Close()
                Me.sqlConn.Close()
            Else
                MessageBox.Show("Serial Number not found", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.sqlReader.Close()
                Me.sqlConn.Close()
            End If

        Catch ex As Exception
            Me.sqlConn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.CleaningUpObjects()
        Me.Close()
    End Sub

    Private Sub btnConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirm.Click
        If Me.cmbBranch.Enabled = True Then
            If Me.txtCSID.TextLength < 8 Then
                MessageBox.Show("Minimum CSID length is 8!", "Otso kabuok numero ba...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            If Me.cmbBranch.SelectedIndex < 1 Then
                MessageBox.Show("Branch invalid!", "Sus! na lang...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            Me.cmbBranch.Enabled = False
            Me.txtCSID.ReadOnly = True
            Me.txtBrand.Focus()
        Else
            Me.cmbBranch.Enabled = True
            Me.txtCSID.ReadOnly = False

            Me.cmbBranch.SelectedIndex = 0
            Me.txtCSID.Clear()
            Me.cmbBranch.Focus()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim LastQtyCount As Double

            If Me.IsInvalid = True Then
                MessageBox.Show("Incomplete input....", "Please be cautious", MessageBoxButtons.OK, MessageBoxIcon.Stop)
                Exit Sub
            End If

            If MessageBox.Show("Are you sure you want to proceed?", " Pag sure " & MDI.stbUserName.Text.ToUpper & "...", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then Exit Sub

            Me.btnSave.Text = "Wait..."
            Me.sqlConn = New SqlConnection(Me.strConn)

            If Me.txtQty.ReadOnly = False Then
                Me.sqlStr = "SELECT Qty FROM [" & Me.cmbBranch.SelectedIndex & "] WHERE item_id = '" & Me.txtItemID.Text & "' AND cs_id = '" & Me.txtCSID.Text & "'"
                If Me.txtClass.Text.Trim <> "" Then Me.sqlStr = Me.sqlStr + " AND class = '" & Me.txtClass.Text & "'"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, Me.sqlConn)
                Me.sqlConn.Open()
                Me.sqlReader = Me.sqlCmd.ExecuteReader

                If Me.sqlReader.Read() Then
                    LastQtyCount = Convert.ToDouble(Me.sqlReader(0))
                    Me.sqlReader.Close()
                    Me.sqlConn.Close()
                Else
                    Me.sqlReader.Close()
                    Me.sqlConn.Close()
                End If

                If LastQtyCount < 1 Then
                    Me.sqlStr = "INSERT INTO [" & Me.cmbBranch.SelectedIndex & "](cs_id, item_id, class, group_id, serial_num, qty) " & _
                    "VALUES('" & Me.txtCSID.Text & "', '" & Me.txtItemID.Text & "', '" & Me.txtClass.Text & "', " & Me.txtGroupID.Text & ", '" & Me.txtSerial.Text.Trim & "', " & Me.txtQty.Text & ")"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = Me.sqlConn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    Me.sqlConn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    Me.sqlConn.Close()
                Else
                    Me.sqlStr = "UPDATE [" & Me.cmbBranch.SelectedIndex & "] SET qty = (" & LastQtyCount & " + " & Me.txtQty.Text & ") " & _
                    "WHERE item_id = '" & Me.txtItemID.Text & "' AND cs_id = '" & Me.txtCSID.Text & "'"
                    If Me.txtClass.Text.Trim <> "" Then Me.sqlStr = Me.sqlStr + " AND class = '" & Me.txtClass.Text & "'"

                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = Me.sqlConn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    Me.sqlConn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    Me.sqlConn.Close()
                End If
            Else
                Me.sqlStr = "INSERT INTO [" & Me.cmbBranch.SelectedIndex & "](cs_id, item_id, class, group_id, serial_num, qty) " & _
                "VALUES('" & Me.txtCSID.Text & "', '" & Me.txtItemID.Text & "', '" & Me.txtClass.Text & "', " & Me.txtGroupID.Text & ", '" & Me.txtSerial.Text.Trim & "', " & Me.txtQty.Text & ")"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = Me.sqlConn
                Me.sqlCmd.CommandText = Me.sqlStr
                Me.sqlConn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                Me.sqlConn.Close()
            End If

            Me.ClearTextBox()
            Me.GetTotalRowCSID()
            Me.btnSave.Text = "&Save"
            MessageBox.Show("Saved successfully!", ":)", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.txtBrand.Focus()

        Catch ex As Exception
            Me.sqlConn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If Me.cmbBranch.SelectedIndex < 1 Or Me.txtCSID.Text.Trim = "" Then
            MessageBox.Show("Unable to proceed when branch or CSID not set", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        My.Forms.frmRptViewerCountSheet.Show()
    End Sub

    Private Sub cbParameter_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbParameter.CheckStateChanged
        Me.txtRptDate.Clear()
        Me.cbSetGroup.CheckState = CheckState.Unchecked

        If Me.cbParameter.CheckState = CheckState.Checked Then
            Me.txtRptDate.Enabled = True
            Me.cbSetGroup.Enabled = True
            Me.txtRptDate.Focus()
        Else
            Me.txtRptDate.Enabled = False
            Me.cbSetGroup.Enabled = False
        End If
    End Sub

    Private Sub CleaningUpObjects()
        Me.Row = Nothing
        Me.sqlCmd = Nothing

        Me.sqlReader = Nothing
        Me.sqlStr = Nothing

        Me.TempDataView = Nothing
        Me.TempTable = Nothing
    End Sub

    Private Sub ClearTextBox()
        Dim ctrl As Control

        For Each ctrl In Me.pnlItemFinder.Controls
            If TypeOf ctrl Is TextBox Then ctrl.Text = ""
        Next

        Me.txtQty.Clear()
        Me.txtSerial.Clear()
        Me.txtItemID.Clear()
        Me.txtClass.Clear()
        Me.txtGroupCode.Clear()
        Me.txtTotalQty.Clear()
    End Sub

    Private Sub cmbBranch_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbBranch.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtCSID.Focus()
    End Sub

    Private Sub dgvConfig()
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In Me.DGVfinder.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "item_id"
                        .Width = 100
                        .HeaderText = "Item ID"
                    Case "brand"
                        .Width = 150
                        .HeaderText = "Brand"
                    Case "product"
                        .Width = 150
                        .HeaderText = "Product"
                    Case "model"
                        .Width = 150
                        .HeaderText = "Model"
                    Case "description"
                        .Width = 150
                        .HeaderText = "Description"
                    Case "group_id"
                        .Width = 50
                        .Visible = False
                    Case "group_code"
                        .Width = 50
                        .HeaderText = "Group"
                    Case "item_type"
                        .Width = 100
                        .HeaderText = "Type"
                    Case "non_stock"
                        .Width = 50
                        .HeaderText = "Non-Stock"
                End Select
            End With
        Next
    End Sub

    Private Sub DGVfinder_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DGVfinder.CellDoubleClick
        If Me.cmbBranch.SelectedIndex = 0 Or Me.txtCSID.Text.Trim = "" Then
            MessageBox.Show("Please fill scope data", "Sus! na lang...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        Me.SelectedItemID()
    End Sub

    Private Sub DGVfinder_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DGVfinder.KeyDown
        If e.KeyCode = Keys.Enter Then
            Me.SelectedItemID()
            e.Handled = True
        End If
    End Sub

    Private Sub FillBranch()
        Try
            Me.sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            Me.sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While Me.sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvView()
        Try
            Me.TempTable = Me.MakeDataTableTemp()

            Me.sqlStr = "SELECT item_id, NonStock = CASE " & _
            "WHEN non_stock = 'True' THEN 'Yes' " & _
            "WHEN non_stock = 'False' THEN 'No' " & _
            "END, brand, product, model, description, group_id, " & _
            "(SELECT code FROM ItemGroups WHERE group_id = item.group_id) AS GroupCode, " & _
            "(SELECT types FROM Library WHERE lib_id = item.type_lib_id) AS ItemType FROM ItemMasterlist as item"

            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            While Me.sqlReader.Read
                Me.Row = Me.TempTable.NewRow()

                Me.Row("item_id") = Me.sqlReader("item_id")
                Me.Row("non_stock") = Me.sqlReader("NonStock")
                Me.Row("brand") = Me.sqlReader("brand")
                Me.Row("product") = Me.sqlReader("product")
                Me.Row("model") = Me.sqlReader("model")
                Me.Row("description") = Me.sqlReader("description")
                Me.Row("group_id") = Me.sqlReader("group_id")
                Me.Row("group_code") = Me.sqlReader("GroupCode")
                Me.Row("item_type") = Me.sqlReader("ItemType")

                Me.TempTable.Rows.Add(Me.Row)
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.TempDataView = New DataView
            Me.TempDataView.Table = Me.TempTable
            Me.TempDataView.Sort = "Brand ASC"
            Me.DGVfinder.DataSource = Me.TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmCountSheet_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.MdiParent = MDI
        Me.LoadDateAndRefresh()
    End Sub

    Private Sub GatherSelItemTotal(ByVal iType As String)
        Try
            Me.sqlConn = New SqlConnection(Me.strConn)

            '----Get Total Quantity
            If iType.Contains("NON") Then
                Me.sqlStr = "SELECT qty AS ret FROM [" & Me.cmbBranch.SelectedIndex & "] WHERE item_id = '" & Me.txtItemID.Text & "' AND cs_id = '" & Me.txtCSID.Text & "'"
                If Me.txtClass.Text.Trim <> "" Then Me.sqlStr = Me.sqlStr + " AND class = '" & Me.txtClass.Text & "'"
            Else
                Me.sqlStr = "SELECT Count(item_id) AS ret FROM [" & Me.cmbBranch.SelectedIndex & "] WHERE item_id = '" & Me.txtItemID.Text & "' AND cs_id = '" & Me.txtCSID.Text & "'"
            End If

            Me.sqlCmd = New SqlCommand(Me.sqlStr, Me.sqlConn)
            Me.sqlConn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read() Then
                Me.txtTotalQty.Text = Convert.ToDouble(Me.sqlReader("ret"))
                Me.sqlReader.Close()
                Me.sqlConn.Close()
            Else
                Me.txtTotalQty.Text = "0"
                Me.sqlReader.Close()
                Me.sqlConn.Close()
            End If

        Catch ex As Exception
            Me.sqlConn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub GetTotalRowCSID()
        Try
            Me.sqlConn = New SqlConnection(Me.strConn)

            Me.sqlStr = "SELECT Count(cs_id) AS ret FROM [" & Me.cmbBranch.SelectedIndex & "] WHERE cs_id = '" & Me.txtCSID.Text & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, Me.sqlConn)
            Me.sqlConn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.sqlReader.Read()
            Me.txtTotalRow.Text = Convert.ToInt32(Me.sqlReader("ret"))

            Me.sqlReader.Close()
            Me.sqlConn.Close()

        Catch ex As Exception
            Me.sqlConn.Close()
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadDateAndRefresh()
        Me.FilldgvView()
        Me.FillBranch()
        Me.dgvConfig()
        Me.ResetControls()
        If Me.DGVfinder.RowCount < 1 Then
            Me.pnlItemFinder.Enabled = False
            Me.gbInputs.Enabled = False
            Me.gbScope.Enabled = False
            Me.gbDetails.Enabled = False
            Me.btnSave.Enabled = False
        End If
        Me.btnConfirm.Focus()
    End Sub

    Private Sub ResetControls()
        Me.txtItemID.Clear()
        Me.txtGroupCode.Clear()
        Me.txtTotalQty.Clear()
        Me.txtTotalRow.Clear()

        Me.cmbBranch.Enabled = False
        Me.txtCSID.ReadOnly = True
        Me.txtItemID.ReadOnly = True
        Me.txtGroupCode.ReadOnly = True
        Me.txtTotalQty.ReadOnly = True
        Me.txtTotalRow.ReadOnly = True

        Me.txtBrand.Clear()
        Me.txtProduct.Clear()
        Me.txtModel.Clear()
        Me.txtDesc.Clear()

        Me.txtQty.Clear()
        Me.txtSerial.Clear()
        Me.txtClass.Clear()
    End Sub

    Private Sub SelectedItemID()
        Try
            Dim rownum As Integer = Me.DGVfinder.CurrentCell.RowIndex
            Dim cat As String = Me.DGVfinder.Item("item_type", rownum).Value.ToString

            Me.txtItemID.Text = Me.DGVfinder.Item("item_id", rownum).Value.ToString
            Me.txtGroupID.Text = Me.DGVfinder.Item("group_id", rownum).Value.ToString
            Me.txtGroupCode.Text = Me.DGVfinder.Item("group_code", rownum).Value.ToString
            Me.GatherSelItemTotal(cat)

            If cat.Contains("NON") Then
                Me.txtSerial.Clear()
                Me.txtSerial.ReadOnly = True
                Me.txtQty.ReadOnly = False
                Me.txtQty.Clear()
                Me.txtClass.ReadOnly = False
                Me.txtQty.Focus()
            Else
                Me.txtQty.Text = 1
                Me.txtQty.ReadOnly = True
                Me.txtClass.Clear()
                Me.txtClass.ReadOnly = True
                Me.txtSerial.ReadOnly = False
                Me.txtSerial.Clear()
                Me.txtSerial.Focus()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtBrand_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBrand.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtProduct.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtBrand_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBrand.TextChanged
        If Me.txtBrand.Text = "" And Me.txtProduct.Text = "" And Me.txtModel.Text = "" And Me.txtDesc.Text = "" Then
            Me.TempDataView.RowFilter = String.Empty
            Me.TempDataView.Sort = "Brand Asc"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "Brand Like '%" & Me.txtBrand.Text & "%' AND Product LIKE '%" & Me.txtProduct.Text & "%' AND Model Like '%" & Me.txtModel.Text & "%' AND Description Like '%" & Me.txtDesc.Text & "%'"
    End Sub

    Private Sub txtClass_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtClass.KeyPress
        Dim ValidInputChar = "abcABC" + vbBack

        If e.KeyChar = Chr(13) Then Me.btnSave.PerformClick()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtCSID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCSID.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.btnConfirm.PerformClick()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtDesc_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDesc.KeyPress
        Try
            Dim ValidInputChar = AlphaNumeric + vbBack

            If e.KeyChar = Chr(13) Then
                Me.DGVfinder.Focus()
                Me.DGVfinder.Item("Enum", 0).Selected = True
            End If
            If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtDesc_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDesc.TextChanged
        If Me.txtBrand.Text = "" And Me.txtProduct.Text = "" And Me.txtModel.Text = "" And Me.txtDesc.Text = "" Then
            Me.TempDataView.RowFilter = String.Empty
            Me.TempDataView.Sort = "Brand Asc"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "Brand Like '%" & Me.txtBrand.Text & "%' AND Product LIKE '%" & Me.txtProduct.Text & "%' AND Model Like '%" & Me.txtModel.Text & "%' AND Description Like '%" & Me.txtDesc.Text & "%'"
    End Sub

    Private Sub txtModel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtModel.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtDesc.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtModel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtModel.TextChanged
        If Me.txtBrand.Text = "" And Me.txtProduct.Text = "" And Me.txtModel.Text = "" And Me.txtDesc.Text = "" Then
            Me.TempDataView.RowFilter = String.Empty
            Me.TempDataView.Sort = "Brand Asc"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "Brand Like '%" & Me.txtBrand.Text & "%' AND Product LIKE '%" & Me.txtProduct.Text & "%' AND Model Like '%" & Me.txtModel.Text & "%' AND Description Like '%" & Me.txtDesc.Text & "%'"
    End Sub

    Private Sub txtProduct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProduct.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtModel.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProduct.TextChanged
        If Me.txtBrand.Text = "" And Me.txtProduct.Text = "" And Me.txtModel.Text = "" And Me.txtDesc.Text = "" Then
            Me.TempDataView.RowFilter = String.Empty
            Me.TempDataView.Sort = "Brand Asc"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "Brand Like '%" & Me.txtBrand.Text & "%' AND Product LIKE '%" & Me.txtProduct.Text & "%' AND Model Like '%" & Me.txtModel.Text & "%' AND Description Like '%" & Me.txtDesc.Text & "%'"
    End Sub

    Private Sub txtQty_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtQty.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtClass.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtRptDate_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRptDate.EnabledChanged
        If Me.txtRptDate.Enabled = False Then
            Me.lblRptDate.Enabled = False
        Else
            Me.lblRptDate.Enabled = True
        End If
    End Sub

    Private Sub txtRptDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRptDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnView.PerformClick()
    End Sub

    Private Sub txtSerial_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSerial.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnSave.PerformClick()
    End Sub

    Private Sub txtSerial_ReadOnlyChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSerial.ReadOnlyChanged
        If Me.txtSerial.ReadOnly = True Then
            Me.btnCheckSerial.Enabled = False
        Else
            Me.btnCheckSerial.Enabled = True
        End If
    End Sub
End Class
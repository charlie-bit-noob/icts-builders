Imports System.Data
Imports System.Data.SqlClient

Public Class Lock

    Private IsPwdCorrect As Boolean = False
    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Sub btnEnd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnd.Click
        Try
            If MessageBox.Show("Are you sure you want to end application?", "Please confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                Me.txtpassword.Focus()
                Exit Sub
            Else
                Me.sqlStr = (("UPDATE UserAccounts SET status = 'Offline' " & _
                "WHERE ua_id = " & MDI.stbUserID.Text & " " & _
                "AND status = 'Online'"))
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr

                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            End If
            End

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            Me.sqlStr = "SELECT * FROM UserAccounts " & _
            "WHERE ua_id = " & MDI.stbUserID.Text & " AND Password = '" & Me.txtpassword.Text & "'"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read Then
                Me.sqlReader.Close()
                sqlDBconn.Close()

                MDI.WindowState = FormWindowState.Maximized
                Me.IsPwdCorrect = True
                Me.Close()
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Me.IsPwdCorrect = False

                MsgBox("Invalid Password. Please type the password correctly or check if Capslock key is on.", vbOKOnly + vbExclamation, "Pag sure ra gud diha yot :)")
                Me.txtpassword.ResetText()
                Me.txtpassword.Focus()
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub Lock_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If Me.IsPwdCorrect = True Then
                e.Cancel = False
            Else
                e.Cancel = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub Lock_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.txtpassword.ResetText()
            Me.txtpassword.Focus()
            Me.IsPwdCorrect = False
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub txtpassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtpassword.KeyPress
        If e.KeyChar = Chr(Keys.Space) Then e.Handled = True
        If e.KeyChar = Chr(13) Then Me.btnUnlock.PerformClick()
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmParaOutgoing

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function TrappingMe() As Boolean
        If Me.mtxtTo.MaskFull = False Then Return True
        If Me.txtSumTitle.Text = "" Then Return True
        If Me.rbGroup.Checked = True Then
            If Me.cmbGroupFrom.SelectedIndex < 1 Or Me.cmbGroupTo.SelectedIndex < 1 Then Return True
        End If
        If Me.rbModel.Checked = True Then
            If Me.txtModelFrom.Text = "" Or Me.txtModelTo.Text = "" Then Return True
        End If
    End Function

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        If Me.TrappingMe() = True Then
            MessageBox.Show("Cannot proceed if the required data is not set.", _
            Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Me.mtxtFrom.MaskFull = True Then
            If IsDate(Me.mtxtFrom.Text) = False Then
                MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
            If Convert.ToDateTime(Me.mtxtFrom.Text).Year < 1900 Then
                MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        End If
        If IsDate(Me.mtxtTo.Text) = False Then
            MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Convert.ToDateTime(Me.mtxtTo.Text).Year < 1900 Then
            MessageBox.Show("Invalid range of date!", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If
        If Me.mtxtFrom.MaskFull = True And Me.mtxtTo.MaskFull = True Then
            If Convert.ToDateTime(Me.mtxtFrom.Text) > Convert.ToDateTime(Me.mtxtTo.Text) Then
                MessageBox.Show("End date should not less than the start date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        End If
        If My.Forms.frmRptOutgoing.Visible = True Then My.Forms.frmRptOutgoing.Close()
        ConstructSQL()
        My.Forms.frmRptOutgoing.Show()
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnItemFinderFrom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnItemFinderFrom.Click
        If Me.btnItemFinderFrom.Text = ">" Then
            Me.btnItemFinderFrom.Text = "<"
            My.Forms.frmFinderItem.ShowDialog()
        Else
            Try
                Me.sqlStr = "SELECT brand + ' ' + product + ' ' + model + ' ' + description AS concat " & _
                "FROM ItemMasterlist AS im WHERE item_id = '" & SelectedItemID & "'"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = sqlCmd.ExecuteReader()

                Me.sqlReader.Read()
                Me.txtModelFrom.Text = sqlReader(0)
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Me.btnItemFinderFrom.Text = ">"
            Catch ex As Exception
                sqlDBconn.Close()
                MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
            End Try
        End If
    End Sub

    Private Sub btnItemFinderTo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnItemFinderTo.Click
        If Me.btnItemFinderTo.Text = ">" Then
            Me.btnItemFinderTo.Text = "<"
            My.Forms.frmFinderItem.ShowDialog()
        Else
            Try
                Me.sqlStr = "SELECT brand + ' ' + product + ' ' + model + ' ' + description AS concat " & _
                "FROM ItemMasterlist AS im WHERE item_id = '" & SelectedItemID & "'"
                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = sqlCmd.ExecuteReader()

                Me.sqlReader.Read()
                Me.txtModelTo.Text = sqlReader(0)
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Me.btnItemFinderTo.Text = ">"
            Catch ex As Exception
                sqlDBconn.Close()
                MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
            End Try
        End If
    End Sub

    Private Sub cmbBranch_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbBranch.SelectionChangeCommitted
        If Me.cmbBranch.SelectedIndex > 0 Then
            Me.txtSumTitle.Text = cmbBranch.Text + " - " + Me.Text.Replace(" Parameters", "")
        Else
            Me.txtSumTitle.Text = Me.Text.Replace(" Parameters", "")
        End If
    End Sub

    Private Sub ConstructSQL()
        Dim sqlDate As String
        Dim sqlLocation As String
        Dim sqlGroup As String
        Dim sqlModel As String

        If Me.mtxtFrom.MaskFull = False Then
            sqlDate = "date_issued <= '" & Me.mtxtTo.Text & "' AND "
        Else
            sqlDate = "date_issued BETWEEN '" & Me.mtxtFrom.Text & "' AND " & _
            "'" & Me.mtxtTo.Text & "' AND "
        End If

        If Me.cmbBranch.SelectedIndex > 0 Then
            sqlLocation = "branch_id = " & Me.cmbBranch.SelectedIndex & " AND "
        Else
            sqlLocation = ""
        End If
        If Me.rbGroup.Checked = True Then
            sqlGroup = "item_id IN (SELECT im3.item_id FROM ItemMasterlist im3 " & _
            "WHERE im3.group_id IN (SELECT group_id FROM ItemGroups " & _
            "WHERE code BETWEEN '" & Me.cmbGroupFrom.Text & "' AND '" & Me.cmbGroupTo.Text & "') " & _
            "AND im3.non_stock = 'False')"
        Else
            sqlGroup = ""
        End If
        If Me.rbModel.Checked = True Then
            sqlModel = "item_id IN (SELECT item_id FROM " & _
            "(SELECT item_id, brand + ' ' + product + ' ' + model + ' ' + " & _
            "description AS ItemDesc FROM ItemMasterlist im3 WHERE im3.non_stock = 'False') Mod " & _
            "WHERE ItemDesc BETWEEN '" & Me.txtModelFrom.Text & "' AND '" & Me.txtModelTo.Text & "') "
        Else
            sqlModel = ""
        End If

        Me.txtSQLpara.Text = sqlDate + sqlLocation + sqlGroup + sqlModel
    End Sub

    Private Sub FillBranch()
        Try
            Me.sqlStr = "SELECT long_name FROM Branches WHERE long_name IS NOT NULL ORDER BY branch_id ASC"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.cmbBranch.Items.Clear()
            Me.cmbBranch.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbBranch.Items.Add(sqlReader("long_name"))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbBranch.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FillGroup()
        Try
            Me.sqlStr = "SELECT code FROM ItemGroups ORDER BY group_id ASC"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            Me.cmbGroupFrom.Items.Clear()
            Me.cmbGroupTo.Items.Clear()
            Me.cmbGroupFrom.Items.Add("- Select One -")
            Me.cmbGroupTo.Items.Add("- Select One -")

            While sqlReader.Read
                Me.cmbGroupFrom.Items.Add(sqlReader(0))
                Me.cmbGroupTo.Items.Add(sqlReader(0))
            End While
            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbGroupFrom.SelectedIndex = 0
            Me.cmbGroupTo.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmParaOutgoing_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If My.Forms.frmRptOutgoing.Visible = True Then My.Forms.frmRptOutgoing.Close()
        MDI.mnuOutgoingStocks.Enabled = True
    End Sub

    Private Sub frmParaOutgoing_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.MdiParent = MDI
            Me.FillBranch()
            Me.FillGroup()
            Me.txtSumTitle.Text = Me.Text.Replace(" Parameters", "")
            Me.rbGroup.Checked = False
            Me.rbModel.Checked = True
            Me.mtxtFrom.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub mtxtFrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtFrom.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.mtxtTo.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub mtxtTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtTo.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.txtSumTitle.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub rbGroup_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbGroup.CheckedChanged
        If Me.rbGroup.Checked = False Then
            Me.cmbGroupFrom.SelectedIndex = 0
            Me.cmbGroupTo.SelectedIndex = 0
            Me.cmbGroupFrom.Enabled = False
            Me.cmbGroupTo.Enabled = False
        Else
            Me.cmbGroupFrom.Enabled = True
            Me.cmbGroupTo.Enabled = True
            Me.cmbGroupFrom.Focus()
        End If
    End Sub

    Private Sub rbModel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbModel.CheckedChanged
        If rbModel.Checked = False Then
            Me.btnItemFinderFrom.Enabled = False
            Me.txtModelFrom.Clear()
            Me.txtItemIDFrom.Clear()
            Me.btnItemFinderTo.Enabled = False
            Me.txtModelTo.Clear()
            Me.txtItemIDTo.Clear()
        Else
            Me.btnItemFinderFrom.Enabled = True
            Me.btnItemFinderTo.Enabled = True
            Me.btnItemFinderFrom.Focus()
        End If
    End Sub

    Private Sub txtSumTitle_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSumTitle.KeyPress
        If e.KeyChar = Chr(13) Then cmbBranch.Focus()
    End Sub
End Class
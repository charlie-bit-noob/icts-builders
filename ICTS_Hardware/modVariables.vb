Imports System.Data
Imports System.Data.SqlClient

Module modVariables

    Public Const AlphaNumeric As String = " -/.0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm"
    Public Const AlphaOnly As String = " QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm"
    Public Const NumericOnly As String = ".0123456789"

    '---For Every Transaction
    Public GenRootID, GenRunID, SelectedItemID, GenShortAF As String
    Public GenPopID, GenAFID, GenBranchID, SelectedSrcBranchID As Integer

    '---For Purchases and Transfer Accountable Forms Only
    Public GenTransIDPurchasesOnly, GenTransIDTransferOnly As Integer

    '---For Item Hsitory
    Public HistoryTable As DataTable
    Public HistoryDataview As DataView
    Public Row As DataRow

    '---For Sourcing Non-Serial Items
    Public selectedCount As Integer = 0
    Public rootList As List(Of String) = New List(Of String)
    Public runList As List(Of String) = New List(Of String)
    Public afList As List(Of String) = New List(Of String)
    Public afnumList As List(Of String) = New List(Of String)
    Public IsFreeList As List(Of String) = New List(Of String)
    Public classList As List(Of String) = New List(Of String)
    Public costList As List(Of String) = New List(Of String)
    Public qtyList As List(Of String) = New List(Of String)

    '---Use when found no source
    Public NullRoot As String = vbNullString
    Public NullRun As String = vbNullString
    Public Nullaf As String = vbNullString
    Public Nullnum As String = vbNullString
    Public NullFree As String = vbNullString
    Public NullClass As String = vbNullString
    Public NullSerial As String = vbNullString

    '---For number of items verification
    Public NumOfItems As Integer = 0

End Module

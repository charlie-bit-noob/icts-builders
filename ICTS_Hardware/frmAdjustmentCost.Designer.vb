<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAdjustmentCost
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtFreight = New System.Windows.Forms.TextBox
        Me.txtTotalAmount = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.lbltotalInAmt = New System.Windows.Forms.Label
        Me.txtArrastre = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtDiscount = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtCost = New System.Windows.Forms.TextBox
        Me.btnUpdateItem = New System.Windows.Forms.Button
        Me.btnUpdatePurchases = New System.Windows.Forms.Button
        Me.btnUpdateEntry = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtFreight
        '
        Me.txtFreight.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtFreight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFreight.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFreight.Location = New System.Drawing.Point(203, 25)
        Me.txtFreight.MaxLength = 50
        Me.txtFreight.Name = "txtFreight"
        Me.txtFreight.Size = New System.Drawing.Size(185, 21)
        Me.txtFreight.TabIndex = 3
        Me.txtFreight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTotalAmount
        '
        Me.txtTotalAmount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtTotalAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalAmount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtTotalAmount.Location = New System.Drawing.Point(12, 25)
        Me.txtTotalAmount.MaxLength = 50
        Me.txtTotalAmount.Name = "txtTotalAmount"
        Me.txtTotalAmount.Size = New System.Drawing.Size(185, 21)
        Me.txtTotalAmount.TabIndex = 1
        Me.txtTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(200, 53)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "Arrastre:"
        '
        'lbltotalInAmt
        '
        Me.lbltotalInAmt.AutoSize = True
        Me.lbltotalInAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotalInAmt.Location = New System.Drawing.Point(9, 9)
        Me.lbltotalInAmt.Name = "lbltotalInAmt"
        Me.lbltotalInAmt.Size = New System.Drawing.Size(75, 13)
        Me.lbltotalInAmt.TabIndex = 0
        Me.lbltotalInAmt.Text = "Total Amount:"
        '
        'txtArrastre
        '
        Me.txtArrastre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtArrastre.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtArrastre.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtArrastre.Location = New System.Drawing.Point(203, 69)
        Me.txtArrastre.MaxLength = 50
        Me.txtArrastre.Name = "txtArrastre"
        Me.txtArrastre.Size = New System.Drawing.Size(185, 21)
        Me.txtArrastre.TabIndex = 7
        Me.txtArrastre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(200, 9)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Freight:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(9, 53)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(52, 13)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "Discoun&t:"
        '
        'txtDiscount
        '
        Me.txtDiscount.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDiscount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDiscount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtDiscount.Location = New System.Drawing.Point(12, 69)
        Me.txtDiscount.MaxLength = 50
        Me.txtDiscount.Name = "txtDiscount"
        Me.txtDiscount.Size = New System.Drawing.Size(185, 21)
        Me.txtDiscount.TabIndex = 5
        Me.txtDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-4, 105)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(407, 2)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(158, 131)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(39, 14)
        Me.Label15.TabIndex = 9
        Me.Label15.Text = "C&ost:"
        '
        'txtCost
        '
        Me.txtCost.BackColor = System.Drawing.SystemColors.Window
        Me.txtCost.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCost.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCost.ForeColor = System.Drawing.SystemColors.ControlText
        Me.txtCost.Location = New System.Drawing.Point(203, 128)
        Me.txtCost.MaxLength = 12
        Me.txtCost.Name = "txtCost"
        Me.txtCost.Size = New System.Drawing.Size(185, 22)
        Me.txtCost.TabIndex = 10
        Me.txtCost.TabStop = False
        Me.txtCost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnUpdateItem
        '
        Me.btnUpdateItem.Location = New System.Drawing.Point(12, 209)
        Me.btnUpdateItem.Name = "btnUpdateItem"
        Me.btnUpdateItem.Size = New System.Drawing.Size(185, 23)
        Me.btnUpdateItem.TabIndex = 12
        Me.btnUpdateItem.Text = "Update I&M"
        Me.btnUpdateItem.UseVisualStyleBackColor = True
        '
        'btnUpdatePurchases
        '
        Me.btnUpdatePurchases.Location = New System.Drawing.Point(12, 180)
        Me.btnUpdatePurchases.Name = "btnUpdatePurchases"
        Me.btnUpdatePurchases.Size = New System.Drawing.Size(185, 23)
        Me.btnUpdatePurchases.TabIndex = 11
        Me.btnUpdatePurchases.Text = "Update &Purchases"
        Me.btnUpdatePurchases.UseVisualStyleBackColor = True
        '
        'btnUpdateEntry
        '
        Me.btnUpdateEntry.Location = New System.Drawing.Point(203, 180)
        Me.btnUpdateEntry.Name = "btnUpdateEntry"
        Me.btnUpdateEntry.Size = New System.Drawing.Size(185, 23)
        Me.btnUpdateEntry.TabIndex = 13
        Me.btnUpdateEntry.Text = "Update this &Entry"
        Me.btnUpdateEntry.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(203, 209)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(185, 23)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmAdjustmentCost
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(401, 245)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnUpdateEntry)
        Me.Controls.Add(Me.btnUpdatePurchases)
        Me.Controls.Add(Me.btnUpdateItem)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.txtCost)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtFreight)
        Me.Controls.Add(Me.txtTotalAmount)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lbltotalInAmt)
        Me.Controls.Add(Me.txtArrastre)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.txtDiscount)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAdjustmentCost"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Costing"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtFreight As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lbltotalInAmt As System.Windows.Forms.Label
    Friend WithEvents txtArrastre As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtDiscount As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtCost As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateItem As System.Windows.Forms.Button
    Friend WithEvents btnUpdatePurchases As System.Windows.Forms.Button
    Friend WithEvents btnUpdateEntry As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
End Class

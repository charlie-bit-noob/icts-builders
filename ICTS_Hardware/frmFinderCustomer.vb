Imports System.Data
Imports System.Data.SqlClient

Public Class frmFinderCustomer

    Private Row As DataRow

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private TempDataView As DataView
    Private TempTable As DataTable

    Private Function MakeDataTableTemp() As DataTable
        Dim TableTemp As DataTable
        TableTemp = New DataTable("Temp")

        Dim col_cust_id As DataColumn = New DataColumn("cust_id")
        col_cust_id.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_cust_id)

        Dim col_type As DataColumn = New DataColumn("type")
        col_type.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_type)

        Dim col_lastname As DataColumn = New DataColumn("lastname")
        col_lastname.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_lastname)

        Dim col_firstname As DataColumn = New DataColumn("firstname")
        col_firstname.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_firstname)

        Dim col_companyname As DataColumn = New DataColumn("companyname")
        col_companyname.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_companyname)

        Dim col_address As DataColumn = New DataColumn("address")
        col_address.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_address)

        Dim col_telnum As DataColumn = New DataColumn("telnum")
        col_telnum.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_telnum)

        Dim col_mobnum As DataColumn = New DataColumn("mobnum")
        col_mobnum.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_mobnum)

        Dim col_emailadd As DataColumn = New DataColumn("emailadd")
        col_emailadd.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_emailadd)

        Dim col_branch As DataColumn = New DataColumn("branch")
        col_branch.DataType = System.Type.GetType("System.String")
        TableTemp.Columns.Add(col_branch)

        TableTemp.DefaultView.Sort = "lastname ASC, firstname ASC"

        MakeDataTableTemp = TableTemp
    End Function

    Public Sub dgvCustomerConfig()
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In Me.dgvCustomers.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "cust_id"
                        .HeaderText = "Cust.ID"
                        .Width = 100
                    Case "type"
                        .HeaderText = "Type"
                        .Width = 100
                    Case "lastname"
                        .HeaderText = "Last Name"
                        .Width = 100
                    Case "firstname"
                        .HeaderText = "First Name"
                        .Width = 100
                    Case "companyname"
                        .HeaderText = "Company Name"
                        .Width = 200
                    Case "address"
                        .HeaderText = "Address"
                        .Width = 200
                    Case "telnum"
                        .HeaderText = "Telephone No."
                        .Width = 100
                    Case "mobnum"
                        .HeaderText = "Mobile No."
                        .Width = 100
                    Case "emailadd"
                        .HeaderText = "E-Mail Add."
                        .Width = 150
                    Case "branch"
                        .HeaderText = "Branch"
                        .Width = 150
                End Select
            End With
        Next
    End Sub

    Private Sub dgvCustomers_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvCustomers.CellDoubleClick
        Try
            Dim rowindex As Integer = Me.dgvCustomers.CurrentCell.RowIndex
            Dim flname, fname, lname, custid, cname As String
            Dim type As String = Me.dgvCustomers.Item("type", rowindex).Value

            custid = Me.dgvCustomers.Item("cust_id", rowindex).Value
            cname = Me.dgvCustomers.Item("companyname", rowindex).Value
            lname = Me.dgvCustomers.Item("lastname", rowindex).Value
            fname = Me.dgvCustomers.Item("firstname", rowindex).Value
            flname = fname + " " + lname

            With My.Settings
                .CustID = custid
                If type.Contains("INDIV") Then
                    .CustName = flname
                Else
                    .CustName = cname
                End If
            End With

            Me.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilldgvCustomers()
        Try
            TempTable = MakeDataTableTemp()

            sqlStr = "SELECT cust_id, branch_id, (SELECT long_name FROM Branches WHERE branch_id = c. branch_id) AS cBranch, " & _
            "cust_type_lib_id, (SELECT cust_types FROM Library WHERE lib_id = c.cust_type_lib_id) AS cType, " & _
            "first_name, last_name, company_name, address, " & _
            "telephone_num, mobile_num, email_add " & _
            "FROM Customers AS c WHERE cust_id IS NOT NULL"

            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = TempTable.NewRow()

                Row("cust_id") = sqlReader("cust_id")
                Row("branch") = sqlReader("cBranch")
                Row("type") = sqlReader("cType")
                Row("firstname") = sqlReader("first_name")
                Row("lastname") = sqlReader("last_name")
                Row("companyname") = sqlReader("company_name")
                Row("address") = sqlReader("address")
                Row("telnum") = sqlReader("telephone_num")
                Row("mobnum") = sqlReader("mobile_num")
                Row("emailadd") = sqlReader("email_add")

                TempTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            TempDataView = New DataView
            TempDataView.Table = TempTable
            TempDataView.Sort = "lastname ASC, firstname ASC"
            Me.dgvCustomers.DataSource = TempDataView

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmFinderCustomer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtLastname.Focus()
    End Sub

    Private Sub frmFinderCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.LoadDataAndRefresh()
            dgvCustomerConfig()

        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub LinkCreate_LinkClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkCreate.LinkClicked
        Me.Close()
        My.Forms.frmNewCustomer.Show()
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FilldgvCustomers()

        If Me.dgvCustomers.RowCount < 1 Then
            Me.panelIndividual.Enabled = False
            Me.panelNonIndividual.Enabled = False
            Me.panelContacts.Enabled = False
        End If

        Me.txtLastname.Focus()
    End Sub

    Private Sub txtCompany_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCompany.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtMobile.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtCompany_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCompany.TextChanged
        If Me.txtCompany.Text = "" Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "lastname ASC, firstname ASC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "companyname LIKE '%" & Me.txtCompany.Text & "%'"
        Me.TempDataView.Sort = "companyname ASC"
    End Sub

    Private Sub txtFirstname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFirstname.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtCompany.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtFirstname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFirstname.TextChanged
        If Me.txtLastname.Text = "" And Me.txtFirstname.Text = "" Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "lastname ASC, firstname ASC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "lastname LIKE '%" & Me.txtLastname.Text & "%' AND firstname LIKE '%" & Me.txtFirstname.Text & "%'"
        Me.TempDataView.Sort = "lastname ASC, firstname ASC"
    End Sub

    Private Sub txtLastname_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLastname.KeyPress
        Dim ValidInputChar = AlphaOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtFirstname.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtLastname_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLastname.TextChanged
        If Me.txtLastname.Text = "" And Me.txtFirstname.Text = "" Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "lastname ASC, firstname ASC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "lastname LIKE '%" & Me.txtLastname.Text & "%' AND firstname LIKE '%" & Me.txtFirstname.Text & "%'"
        Me.TempDataView.Sort = "lastname ASC, firstname ASC"
    End Sub

    Private Sub txtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMobile.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtTelephone.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtMobile_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMobile.TextChanged
        If Me.txtMobile.Text = "" Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "lastname ASC, firstname ASC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "mobnum LIKE '%" & Me.txtMobile.Text & "%'"
        Me.TempDataView.Sort = "mobnum ASC"
    End Sub

    Private Sub txtTelephone_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelephone.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtLastname.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtTelephone_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTelephone.TextChanged
        If Me.txtTelephone.Text = "" Then
            Me.TempDataView.RowFilter = ""
            Me.TempDataView.Sort = "lastname ASC, firstname ASC"
            Exit Sub
        End If
        Me.TempDataView.RowFilter = "telnum LIKE '%" & Me.txtTelephone.Text & "%'"
        Me.TempDataView.Sort = "telnum ASC"
    End Sub
End Class
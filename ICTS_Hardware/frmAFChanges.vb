Imports System.Data
Imports System.Data.SqlClient

Public Class frmAFChanges
    Public frmView, frmList As Form

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Public strNumber, strAFtype, strMsg As String

    Private Function HasInvalidInput() As Boolean
        If Me.rbDate.Checked = False And Me.rbNumber.Checked = False And Me.rbTransaction.Checked = False And Me.rbRemarks.Checked = False Then Return True
        If Me.rbTransaction.Checked = True And Me.cmbTransaction.SelectedIndex = 0 Then Return True

        If Me.mtxtDate.Enabled = True Then
            If IsDate(Me.mtxtDate.Text) = False Then
                Me.strMsg = "Invalid Date!"
                Return True
            End If
        End If

        If Me.txtNumber.Enabled = True Then
            If Me.txtNumber.Text.Trim = "" Then
                Me.strMsg = "Invalid Number!"
                Return True
            Else
                If Me.NumberReplacementExists(Me.txtNumber.Text, GenAFID) = True Then Return True
            End If
        End If

        If Me.cmbTransaction.Enabled = True Then
            If Me.cmbTransaction.SelectedIndex = 0 Then
                Me.strMsg = "Invalid Transaction!"
                Return True
            End If
        End If

        If Me.txtRemarks.Enabled = True Then
            If Me.txtRemarks.Text.Trim = "" Then
                Me.strMsg = "Invalid Remarks!"
                Return True
            End If
        End If
    End Function

    Private Function NumberReplacementExists(ByVal num As String, ByVal bid As Integer) As Boolean
        Try
            Me.sqlStr = "SELECT * FROM AFs" & Me.strAFtype & " WHERE branch_id = " & GenBranchID & " " & _
            "AND af_num = '" & num & "' AND af_id = " & GenAFID & ""
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            If Me.sqlReader.Read() Then
                Me.strMsg = "Number already exists!"
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Return True
            Else
                Me.sqlReader.Close()
                sqlDBconn.Close()

                Return False
            End If

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Function

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Me.GetAFtype()

        If Me.HasInvalidInput = True Then
            MessageBox.Show(Me.strMsg + " Please be cautious.", "STOP!", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            Exit Sub
        End If

        If MessageBox.Show("Are you sure you want to apply this changes? If this feature is misused, you might break some of the entries.", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.No Then Exit Sub

        Dim strCol As String = Nothing
        Dim ctrlCol As New Control

        If Me.rbNumber.Checked = True Then
            ctrlCol.Text = Me.txtNumber.Text
            strCol = "af_num"
        End If

        If Me.rbDate.Checked = True Then
            ctrlCol.Text = Me.mtxtDate.Text
            strCol = "date_issued"
        End If

        If Me.rbTransaction.Checked = True Then
            ctrlCol.Text = Me.cmbTransaction.SelectedIndex
            strCol = "trans_id"
        End If

        If Me.rbRemarks.Checked = True Then
            ctrlCol.Text = Me.txtRemarks.Text
            strCol = "remarks"
        End If

        Try
            If Me.rbRemarks.Checked = True Then
                Me.sqlStr = "UPDATE AFsRemarks SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                "AND branch_id = " & GenBranchID & ""
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()
            Else
                Me.sqlStr = "UPDATE AFs" & Me.strAFtype & " SET " & strCol & " = '" & ctrlCol.Text.Trim & "', " & _
                "ua_id = " & MDI.stbUserID.Text & ", date_updated = getdate() " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                "AND branch_id = " & GenBranchID & ""
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.sqlStr = "UPDATE TypeNonSerial SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                "AND branch_id = " & GenBranchID & ""
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.sqlStr = "UPDATE TypeSerial SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                "AND branch_id = " & GenBranchID & ""
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                If Me.rbTransaction.Checked = False Then
                    Me.sqlStr = "UPDATE AFsRemarks SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                    "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                    "AND branch_id = " & GenBranchID & ""
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                End If
            End If

            If Me.rbNumber.Checked = True Then
                Me.sqlStr = "UPDATE TypeSerial SET ref_af_num = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE ref_af_id = " & GenAFID & " AND ref_af_num = '" & Me.strNumber & "'"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.sqlStr = "UPDATE TypeNonSerial SET ref_af_num = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE ref_af_id = " & GenAFID & " AND ref_af_num = '" & Me.strNumber & "'"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Dim ArrayAFs(0 To 2) As String

                ArrayAFs(0) = "AFsIn"
                ArrayAFs(1) = "AFsTransfer"
                ArrayAFs(2) = "AFsOut"

                For Each item As Object In ArrayAFs
                    Me.sqlStr = "UPDATE " & item.ToString & " SET ref_af_num = '" & ctrlCol.Text.Trim & "' " & _
                    "WHERE ref_af_id = " & GenAFID & " AND ref_af_num = '" & Me.strNumber & "'"
                    Me.sqlCmd = New SqlCommand
                    Me.sqlCmd.Connection = sqlDBconn
                    Me.sqlCmd.CommandText = Me.sqlStr
                    sqlDBconn.Open()
                    Me.sqlCmd.ExecuteNonQuery()
                    sqlDBconn.Close()
                Next

                Me.sqlStr = "UPDATE  PurchasesAmounts SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "'"
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

                Me.sqlStr = "UPDATE AFsRemarks SET " & strCol & " = '" & ctrlCol.Text.Trim & "' " & _
                "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strNumber & "' " & _
                "AND branch_id = " & GenBranchID & ""
                Me.sqlCmd = New SqlCommand
                Me.sqlCmd.Connection = sqlDBconn
                Me.sqlCmd.CommandText = Me.sqlStr
                sqlDBconn.Open()
                Me.sqlCmd.ExecuteNonQuery()
                sqlDBconn.Close()

            End If

            MessageBox.Show("Changes has been successfully applied. All the list and viewer forms will be closed in order to see changes.", "Congratulations :)", MessageBoxButtons.OK, MessageBoxIcon.Information)

            CloseMe(Me.frmList, Me.frmView)

            Me.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub cmbTransaction_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cmbTransaction.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
    End Sub

    Private Sub FillTransaction()
        Dim list As New List(Of Integer)

        list.Add(1)
        list.Add(4)
        list.Add(5)

        Me.cmbTransaction.Items.Clear()
        Me.cmbTransaction.Items.Add("- Select One -")
        Me.cmbTransaction.SelectedIndex = 0

        If list.Contains(GenAFID) Then Exit Sub

        Try
            Me.sqlStr = "SELECT " & GenShortAF & " FROM Transactions WHERE " & GenShortAF & " IS NOT NULL ORDER BY trans_id ASC"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()

            While Me.sqlReader.Read()
                Me.cmbTransaction.Items.Add(Me.sqlReader(0))
            End While

            Me.sqlReader.Close()
            sqlDBconn.Close()

            Me.cmbTransaction.SelectedIndex = 0

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmAFChanges_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.FillTransaction()
        Me.rbDate.Checked = False
        Me.mtxtDate.Enabled = False
        Me.rbNumber.Checked = False
        Me.txtNumber.Enabled = False
        Me.rbTransaction.Checked = False
        Me.cmbTransaction.Enabled = False
        Me.rbRemarks.Checked = False
        Me.txtRemarks.Enabled = False
    End Sub

    Private Sub GetAFtype()
        Try
            Me.sqlStr = "SELECT af_type FROM AFs WHERE af_id = " & GenAFID & ""
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = Me.sqlCmd.ExecuteReader()
            Me.sqlReader.Read()
            Me.strAFtype = Me.sqlReader(0)
            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub mtxtDate_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles mtxtDate.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
    End Sub

    Private Sub rbDate_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbDate.CheckedChanged
        If Me.rbDate.Checked = True Then
            Me.mtxtDate.Enabled = True
            Me.mtxtDate.Focus()
        Else
            Me.mtxtDate.ResetText()
            Me.mtxtDate.Enabled = False
        End If
    End Sub

    Private Sub rbNumber_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbNumber.CheckedChanged
        If Me.rbNumber.Checked = True Then
            Me.txtNumber.Enabled = True
            Me.txtNumber.Focus()
        Else
            Me.txtNumber.Clear()
            Me.txtNumber.Enabled = False
        End If
    End Sub

    Private Sub rbNumber_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles rbNumber.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub rbRemarks_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbRemarks.CheckedChanged
        If Me.rbRemarks.Checked = True Then
            Me.txtRemarks.Enabled = True
            Me.txtRemarks.Focus()
        Else
            Me.txtRemarks.Clear()
            Me.txtRemarks.Enabled = False
        End If
    End Sub

    Private Sub rbTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbTransaction.CheckedChanged
        If Me.rbTransaction.Checked = True Then
            Me.cmbTransaction.Enabled = True
            Me.cmbTransaction.Focus()
        Else
            Me.cmbTransaction.SelectedIndex = 0
            Me.cmbTransaction.Enabled = False
        End If
    End Sub

    Private Sub txtRemarks_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtRemarks.KeyPress
        If e.KeyChar = Chr(13) Then Me.btnApply.PerformClick()
    End Sub
End Class
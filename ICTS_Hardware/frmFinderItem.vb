Imports System.Data
Imports System.Data.SqlClient

Public Class frmFinderItem

    Private ItemsDataView As DataView
    Private ItemsTable As DataTable
    Private Row As DataRow
    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private Function MakeDataTableItems() As DataTable
        Dim TableItems As DataTable
        TableItems = New DataTable("ItemMasterlist")

        Dim col_item_id As DataColumn = New DataColumn("item_id")
        col_item_id.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_item_id)

        Dim col_type As DataColumn = New DataColumn("type")
        col_type.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_type)

        Dim col_brand As DataColumn = New DataColumn("brand")
        col_brand.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_brand)

        Dim col_product As DataColumn = New DataColumn("product")
        col_product.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_product)

        Dim col_model As DataColumn = New DataColumn("model")
        col_model.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_model)

        Dim col_description As DataColumn = New DataColumn("description")
        col_description.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_description)

        Dim col_code As DataColumn = New DataColumn("code")
        col_code.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_code)

        Dim col_class As DataColumn = New DataColumn("class")
        col_class.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_class)

        Dim col_unit As DataColumn = New DataColumn("unit")
        col_unit.DataType = System.Type.GetType("System.String")
        TableItems.Columns.Add(col_unit)

        TableItems.DefaultView.Sort = "brand ASC"

        MakeDataTableItems = TableItems
    End Function

    Private Sub btnNewItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewItem.Click
        My.Forms.frmItemMasterlist.ShowDialog()
        Me.Close()
    End Sub

    Private Sub dgvItems_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvItems.CellDoubleClick
        Try
            If Me.dgvItems.RowCount < 1 Then Exit Sub

            Dim RowIndex As Integer = dgvItems.CurrentCell.RowIndex

            With dgvItems
                SelectedItemID = .Item("item_id", RowIndex).Value
            End With

            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub dgvItemsConfig()
        Dim Col As DataGridViewColumn
        Dim ColName As String

        For Each Col In dgvItems.Columns
            ColName = Col.Name
            With Col
                Select Case ColName
                    Case "item_id"
                        .HeaderText = "Item ID"
                        .Width = 100
                    Case "type"
                        .HeaderText = "Type"
                        .Width = 150
                    Case "brand"
                        .HeaderText = "Brand"
                        .Width = 200
                    Case "product"
                        .HeaderText = "Product"
                        .Width = 200
                    Case "model"
                        .HeaderText = "Model"
                        .Width = 200
                    Case "description"
                        .HeaderText = "Description"
                        .Width = 200
                    Case "code"
                        .HeaderText = "Group"
                        .Width = 100
                    Case "class"
                        .HeaderText = "Class"
                        .Width = 100
                    Case "unit"
                        .HeaderText = "Unit"
                        .Width = 100
                End Select
            End With
        Next
    End Sub

    Private Sub FilldgvItems()
        Try
            ItemsTable = MakeDataTableItems()

            sqlStr = "SELECT item_id, (SELECT types FROM Library " & _
            "WHERE lib_id = im.type_lib_id) AS imType, brand, product, " & _
            "model, description, (SELECT code FROM ItemGroups WHERE group_id = im.group_id) AS imCode, " & _
            "(SELECT class FROM ItemGroups WHERE group_id = im.group_id) AS imClass, " & _
            "(SELECT units FROM Library WHERE lib_id = im.unit_lib_id) AS imUnit " & _
            "FROM ItemMasterlist AS im"
            sqlCmd = New SqlCommand(sqlStr, sqlDBconn)
            sqlDBconn.Open()
            sqlReader = sqlCmd.ExecuteReader()
            While sqlReader.Read
                Row = ItemsTable.NewRow()
                Row("item_id") = sqlReader("item_id")
                Row("type") = sqlReader("imType")
                Row("brand") = sqlReader("brand")
                Row("product") = sqlReader("product")
                Row("model") = sqlReader("model")
                Row("description") = sqlReader("description")
                Row("code") = sqlReader("imCode")
                Row("class") = sqlReader("imClass")
                Row("unit") = sqlReader("imUnit")

                ItemsTable.Rows.Add(Row)
            End While
            sqlReader.Close()
            sqlDBconn.Close()

            ItemsDataView = New DataView
            ItemsDataView.Table = ItemsTable
            ItemsDataView.Sort = "Brand ASC"
            dgvItems.DataSource = ItemsDataView

            dgvItemsConfig()

        Catch ex As Exception
            sqlDBconn.Close()

            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub FilterItem()
        If Me.txtBrand.Text = "" And Me.txtProduct.Text = "" And Me.txtModel.Text = "" And Me.txtDescription.Text = "" Then
            ItemsDataView.RowFilter = ""
            ItemsDataView.Sort = "brand ASC"
        End If
        ItemsDataView.RowFilter = "brand LIKE '%" & Me.txtBrand.Text & "%' AND product LIKE '%" & Me.txtProduct.Text & "%' AND model LIKE '%" & Me.txtModel.Text & "%' AND description LIKE '%" & Me.txtDescription.Text & "%'"
        ItemsDataView.Sort = "brand, product, model, description"
    End Sub

    Private Sub frmFinderItem_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        Me.txtBrand.Focus()
    End Sub

    Private Sub frmFinderItem_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.LastUsedItemSet()
    End Sub

    Private Sub frmFinderItem_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.LoadDataAndRefresh()
    End Sub

    Private Sub LastUsedItemLoad()
        Try
            Me.txtBrand.Text = My.Settings.Set_Brand
            Me.txtProduct.Text = My.Settings.Set_Product
            Me.txtModel.Text = My.Settings.Set_Model
            Me.txtDescription.Text = My.Settings.Set_Description
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub LastUsedItemSet()
        Try
            My.Settings.Set_Brand = Me.txtBrand.Text
            My.Settings.Set_Product = Me.txtProduct.Text
            My.Settings.Set_Model = Me.txtModel.Text
            My.Settings.Set_Description = Me.txtDescription.Text
        Catch ex As Exception
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub LoadDataAndRefresh()
        Me.FilldgvItems()
        Me.LastUsedItemLoad()
        'Me.ResetControls()
    End Sub

    Private Sub ResetControls()
        Dim ctrl As Control

        For Each ctrl In Me.PanelSearch.Controls
            If TypeOf ctrl Is TextBox Then ClearTextBox(ctrl)
        Next
    End Sub

    Private Sub txtBrand_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBrand.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtProduct.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtBrand_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBrand.TextChanged
        FilterItem()
    End Sub

    Private Sub txtDescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDescription.KeyPress
        If e.KeyChar = Chr(13) Then Me.dgvItems.Focus()
    End Sub

    Private Sub txtDescription_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescription.TextChanged
        FilterItem()
    End Sub

    Private Sub txtModel_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtModel.KeyPress
        If e.KeyChar = Chr(13) Then Me.txtDescription.Focus()
    End Sub

    Private Sub txtModel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtModel.TextChanged
        FilterItem()
    End Sub

    Private Sub txtProduct_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtProduct.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack

        If e.KeyChar = Chr(13) Then Me.txtModel.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtProduct_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtProduct.TextChanged
        FilterItem()
    End Sub
End Class
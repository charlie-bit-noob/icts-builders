Imports System.Data
Imports System.Data.SqlClient

Public Class frmAdjustmentChangeItem
    Private dtSourceDate As Date
    Private intSourceOrderID, intSourceQuantity As Integer

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String

    Private strSourceNumber, strSourceItemID, strSourceClass, strItemType As String

    Private Sub btnApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApply.Click
        Try
            If Me.txtItemID.ReadOnly = False Then Exit Sub

            If MessageBox.Show("Are you sure you want to proceed?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Me.StockPassToReplacement()
            Me.ReplaceItem()

            MessageBox.Show("The item(s) changed successfully!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnVerify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        If Me.txtItemID.ReadOnly = True Then Exit Sub

        If Me.strSourceItemID = Me.txtItemID.Text Then
            MessageBox.Show("Item ID's cannot be equal", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Me.txtItemID.Focus()
            Me.txtItemID.SelectAll()
            Exit Sub
        End If

        Try
            Me.sqlStr = "SELECT im.item_id, im.type_lib_id, lib.type_tables FROM Library lib, ItemMasterlist im " & _
            "WHERE im.item_id = '" & Me.txtItemID.Text & "' AND lib.lib_id = im.type_lib_id"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()

            If Me.sqlReader.Read() Then
                Me.strItemType = Me.sqlReader("type_tables")
                Me.sqlReader.Close()
                sqlDBconn.Close()
            Else
                'Take note: Dapat dynamic imong Item Adjustment, wether Non-Serial or Serial iyang dawaton
                MessageBox.Show("Item ID not found!", "Pag sure ba!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Me.txtItemID.Focus()
                Me.txtItemID.SelectAll()
                Me.sqlReader.Close()
                sqlDBconn.Close()
                Exit Sub
            End If

            Me.txtItemID.ReadOnly = True

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub frmAdjustmentChangeItem_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.txtItemID.ReadOnly = False
        Me.txtItemID.Clear()
        Me.txtItemID.Focus()
        Me.btnVerify.Enabled = False
    End Sub

    Public Sub MainSourceData(ByVal strNumber As String, ByVal dtDate As Date, ByVal dgv As DataGridView)
        Me.strSourceNumber = strNumber
        Me.dtSourceDate = dtDate
        Me.strSourceItemID = dgv.Item("item_id", dgv.CurrentCell.RowIndex).Value
        Me.strSourceClass = dgv.Item("class", dgv.CurrentCell.RowIndex).Value
        Me.intSourceQuantity = dgv.Item("qty", dgv.CurrentCell.RowIndex).Value
        Me.intSourceOrderID = dgv.Item("pop_id", dgv.CurrentCell.RowIndex).Value
    End Sub

    Private Sub ReplaceItem()
        Try
            'update AFsIn
            Me.sqlStr = "UPDATE AFsIn SET item_id = '" & Me.txtItemID.Text & "', " & _
            "date_updated = getdate() WHERE branch_id = " & GenBranchID & " AND af_id = " & GenAFID & " AND af_num = '" & Me.strSourceNumber & "' " & _
            "AND item_id = '" & Me.strSourceItemID & "' AND order_id = " & Me.intSourceOrderID & ""

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr

            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            'update table item type
            Me.sqlStr = "UPDATE " & strItemType & " SET item_id = '" & Me.txtItemID.Text & "' " & _
            "WHERE branch_id = " & GenBranchID & " AND af_id = " & GenAFID & " AND af_num = '" & Me.strSourceNumber & "' " & _
            "AND item_id = '" & Me.strSourceItemID & "' AND order_id = " & Me.intSourceOrderID & ""

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr

            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub StockPassToReplacement()
        Try
            Dim tblStock As String

            If Me.strSourceClass <> "" Then
                tblStock = "StockFragile"
            Else
                tblStock = "StockDurable"
            End If

            'Gikuha-an ang current item ug stock according to quanity
            Me.sqlStr = "UPDATE " & tblStock & " SET [" & GenBranchID & "] = [" & GenBranchID & "] - " & Me.intSourceQuantity & " " & _
            "WHERE item_id = '" & Me.strSourceItemID & "' "

            If tblStock = "StockFragile" Then Me.sqlStr = Me.sqlStr + "AND class = '" & Me.strSourceClass & "'"

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr

            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

            'Gidugangan ang current item ug stock according to quanity
            Me.sqlStr = "UPDATE " & tblStock & " SET [" & GenBranchID & "] = [" & GenBranchID & "] + " & Me.intSourceQuantity & " " & _
            "WHERE item_id = '" & Me.txtItemID.Text & "' "

            If tblStock = "StockFragile" Then Me.sqlStr = Me.sqlStr + "AND class = '" & Me.strSourceClass & "'"

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr

            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub txtItemID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtItemID.KeyPress
        Dim ValidInputChar = AlphaNumeric + vbBack
        If e.KeyChar = Chr(13) Then btnVerify.PerformClick()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtItemID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtItemID.TextChanged
        If Me.txtItemID.Text = "" Then
            Me.btnVerify.Enabled = False
        Else
            Me.btnVerify.Enabled = True
        End If
    End Sub
End Class
Imports System.Data
Imports System.Data.SqlClient

Public Class frmAdjustmentCost
    Private dtSourceDate As Date
    Private intSourceOrderID As Integer

    Private sqlCmd As SqlCommand
    Private sqlReader As SqlDataReader
    Private sqlStr As String
    Private strSourceNumber, strSourceItemID, strSourceClass, strItemType As String

    Private Function InvalidInputs() As Boolean
        For Each ctl As Control In Me.Controls
            If TypeOf ctl Is TextBox Then
                If ctl.Text = String.Empty Then
                    MessageBox.Show("Cannot proceed when there is an empty text box.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Stop)
                    Return True
                    Exit Function
                End If
            End If
        Next

        If MessageBox.Show("Are all data correct? click Yes to continue, No to cancel", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
            Return True
            Exit Function
        End If
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUpdateEntry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateEntry.Click
        If Me.InvalidInputs = True Then Exit Sub
        Me.GetItemType()
        Me.UpdateEntryOnAFs()
        Me.UpdateOnTypeTables()
        MessageBox.Show("The cost has been successfully applied on the current entry!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnUpdateItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateItem.Click
        If Me.InvalidInputs = True Then Exit Sub
        Me.UpdateCostOnItem()
        MessageBox.Show("The cost has been applied on the current item!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnUpdatePurchases_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePurchases.Click
        If Me.InvalidInputs = True Then Exit Sub
        Me.UpdateOnPurchases()
        MessageBox.Show("Purchases has been updated!", "YahoOo!", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub frmAdjustmentCost_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub GetItemType()
        Try
            Me.sqlStr = "SELECT im.item_id, im.type_lib_id, lib.type_tables FROM Library lib, ItemMasterlist im " & _
            "WHERE im.item_id = '" & Me.strSourceItemID & "' AND lib.lib_id = im.type_lib_id"
            Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
            sqlDBconn.Open()
            Me.sqlReader = sqlCmd.ExecuteReader()
            Me.sqlReader.Read()
            Me.strItemType = Me.sqlReader("type_tables")
            Me.sqlReader.Close()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Public Sub MainSourceData(ByVal strNumber As String, ByVal dtDate As Date, ByVal dgv As DataGridView)
        Me.strSourceNumber = strNumber
        Me.dtSourceDate = dtDate
        Me.strSourceItemID = dgv.Item("item_id", dgv.CurrentCell.RowIndex).Value
        Me.strSourceClass = dgv.Item("class", dgv.CurrentCell.RowIndex).Value
        Me.intSourceOrderID = dgv.Item("pop_id", dgv.CurrentCell.RowIndex).Value
    End Sub

    Private Sub txtArrastre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtArrastre.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtCost.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtArrastre_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArrastre.Leave
        OnLeaveFormat(Me.txtArrastre)
    End Sub

    Private Sub txtArrastre_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtArrastre.TextChanged
        InputFormat(Me.txtArrastre)
    End Sub

    Private Sub txtCost_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCost.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtCost_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCost.Leave
        OnLeaveFormat(Me.txtCost)
    End Sub

    Private Sub txtCost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCost.TextChanged
        InputFormat(Me.txtCost)
    End Sub

    Private Sub txtDiscount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDiscount.KeyPress
        Dim ValidInputChar = NumericOnly + vbBack

        If e.KeyChar = Chr(13) Then Me.txtArrastre.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtDiscount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDiscount.Leave
        OnLeaveFormat(Me.txtDiscount)
    End Sub

    Private Sub txtFreight_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtFreight.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtDiscount.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtFreight_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFreight.Leave
        OnLeaveFormat(Me.txtFreight)
    End Sub

    Private Sub txtFreight_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFreight.TextChanged
        InputFormat(Me.txtFreight)
    End Sub

    Private Sub txtTotalAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTotalAmount.KeyPress
        Dim ValidInputChar = NumericOnly + "," + vbBack

        If e.KeyChar = Chr(13) Then Me.txtFreight.Focus()
        If Not ValidInputChar.Contains(e.KeyChar) Then e.KeyChar = Nothing
    End Sub

    Private Sub txtTotalAmount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalAmount.Leave
        OnLeaveFormat(Me.txtTotalAmount)
    End Sub

    Private Sub txtTotalAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalAmount.TextChanged
        InputFormat(Me.txtTotalAmount)
    End Sub

    Private Sub UpdateCostOnItem()
        Try
            Me.sqlStr = "UPDATE ItemMasterlist SET cost = " & Me.txtCost.Text.Replace(",", "") & ", " & _
            "date_updated = getdate() WHERE item_id = '" & Me.strSourceItemID & "'"
            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr
            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateEntryOnAFs()
        Try
            Dim ArrayAFs(0 To 2) As String
            Dim objAFs As Object

            ArrayAFs(0) = "AFsIn"
            ArrayAFs(1) = "AFsTransfer"
            ArrayAFs(2) = "AFsOut"

            For Each objAFs In ArrayAFs
                Me.sqlStr = "SELECT * FROM " & objAFs.ToString & " WHERE item_id = '" & Me.strSourceItemID & "' " & _
                "AND class = '" & Me.strSourceClass & "' AND COALESCE(ref_af_id, 0) <> 3 AND af_id <> 3 " & _
                "AND run_id = '" & GenRunID & "' AND root_id = '" & GenRootID & "'"

                Me.sqlCmd = New SqlCommand(Me.sqlStr, sqlDBconn)
                sqlDBconn.Open()
                Me.sqlReader = sqlCmd.ExecuteReader()

                If Me.sqlReader.Read Then
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                    Try
                        Dim SQLstrUpdate = "UPDATE  " & objAFs.ToString & " " & _
                        "SET cost = " & Me.txtCost.Text.Replace(",", "") & ", ua_id = " & MDI.stbUserID.Text & ", " & _
                        "date_updated = getdate() WHERE item_id = '" & Me.strSourceItemID & "' AND class = '" & Me.strSourceClass & "' " & _
                        "AND COALESCE(ref_af_id, 0) <> 3 AND af_id <> 3 AND " & _
                        "run_id = '" & GenRunID & "' AND root_id = '" & GenRootID & "'"

                        Me.sqlCmd = New SqlCommand
                        Me.sqlCmd.Connection = sqlDBconn
                        Me.sqlCmd.CommandText = SQLstrUpdate
                        sqlDBconn.Open()
                        Me.sqlCmd.ExecuteNonQuery()
                        sqlDBconn.Close()

                    Catch ex As Exception
                        sqlDBconn.Close()
                        MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
                    End Try
                Else
                    Me.sqlReader.Close()
                    sqlDBconn.Close()
                End If
            Next

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateOnPurchases()
        Try
            Me.sqlStr = "UPDATE PurchasesAmounts " & _
            "SET total = " & Me.txtTotalAmount.Text.Replace(",", "") & ", " & _
            "freight = " & Me.txtFreight.Text.Replace(",", "") & ", " & _
            "discount = " & Me.txtDiscount.Text.Replace(",", "") & ", " & _
            "arrastre = " & Me.txtArrastre.Text.Replace(",", "") & " " & _
            "WHERE af_id = " & GenAFID & " AND af_num = '" & Me.strSourceNumber & "' " & _
            "AND run_id = '" & GenRunID & "' AND date_issued = '" & Me.dtSourceDate & "'"

            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr
            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub

    Private Sub UpdateOnTypeTables()
        Try
            Me.sqlStr = "UPDATE  " & Me.strItemType & " SET cost = " & Me.txtCost.Text.Replace(",", "") & " " & _
            "WHERE item_id = '" & Me.strSourceItemID & "' " & _
            "AND run_id = '" & GenRunID & "' AND root_id = '" & GenRootID & "' " & _
            "AND af_id <> 3 AND COALESCE(ref_af_id, 0) <> 3"
            Me.sqlCmd = New SqlCommand
            Me.sqlCmd.Connection = sqlDBconn
            Me.sqlCmd.CommandText = Me.sqlStr
            sqlDBconn.Open()
            Me.sqlCmd.ExecuteNonQuery()
            sqlDBconn.Close()

        Catch ex As Exception
            sqlDBconn.Close()
            MessageBox.Show(ex.Message + ", " & New StackTrace().GetFrame(0).GetMethod.ToString())
        End Try
    End Sub
End Class